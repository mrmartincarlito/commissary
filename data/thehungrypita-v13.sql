-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 30, 2021 at 10:37 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thehungrypita`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_levels`
--

CREATE TABLE `access_levels` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `access_dashboard` int(11) NOT NULL DEFAULT 0,
  `access_access_levels` int(11) NOT NULL DEFAULT 0,
  `access_user_accounts` int(11) NOT NULL DEFAULT 0,
  `access_franchise_accounts` int(11) NOT NULL DEFAULT 0,
  `access_commissary_accounts` int(11) NOT NULL DEFAULT 0,
  `access_employee_entries` int(11) NOT NULL DEFAULT 0,
  `access_encode_attendance` int(11) NOT NULL DEFAULT 0,
  `access_payroll` int(11) NOT NULL DEFAULT 0,
  `access_product_information` int(11) NOT NULL DEFAULT 0,
  `access_purchase_order` int(11) NOT NULL DEFAULT 0,
  `access_franchise_branch` int(11) NOT NULL DEFAULT 0,
  `access_franchise_entries` int(11) NOT NULL DEFAULT 0,
  `access_billing_franchising` int(11) NOT NULL DEFAULT 0,
  `access_order_payments` int(11) NOT NULL DEFAULT 0,
  `access_expenses` int(11) NOT NULL DEFAULT 0,
  `access_reports` int(11) NOT NULL DEFAULT 0,
  `access_logs` int(11) NOT NULL DEFAULT 0,
  `access_approval_power` int(11) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_levels`
--

INSERT INTO `access_levels` (`id`, `description`, `access_dashboard`, `access_access_levels`, `access_user_accounts`, `access_franchise_accounts`, `access_commissary_accounts`, `access_employee_entries`, `access_encode_attendance`, `access_payroll`, `access_product_information`, `access_purchase_order`, `access_franchise_branch`, `access_franchise_entries`, `access_billing_franchising`, `access_order_payments`, `access_expenses`, `access_reports`, `access_logs`, `access_approval_power`, `is_deleted`) VALUES
(3, 'Administrator', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0),
(4, 'User', 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(45, 'Customer', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `image_file` text NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT 0,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `role`, `image_file`, `is_deleted`, `date`) VALUES
(1, 'juhshd', 'dfd', 'dssd', 'utet@gmail.com', 'admin', '$2y$10$7e9rSp7gC.GslurbeSlIL.6NnSdN6/ejq0u/ueIn.00Kc7bMnxWBa', '3', 'uploads/532d0341186be12412c6a4362cc08710.jpg', 0, '2021-10-20 23:35:00'),
(5, 'Nashh', 'Risa David', 'Dominguez', 'licyvanox@mailinator.com', 'xicof', '$2y$10$.Nx2JvDvduut6Zk0fl/EOefUhm9K.CBxQ7eLNqYleOQ0O0LndbSVa', '3', '', 1, '2021-10-09 07:52:25'),
(7, 'Hayden', 'Leah York', 'Neal', 'hydoqu@mailinator.com', 'tutigely', '$2y$10$pGvAKzUbcHJE6vDUap.4Fe5V0PIre/thkcFvPu0xRQm5t4Hov3uzK', '3', '', 0, '2021-10-10 23:39:16'),
(8, 'Victoria', 'Stone Hunt', 'Moon', 'pehahemir@mailinator.com', 'niqezex', '$2y$10$r5xuDpP89LTsGV3yaf17Ie7mYQuaRun7mFYKreFsTaGC58xzveKXe', '3', '', 0, '2021-10-10 23:39:20'),
(9, 'Rosalyn', 'Fredericka Mathews', 'Fulton', 'hitetas@mailinator.com', 'kijog', '$2y$10$GcLRA3RdOrGNjx6.I5CycOvD9/YojXLDVbNUkX5n7lDTBQdsGKCGC', '3', '', 0, '2021-10-11 01:36:48'),
(10, 'Cherokee', 'Trevor Maxwell', 'Black', 'goxylyn@mailinator.com', 'cytesohere', '$2y$10$NhO5UnQRSVmWFrTHRlkVT.tZj7GoQjYt772QFZ1UGfY4ApHMcS52C', '3', '', 0, '2021-10-11 01:37:37'),
(11, 'Sade', 'Timon Barlow', 'Workman', 'nywylogy@mailinator.com', 'user', '$2y$10$W6vlgjt/EZIfMUbmEIyiOusvCj6bTXuO.bypMt6KoUlOo2q.8vvmO', '4', '', 0, '2021-10-19 08:38:00');

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `total_minutes` decimal(10,2) NOT NULL,
  `regular_legal_ot` decimal(10,2) NOT NULL,
  `special_ot` decimal(10,2) NOT NULL,
  `special_holiday` decimal(10,2) NOT NULL,
  `legal_holiday` decimal(10,2) NOT NULL,
  `total_hours` decimal(11,2) NOT NULL,
  `legend` varchar(50) NOT NULL,
  `work_date` date NOT NULL,
  `added_by` text NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `emp_id`, `total_minutes`, `regular_legal_ot`, `special_ot`, `special_holiday`, `legal_holiday`, `total_hours`, `legend`, `work_date`, `added_by`, `is_deleted`, `date_time`) VALUES
(1, 5, '24.00', '32.00', '2.00', '0.00', '0.00', '1.00', 'PRESENT', '2021-08-07', 'admin', 0, '0000-00-00 00:00:00'),
(2, 1, '86.00', '82.00', '56.00', '0.00', '0.00', '3.00', 'PRESENT', '1972-01-06', 'admin', 0, '0000-00-00 00:00:00'),
(3, 1, '480.00', '227.00', '0.00', '0.00', '0.00', '11.00', 'PRESENT', '2021-09-16', 'admin', 1, '0000-00-00 00:00:00'),
(4, 1, '480.00', '227.00', '0.00', '0.00', '0.00', '11.00', 'PRESENT', '2021-10-13', 'admin', 0, '0000-00-00 00:00:00'),
(5, 1, '480.00', '63.00', '0.00', '0.00', '0.00', '9.30', 'PRESENT', '2021-10-12', 'admin', 0, '0000-00-00 00:00:00'),
(6, 5, '480.00', '227.00', '0.00', '0.00', '0.00', '11.47', 'PRESENT', '2021-10-14', 'admin', 0, '0000-00-00 00:00:00'),
(7, 1, '480.00', '0.00', '0.00', '0.00', '0.00', '8.00', 'PRESENT', '2021-10-12', 'admin', 0, '0000-00-00 00:00:00'),
(8, 4, '62.00', '24.00', '79.00', '0.00', '0.00', '2.55', 'DAY-OFF', '1986-07-31', 'admin', 0, '0000-00-00 00:00:00'),
(9, 1, '26.00', '92.00', '86.00', '0.00', '0.00', '3.42', 'DAY-OFF', '1983-08-24', 'admin', 0, '0000-00-00 00:00:00'),
(10, 5, '98.00', '13.00', '12.00', '0.00', '0.00', '2.27', 'DAY-OFF', '2013-05-01', 'admin', 0, '0000-00-00 00:00:00'),
(11, 5, '54.00', '4.00', '20.00', '0.00', '0.00', '1.37', 'DAY-OFF', '2003-07-21', 'admin', 0, '0000-00-00 00:00:00'),
(12, 4, '11.00', '87.00', '72.00', '0.00', '0.00', '3.90', 'DAY-OFF', '1993-04-29', 'admin', 0, '0000-00-00 00:00:00'),
(13, 1, '24.00', '68.00', '31.00', '0.00', '0.00', '2.19', 'PRESENT', '1994-08-02', 'admin', 0, '0000-00-00 00:00:00'),
(14, 1, '73.00', '99.00', '62.00', '0.00', '0.00', '4.14', 'PRESENT', '1996-04-17', 'admin', 0, '0000-00-00 00:00:00'),
(15, 6, '31.00', '35.00', '74.00', '0.00', '0.00', '2.23', 'DAY-OFF', '1972-10-20', 'admin', 0, '2021-10-12 01:12:33'),
(16, 4, '480.00', '60.00', '0.00', '0.00', '0.00', '9.00', 'DAY-OFF', '1980-09-21', 'admin', 0, '2021-10-12 01:13:34'),
(17, 5, '480.00', '133.00', '0.00', '0.00', '0.00', '10.13', 'PRESENT', '2012-11-01', 'admin', 0, '2021-10-12 01:13:55'),
(18, 6, '69.00', '39.00', '67.00', '0.00', '0.00', '3.10', 'DAY-OFF', '1978-12-21', 'admin', 0, '2021-10-12 03:36:20'),
(19, 4, '9.00', '55.00', '100.00', '0.00', '0.00', '2.48', 'PRESENT', '2020-09-14', 'admin', 0, '2021-10-12 03:37:01'),
(20, 4, '9.00', '55.00', '100.00', '0.00', '0.00', '2.48', 'PRESENT', '2020-09-15', 'admin', 0, '2021-10-12 03:37:15'),
(21, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'PRESENT', '2021-10-14', 'admin', 0, '2021-10-12 03:39:21'),
(22, 4, '480.00', '0.00', '0.00', '0.00', '0.00', '8.00', 'PRESENT', '2021-10-15', 'admin', 0, '2021-10-15 01:04:18'),
(23, 4, '480.00', '230.00', '0.00', '0.00', '0.00', '11.50', 'PRESENT', '2021-10-16', 'admin', 0, '2021-10-12 03:39:50'),
(24, 4, '0.00', '0.00', '100.00', '0.00', '0.00', '1.40', 'PRESENT', '2021-10-17', 'admin', 0, '2021-10-12 03:40:02'),
(25, 7, '30.00', '34.00', '11.00', '0.00', '0.00', '1.16', 'DAY-OFF', '1972-09-21', 'admin', 0, '2021-10-12 04:18:23'),
(26, 7, '9.00', '38.00', '24.00', '0.00', '0.00', '1.28', 'PRESENT', '2012-08-26', 'admin', 0, '2021-10-12 04:26:26'),
(27, 7, '480.00', '0.00', '0.00', '0.00', '0.00', '8.00', 'PRESENT', '2012-08-27', 'admin', 0, '2021-10-13 23:47:01'),
(28, 10, '480.00', '0.00', '0.00', '0.00', '0.00', '8.00', 'PRESENT', '2021-10-14', 'admin', 0, '2021-10-13 23:47:36'),
(29, 5, '92.00', '98.00', '62.00', '0.00', '0.00', '4.35', 'PRESENT', '2014-04-07', 'admin', 0, '2021-10-13 23:53:15'),
(30, 7, '51.00', '100.00', '56.00', '0.00', '0.00', '3.55', 'DAY-OFF', '1993-07-06', 'admin', 0, '2021-10-13 23:53:19'),
(31, 5, '48.00', '42.00', '38.00', '0.00', '0.00', '2.31', 'DAY-OFF', '2018-08-25', 'admin', 0, '2021-10-13 23:53:22'),
(32, 12, '92.00', '81.00', '7.00', '0.00', '0.00', '3.10', 'PRESENT', '1973-07-25', 'admin', 0, '2021-10-13 23:53:25'),
(33, 10, '86.00', '77.00', '46.00', '0.00', '0.00', '3.31', 'DAY-OFF', '1980-02-11', 'admin', 0, '2021-10-13 23:53:30'),
(34, 9, '100.00', '100.00', '62.00', '0.00', '0.00', '4.45', 'DAY-OFF', '2007-01-28', 'admin', 0, '2021-10-13 23:53:33'),
(35, 7, '39.00', '6.00', '21.00', '0.00', '0.00', '1.27', 'DAY-OFF', '1985-10-27', 'admin', 0, '2021-10-13 23:53:36'),
(36, 13, '3.00', '66.00', '51.00', '480.00', '480.00', '10.00', 'DAY-OFF', '1994-02-25', 'admin', 0, '2021-10-15 07:06:26'),
(37, 1, '6240.00', '0.00', '0.00', '0.00', '0.00', '104.00', 'PRESENT', '2021-10-22', 'admin', 0, '2021-10-15 07:22:43');

-- --------------------------------------------------------

--
-- Table structure for table `billing_payments`
--

CREATE TABLE `billing_payments` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `image_file` text NOT NULL,
  `check_no` int(11) NOT NULL,
  `check_details` varchar(50) NOT NULL,
  `bank` varchar(50) NOT NULL,
  `date_paid` date NOT NULL,
  `remarks` text NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'PENDING',
  `approved_by` varchar(100) NOT NULL,
  `approved_date` datetime DEFAULT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `billing_payments`
--

INSERT INTO `billing_payments` (`id`, `franchise_id`, `description`, `total_amount`, `image_file`, `check_no`, `check_details`, `bank`, `date_paid`, `remarks`, `status`, `approved_by`, `approved_date`, `is_deleted`, `date_time`) VALUES
(1, 7, 'FRANCHISING FEE', '2720.00', '', 0, '', '', '2021-10-13', '', 'APPROVED', 'admin', '2021-10-30 16:33:55', 0, ''),
(2, 7, 'SECURITY DEPOSITS', '37.00', '', 0, '', '', '2020-07-19', 'Fugiat quisquam expe', 'APPROVED', 'admin', '2021-10-24 11:53:45', 0, ''),
(3, 7, 'FRANCHISING FEE', '2720.00', '', 0, '', '', '2021-10-14', '', 'APPROVED', 'admin', '2021-10-21 16:25:04', 0, ''),
(4, 7, 'OTHER FRANCHISING INCLUSION', '2720.00', '', 0, '', '', '2021-10-14', '', 'APPROVED', 'admin', '2021-10-14 15:58:41', 0, ''),
(5, 7, 'SECURITY DEPOSITS', '41.00', '', 0, '', '', '1974-02-28', 'Est velit occaecat ', 'APPROVED', 'admin', '2021-10-13 15:48:12', 0, ''),
(6, 7, 'FRANCHISING FEE', '2720.00', 'uploads/wp1808930.jpg', 0, '', '', '2021-10-13', 'inconsistent', 'REJECTED', '', NULL, 0, ''),
(7, 7, 'FRANCHISING FEE', '2720.00', '', 0, '', '', '2021-10-13', '', 'APPROVED', 'admin', '2021-10-13 15:47:42', 0, ''),
(8, 7, 'SECURITY DEPOSITS', '2720.00', 'uploads/532d0341186be12412c6a4362cc08710.jpg', 0, '', '', '2021-10-14', '', 'APPROVED', 'admin', '2021-10-13 00:00:00', 0, ''),
(9, 7, 'OTHER FRANCHISING INCLUSION', '7.00', '', 0, '', '', '2018-04-25', 'Nulla nihil mollitia', 'APPROVED', 'admin', '2021-10-14 07:51:51', 0, ''),
(10, 8, 'SYSTEM WIDE AD FUND', '1960.00', '', 0, '', '', '2021-10-15', '', 'APPROVED', 'admin', '2021-10-14 15:15:59', 0, ''),
(11, 7, 'SECURITY DEPOSITS', '5000.00', '', 11, 'Est exercitation eiu', 'Ex perspiciatis con', '2021-04-08', 'Labore architecto ve', 'APPROVED', 'admin', '2021-10-30 16:33:31', 0, ''),
(12, 7, 'SYSTEM WIDE AD FUND', '5000.00', '', 26, 'Rerum anim earum aut', 'Vel suscipit minim e', '1976-05-25', 'Vel recusandae Dolo', 'APPROVED', 'admin', '2021-10-30 16:32:31', 0, ''),
(13, 11, 'SECURITY DEPOSITS', '900.00', '', 33, 'Voluptatibus sunt op', 'Unde vitae quo recus', '1996-04-06', 'Duis et fugiat ut u', 'APPROVED', 'admin', '2021-10-30 16:35:42', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `description`, `is_deleted`, `date_time`) VALUES
(1, 'Cart 20', 0, '2021-10-23 21:31:07'),
(2, 'Cart 1', 0, '2021-10-23 21:53:48'),
(3, 'Cart 11', 0, '2021-10-23 22:22:17');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `type`, `category_name`, `is_deleted`, `date_time`) VALUES
(4, 'Non-Operational', 'Taxes', 0, '2021-10-24 00:02:08'),
(5, 'Non-Operational', 'Interest', 0, '2021-10-24 00:02:16'),
(6, 'Operational', 'Cost of Sales', 0, '2021-10-24 00:02:43'),
(7, 'Operational', 'Marketing, Advertising and Promotion', 0, '2021-10-24 00:03:14'),
(8, 'Operational', 'Travel Expenses', 0, '2021-10-24 00:03:25'),
(9, 'Operational', 'Administrative Expenses', 0, '2021-10-24 00:03:40'),
(10, 'Operational', 'Rent and Insurance', 0, '2021-10-24 00:04:00'),
(11, 'Operational', 'Depreciation and Amortization', 0, '2021-10-24 00:04:18'),
(12, 'Operational', 'Commission', 0, '2021-10-24 00:04:33'),
(13, 'Operational', 'In house expenses', 0, '2021-10-24 00:04:48');

-- --------------------------------------------------------

--
-- Table structure for table `commissary_accounts`
--

CREATE TABLE `commissary_accounts` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT 0,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `commissary_accounts`
--

INSERT INTO `commissary_accounts` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `is_deleted`, `date`) VALUES
(1, 'DSFDF', 'Thaddeus Oneal', 'Vasquez', 'admin@commissary.com', 'admin', '$2y$10$bOcGC6r9kK6ZNBKjgBhv7uI7k2W6TUfaBBBFIsG8xRrv7qVkmFoKW', 1, '2021-10-10 08:18:48'),
(2, 'Margaret', 'Sonia Chavez', 'Allison', 'kynyro@mailinator.com', 'bikidom', '$2y$10$pSG4dOBfJmkaOpu0DvdeGeH/ba4LvoQaqPU856MnqiCMEJOfCpoxS', 0, '2021-10-10 06:26:39'),
(3, 'Eric', 'Eve Cortez', 'Lara', 'gywewiti@mailinator.com', 'gogyqyrul', '$2y$10$49Le1R2WBJ5QWKVeED1HlumtZJKoQqLjR1AR1Fv0g8I60XAgO/jGy', 0, '2021-10-10 06:26:52');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `city` varchar(100) NOT NULL,
  `province` varchar(100) NOT NULL,
  `employment_date` date NOT NULL,
  `regularization_date` date NOT NULL,
  `employment_type` varchar(100) NOT NULL,
  `rate` decimal(11,2) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `middle_name`, `last_name`, `city`, `province`, `employment_date`, `regularization_date`, `employment_type`, `rate`, `date`, `is_deleted`) VALUES
(1, 'Julcess', 'P', 'Mercado', 'Calumpitt', 'Bulacan', '2021-10-01', '2021-10-09', 'ADMIN - ACCOUNTING', '9000.00', '2021-10-12 05:50:34', 0),
(2, 'Jack', 'Oleg Gordon', 'Burgess', 'Eligendi dolores sun', 'Eu incididunt sequi ', '2020-06-22', '2011-05-02', '', '3.00', '2021-10-09 06:15:50', 1),
(3, 'Signe', 'Flynn Hendricks', 'Hardy', 'Officiis soluta duci', 'Earum aut vel delect', '2008-07-29', '2002-06-19', '', '18.00', '2021-10-09 07:57:39', 1),
(4, 'Madeson', 'Brett Rodriquez', 'Mccarty', 'Aut distinctio Quae', 'Officia esse sapient', '1971-11-27', '1984-10-29', 'SERVICE CREW', '57.00', '2021-10-12 05:50:29', 0),
(5, 'Lani', 'Lani Morrison', 'Cruz', 'Ut dolores ex delect', 'Voluptatem ipsam et', '1985-02-12', '1993-06-28', 'COMMISSARY OFFICER', '46.00', '2021-10-12 05:50:18', 0),
(6, 'Cooper', 'Genevieve Ruiz', 'Nguyen', 'Tempor repudiandae q', 'Sunt quisquam Nam su', '2013-04-06', '1971-02-14', 'SERVICE CREW', '48.00', '2021-10-12 05:50:26', 0),
(7, 'Caleb', 'Tatyana Haley', 'Harris', 'Eaque ad quo in mini', 'Cum expedita neque i', '1986-10-20', '1993-05-24', 'OFFICE STAFF', '100.00', '2021-10-12 05:50:22', 0),
(8, 'Kirestin', 'Stewart Charles', 'Flores', 'Sunt esse commodi al', 'Laboris est labore c', '1996-03-09', '1985-03-14', 'DIGITAL MARKETING', '23.00', '2021-10-12 05:50:10', 0),
(9, 'Madonna', 'Abra Mcclure', 'Silva', 'Nesciunt excepteur ', 'Veritatis ullam labo', '2003-12-17', '2007-10-12', 'ADMIN STAFF / TRAINOR', '39.00', '2021-10-12 05:50:14', 0),
(10, 'Yuri', 'Benedict Young', 'Buck', 'Autem alias consequa', 'Blanditiis qui sint ', '1975-12-12', '2006-11-14', 'ADMIN STAFF / TRAINOR', '70.00', '2021-10-12 05:50:01', 0),
(11, 'Lester', 'Nelle Reese', 'Montoya', 'Sit mollit ab in vol', 'Modi aut aut amet i', '1979-01-23', '2020-12-10', 'DIGITAL MARKETING', '79.00', '2021-10-12 05:50:05', 0),
(12, 'Rosalyn', 'Boris Sweeney', 'Green', 'Dolorem assumenda mo', 'Neque laborum elit ', '1979-05-08', '1996-01-14', 'COMMISSARY OFFICER', '2.00', '2021-10-12 05:49:57', 0),
(13, 'Anastasia', 'Delilah Booker', 'Valencia', 'Quia similique labor', 'Ut perferendis repre', '2006-01-24', '1971-08-16', 'OFFICE STAFF', '3.00', '2021-10-12 05:49:54', 0);

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(11) NOT NULL,
  `date_paid` date NOT NULL,
  `expense_type` varchar(50) NOT NULL,
  `voucher_number` int(11) NOT NULL,
  `category` varchar(50) NOT NULL,
  `sub_category` varchar(50) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `added_by` varchar(50) NOT NULL,
  `remarks` text NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `date_paid`, `expense_type`, `voucher_number`, `category`, `sub_category`, `total_amount`, `added_by`, `remarks`, `is_deleted`, `date_time`) VALUES
(1, '1999-12-01', 'Operational', 385, 'Commission', '', '20.00', 'admin', 'Omnis veniam quos f', 0, '2021-10-18 03:15:52'),
(2, '1989-08-18', 'Operational', 60, 'Administrative Expenses', 'Repair and Maintenance Works', '95.00', 'admin', 'Dolorem ullamco nihi', 0, '2021-10-18 03:33:36'),
(3, '1973-02-27', 'Non-Operational', 396, 'Interest', '', '89.00', 'admin', 'Autem et rerum et as', 0, '2021-10-18 03:34:33');

-- --------------------------------------------------------

--
-- Table structure for table `franchisee`
--

CREATE TABLE `franchisee` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `mode_of_transaction` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `contactno` varchar(50) NOT NULL,
  `birthday` date NOT NULL,
  `tin_number` text NOT NULL,
  `package_type` varchar(50) NOT NULL,
  `date_contract_signing` date NOT NULL,
  `date_branch_opening` date NOT NULL,
  `date_contract_expiry` date NOT NULL,
  `billing_balance` decimal(10,2) NOT NULL,
  `order_balance` decimal(10,2) NOT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `last_payment_amount` decimal(10,2) DEFAULT NULL,
  `last_payment_date` datetime DEFAULT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchisee`
--

INSERT INTO `franchisee` (`id`, `branch_id`, `mode_of_transaction`, `name`, `address`, `contactno`, `birthday`, `tin_number`, `package_type`, `date_contract_signing`, `date_branch_opening`, `date_contract_expiry`, `billing_balance`, `order_balance`, `total_amount`, `last_payment_amount`, `last_payment_date`, `is_deleted`, `date_time`) VALUES
(7, 1, 'Pick-up', 'Anthony Farmer', 'Quo dolorem sunt dol', '79', '1993-10-10', '', '3', '1974-08-04', '2020-03-08', '1975-10-22', '42280.00', '45000.00', '87280.00', '2720.00', '2021-10-13 00:00:00', 0, '2021-10-30 08:33:55'),
(8, 2, 'Delivery', 'Rigel Arnold', 'Adipisicing aut veni', '62', '2005-10-03', '', '1', '2003-08-27', '1983-01-14', '1977-12-04', '59000.00', '20000.00', '79000.00', '0.00', NULL, 0, '2021-10-30 08:35:03'),
(9, 3, 'Pick-up', 'Gemma Dean', 'Harum porro quod lab', '68', '1996-06-07', '', '2', '2000-07-23', '2017-02-18', '1980-06-02', '450000.00', '50000.00', '500000.00', '0.00', NULL, 0, '2021-10-30 08:34:48'),
(10, 2, 'Pick-up', 'Eric Pena', 'Omnis laudantium re', '15', '1988-11-23', '', '1', '1989-03-21', '2013-10-23', '2021-12-31', '560000.00', '0.00', '0.00', '0.00', NULL, 0, '2021-10-30 07:45:08'),
(11, 2, 'Delivery', 'Christine Zimmerman', 'Officia labore nobis', '66', '1978-05-02', '5345345', '1', '2015-02-23', '1980-08-23', '2021-11-06', '249100.00', '15000.00', '264100.00', '900.00', '1996-04-06 00:00:00', 0, '2021-10-30 08:35:42'),
(12, 6, 'Pick-up', 'Serena Haynes', 'Dignissimos laboris ', '100', '2015-08-26', '686', '1', '2008-09-22', '2001-06-05', '1997-11-27', '25000.00', '90000.00', '115000.00', '4000.00', '1982-02-23 00:00:00', 0, '2021-10-30 08:31:01');

-- --------------------------------------------------------

--
-- Table structure for table `franchise_accounts`
--

CREATE TABLE `franchise_accounts` (
  `id` int(11) NOT NULL,
  `franchisee_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT 0,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchise_accounts`
--

INSERT INTO `franchise_accounts` (`id`, `franchisee_id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `is_deleted`, `date`) VALUES
(1, 10, 'Christian', 'Deborah Vaughn', 'Reynolds', 'xytylida@mailinator.com', 'qugebudega', '$2y$10$MxYwqZU6Liowbw4eimq6aumosRbzZ024rFZrZlFCx2Mp9ojM3opDy', 0, '2021-10-18 03:34:04'),
(2, 11, 'Bruce', 'Rae Vincent', 'Hardin', 'nepahipas@mailinator.com', 'jozosemote', '$2y$10$FjCKQwaU0knnezzkHe6Dl.2QS18i7U3MRSBraQ/2LT3GcOh9q9umW', 0, '2021-10-18 03:34:13');

-- --------------------------------------------------------

--
-- Table structure for table `franchise_branch`
--

CREATE TABLE `franchise_branch` (
  `id` int(11) NOT NULL,
  `branch_code` varchar(100) NOT NULL,
  `branch_name` varchar(100) NOT NULL,
  `branch_location` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchise_branch`
--

INSERT INTO `franchise_branch` (`id`, `branch_code`, `branch_name`, `branch_location`, `is_deleted`, `date_time`) VALUES
(1, 'B-000', 'Darrel Brock', 'Blanditiis qui magna', 0, '2021-10-12 07:38:05'),
(2, 'B-001', 'Elvis Clay', 'Quis vitae est est a', 0, '2021-10-12 07:38:12'),
(3, 'B-002', 'Alika Holt', 'Architecto pariatur', 0, '2021-10-13 01:45:18'),
(4, 'B-0003', 'test', 'test', 0, '2021-10-20 06:54:29'),
(5, 'B-0004', 'Jasmine Doyle', 'Nulla molestiae enim', 0, '2021-10-20 06:54:37'),
(6, 'B-0005', 'Molly Cook', 'Eligendi qui dolor m', 0, '2021-10-20 06:54:43'),
(7, 'B-0006', 'Magee Estes', 'Velit dolore ipsa e', 0, '2021-10-20 06:54:47'),
(8, 'B-0007', 'Judith Hart', 'Aliqua In nulla sin', 0, '2021-10-20 06:54:51'),
(9, 'B-0008', 'Candice Patel', 'Atque in maiores pra', 0, '2021-10-20 06:55:03'),
(10, 'B-0009', 'Zena Castillo', 'Qui in quod dolorum ', 0, '2021-10-20 06:55:08'),
(11, 'B-0010', 'Tate Cardenas', 'Esse ut dolor aut s', 0, '2021-10-20 06:55:17');

-- --------------------------------------------------------

--
-- Table structure for table `franchise_renewal_log`
--

CREATE TABLE `franchise_renewal_log` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) NOT NULL,
  `expiry_date` date NOT NULL,
  `renewed_date` date NOT NULL,
  `new_expiry_date` date NOT NULL,
  `approved_by` text DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `franchise_renewal_log`
--

INSERT INTO `franchise_renewal_log` (`id`, `franchise_id`, `expiry_date`, `renewed_date`, `new_expiry_date`, `approved_by`, `date_time`) VALUES
(8, 10, '2021-08-04', '2021-10-25', '2021-12-31', 'admin', '2021-10-24 06:00:36');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `account_id` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `module` text NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `account_id`, `description`, `ip`, `hostname`, `timestamp`, `module`, `is_deleted`) VALUES
(275, '3', 'Add access level: test', '::1', 'VivoBook-S14', '2018-10-09 03:38:44', '', 0),
(276, '3', 'Delete access level id 5', '::1', 'VivoBook-S14', '2018-10-09 03:39:13', '', 0),
(277, '3', 'Add access level: test', '::1', 'VivoBook-S14', '2018-10-09 03:39:16', '', 0),
(278, '3', 'Add access level: test', '::1', 'VivoBook-S14', '2018-10-09 03:39:50', '', 0),
(365, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-05 23:47:52', '', 0),
(366, '3', 'Add client: deborah', '::1', 'DESKTOP-FO8SD5D', '2021-10-05 23:50:52', '', 0),
(367, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 00:45:16', '', 0),
(368, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 00:46:53', '', 0),
(369, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 00:47:14', '', 0),
(370, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 00:47:59', '', 0),
(371, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:08:22', '', 0),
(372, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:25:40', '', 0),
(373, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:29:08', '', 0),
(374, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:29:32', '', 0),
(375, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:30:43', '', 0),
(376, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:31:24', '', 0),
(377, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:49:20', '', 0),
(378, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:53:37', '', 0),
(379, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:54:02', '', 0),
(380, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:54:27', '', 0),
(381, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:54:39', '', 0),
(382, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:56:10', '', 0),
(383, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-07 23:38:49', '', 0),
(384, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-08 06:38:59', '', 0),
(385, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-08 06:51:24', '', 0),
(386, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-08 06:51:39', '', 0),
(387, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-08 07:03:22', '', 0),
(388, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-08 08:05:11', '', 0),
(389, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-08 08:27:13', '', 0),
(390, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-08 08:27:24', '', 0),
(391, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:03:11', '', 0),
(392, '3', 'Add account: xantha underwood', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:04:52', '', 0),
(393, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:11:18', '', 0),
(394, '3', 'Edit access level id 3', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:17:35', '', 0),
(395, '3', 'Add account: ori serrano', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:20:25', '', 0),
(396, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:23:02', '', 0),
(397, '3', 'Add account: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:38:46', '', 0),
(398, '3', 'Add account: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:48:15', '', 0),
(399, '3', 'Add account: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:48:18', '', 0),
(400, '3', 'Add account: allen kramer', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:48:51', '', 0),
(401, '3', 'Add account: inez roman', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:49:35', '', 0),
(402, '3', 'Add account: sydney underwood', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:49:52', '', 0),
(403, '3', 'Edit account id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:52:23', '', 0),
(404, '3', 'Edit account id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:52:31', '', 0),
(405, '3', 'Edit account id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:52:53', '', 0),
(406, '3', 'Edit account id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:53:04', '', 0),
(407, '3', 'Edit account id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:54:49', '', 0),
(408, '3', 'Delete account id 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:56:58', '', 0),
(409, '3', 'Delete account id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:59:04', '', 0),
(410, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 02:35:09', '', 0),
(411, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 02:35:54', '', 0),
(412, '3', 'Add product: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:27:13', '', 0),
(413, '3', 'Add product: sample item', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:27:51', '', 0),
(414, '3', 'Add product: 50% sugar level', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:39:29', '', 0),
(415, '3', 'Add product: sdfsdf', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:40:25', '', 0),
(416, '3', 'Add product: sdffsdf', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:40:41', '', 0),
(417, '3', 'Edit product id 2', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:41:58', '', 0),
(418, '3', 'Edit product id 2', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:42:03', '', 0),
(419, '3', 'Edit product id 2', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:45:26', '', 0),
(420, '3', 'Edit product id 2', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:46:05', '', 0),
(421, '3', 'Edit product id 3', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:46:12', '', 0),
(422, '3', 'Edit product id 3', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:46:18', '', 0),
(423, '3', 'Edit product id 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:46:56', '', 0),
(424, '3', 'Edit product id 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:47:23', '', 0),
(425, '3', 'Edit product id 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:47:28', '', 0),
(426, '3', 'Edit product id 2', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:47:38', '', 0),
(427, '3', 'Edit product id 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:48:16', '', 0),
(428, '3', 'Edit product id 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:48:25', '', 0),
(429, '3', 'Add product: nemo voluptas volupt', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 04:38:31', '', 0),
(430, '3', 'Add product: labore beatae ducimu', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 04:38:38', '', 0),
(431, '3', 'Add product: veniam irure aliqua', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 04:38:43', '', 0),
(432, '3', 'Add product: officia ab voluptate', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 04:38:50', '', 0),
(433, '3', 'Add product: at nihil qui sint po', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 04:38:53', '', 0),
(434, '3', 'Add product: fugit et et non et ', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 04:38:57', '', 0),
(435, '3', 'Add product: fugiat eiusmod conse', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 04:39:12', '', 0),
(436, '3', 'Delete product id 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:22:18', '', 0),
(437, '3', 'Delete product id 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:22:28', '', 0),
(438, '3', 'Delete product id 10', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:22:49', '', 0),
(439, '3', 'Edit product id 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:25:07', '', 0),
(440, '3', 'Delete product id 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:25:12', '', 0),
(441, '3', 'Delete product id 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:26:15', '', 0),
(442, '3', 'Add product: culpa neque dolores', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:30:18', '', 0),
(443, '3', 'Add product: hgjghj', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:30:24', '', 0),
(444, '3', 'Delete product id 13', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:30:28', '', 0),
(445, '3', 'Delete product id 9', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:30:33', '', 0),
(446, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:34:12', '', 0),
(447, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:35:55', '', 0),
(448, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 06:16:55', '', 0),
(449, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:14:23', '', 0),
(450, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:18:54', '', 0),
(451, '3', 'Add account: dominique', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:19:22', '', 0),
(452, '3', 'Add account: blake', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:19:32', '', 0),
(453, '3', 'Add account: madeson', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:23:11', '', 0),
(454, '3', 'Add account: lani', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:23:16', '', 0),
(455, '3', 'Add account: cooper', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:23:25', '', 0),
(456, '3', 'Add account: daryl', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:25:10', '', 0),
(457, '3', 'Add account: margaret', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:26:39', '', 0),
(458, '3', 'Add account: eric', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:26:52', '', 0),
(459, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:33:31', '', 0),
(460, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:47:02', '', 0),
(461, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:47:46', '', 0),
(462, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:48:02', '', 0),
(463, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:48:17', '', 0),
(464, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:55:03', '', 0),
(465, '3', 'Add supplier name: kane garrison', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 07:07:33', '', 0),
(466, '3', 'Add supplier name: quintessa coffey', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 07:07:45', '', 0),
(467, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 07:55:31', '', 0),
(468, '1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:00:59', '', 0),
(469, '1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:01:39', '', 0),
(470, '1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:01:49', '', 0),
(471, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:08:48', '', 0),
(472, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:12:03', '', 0),
(473, '1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:18:27', '', 0),
(474, '1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:18:55', '', 0),
(475, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:20:01', '', 0),
(476, '3', 'Logged in', '192.168.1.240', 'Carlitos-Galaxy-Note9.lan', '2021-10-10 08:23:51', '', 0),
(477, '3', 'Logged in', '192.168.1.240', 'Carlitos-Galaxy-Note9.lan', '2021-10-10 08:24:58', '', 0),
(478, '3', 'Logged in', '192.168.1.240', 'Carlitos-Galaxy-Note9.lan', '2021-10-10 08:26:22', '', 0),
(479, '1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:29:38', '', 0),
(480, '1', 'Logged in', '192.168.1.240', 'Carlitos-Galaxy-Note9.lan', '2021-10-10 08:30:18', '', 0),
(481, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 23:36:52', '', 0),
(482, '3', 'Add account: hayden', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 23:39:16', '', 0),
(483, '3', 'Add account: victoria', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 23:39:20', '', 0),
(484, '1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 23:59:10', '', 0),
(485, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 00:03:38', '', 0),
(486, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 00:49:16', '', 0),
(487, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 01:32:39', 'ACCOUNTING', 0),
(488, '3', 'Add account: rosalyn', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 01:36:48', 'ACCOUNTING', 0),
(489, '1', 'Add account: cherokee', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 01:37:37', 'ACCOUNTING', 0),
(490, '1', 'Add account: lillian', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 01:38:12', 'ACCOUNTING', 0),
(491, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 01:40:50', 'ACCOUNTING', 0),
(492, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 01:43:13', 'ACCOUNTING', 0),
(493, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 02:43:08', 'ACCOUNTING', 0),
(494, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 05:04:29', 'ACCOUNTING', 0),
(495, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 05:53:17', 'ACCOUNTING', 0),
(496, 'juhshd dfd dssd', 'Edit product id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 06:56:34', 'ACCOUNTING', 0),
(497, 'juhshd dfd dssd', 'Edit product id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 06:57:56', 'ACCOUNTING', 0),
(498, 'juhshd dfd dssd', 'Edit product id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 06:58:08', 'ACCOUNTING', 0),
(499, 'juhshd dfd dssd', 'Edit product id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:02:55', 'ACCOUNTING', 0),
(500, 'juhshd dfd dssd', 'Edit product id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:03:39', 'ACCOUNTING', 0),
(501, 'juhshd dfd dssd', 'Edit product id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:04:32', 'ACCOUNTING', 0),
(502, 'juhshd dfd dssd', 'Edit product id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:04:43', 'ACCOUNTING', 0),
(503, 'juhshd dfd dssd', 'Edit product id 8', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:05:39', 'ACCOUNTING', 0),
(504, 'juhshd dfd dssd', 'Edit product id 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:05:43', 'ACCOUNTING', 0),
(505, 'juhshd dfd dssd', 'Edit product id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:05:46', 'ACCOUNTING', 0),
(506, 'juhshd dfd dssd', 'Edit product id 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:05:50', 'ACCOUNTING', 0),
(507, 'juhshd dfd dssd', 'Edit product id 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:06:00', 'ACCOUNTING', 0),
(508, 'juhshd dfd dssd', 'Add product: qui voluptas ad modi', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:06:09', 'ACCOUNTING', 0),
(509, 'juhshd dfd dssd', 'Add product: accusantium omnis ev', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:06:15', 'ACCOUNTING', 0),
(510, 'juhshd dfd dssd', 'Add product: autem sit laudantiu', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:06:24', 'ACCOUNTING', 0),
(511, 'juhshd dfd dssd', 'Edit product id 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:13:52', 'ACCOUNTING', 0),
(512, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:44:42', 'ACCOUNTING', 0),
(513, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:46:01', 'ACCOUNTING', 0),
(514, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 23:28:59', 'ACCOUNTING', 0),
(515, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 23:38:28', 'ACCOUNTING', 0),
(516, 'juhshd dfd dssd', 'Add product: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 00:48:09', 'ACCOUNTING', 0),
(517, 'juhshd dfd dssd', 'Add added attendance to employee id: 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 00:49:36', 'ACCOUNTING', 0),
(518, 'juhshd dfd dssd', 'Add added attendance to employee id: 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 00:58:44', 'ACCOUNTING', 0),
(519, 'juhshd dfd dssd', 'Edit updated attendance to employee id 3', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:00:18', 'ACCOUNTING', 0),
(520, 'juhshd dfd dssd', 'Delete updated attendance to employee id 3', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:00:26', 'ACCOUNTING', 0),
(521, 'juhshd dfd dssd', 'Add added attendance to employee id: 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:00:40', 'ACCOUNTING', 0),
(522, 'juhshd dfd dssd', 'Add added attendance to employee id: 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:02:32', 'ACCOUNTING', 0),
(523, 'juhshd dfd dssd', 'Add added attendance to employee id: 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:03:12', 'ACCOUNTING', 0),
(524, 'juhshd dfd dssd', 'Edit updated attendance to employee id 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:03:51', 'ACCOUNTING', 0),
(525, 'juhshd dfd dssd', 'Add added attendance to employee id: 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:05:04', 'ACCOUNTING', 0),
(526, 'juhshd dfd dssd', 'Add added attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:07:39', 'ACCOUNTING', 0),
(527, 'juhshd dfd dssd', 'Add added attendance to employee id: 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:07:43', 'ACCOUNTING', 0),
(528, 'juhshd dfd dssd', 'Add added attendance to employee id: 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:07:47', 'ACCOUNTING', 0),
(529, 'juhshd dfd dssd', 'Add added attendance to employee id: 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:07:55', 'ACCOUNTING', 0),
(530, 'juhshd dfd dssd', 'Add added attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:08:50', 'ACCOUNTING', 0),
(531, 'juhshd dfd dssd', 'Add added attendance to employee id: 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:08:55', 'ACCOUNTING', 0),
(532, 'juhshd dfd dssd', 'Add added attendance to employee id: 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:12:00', 'ACCOUNTING', 0),
(533, 'juhshd dfd dssd', 'Add added attendance to employee id: 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:12:33', 'ACCOUNTING', 0),
(534, 'juhshd dfd dssd', 'Add added attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:13:34', 'ACCOUNTING', 0),
(535, 'juhshd dfd dssd', 'Add added attendance to employee id: 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:13:55', 'ACCOUNTING', 0),
(536, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:32:09', 'ACCOUNTING', 0),
(537, 'juhshd dfd dssd', 'Add attendance to employee id: 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:36:20', 'ACCOUNTING', 0),
(538, 'juhshd dfd dssd', 'Add attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:37:01', 'ACCOUNTING', 0),
(539, 'juhshd dfd dssd', 'Add attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:37:15', 'ACCOUNTING', 0),
(540, 'juhshd dfd dssd', 'Add attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:39:21', 'ACCOUNTING', 0),
(541, 'juhshd dfd dssd', 'Add attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:39:39', 'ACCOUNTING', 0),
(542, 'juhshd dfd dssd', 'Add attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:39:50', 'ACCOUNTING', 0),
(543, 'juhshd dfd dssd', 'Add attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:40:02', 'ACCOUNTING', 0),
(544, 'juhshd dfd dssd', 'Add account: caleb', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:49:27', 'ACCOUNTING', 0),
(545, 'juhshd dfd dssd', 'Add account: kirestin', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:49:33', 'ACCOUNTING', 0),
(546, 'juhshd dfd dssd', 'Add account: madonna', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:49:38', 'ACCOUNTING', 0),
(547, 'juhshd dfd dssd', 'Add account: yuri', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:49:42', 'ACCOUNTING', 0),
(548, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 04:12:56', 'ACCOUNTING', 0),
(549, 'juhshd dfd dssd', 'Add attendance to employee id: 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 04:18:23', 'ACCOUNTING', 0),
(550, 'juhshd dfd dssd', 'Add attendance to employee id: 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 04:26:26', 'ACCOUNTING', 0),
(551, 'juhshd dfd dssd', 'Add attendance to employee id: 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 04:26:34', 'ACCOUNTING', 0),
(552, 'juhshd dfd dssd', 'Logged in', '192.168.1.240', 'Carlitos-Galaxy-Note9.lan', '2021-10-12 04:35:35', 'ACCOUNTING', 0),
(553, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 04:41:09', 'ACCOUNTING', 0),
(554, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:03:39', 'ACCOUNTING', 0),
(555, 'juhshd dfd dssd', 'Add account: lester', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:06:29', 'ACCOUNTING', 0),
(556, 'juhshd dfd dssd', 'Add account: rosalyn', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:06:37', 'ACCOUNTING', 0),
(557, 'juhshd dfd dssd', 'Add account: anastasia', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:06:42', 'ACCOUNTING', 0),
(558, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:08:40', 'ACCOUNTING', 0),
(559, 'juhshd dfd dssd', 'Logged in', '192.168.1.240', 'Carlitos-Galaxy-Note9.lan', '2021-10-12 05:10:54', 'ACCOUNTING', 0),
(560, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:16:06', 'ACCOUNTING', 0),
(561, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:20:53', 'ACCOUNTING', 0),
(562, 'juhshd dfd dssd', 'Edit account id 13', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:49:54', 'ACCOUNTING', 0),
(563, 'juhshd dfd dssd', 'Edit account id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:49:57', 'ACCOUNTING', 0),
(564, 'juhshd dfd dssd', 'Edit account id 10', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:01', 'ACCOUNTING', 0),
(565, 'juhshd dfd dssd', 'Edit account id 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:05', 'ACCOUNTING', 0),
(566, 'juhshd dfd dssd', 'Edit account id 8', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:10', 'ACCOUNTING', 0),
(567, 'juhshd dfd dssd', 'Edit account id 9', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:14', 'ACCOUNTING', 0),
(568, 'juhshd dfd dssd', 'Edit account id 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:18', 'ACCOUNTING', 0),
(569, 'juhshd dfd dssd', 'Edit account id 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:22', 'ACCOUNTING', 0),
(570, 'juhshd dfd dssd', 'Edit account id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:26', 'ACCOUNTING', 0),
(571, 'juhshd dfd dssd', 'Edit account id 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:29', 'ACCOUNTING', 0),
(572, 'juhshd dfd dssd', 'Edit account id 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:34', 'ACCOUNTING', 0),
(573, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 07:37:50', 'ACCOUNTING', 0),
(574, 'juhshd dfd dssd', 'Add account: darrel brock', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 07:38:05', 'ACCOUNTING', 0),
(575, 'juhshd dfd dssd', 'Add account: elvis clay', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 07:38:12', 'ACCOUNTING', 0),
(576, 'juhshd dfd dssd', 'Add account: maggy bernard', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 07:38:26', 'ACCOUNTING', 0),
(577, 'juhshd dfd dssd', 'Add account: anthony farmer', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 07:43:27', 'ACCOUNTING', 0),
(578, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 01:44:42', 'ACCOUNTING', 0),
(579, 'juhshd dfd dssd', 'Add account: alika holt', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 01:45:18', 'ACCOUNTING', 0),
(580, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 01:57:21', 'ACCOUNTING', 0),
(581, '1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 05:32:17', '', 0),
(582, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 05:32:56', 'ACCOUNTING', 0),
(583, 'juhshd dfd dssd', 'Add billing payment: pending', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 06:53:57', 'ACCOUNTING', 0),
(584, 'juhshd dfd dssd', 'Add billing payment: pending', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 06:54:23', 'ACCOUNTING', 0),
(585, 'juhshd dfd dssd', 'Add billing payment: pending', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 06:54:38', 'ACCOUNTING', 0),
(586, 'juhshd dfd dssd', 'Add billing payment: approved', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 06:54:51', 'ACCOUNTING', 0),
(587, 'juhshd dfd dssd', 'Add billing payment: approved', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 06:55:11', 'ACCOUNTING', 0),
(588, 'juhshd dfd dssd', 'Add billing payment: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 06:59:46', 'ACCOUNTING', 0),
(589, 'juhshd dfd dssd', 'Add billing payment: security deposits', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:05:56', 'ACCOUNTING', 0),
(590, 'juhshd dfd dssd', 'Add billing payment: security deposits', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:06:21', 'ACCOUNTING', 0),
(591, 'juhshd dfd dssd', 'Add billing payment: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:08:19', 'ACCOUNTING', 0),
(592, 'juhshd dfd dssd', 'Add billing payment: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:08:20', 'ACCOUNTING', 0),
(593, 'juhshd dfd dssd', 'Add billing payment: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:08:44', 'ACCOUNTING', 0),
(594, 'juhshd dfd dssd', 'Add billing payment: other franchising inclusion', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:09:38', 'ACCOUNTING', 0),
(595, 'juhshd dfd dssd', 'Add billing payment: security deposits', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:10:16', 'ACCOUNTING', 0),
(596, 'juhshd dfd dssd', 'Add billing payment: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:11:17', 'ACCOUNTING', 0),
(597, 'juhshd dfd dssd', 'Add billing payment: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:13:10', 'ACCOUNTING', 0),
(598, 'juhshd dfd dssd', 'Add billing payment: security deposits', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:13:53', 'ACCOUNTING', 0),
(599, 'juhshd dfd dssd', 'Edit billing payment id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:26:25', 'ACCOUNTING', 0),
(600, 'juhshd dfd dssd', 'Edit billing payment id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:26:45', 'ACCOUNTING', 0),
(601, 'juhshd dfd dssd', 'Edit billing payment id 8', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:47:05', 'ACCOUNTING', 0),
(602, 'juhshd dfd dssd', 'Edit billing payment id 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:47:42', 'ACCOUNTING', 0),
(603, 'juhshd dfd dssd', 'Edit billing payment id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:48:00', 'ACCOUNTING', 0),
(604, 'juhshd dfd dssd', 'Edit billing payment id 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:48:12', 'ACCOUNTING', 0),
(605, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:41:55', 'ACCOUNTING', 0),
(606, 'juhshd dfd dssd', 'Add billing payment: other franchising inclusion', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:43:43', 'ACCOUNTING', 0),
(607, 'juhshd dfd dssd', 'Add billing payment: other franchising inclusion', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:43:46', 'ACCOUNTING', 0),
(608, 'juhshd dfd dssd', 'Edit attendance to employee id 27', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:46:49', 'ACCOUNTING', 0),
(609, 'juhshd dfd dssd', 'Edit attendance to employee id 27', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:46:54', 'ACCOUNTING', 0),
(610, 'juhshd dfd dssd', 'Edit attendance to employee id 27', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:47:02', 'ACCOUNTING', 0),
(611, 'juhshd dfd dssd', 'Add attendance to employee id: 10', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:47:36', 'ACCOUNTING', 0),
(612, 'juhshd dfd dssd', 'Edit billing payment id 9', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:50:47', 'ACCOUNTING', 0),
(613, 'juhshd dfd dssd', 'Edit billing payment id 9', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:51:51', 'ACCOUNTING', 0),
(614, 'juhshd dfd dssd', 'Add attendance to employee id: 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:53:15', 'ACCOUNTING', 0),
(615, 'juhshd dfd dssd', 'Add attendance to employee id: 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:53:19', 'ACCOUNTING', 0),
(616, 'juhshd dfd dssd', 'Add attendance to employee id: 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:53:22', 'ACCOUNTING', 0),
(617, 'juhshd dfd dssd', 'Add attendance to employee id: 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:53:25', 'ACCOUNTING', 0),
(618, 'juhshd dfd dssd', 'Add attendance to employee id: 10', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:53:30', 'ACCOUNTING', 0),
(619, 'juhshd dfd dssd', 'Add attendance to employee id: 9', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:53:33', 'ACCOUNTING', 0),
(620, 'juhshd dfd dssd', 'Add attendance to employee id: 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:53:36', 'ACCOUNTING', 0),
(621, 'juhshd dfd dssd', 'Add attendance to employee id: 13', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:53:41', 'ACCOUNTING', 0),
(622, 'juhshd dfd dssd', 'Add franchisee: rigel arnold', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:54:32', 'ACCOUNTING', 0),
(623, 'juhshd dfd dssd', 'Add franchisee: gemma dean', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:54:36', 'ACCOUNTING', 0),
(624, 'juhshd dfd dssd', 'Add franchisee: eric pena', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:54:41', 'ACCOUNTING', 0),
(625, 'juhshd dfd dssd', 'Add franchisee: christine zimmerman', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 23:54:45', 'ACCOUNTING', 0),
(626, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-14 05:51:53', 'ACCOUNTING', 0),
(627, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-14 05:52:07', 'ACCOUNTING', 0),
(628, 'juhshd dfd dssd', 'Add order payment: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-14 05:57:52', 'ACCOUNTING', 0),
(629, 'juhshd dfd dssd', 'Add order payment: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-14 05:58:10', 'ACCOUNTING', 0),
(630, 'juhshd dfd dssd', 'Add order payment: 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-14 05:59:12', 'ACCOUNTING', 0),
(631, 'juhshd dfd dssd', 'Edit order payment id 0', '::1', 'DESKTOP-FO8SD5D', '2021-10-14 05:59:47', 'ACCOUNTING', 0),
(632, 'juhshd dfd dssd', 'Add order payment: 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-14 06:00:10', 'ACCOUNTING', 0),
(633, 'juhshd dfd dssd', 'Add order payment: 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-14 06:00:28', 'ACCOUNTING', 0),
(634, 'juhshd dfd dssd', 'Add billing payment: system wide ad fund', '::1', 'DESKTOP-FO8SD5D', '2021-10-14 06:05:53', 'ACCOUNTING', 0),
(635, 'juhshd dfd dssd', 'Add supplier name: raphael goodwin', '::1', 'DESKTOP-FO8SD5D', '2021-10-14 06:51:45', 'ACCOUNTING', 0),
(636, 'juhshd dfd dssd', 'Edit billing payment id 10', '::1', 'DESKTOP-FO8SD5D', '2021-10-14 07:15:59', 'ACCOUNTING', 0),
(637, 'juhshd dfd dssd', 'Add order payment: 8', '::1', 'DESKTOP-FO8SD5D', '2021-10-14 07:28:49', 'ACCOUNTING', 0),
(638, 'juhshd dfd dssd', 'Edit billing payment id 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-14 07:58:41', 'ACCOUNTING', 0),
(639, 'juhshd dfd dssd', 'Add order payment: 10', '::1', 'DESKTOP-FO8SD5D', '2021-10-14 07:59:06', 'ACCOUNTING', 0),
(640, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 00:56:47', 'ACCOUNTING', 0),
(641, 'juhshd dfd dssd', 'Edit attendance to employee id 22', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 01:04:18', 'ACCOUNTING', 0),
(642, 'juhshd dfd dssd', 'Add payroll: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 05:26:00', 'ACCOUNTING', 0),
(643, 'juhshd dfd dssd', 'Add payroll: 0001', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 05:26:22', 'ACCOUNTING', 0),
(644, 'juhshd dfd dssd', 'Delete payroll id 2', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 05:31:56', 'ACCOUNTING', 0),
(645, 'juhshd dfd dssd', 'Delete payroll id 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 05:31:59', 'ACCOUNTING', 0),
(646, 'juhshd dfd dssd', 'Add payroll: 1634275927-1', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 05:32:07', 'ACCOUNTING', 0),
(647, 'juhshd dfd dssd', 'Add payroll: 1634276001-1', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 05:33:21', 'ACCOUNTING', 0),
(648, 'juhshd dfd dssd', 'Edit attendance to employee id 36', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 07:06:26', 'ACCOUNTING', 0),
(649, 'juhshd dfd dssd', 'Edit attendance to employee id 36', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 07:06:29', 'ACCOUNTING', 0),
(650, 'juhshd dfd dssd', 'Edit attendance to employee id 36', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 07:06:32', 'ACCOUNTING', 0),
(651, 'juhshd dfd dssd', 'Edit attendance to employee id 36', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 07:06:46', 'ACCOUNTING', 0),
(652, 'juhshd dfd dssd', 'Add attendance to employee id: 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 07:07:21', 'ACCOUNTING', 0),
(653, 'juhshd dfd dssd', 'Edit attendance to employee id 37', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 07:22:43', 'ACCOUNTING', 0),
(654, 'juhshd dfd dssd', 'Edit payroll id 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 07:46:32', 'ACCOUNTING', 0),
(655, 'juhshd dfd dssd', 'Edit payroll id 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 07:46:57', 'ACCOUNTING', 0),
(656, 'juhshd dfd dssd', 'Delete payroll id 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 07:47:12', 'ACCOUNTING', 0),
(657, 'juhshd dfd dssd', 'Edit payroll id 3', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 07:47:16', 'ACCOUNTING', 0),
(658, 'juhshd dfd dssd', 'Edit payroll id 3', '::1', 'DESKTOP-FO8SD5D', '2021-10-15 07:47:23', 'ACCOUNTING', 0),
(659, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-17 00:09:04', 'ACCOUNTING', 0),
(660, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-17 01:02:14', 'ACCOUNTING', 0),
(661, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-17 01:08:33', 'ACCOUNTING', 0),
(662, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-17 02:46:36', 'ACCOUNTING', 0),
(663, 'juhshd dfd dssd', 'Add payroll: 1634441018-1', '::1', 'DESKTOP-FO8SD5D', '2021-10-17 03:23:39', 'ACCOUNTING', 0),
(664, 'juhshd dfd dssd', 'Delete payroll id 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-17 03:23:52', 'ACCOUNTING', 0),
(665, 'juhshd dfd dssd', 'Add payroll: 1634441045-1', '::1', 'DESKTOP-FO8SD5D', '2021-10-17 03:24:05', 'ACCOUNTING', 0),
(666, 'juhshd dfd dssd', 'Edit payroll id 3', '::1', 'DESKTOP-FO8SD5D', '2021-10-17 03:24:14', 'ACCOUNTING', 0),
(667, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-17 06:01:16', 'ACCOUNTING', 0),
(668, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-17 06:22:21', 'ACCOUNTING', 0),
(669, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-17 07:36:43', 'ACCOUNTING', 0),
(670, 'juhshd dfd dssd', 'Edit payroll id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-17 07:38:16', 'ACCOUNTING', 0),
(671, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-17 23:51:47', 'ACCOUNTING', 0),
(672, 'juhshd dfd dssd', 'Edit product id 3', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 00:05:46', 'ACCOUNTING', 0),
(673, 'juhshd dfd dssd', 'Add expense: operational', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 03:12:56', 'ACCOUNTING', 0),
(674, 'juhshd dfd dssd', 'Add expense: operational', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 03:15:08', 'ACCOUNTING', 0),
(675, 'juhshd dfd dssd', 'Add expense: operational', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 03:15:14', 'ACCOUNTING', 0),
(676, 'juhshd dfd dssd', 'Add expense: operational', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 03:15:52', 'ACCOUNTING', 0),
(677, 'juhshd dfd dssd', 'Add expense: operational', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 03:33:36', 'ACCOUNTING', 0),
(678, 'juhshd dfd dssd', 'Add account: christian', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 03:33:52', 'ACCOUNTING', 0),
(679, 'juhshd dfd dssd', 'Edit account id 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 03:34:04', 'ACCOUNTING', 0),
(680, 'juhshd dfd dssd', 'Add account: bruce', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 03:34:13', 'ACCOUNTING', 0),
(681, 'juhshd dfd dssd', 'Add expense: non-operational', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 03:34:33', 'ACCOUNTING', 0),
(682, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 04:35:02', 'ACCOUNTING', 0),
(683, 'juhshd dfd dssd', 'Add product: inventore veritatis ', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 07:11:01', 'ACCOUNTING', 0),
(684, 'juhshd dfd dssd', 'Edit product id 16', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 07:11:09', 'ACCOUNTING', 0),
(685, 'juhshd dfd dssd', 'Edit product id 15', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 07:11:13', 'ACCOUNTING', 0),
(686, 'juhshd dfd dssd', 'Edit product id 14', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 07:11:18', 'ACCOUNTING', 0),
(687, 'juhshd dfd dssd', 'Edit product id 8', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 07:11:23', 'ACCOUNTING', 0),
(688, 'juhshd dfd dssd', 'Edit product id 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 07:11:28', 'ACCOUNTING', 0),
(689, 'juhshd dfd dssd', 'Edit product id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 07:11:35', 'ACCOUNTING', 0),
(690, 'juhshd dfd dssd', 'Edit product id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 07:11:52', 'ACCOUNTING', 0),
(691, 'juhshd dfd dssd', 'Edit product id 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 07:11:57', 'ACCOUNTING', 0),
(692, 'juhshd dfd dssd', 'Edit product id 2', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 07:12:02', 'ACCOUNTING', 0),
(693, 'juhshd dfd dssd', 'Edit product id 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 07:12:09', 'ACCOUNTING', 0),
(694, 'juhshd dfd dssd', 'Edit product id 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 07:12:14', 'ACCOUNTING', 0),
(695, 'juhshd dfd dssd', 'Edit product id 3', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 07:12:19', 'ACCOUNTING', 0),
(696, 'juhshd dfd dssd', 'Add purchase order: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 08:02:25', 'ACCOUNTING', 0),
(697, 'juhshd dfd dssd', 'Add purchase order: 1634544203-1', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 08:03:24', 'ACCOUNTING', 0),
(698, 'juhshd dfd dssd', 'Add purchase order: 1634547504-1', '::1', 'DESKTOP-FO8SD5D', '2021-10-18 08:58:24', 'ACCOUNTING', 0),
(699, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-19 07:10:51', 'ACCOUNTING', 0),
(700, 'juhshd dfd dssd', 'Add purchase order: 1634631362-1', '::1', 'DESKTOP-FO8SD5D', '2021-10-19 08:16:02', 'ACCOUNTING', 0),
(701, 'juhshd dfd dssd', 'Edit purcahse order id 2', '::1', 'DESKTOP-FO8SD5D', '2021-10-19 08:21:46', 'ACCOUNTING', 0),
(702, 'juhshd dfd dssd', 'Edit access level id 3', '::1', 'DESKTOP-FO8SD5D', '2021-10-19 08:37:24', 'ACCOUNTING', 0),
(703, 'juhshd dfd dssd', 'Edit access level id 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-19 08:37:35', 'ACCOUNTING', 0),
(704, 'juhshd dfd dssd', 'Add account: sade', '::1', 'DESKTOP-FO8SD5D', '2021-10-19 08:38:00', 'ACCOUNTING', 0),
(705, 'Sade Timon Barlow Workman', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-19 08:38:11', 'ACCOUNTING', 0),
(706, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-19 08:38:30', 'ACCOUNTING', 0),
(707, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-19 23:42:12', 'ACCOUNTING', 0),
(708, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-19 23:48:05', 'ACCOUNTING', 0),
(709, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 00:01:46', 'ACCOUNTING', 0),
(710, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 00:02:03', 'ACCOUNTING', 0),
(711, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 00:10:45', 'ACCOUNTING', 0),
(712, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 00:29:14', 'ACCOUNTING', 0),
(713, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 06:30:05', 'ACCOUNTING', 0),
(714, 'juhshd dfd dssd', 'Add account: test', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 06:54:29', 'ACCOUNTING', 0),
(715, 'juhshd dfd dssd', 'Add account: jasmine doyle', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 06:54:37', 'ACCOUNTING', 0),
(716, 'juhshd dfd dssd', 'Add account: molly cook', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 06:54:43', 'ACCOUNTING', 0),
(717, 'juhshd dfd dssd', 'Add account: magee estes', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 06:54:47', 'ACCOUNTING', 0),
(718, 'juhshd dfd dssd', 'Add account: judith hart', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 06:54:51', 'ACCOUNTING', 0),
(719, 'juhshd dfd dssd', 'Add account: candice patel', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 06:55:03', 'ACCOUNTING', 0),
(720, 'juhshd dfd dssd', 'Add account: zena castillo', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 06:55:08', 'ACCOUNTING', 0),
(721, 'juhshd dfd dssd', 'Add account: tate cardenas', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 06:55:17', 'ACCOUNTING', 0),
(722, 'juhshd dfd dssd', 'Add product: veritatis proident ', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 06:56:24', 'ACCOUNTING', 0),
(723, 'juhshd dfd dssd', 'Add product: et rerum debitis fug', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 06:56:56', 'ACCOUNTING', 0),
(724, 'juhshd dfd dssd', 'Add payroll: p-34441046', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 06:57:53', 'ACCOUNTING', 0),
(725, 'juhshd dfd dssd', 'Add payroll: p-0001', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 06:58:26', 'ACCOUNTING', 0),
(726, 'juhshd dfd dssd', 'Add purchase order: d-0001', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 07:00:23', 'ACCOUNTING', 0),
(727, 'juhshd dfd dssd', 'Add purchase order: d-00002', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 07:01:04', 'ACCOUNTING', 0),
(728, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 07:35:33', 'ACCOUNTING', 0),
(729, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 07:38:02', 'ACCOUNTING', 0),
(730, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 07:39:40', 'ACCOUNTING', 0),
(731, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 07:53:26', 'ACCOUNTING', 0),
(732, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 08:19:17', 'ACCOUNTING', 0),
(733, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 08:30:50', 'ACCOUNTING', 0),
(734, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-20 23:32:40', 'ACCOUNTING', 0),
(735, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-21 08:22:31', 'ACCOUNTING', 0),
(736, 'juhshd dfd dssd', 'Add purchase order: d-00003', '::1', 'DESKTOP-FO8SD5D', '2021-10-21 08:24:01', 'ACCOUNTING', 0),
(737, 'juhshd dfd dssd', 'Edit billing payment id 3', '::1', 'DESKTOP-FO8SD5D', '2021-10-21 08:25:05', 'ACCOUNTING', 0),
(738, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-21 08:37:56', 'ACCOUNTING', 0),
(739, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-24 02:45:24', 'ACCOUNTING', 0),
(740, 'juhshd dfd dssd', 'Edit payroll id 8', '::1', 'DESKTOP-FO8SD5D', '2021-10-24 03:49:43', 'ACCOUNTING', 0),
(741, 'juhshd dfd dssd', 'Edit payroll id 8', '::1', 'DESKTOP-FO8SD5D', '2021-10-24 03:50:11', 'ACCOUNTING', 0),
(742, 'juhshd dfd dssd', 'Edit payroll id 8', '::1', 'DESKTOP-FO8SD5D', '2021-10-24 03:50:16', 'ACCOUNTING', 0),
(743, 'juhshd dfd dssd', 'Edit payroll id 8', '::1', 'DESKTOP-FO8SD5D', '2021-10-24 03:50:20', 'ACCOUNTING', 0),
(744, 'juhshd dfd dssd', 'Add payroll: p-00002', '::1', 'DESKTOP-FO8SD5D', '2021-10-24 03:50:32', 'ACCOUNTING', 0),
(745, 'juhshd dfd dssd', 'Edit purcahse order id 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-24 03:51:17', 'ACCOUNTING', 0),
(746, 'juhshd dfd dssd', 'Edit purcahse order id 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-24 03:52:59', 'ACCOUNTING', 0),
(747, 'juhshd dfd dssd', 'Edit billing payment id 2', '::1', 'DESKTOP-FO8SD5D', '2021-10-24 03:53:45', 'ACCOUNTING', 0),
(748, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-24 04:31:05', 'ACCOUNTING', 0),
(749, 'juhshd dfd dssd', 'Edit purcahse order id 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-24 06:02:41', 'ACCOUNTING', 0),
(750, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-25 08:32:14', 'ACCOUNTING', 0),
(751, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-25 08:32:29', 'ACCOUNTING', 0),
(752, 'juhshd dfd dssd', 'Add product: possimus et exercit', '::1', 'DESKTOP-FO8SD5D', '2021-10-25 08:36:01', 'ACCOUNTING', 0),
(753, 'juhshd dfd dssd', 'Edit purcahse order id 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-25 08:37:57', 'ACCOUNTING', 0),
(754, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-26 01:27:13', 'ACCOUNTING', 0),
(755, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-26 08:33:54', 'ACCOUNTING', 0),
(756, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-26 23:42:44', 'ACCOUNTING', 0),
(757, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-27 02:22:29', 'ACCOUNTING', 0),
(758, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-27 02:27:52', 'ACCOUNTING', 0),
(759, 'juhshd dfd dssd', 'Edit franchisee id 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-27 02:53:17', 'ACCOUNTING', 0),
(760, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-27 04:37:03', 'ACCOUNTING', 0),
(761, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-27 04:43:52', 'ACCOUNTING', 0),
(762, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-27 04:50:16', 'ACCOUNTING', 0),
(763, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-27 05:38:26', 'ACCOUNTING', 0),
(764, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-27 07:43:34', 'ACCOUNTING', 0),
(765, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 07:10:07', 'ACCOUNTING', 0),
(766, 'juhshd dfd dssd', 'Edit franchisee id 10', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 07:44:51', 'ACCOUNTING', 0),
(767, 'juhshd dfd dssd', 'Edit franchisee id 10', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 07:45:08', 'ACCOUNTING', 0),
(768, 'juhshd dfd dssd', 'Edit franchisee id 9', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 07:45:13', 'ACCOUNTING', 0),
(769, 'juhshd dfd dssd', 'Edit franchisee id 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 07:45:17', 'ACCOUNTING', 0),
(770, 'juhshd dfd dssd', 'Edit franchisee id 8', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 07:45:21', 'ACCOUNTING', 0),
(771, 'juhshd dfd dssd', 'Add franchisee: serena haynes', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 07:45:58', 'ACCOUNTING', 0),
(772, 'juhshd dfd dssd', 'Edit franchisee id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 07:48:21', 'ACCOUNTING', 0),
(773, 'juhshd dfd dssd', 'Edit franchisee id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 07:49:57', 'ACCOUNTING', 0),
(774, 'juhshd dfd dssd', 'Edit billing payment id 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:11:41', 'ACCOUNTING', 0),
(775, 'juhshd dfd dssd', 'Add billing payment: system wide ad fund', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:12:02', 'ACCOUNTING', 0),
(776, 'juhshd dfd dssd', 'Add billing payment: security deposits', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:12:10', 'ACCOUNTING', 0),
(777, 'juhshd dfd dssd', 'Add order payment: 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:13:12', 'ACCOUNTING', 0),
(778, 'juhshd dfd dssd', 'Delete order payment id 8', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:13:22', 'ACCOUNTING', 0),
(779, 'juhshd dfd dssd', 'Add order payment: 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:13:32', 'ACCOUNTING', 0),
(780, 'juhshd dfd dssd', 'Edit order payment id 10', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:27:06', 'ACCOUNTING', 0),
(781, 'juhshd dfd dssd', 'Edit order payment id 10', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:27:28', 'ACCOUNTING', 0),
(782, 'juhshd dfd dssd', 'Edit order payment id 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:28:32', 'ACCOUNTING', 0),
(783, 'juhshd dfd dssd', 'Edit order payment id 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:28:47', 'ACCOUNTING', 0),
(784, 'juhshd dfd dssd', 'Add order payment: 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:29:49', 'ACCOUNTING', 0),
(785, 'juhshd dfd dssd', 'Edit order payment id 19', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:29:56', 'ACCOUNTING', 0),
(786, 'juhshd dfd dssd', 'Edit order payment id 9', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:30:54', 'ACCOUNTING', 0),
(787, 'juhshd dfd dssd', 'Edit order payment id 9', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:31:01', 'ACCOUNTING', 0),
(788, 'juhshd dfd dssd', 'Edit billing payment id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:32:26', 'ACCOUNTING', 0),
(789, 'juhshd dfd dssd', 'Edit billing payment id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:32:31', 'ACCOUNTING', 0),
(790, 'juhshd dfd dssd', 'Edit billing payment id 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:33:25', 'ACCOUNTING', 0),
(791, 'juhshd dfd dssd', 'Edit billing payment id 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:33:31', 'ACCOUNTING', 0),
(792, 'juhshd dfd dssd', 'Edit billing payment id 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:33:55', 'ACCOUNTING', 0),
(793, 'juhshd dfd dssd', 'Edit franchisee id 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:34:37', 'ACCOUNTING', 0),
(794, 'juhshd dfd dssd', 'Edit franchisee id 9', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:34:48', 'ACCOUNTING', 0),
(795, 'juhshd dfd dssd', 'Edit franchisee id 8', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:35:03', 'ACCOUNTING', 0),
(796, 'juhshd dfd dssd', 'Delete order payment id 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:35:22', 'ACCOUNTING', 0),
(797, 'juhshd dfd dssd', 'Edit billing payment id 13', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:35:37', 'ACCOUNTING', 0),
(798, 'juhshd dfd dssd', 'Edit billing payment id 13', '::1', 'DESKTOP-FO8SD5D', '2021-10-30 08:35:42', 'ACCOUNTING', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_payments`
--

CREATE TABLE `order_payments` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `image_file` text NOT NULL,
  `date_paid` date NOT NULL,
  `check_no` int(11) NOT NULL,
  `check_details` varchar(50) NOT NULL,
  `bank` varchar(50) NOT NULL,
  `remarks` text NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'PENDING',
  `approved_by` varchar(100) NOT NULL,
  `approved_date` datetime DEFAULT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_payments`
--

INSERT INTO `order_payments` (`id`, `franchise_id`, `order_id`, `total_amount`, `image_file`, `date_paid`, `check_no`, `check_details`, `bank`, `remarks`, `status`, `approved_by`, `approved_date`, `is_deleted`, `date_time`) VALUES
(1, 7, 0, '2720.00', '', '2021-10-14', 0, '', '', '', 'APPROVED', 'admin', '2021-10-14 13:59:47', 0, ''),
(2, 7, 0, '2720.00', '', '2021-10-14', 0, '', '', '', 'APPROVED', 'admin', '2021-10-14 13:59:47', 0, ''),
(3, 7, 0, '2720.00', '', '2021-10-14', 0, '', '', '', 'APPROVED', 'admin', '2021-10-14 13:59:47', 0, ''),
(4, 11, 0, '79.00', '', '1980-02-05', 0, '', '', 'Voluptatum nulla cum', 'APPROVED', 'admin', '2021-10-14 14:00:10', 0, ''),
(5, 7, 0, '10000.00', '', '1972-12-14', 0, '', '', 'Et architecto except', 'APPROVED', 'admin', '2021-10-30 16:28:47', 0, ''),
(6, 8, 0, '2.00', '', '2001-08-03', 0, '', '', 'Eius odio aut fugit', 'REJECTED', '', NULL, 0, ''),
(7, 10, 0, '41.00', '', '2019-09-08', 0, '', '', 'Velit voluptatem Vo', 'PENDING', '', NULL, 1, ''),
(8, 3, 0, '95.00', '', '1997-07-25', 85, 'Sint commodi et ill', 'Consequatur Incidun', 'Pariatur Itaque ill', 'PENDING', '', NULL, 1, ''),
(9, 12, 0, '4000.00', '', '1982-02-23', 71, 'Sunt laboris culpa', 'Voluptatem Rerum de', 'Sunt fugiat ipsam e', 'APPROVED', 'admin', '2021-10-30 16:31:01', 0, ''),
(10, 7, 0, '100.00', '', '1978-01-06', 11, 'Dolor ab rerum id q', 'Doloremque sequi quo', 'Ipsum quod sed dicta', 'APPROVED', 'admin', '2021-10-30 16:27:28', 0, ''),
(19, 7, 0, '5000.00', '', '2021-10-30', 0, '', '', '', 'APPROVED', 'admin', '2021-10-30 16:29:56', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

CREATE TABLE `payroll` (
  `id` int(11) NOT NULL,
  `payroll_no` varchar(100) NOT NULL,
  `cut_off_date` date NOT NULL,
  `release_date` date NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `added_by` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payroll`
--

INSERT INTO `payroll` (`id`, `payroll_no`, `cut_off_date`, `release_date`, `total_amount`, `added_by`, `status`, `is_deleted`, `date_time`) VALUES
(8, 'P-0001', '2021-10-20', '2021-10-31', '500.00', 'admin', 'APPROVED', 0, '2021-10-24 03:50:20'),
(9, 'P-00002', '2021-10-25', '2021-10-29', '0.00', 'admin', 'PENDING', 0, '2021-10-24 03:50:32');

-- --------------------------------------------------------

--
-- Table structure for table `payroll_employee`
--

CREATE TABLE `payroll_employee` (
  `id` int(11) NOT NULL,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `base_rate` decimal(10,2) NOT NULL,
  `allowance` decimal(10,2) NOT NULL,
  `comm_allowance` decimal(10,2) NOT NULL,
  `ot_reg_day` decimal(10,2) NOT NULL,
  `ot_spcl_day` decimal(10,2) NOT NULL,
  `ot_leg_day` decimal(10,2) NOT NULL,
  `special_holiday` decimal(10,2) NOT NULL,
  `regular_holiday` decimal(10,2) NOT NULL,
  `sss` decimal(10,2) NOT NULL,
  `pag_ibig` decimal(10,2) NOT NULL,
  `philhealth` decimal(10,2) NOT NULL,
  `lates` decimal(10,2) NOT NULL,
  `absent` decimal(10,2) NOT NULL,
  `tax_deduct` decimal(10,2) NOT NULL,
  `sss_loan` decimal(10,2) NOT NULL,
  `pag_ibig_loan` decimal(10,2) NOT NULL,
  `cash_advance` decimal(10,2) NOT NULL,
  `short_remittance` decimal(10,2) NOT NULL,
  `gross_pay` decimal(10,2) NOT NULL,
  `total_deductions` decimal(10,2) NOT NULL,
  `net_pay` decimal(10,2) NOT NULL,
  `total_min_work` decimal(10,2) NOT NULL,
  `reg_legal_ot_work` decimal(10,2) NOT NULL,
  `special_ot_work` decimal(10,2) NOT NULL,
  `special_holiday_work` decimal(10,2) NOT NULL,
  `legal_holiday_work` decimal(10,2) NOT NULL,
  `added_by` varchar(100) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payroll_employee`
--

INSERT INTO `payroll_employee` (`id`, `payroll_id`, `emp_id`, `base_rate`, `allowance`, `comm_allowance`, `ot_reg_day`, `ot_spcl_day`, `ot_leg_day`, `special_holiday`, `regular_holiday`, `sss`, `pag_ibig`, `philhealth`, `lates`, `absent`, `tax_deduct`, `sss_loan`, `pag_ibig_loan`, `cash_advance`, `short_remittance`, `gross_pay`, `total_deductions`, `net_pay`, `total_min_work`, `reg_legal_ot_work`, `special_ot_work`, `special_holiday_work`, `legal_holiday_work`, `added_by`, `date_time`) VALUES
(4, 3, 4, '57.00', '3.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.00', '0.00', '60.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '2021-10-17 03:20:34'),
(5, 3, 5, '46.00', '4.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '10.00', '50.00', '10.00', '40.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '2021-10-17 03:20:59'),
(6, 3, 13, '1000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '1000.00', '0.00', '1000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '2021-10-17 07:19:55'),
(7, 3, 12, '5000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '200.00', '200.00', '0.00', '0.00', '5000.00', '400.00', '4600.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '2021-10-17 06:39:55'),
(8, 3, 10, '5000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '200.00', '200.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '5000.00', '400.00', '4600.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '2021-10-17 07:19:35'),
(11, 6, 5, '500.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '500.00', '0.00', '500.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '2021-10-17 06:47:45'),
(13, 6, 6, '500.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '100.00', '0.00', '0.00', '0.00', '0.00', '500.00', '100.00', '400.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'admin', '2021-10-17 07:29:12'),
(14, 6, 12, '5000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '500.00', '0.00', '5000.00', '500.00', '4500.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'admin', '2021-10-17 07:29:33'),
(16, 8, 1, '500.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '500.00', '0.00', '500.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'admin', '2021-10-20 06:59:06');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `image_file` text NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `category` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `supplier_id`, `image_file`, `product_code`, `description`, `cost`, `price`, `category`, `is_deleted`, `date_time`) VALUES
(1, 1, 'uploads/wp1808930.jpg', '023423', 'sample itemlllll', '0.00', '0.00', 'FOOD', 0, '2021-10-18 07:12:14'),
(2, 2, 'uploads/wp1808930.jpg', 'rtyrtkjkj', '50% Sugar Levelfdgdfg', '0.00', '0.00', 'NON FOOD', 0, '2021-10-18 07:12:02'),
(3, 1, 'uploads/wp1808930.jpg', 'sdfsdf', 'sdfsdfffff', '0.00', '0.00', 'NON FOOD', 0, '2021-10-18 07:12:19'),
(4, 3, 'uploads/532d0341186be12412c6a4362cc08710.jpg', 'dfgdfg', 'sdffsdf', '0.00', '0.00', 'NON FOOD', 0, '2021-10-18 07:11:57'),
(5, 2, 'uploads/532d0341186be12412c6a4362cc08710.jpg', 'Ea ut sed in corrupt', 'Nemo voluptas volupt', '0.00', '0.00', 'NON FOOD', 0, '2021-10-18 07:12:09'),
(6, 2, 'uploads/532d0341186be12412c6a4362cc08710.jpg', 'Vitae fuga Est adip', 'Labore beatae ducimu', '0.00', '0.00', 'OTHERS', 0, '2021-10-18 07:11:52'),
(7, 1, 'uploads/532d0341186be12412c6a4362cc08710.jpg', '1634541088', 'Veniam irure aliqua', '60.00', '100.00', 'OTHERS', 0, '2021-10-18 07:11:28'),
(8, 3, '', '1634541083', 'Officia ab voluptate', '0.00', '0.00', 'NON FOOD', 0, '2021-10-18 07:11:23'),
(9, 0, '', 'Aliqua Sapiente min', 'At nihil qui sint po', '0.00', '0.00', '', 1, '2021-10-09 05:30:33'),
(10, 0, '', 'Eligendi sit ration', 'Fugit et et non et ', '0.00', '0.00', '', 1, '2021-10-09 05:22:49'),
(11, 0, '', 'Ad natus nostrum ad ', 'Fugiat eiusmod conseggggggggg', '0.00', '0.00', '', 1, '2021-10-09 05:25:07'),
(12, 2, 'uploads/532d0341186be12412c6a4362cc08710.jpg', '1634541095', 'Culpa neque dolores', '500.00', '1000.00', 'FOOD', 0, '2021-10-18 07:11:35'),
(13, 0, '', 'u67867', 'hgjghj', '0.00', '0.00', '', 1, '2021-10-09 05:30:28'),
(14, 2, '', '1634541078', 'Qui voluptas ad modi', '32.00', '210.00', 'FOOD', 0, '2021-10-18 07:11:18'),
(15, 2, '', '1634541073', 'Accusantium omnis ev', '17.00', '494.00', 'NON FOOD', 0, '2021-10-18 07:11:13'),
(16, 2, '', '1634541069', 'Autem sit laudantiu', '25.00', '610.00', 'FOOD', 0, '2021-10-18 07:11:09'),
(17, 1, '', '1634541061', 'Inventore veritatis ', '82.00', '368.00', 'NON FOOD', 0, '2021-10-18 07:11:01'),
(18, 3, '', 'I-0001', 'Veritatis proident ', '77.00', '634.00', 'NON FOOD', 0, '2021-10-20 06:56:24'),
(19, 3, '', 'I-0002', 'Et rerum debitis fug', '46.00', '858.00', 'FOOD', 0, '2021-10-20 06:56:55'),
(20, 1, '', 'I-0003', 'Possimus et exercit', '39.00', '330.00', 'FOOD', 0, '2021-10-25 08:36:01');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` int(11) NOT NULL,
  `po_number` varchar(100) NOT NULL,
  `date_po` date NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `mode_of_payment` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `added_by` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`id`, `po_number`, `date_po`, `supplier_id`, `total_amount`, `mode_of_payment`, `status`, `added_by`, `is_deleted`, `date_time`) VALUES
(3, 'D-0001', '2021-10-20', 1, '0.00', 'PICKUP', 'PENDING', 'admin', 0, '2021-10-20 07:00:23'),
(4, 'D-00002', '2021-10-22', 2, '200.00', 'PICKUP', 'APPROVED', 'admin', 0, '2021-10-25 08:37:57'),
(5, 'D-00003', '2021-10-21', 1, '31000.00', 'PICKUP', 'RECEIVED', 'admin', 0, '2021-10-24 06:02:41');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_items`
--

CREATE TABLE `purchase_order_items` (
  `id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_order_items`
--

INSERT INTO `purchase_order_items` (`id`, `purchase_order_id`, `product_id`, `qty`, `cost`, `total_amount`) VALUES
(1, 0, 5, 10, '0.00', '0.00'),
(8, 2, 14, 10, '32.00', '320.00'),
(10, 2, 5, 50, '100.00', '5000.00'),
(11, 2, 2, 100, '20.00', '2000.00'),
(12, 1, 3, 5, '200.00', '1000.00'),
(13, 1, 1, 100, '50.00', '5000.00'),
(14, 1, 17, 1, '82.00', '82.00'),
(15, 4, 5, 10, '20.00', '200.00'),
(16, 5, 3, 100, '500.00', '25000.00'),
(17, 5, 7, 100, '60.00', '6000.00');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_name` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `category_id`, `sub_category_name`, `is_deleted`, `date_time`) VALUES
(1, 6, 'Cost of Goods', 0, '2021-10-24 00:05:25'),
(2, 8, 'Gas Allowance', 0, '2021-10-24 00:05:43'),
(3, 8, 'Food Allowance', 0, '2021-10-24 00:05:58'),
(4, 8, 'Toll Fee', 0, '2021-10-24 00:06:07'),
(5, 8, 'Board and Lodging', 0, '2021-10-24 00:06:25'),
(6, 9, 'Repair and Maintenance Works', 0, '2021-10-24 00:06:50'),
(7, 9, 'Telephone and Internet Expenses', 0, '2021-10-24 00:07:05'),
(8, 9, 'Electricity and Water Bill', 0, '2021-10-24 00:07:22'),
(9, 9, 'Office Supplies', 0, '2021-10-24 00:07:34'),
(10, 9, 'Professional Fee', 0, '2021-10-24 00:07:46'),
(11, 9, 'Permit and Licenses', 0, '2021-10-24 00:07:56');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_address` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `supplier_name`, `supplier_address`, `is_deleted`, `date_time`) VALUES
(1, 'Kane Garrison', 'Et laudantium volup', 0, '2021-10-10 07:07:33'),
(2, 'Quintessa Coffey', 'Voluptas similique e', 0, '2021-10-10 07:07:45'),
(3, 'Raphael Goodwin', 'Et iusto consectetur', 0, '2021-10-14 06:51:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_levels`
--
ALTER TABLE `access_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_payments`
--
ALTER TABLE `billing_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commissary_accounts`
--
ALTER TABLE `commissary_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchisee`
--
ALTER TABLE `franchisee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `franchisee_ibfk_1` (`branch_id`);

--
-- Indexes for table `franchise_accounts`
--
ALTER TABLE `franchise_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchise_branch`
--
ALTER TABLE `franchise_branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchise_renewal_log`
--
ALTER TABLE `franchise_renewal_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_payments`
--
ALTER TABLE `order_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll`
--
ALTER TABLE `payroll`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll_employee`
--
ALTER TABLE `payroll_employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_code` (`product_code`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `po_number` (`po_number`);

--
-- Indexes for table `purchase_order_items`
--
ALTER TABLE `purchase_order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_category` (`category_id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_levels`
--
ALTER TABLE `access_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `billing_payments`
--
ALTER TABLE `billing_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `commissary_accounts`
--
ALTER TABLE `commissary_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `franchisee`
--
ALTER TABLE `franchisee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `franchise_accounts`
--
ALTER TABLE `franchise_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `franchise_branch`
--
ALTER TABLE `franchise_branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `franchise_renewal_log`
--
ALTER TABLE `franchise_renewal_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=799;

--
-- AUTO_INCREMENT for table `order_payments`
--
ALTER TABLE `order_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `payroll`
--
ALTER TABLE `payroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `payroll_employee`
--
ALTER TABLE `payroll_employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `purchase_order_items`
--
ALTER TABLE `purchase_order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `franchisee`
--
ALTER TABLE `franchisee`
  ADD CONSTRAINT `franchisee_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `franchise_branch` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
