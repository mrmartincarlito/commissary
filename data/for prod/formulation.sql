-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 08, 2022 at 01:45 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thehungrypita`
--

-- --------------------------------------------------------

--
-- Table structure for table `formulation`
--

CREATE TABLE `formulation` (
  `form_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `produce` int(11) DEFAULT 0,
  `date_process` date NOT NULL,
  `remarks` text NOT NULL,
  `added_by` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `formulation`
--

INSERT INTO `formulation` (`form_id`, `item_id`, `description`, `produce`, `date_process`, `remarks`, `added_by`, `date_time`, `is_deleted`) VALUES
(5, 21, 'Sample ITEM 1', 0, '2021-12-04', 'N/A', 'commissary', '2021-12-03 22:45:24', 0),
(6, 24, 'SAmple for Item 4', 0, '2021-12-04', 'N/A', 'commissary', '2021-12-03 22:51:55', 0),
(7, 22, 'Sample Formulation for Sample Item 2', 0, '2021-12-04', 'N/A', 'commissary', '2021-12-04 06:54:38', 0),
(8, 23, 'For Item 3', 0, '2021-12-05', 'N/A', 'commissary', '2021-12-05 02:10:39', 0),
(9, 21, 'Test', 150, '2022-04-08', 'N/A', 'commissary', '2022-04-07 23:40:58', 0),
(10, 22, 'Sample', 200, '2022-04-08', 'N/A', 'commissary', '2022-04-07 23:45:09', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `formulation`
--
ALTER TABLE `formulation`
  ADD PRIMARY KEY (`form_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `formulation`
--
ALTER TABLE `formulation`
  MODIFY `form_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
