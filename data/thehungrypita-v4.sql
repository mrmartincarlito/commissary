-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 13, 2021 at 09:49 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thehungrypita`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_levels`
--

CREATE TABLE `access_levels` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `access_user_accounts` int(11) NOT NULL DEFAULT 0,
  `access_access_levels` int(11) NOT NULL DEFAULT 0,
  `access_dashboard` int(11) NOT NULL DEFAULT 0,
  `access_billing_franchising` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_levels`
--

INSERT INTO `access_levels` (`id`, `description`, `access_user_accounts`, `access_access_levels`, `access_dashboard`, `access_billing_franchising`, `is_deleted`) VALUES
(3, 'Administrator', 1, 1, 1, 1, 0),
(4, 'User', 1, 0, 0, 0, 0),
(45, 'Customer', 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `image_file` text NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT 0,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `role`, `image_file`, `is_deleted`, `date`) VALUES
(1, 'juhshd', 'dfd', 'dssd', 'utet@gmail.com', 'admin', '$2y$10$7e9rSp7gC.GslurbeSlIL.6NnSdN6/ejq0u/ueIn.00Kc7bMnxWBa', '3', 'uploads/wp1808930.jpg', 0, '2021-10-11 06:55:44'),
(5, 'Nashh', 'Risa David', 'Dominguez', 'licyvanox@mailinator.com', 'xicof', '$2y$10$.Nx2JvDvduut6Zk0fl/EOefUhm9K.CBxQ7eLNqYleOQ0O0LndbSVa', '3', '', 1, '2021-10-09 07:52:25'),
(7, 'Hayden', 'Leah York', 'Neal', 'hydoqu@mailinator.com', 'tutigely', '$2y$10$pGvAKzUbcHJE6vDUap.4Fe5V0PIre/thkcFvPu0xRQm5t4Hov3uzK', '3', '', 0, '2021-10-10 23:39:16'),
(8, 'Victoria', 'Stone Hunt', 'Moon', 'pehahemir@mailinator.com', 'niqezex', '$2y$10$r5xuDpP89LTsGV3yaf17Ie7mYQuaRun7mFYKreFsTaGC58xzveKXe', '3', '', 0, '2021-10-10 23:39:20'),
(9, 'Rosalyn', 'Fredericka Mathews', 'Fulton', 'hitetas@mailinator.com', 'kijog', '$2y$10$GcLRA3RdOrGNjx6.I5CycOvD9/YojXLDVbNUkX5n7lDTBQdsGKCGC', '3', '', 0, '2021-10-11 01:36:48'),
(10, 'Cherokee', 'Trevor Maxwell', 'Black', 'goxylyn@mailinator.com', 'cytesohere', '$2y$10$NhO5UnQRSVmWFrTHRlkVT.tZj7GoQjYt772QFZ1UGfY4ApHMcS52C', '3', '', 0, '2021-10-11 01:37:37');

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `total_minutes` int(11) NOT NULL,
  `regular_legal_ot` int(11) NOT NULL,
  `special_ot` int(11) NOT NULL,
  `special_holiday` int(11) NOT NULL,
  `total_hours` float(11,2) NOT NULL,
  `legend` varchar(50) NOT NULL,
  `work_date` date NOT NULL,
  `added_by` text NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `emp_id`, `total_minutes`, `regular_legal_ot`, `special_ot`, `special_holiday`, `total_hours`, `legend`, `work_date`, `added_by`, `is_deleted`, `date_time`) VALUES
(1, 5, 24, 32, 2, 0, 1.00, 'PRESENT', '2021-08-07', 'admin', 0, '0000-00-00 00:00:00'),
(2, 1, 86, 82, 56, 0, 3.00, 'PRESENT', '1972-01-06', 'admin', 0, '0000-00-00 00:00:00'),
(3, 1, 480, 227, 0, 0, 11.00, 'PRESENT', '2021-09-16', 'admin', 1, '0000-00-00 00:00:00'),
(4, 1, 480, 227, 0, 0, 11.00, 'PRESENT', '2021-10-13', 'admin', 0, '0000-00-00 00:00:00'),
(5, 1, 480, 63, 0, 0, 9.30, 'PRESENT', '2021-10-12', 'admin', 0, '0000-00-00 00:00:00'),
(6, 5, 480, 227, 0, 0, 11.47, 'PRESENT', '2021-10-14', 'admin', 0, '0000-00-00 00:00:00'),
(7, 1, 480, 0, 0, 0, 8.00, 'PRESENT', '2021-10-12', 'admin', 0, '0000-00-00 00:00:00'),
(8, 4, 62, 24, 79, 0, 2.55, 'DAY-OFF', '1986-07-31', 'admin', 0, '0000-00-00 00:00:00'),
(9, 1, 26, 92, 86, 0, 3.42, 'DAY-OFF', '1983-08-24', 'admin', 0, '0000-00-00 00:00:00'),
(10, 5, 98, 13, 12, 0, 2.27, 'DAY-OFF', '2013-05-01', 'admin', 0, '0000-00-00 00:00:00'),
(11, 5, 54, 4, 20, 0, 1.37, 'DAY-OFF', '2003-07-21', 'admin', 0, '0000-00-00 00:00:00'),
(12, 4, 11, 87, 72, 0, 3.90, 'DAY-OFF', '1993-04-29', 'admin', 0, '0000-00-00 00:00:00'),
(13, 1, 24, 68, 31, 0, 2.19, 'PRESENT', '1994-08-02', 'admin', 0, '0000-00-00 00:00:00'),
(14, 1, 73, 99, 62, 0, 4.14, 'PRESENT', '1996-04-17', 'admin', 0, '0000-00-00 00:00:00'),
(15, 6, 31, 35, 74, 0, 2.23, 'DAY-OFF', '1972-10-20', 'admin', 0, '2021-10-12 01:12:33'),
(16, 4, 480, 60, 0, 0, 9.00, 'DAY-OFF', '1980-09-21', 'admin', 0, '2021-10-12 01:13:34'),
(17, 5, 480, 133, 0, 0, 10.13, 'PRESENT', '2012-11-01', 'admin', 0, '2021-10-12 01:13:55'),
(18, 6, 69, 39, 67, 0, 3.10, 'DAY-OFF', '1978-12-21', 'admin', 0, '2021-10-12 03:36:20'),
(19, 4, 9, 55, 100, 0, 2.48, 'PRESENT', '2020-09-14', 'admin', 0, '2021-10-12 03:37:01'),
(20, 4, 9, 55, 100, 0, 2.48, 'PRESENT', '2020-09-15', 'admin', 0, '2021-10-12 03:37:15'),
(21, 4, 0, 0, 0, 0, 0.00, 'PRESENT', '2021-10-14', 'admin', 0, '2021-10-12 03:39:21'),
(22, 4, 500, 0, 0, 0, 8.20, 'PRESENT', '2021-10-15', 'admin', 0, '2021-10-12 03:39:39'),
(23, 4, 480, 230, 0, 0, 11.50, 'PRESENT', '2021-10-16', 'admin', 0, '2021-10-12 03:39:50'),
(24, 4, 0, 0, 100, 0, 1.40, 'PRESENT', '2021-10-17', 'admin', 0, '2021-10-12 03:40:02'),
(25, 7, 30, 34, 11, 0, 1.16, 'DAY-OFF', '1972-09-21', 'admin', 0, '2021-10-12 04:18:23'),
(26, 7, 9, 38, 24, 0, 1.28, 'PRESENT', '2012-08-26', 'admin', 0, '2021-10-12 04:26:26'),
(27, 7, 0, 0, 0, 0, 0.00, 'PRESENT', '2012-08-27', 'admin', 0, '2021-10-12 04:26:34');

-- --------------------------------------------------------

--
-- Table structure for table `billing_payments`
--

CREATE TABLE `billing_payments` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `image_file` text NOT NULL,
  `date_paid` date NOT NULL,
  `remarks` text NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'PENDING',
  `approved_by` varchar(100) NOT NULL,
  `approved_date` datetime DEFAULT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `billing_payments`
--

INSERT INTO `billing_payments` (`id`, `franchise_id`, `description`, `total_amount`, `image_file`, `date_paid`, `remarks`, `status`, `approved_by`, `approved_date`, `is_deleted`, `date_time`) VALUES
(1, 7, 'FRANCHISING FEE', '2720.00', '', '2021-10-13', '', 'PENDING', '', NULL, 0, ''),
(2, 7, 'SECURITY DEPOSITS', '37.00', '', '2020-07-19', 'Fugiat quisquam expe', 'PENDING', '', NULL, 0, ''),
(3, 7, 'FRANCHISING FEE', '2720.00', '', '2021-10-14', '', 'PENDING', '', NULL, 0, ''),
(4, 7, 'OTHER FRANCHISING INCLUSION', '2720.00', '', '2021-10-14', '', 'PENDING', '', NULL, 0, ''),
(5, 7, 'SECURITY DEPOSITS', '41.00', '', '1974-02-28', 'Est velit occaecat ', 'APPROVED', 'admin', '2021-10-13 15:48:12', 0, ''),
(6, 7, 'FRANCHISING FEE', '2720.00', 'uploads/wp1808930.jpg', '2021-10-13', 'inconsistent', 'REJECTED', '', NULL, 0, ''),
(7, 7, 'FRANCHISING FEE', '2720.00', '', '2021-10-13', '', 'APPROVED', 'admin', '2021-10-13 15:47:42', 0, ''),
(8, 7, 'SECURITY DEPOSITS', '2720.00', 'uploads/532d0341186be12412c6a4362cc08710.jpg', '2021-10-14', '', 'APPROVED', 'admin', '2021-10-13 00:00:00', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `commissary_accounts`
--

CREATE TABLE `commissary_accounts` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT 0,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `commissary_accounts`
--

INSERT INTO `commissary_accounts` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `is_deleted`, `date`) VALUES
(1, 'DSFDF', 'Thaddeus Oneal', 'Vasquez', 'admin@commissary.com', 'admin', '$2y$10$bOcGC6r9kK6ZNBKjgBhv7uI7k2W6TUfaBBBFIsG8xRrv7qVkmFoKW', 1, '2021-10-10 08:18:48'),
(2, 'Margaret', 'Sonia Chavez', 'Allison', 'kynyro@mailinator.com', 'bikidom', '$2y$10$pSG4dOBfJmkaOpu0DvdeGeH/ba4LvoQaqPU856MnqiCMEJOfCpoxS', 0, '2021-10-10 06:26:39'),
(3, 'Eric', 'Eve Cortez', 'Lara', 'gywewiti@mailinator.com', 'gogyqyrul', '$2y$10$49Le1R2WBJ5QWKVeED1HlumtZJKoQqLjR1AR1Fv0g8I60XAgO/jGy', 0, '2021-10-10 06:26:52');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `city` varchar(100) NOT NULL,
  `province` varchar(100) NOT NULL,
  `employment_date` date NOT NULL,
  `regularization_date` date NOT NULL,
  `employment_type` varchar(100) NOT NULL,
  `rate` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `middle_name`, `last_name`, `city`, `province`, `employment_date`, `regularization_date`, `employment_type`, `rate`, `date`, `is_deleted`) VALUES
(1, 'Julcess', 'P', 'Mercado', 'Calumpitt', 'Bulacan', '2021-10-01', '2021-10-09', 'ADMIN - ACCOUNTING', 9000, '2021-10-12 05:50:34', 0),
(2, 'Jack', 'Oleg Gordon', 'Burgess', 'Eligendi dolores sun', 'Eu incididunt sequi ', '2020-06-22', '2011-05-02', '', 3, '2021-10-09 06:15:50', 1),
(3, 'Signe', 'Flynn Hendricks', 'Hardy', 'Officiis soluta duci', 'Earum aut vel delect', '2008-07-29', '2002-06-19', '', 18, '2021-10-09 07:57:39', 1),
(4, 'Madeson', 'Brett Rodriquez', 'Mccarty', 'Aut distinctio Quae', 'Officia esse sapient', '1971-11-27', '1984-10-29', 'SERVICE CREW', 57, '2021-10-12 05:50:29', 0),
(5, 'Lani', 'Lani Morrison', 'Cruz', 'Ut dolores ex delect', 'Voluptatem ipsam et', '1985-02-12', '1993-06-28', 'COMMISSARY OFFICER', 46, '2021-10-12 05:50:18', 0),
(6, 'Cooper', 'Genevieve Ruiz', 'Nguyen', 'Tempor repudiandae q', 'Sunt quisquam Nam su', '2013-04-06', '1971-02-14', 'SERVICE CREW', 48, '2021-10-12 05:50:26', 0),
(7, 'Caleb', 'Tatyana Haley', 'Harris', 'Eaque ad quo in mini', 'Cum expedita neque i', '1986-10-20', '1993-05-24', 'OFFICE STAFF', 100, '2021-10-12 05:50:22', 0),
(8, 'Kirestin', 'Stewart Charles', 'Flores', 'Sunt esse commodi al', 'Laboris est labore c', '1996-03-09', '1985-03-14', 'DIGITAL MARKETING', 23, '2021-10-12 05:50:10', 0),
(9, 'Madonna', 'Abra Mcclure', 'Silva', 'Nesciunt excepteur ', 'Veritatis ullam labo', '2003-12-17', '2007-10-12', 'ADMIN STAFF / TRAINOR', 39, '2021-10-12 05:50:14', 0),
(10, 'Yuri', 'Benedict Young', 'Buck', 'Autem alias consequa', 'Blanditiis qui sint ', '1975-12-12', '2006-11-14', 'ADMIN STAFF / TRAINOR', 70, '2021-10-12 05:50:01', 0),
(11, 'Lester', 'Nelle Reese', 'Montoya', 'Sit mollit ab in vol', 'Modi aut aut amet i', '1979-01-23', '2020-12-10', 'DIGITAL MARKETING', 79, '2021-10-12 05:50:05', 0),
(12, 'Rosalyn', 'Boris Sweeney', 'Green', 'Dolorem assumenda mo', 'Neque laborum elit ', '1979-05-08', '1996-01-14', 'COMMISSARY OFFICER', 2, '2021-10-12 05:49:57', 0),
(13, 'Anastasia', 'Delilah Booker', 'Valencia', 'Quia similique labor', 'Ut perferendis repre', '2006-01-24', '1971-08-16', 'OFFICE STAFF', 3, '2021-10-12 05:49:54', 0);

-- --------------------------------------------------------

--
-- Table structure for table `franchisee`
--

CREATE TABLE `franchisee` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `mode_of_transaction` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `contactno` varchar(50) NOT NULL,
  `birthday` date NOT NULL,
  `package_type` varchar(50) NOT NULL,
  `date_contract_signing` date NOT NULL,
  `date_branch_opening` date NOT NULL,
  `date_contract_expiry` date NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchisee`
--

INSERT INTO `franchisee` (`id`, `branch_id`, `mode_of_transaction`, `name`, `address`, `contactno`, `birthday`, `package_type`, `date_contract_signing`, `date_branch_opening`, `date_contract_expiry`, `is_deleted`, `date_time`) VALUES
(7, 1, 'Pick-up', 'Anthony Farmer', 'Quo dolorem sunt dol', '79', '1993-10-10', 'Cart1', '1974-08-04', '2020-03-08', '1975-10-22', 0, '2021-10-12 07:43:27');

-- --------------------------------------------------------

--
-- Table structure for table `franchise_accounts`
--

CREATE TABLE `franchise_accounts` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT 0,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchise_accounts`
--

INSERT INTO `franchise_accounts` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `is_deleted`, `date`) VALUES
(1, 'Briar', 'dgfgf', 'Riddle', 'juzebimote@mailinator.com', 'bihiroku', '$2y$10$NyX4QDL6bo9o6WlCOI/uWeb.EHNkwJkHRtKYGQJihIVIIxnal/8W.', 1, '2021-10-09 08:22:15'),
(2, 'Dominique', 'Guy Vasquez', 'Cooke', 'zonupuno@mailinator.com', 'nyqok', '$2y$10$y7LovV3akGf6coUSpN4EeugkwFvVktQj0fhXfp0eppyCOOrhzpHWu', 0, '2021-10-10 06:19:22'),
(3, 'Blake', 'Clarke Jones', 'Nieves', 'veka@mailinator.com', 'typym', '$2y$10$nmhK19uDWtPRCKL7HBodSuYkMOyCpz7UqUfESrt1q6FoCmEugKgYS', 0, '2021-10-10 06:19:31'),
(4, 'Lillian', 'Wynne Andrews', 'Diaz', 'sugyq@mailinator.com', 'fomytabeve', '$2y$10$wCOTee.JPJyJTJTsOA8ADON6Ghd8vd39Xfjq0pKmApU9c4IxstDxG', 0, '2021-10-11 01:38:12');

-- --------------------------------------------------------

--
-- Table structure for table `franchise_branch`
--

CREATE TABLE `franchise_branch` (
  `id` int(11) NOT NULL,
  `branch_code` varchar(100) NOT NULL,
  `branch_name` varchar(100) NOT NULL,
  `branch_location` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchise_branch`
--

INSERT INTO `franchise_branch` (`id`, `branch_code`, `branch_name`, `branch_location`, `is_deleted`, `date_time`) VALUES
(1, 'B-000', 'Darrel Brock', 'Blanditiis qui magna', 0, '2021-10-12 07:38:05'),
(2, 'B-001', 'Elvis Clay', 'Quis vitae est est a', 0, '2021-10-12 07:38:12'),
(3, 'B-002', 'Alika Holt', 'Architecto pariatur', 0, '2021-10-13 01:45:18');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `account_id` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `module` text NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `account_id`, `description`, `ip`, `hostname`, `timestamp`, `module`, `is_deleted`) VALUES
(275, '3', 'Add access level: test', '::1', 'VivoBook-S14', '2018-10-09 03:38:44', '', 0),
(276, '3', 'Delete access level id 5', '::1', 'VivoBook-S14', '2018-10-09 03:39:13', '', 0),
(277, '3', 'Add access level: test', '::1', 'VivoBook-S14', '2018-10-09 03:39:16', '', 0),
(278, '3', 'Add access level: test', '::1', 'VivoBook-S14', '2018-10-09 03:39:50', '', 0),
(365, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-05 23:47:52', '', 0),
(366, '3', 'Add client: deborah', '::1', 'DESKTOP-FO8SD5D', '2021-10-05 23:50:52', '', 0),
(367, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 00:45:16', '', 0),
(368, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 00:46:53', '', 0),
(369, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 00:47:14', '', 0),
(370, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 00:47:59', '', 0),
(371, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:08:22', '', 0),
(372, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:25:40', '', 0),
(373, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:29:08', '', 0),
(374, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:29:32', '', 0),
(375, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:30:43', '', 0),
(376, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:31:24', '', 0),
(377, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:49:20', '', 0),
(378, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:53:37', '', 0),
(379, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:54:02', '', 0),
(380, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:54:27', '', 0),
(381, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:54:39', '', 0),
(382, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-06 01:56:10', '', 0),
(383, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-07 23:38:49', '', 0),
(384, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-08 06:38:59', '', 0),
(385, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-08 06:51:24', '', 0),
(386, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-08 06:51:39', '', 0),
(387, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-08 07:03:22', '', 0),
(388, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-08 08:05:11', '', 0),
(389, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-08 08:27:13', '', 0),
(390, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-08 08:27:24', '', 0),
(391, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:03:11', '', 0),
(392, '3', 'Add account: xantha underwood', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:04:52', '', 0),
(393, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:11:18', '', 0),
(394, '3', 'Edit access level id 3', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:17:35', '', 0),
(395, '3', 'Add account: ori serrano', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:20:25', '', 0),
(396, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:23:02', '', 0),
(397, '3', 'Add account: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:38:46', '', 0),
(398, '3', 'Add account: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:48:15', '', 0),
(399, '3', 'Add account: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:48:18', '', 0),
(400, '3', 'Add account: allen kramer', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:48:51', '', 0),
(401, '3', 'Add account: inez roman', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:49:35', '', 0),
(402, '3', 'Add account: sydney underwood', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:49:52', '', 0),
(403, '3', 'Edit account id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:52:23', '', 0),
(404, '3', 'Edit account id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:52:31', '', 0),
(405, '3', 'Edit account id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:52:53', '', 0),
(406, '3', 'Edit account id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:53:04', '', 0),
(407, '3', 'Edit account id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:54:49', '', 0),
(408, '3', 'Delete account id 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:56:58', '', 0),
(409, '3', 'Delete account id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 01:59:04', '', 0),
(410, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 02:35:09', '', 0),
(411, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 02:35:54', '', 0),
(412, '3', 'Add product: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:27:13', '', 0),
(413, '3', 'Add product: sample item', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:27:51', '', 0),
(414, '3', 'Add product: 50% sugar level', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:39:29', '', 0),
(415, '3', 'Add product: sdfsdf', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:40:25', '', 0),
(416, '3', 'Add product: sdffsdf', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:40:41', '', 0),
(417, '3', 'Edit product id 2', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:41:58', '', 0),
(418, '3', 'Edit product id 2', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:42:03', '', 0),
(419, '3', 'Edit product id 2', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:45:26', '', 0),
(420, '3', 'Edit product id 2', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:46:05', '', 0),
(421, '3', 'Edit product id 3', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:46:12', '', 0),
(422, '3', 'Edit product id 3', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:46:18', '', 0),
(423, '3', 'Edit product id 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:46:56', '', 0),
(424, '3', 'Edit product id 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:47:23', '', 0),
(425, '3', 'Edit product id 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:47:28', '', 0),
(426, '3', 'Edit product id 2', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:47:38', '', 0),
(427, '3', 'Edit product id 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:48:16', '', 0),
(428, '3', 'Edit product id 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 03:48:25', '', 0),
(429, '3', 'Add product: nemo voluptas volupt', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 04:38:31', '', 0),
(430, '3', 'Add product: labore beatae ducimu', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 04:38:38', '', 0),
(431, '3', 'Add product: veniam irure aliqua', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 04:38:43', '', 0),
(432, '3', 'Add product: officia ab voluptate', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 04:38:50', '', 0),
(433, '3', 'Add product: at nihil qui sint po', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 04:38:53', '', 0),
(434, '3', 'Add product: fugit et et non et ', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 04:38:57', '', 0),
(435, '3', 'Add product: fugiat eiusmod conse', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 04:39:12', '', 0),
(436, '3', 'Delete product id 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:22:18', '', 0),
(437, '3', 'Delete product id 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:22:28', '', 0),
(438, '3', 'Delete product id 10', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:22:49', '', 0),
(439, '3', 'Edit product id 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:25:07', '', 0),
(440, '3', 'Delete product id 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:25:12', '', 0),
(441, '3', 'Delete product id 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:26:15', '', 0),
(442, '3', 'Add product: culpa neque dolores', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:30:18', '', 0),
(443, '3', 'Add product: hgjghj', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:30:24', '', 0),
(444, '3', 'Delete product id 13', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:30:28', '', 0),
(445, '3', 'Delete product id 9', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:30:33', '', 0),
(446, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:34:12', '', 0),
(447, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 05:35:55', '', 0),
(448, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-09 06:16:55', '', 0),
(449, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:14:23', '', 0),
(450, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:18:54', '', 0),
(451, '3', 'Add account: dominique', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:19:22', '', 0),
(452, '3', 'Add account: blake', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:19:32', '', 0),
(453, '3', 'Add account: madeson', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:23:11', '', 0),
(454, '3', 'Add account: lani', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:23:16', '', 0),
(455, '3', 'Add account: cooper', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:23:25', '', 0),
(456, '3', 'Add account: daryl', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:25:10', '', 0),
(457, '3', 'Add account: margaret', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:26:39', '', 0),
(458, '3', 'Add account: eric', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:26:52', '', 0),
(459, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:33:31', '', 0),
(460, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:47:02', '', 0),
(461, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:47:46', '', 0),
(462, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:48:02', '', 0),
(463, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:48:17', '', 0),
(464, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 06:55:03', '', 0),
(465, '3', 'Add supplier name: kane garrison', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 07:07:33', '', 0),
(466, '3', 'Add supplier name: quintessa coffey', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 07:07:45', '', 0),
(467, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 07:55:31', '', 0),
(468, '1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:00:59', '', 0),
(469, '1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:01:39', '', 0),
(470, '1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:01:49', '', 0),
(471, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:08:48', '', 0),
(472, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:12:03', '', 0),
(473, '1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:18:27', '', 0),
(474, '1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:18:55', '', 0),
(475, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:20:01', '', 0),
(476, '3', 'Logged in', '192.168.1.240', 'Carlitos-Galaxy-Note9.lan', '2021-10-10 08:23:51', '', 0),
(477, '3', 'Logged in', '192.168.1.240', 'Carlitos-Galaxy-Note9.lan', '2021-10-10 08:24:58', '', 0),
(478, '3', 'Logged in', '192.168.1.240', 'Carlitos-Galaxy-Note9.lan', '2021-10-10 08:26:22', '', 0),
(479, '1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 08:29:38', '', 0),
(480, '1', 'Logged in', '192.168.1.240', 'Carlitos-Galaxy-Note9.lan', '2021-10-10 08:30:18', '', 0),
(481, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 23:36:52', '', 0),
(482, '3', 'Add account: hayden', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 23:39:16', '', 0),
(483, '3', 'Add account: victoria', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 23:39:20', '', 0),
(484, '1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-10 23:59:10', '', 0),
(485, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 00:03:38', '', 0),
(486, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 00:49:16', '', 0),
(487, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 01:32:39', 'ACCOUNTING', 0),
(488, '3', 'Add account: rosalyn', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 01:36:48', 'ACCOUNTING', 0),
(489, '1', 'Add account: cherokee', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 01:37:37', 'ACCOUNTING', 0),
(490, '1', 'Add account: lillian', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 01:38:12', 'ACCOUNTING', 0),
(491, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 01:40:50', 'ACCOUNTING', 0),
(492, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 01:43:13', 'ACCOUNTING', 0),
(493, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 02:43:08', 'ACCOUNTING', 0),
(494, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 05:04:29', 'ACCOUNTING', 0),
(495, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 05:53:17', 'ACCOUNTING', 0),
(496, 'juhshd dfd dssd', 'Edit product id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 06:56:34', 'ACCOUNTING', 0),
(497, 'juhshd dfd dssd', 'Edit product id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 06:57:56', 'ACCOUNTING', 0),
(498, 'juhshd dfd dssd', 'Edit product id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 06:58:08', 'ACCOUNTING', 0),
(499, 'juhshd dfd dssd', 'Edit product id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:02:55', 'ACCOUNTING', 0),
(500, 'juhshd dfd dssd', 'Edit product id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:03:39', 'ACCOUNTING', 0),
(501, 'juhshd dfd dssd', 'Edit product id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:04:32', 'ACCOUNTING', 0),
(502, 'juhshd dfd dssd', 'Edit product id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:04:43', 'ACCOUNTING', 0),
(503, 'juhshd dfd dssd', 'Edit product id 8', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:05:39', 'ACCOUNTING', 0),
(504, 'juhshd dfd dssd', 'Edit product id 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:05:43', 'ACCOUNTING', 0),
(505, 'juhshd dfd dssd', 'Edit product id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:05:46', 'ACCOUNTING', 0),
(506, 'juhshd dfd dssd', 'Edit product id 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:05:50', 'ACCOUNTING', 0),
(507, 'juhshd dfd dssd', 'Edit product id 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:06:00', 'ACCOUNTING', 0),
(508, 'juhshd dfd dssd', 'Add product: qui voluptas ad modi', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:06:09', 'ACCOUNTING', 0),
(509, 'juhshd dfd dssd', 'Add product: accusantium omnis ev', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:06:15', 'ACCOUNTING', 0),
(510, 'juhshd dfd dssd', 'Add product: autem sit laudantiu', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:06:24', 'ACCOUNTING', 0),
(511, 'juhshd dfd dssd', 'Edit product id 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:13:52', 'ACCOUNTING', 0),
(512, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:44:42', 'ACCOUNTING', 0),
(513, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 08:46:01', 'ACCOUNTING', 0),
(514, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 23:28:59', 'ACCOUNTING', 0),
(515, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-11 23:38:28', 'ACCOUNTING', 0),
(516, 'juhshd dfd dssd', 'Add product: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 00:48:09', 'ACCOUNTING', 0),
(517, 'juhshd dfd dssd', 'Add added attendance to employee id: 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 00:49:36', 'ACCOUNTING', 0),
(518, 'juhshd dfd dssd', 'Add added attendance to employee id: 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 00:58:44', 'ACCOUNTING', 0),
(519, 'juhshd dfd dssd', 'Edit updated attendance to employee id 3', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:00:18', 'ACCOUNTING', 0),
(520, 'juhshd dfd dssd', 'Delete updated attendance to employee id 3', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:00:26', 'ACCOUNTING', 0),
(521, 'juhshd dfd dssd', 'Add added attendance to employee id: 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:00:40', 'ACCOUNTING', 0),
(522, 'juhshd dfd dssd', 'Add added attendance to employee id: 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:02:32', 'ACCOUNTING', 0),
(523, 'juhshd dfd dssd', 'Add added attendance to employee id: 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:03:12', 'ACCOUNTING', 0),
(524, 'juhshd dfd dssd', 'Edit updated attendance to employee id 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:03:51', 'ACCOUNTING', 0),
(525, 'juhshd dfd dssd', 'Add added attendance to employee id: 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:05:04', 'ACCOUNTING', 0),
(526, 'juhshd dfd dssd', 'Add added attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:07:39', 'ACCOUNTING', 0),
(527, 'juhshd dfd dssd', 'Add added attendance to employee id: 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:07:43', 'ACCOUNTING', 0),
(528, 'juhshd dfd dssd', 'Add added attendance to employee id: 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:07:47', 'ACCOUNTING', 0),
(529, 'juhshd dfd dssd', 'Add added attendance to employee id: 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:07:55', 'ACCOUNTING', 0),
(530, 'juhshd dfd dssd', 'Add added attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:08:50', 'ACCOUNTING', 0),
(531, 'juhshd dfd dssd', 'Add added attendance to employee id: 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:08:55', 'ACCOUNTING', 0),
(532, 'juhshd dfd dssd', 'Add added attendance to employee id: 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:12:00', 'ACCOUNTING', 0),
(533, 'juhshd dfd dssd', 'Add added attendance to employee id: 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:12:33', 'ACCOUNTING', 0),
(534, 'juhshd dfd dssd', 'Add added attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:13:34', 'ACCOUNTING', 0),
(535, 'juhshd dfd dssd', 'Add added attendance to employee id: 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 01:13:55', 'ACCOUNTING', 0),
(536, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:32:09', 'ACCOUNTING', 0),
(537, 'juhshd dfd dssd', 'Add attendance to employee id: 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:36:20', 'ACCOUNTING', 0),
(538, 'juhshd dfd dssd', 'Add attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:37:01', 'ACCOUNTING', 0),
(539, 'juhshd dfd dssd', 'Add attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:37:15', 'ACCOUNTING', 0),
(540, 'juhshd dfd dssd', 'Add attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:39:21', 'ACCOUNTING', 0),
(541, 'juhshd dfd dssd', 'Add attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:39:39', 'ACCOUNTING', 0),
(542, 'juhshd dfd dssd', 'Add attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:39:50', 'ACCOUNTING', 0),
(543, 'juhshd dfd dssd', 'Add attendance to employee id: 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:40:02', 'ACCOUNTING', 0),
(544, 'juhshd dfd dssd', 'Add account: caleb', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:49:27', 'ACCOUNTING', 0),
(545, 'juhshd dfd dssd', 'Add account: kirestin', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:49:33', 'ACCOUNTING', 0),
(546, 'juhshd dfd dssd', 'Add account: madonna', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:49:38', 'ACCOUNTING', 0),
(547, 'juhshd dfd dssd', 'Add account: yuri', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 03:49:42', 'ACCOUNTING', 0),
(548, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 04:12:56', 'ACCOUNTING', 0),
(549, 'juhshd dfd dssd', 'Add attendance to employee id: 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 04:18:23', 'ACCOUNTING', 0),
(550, 'juhshd dfd dssd', 'Add attendance to employee id: 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 04:26:26', 'ACCOUNTING', 0),
(551, 'juhshd dfd dssd', 'Add attendance to employee id: 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 04:26:34', 'ACCOUNTING', 0),
(552, 'juhshd dfd dssd', 'Logged in', '192.168.1.240', 'Carlitos-Galaxy-Note9.lan', '2021-10-12 04:35:35', 'ACCOUNTING', 0),
(553, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 04:41:09', 'ACCOUNTING', 0),
(554, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:03:39', 'ACCOUNTING', 0),
(555, 'juhshd dfd dssd', 'Add account: lester', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:06:29', 'ACCOUNTING', 0),
(556, 'juhshd dfd dssd', 'Add account: rosalyn', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:06:37', 'ACCOUNTING', 0),
(557, 'juhshd dfd dssd', 'Add account: anastasia', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:06:42', 'ACCOUNTING', 0),
(558, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:08:40', 'ACCOUNTING', 0),
(559, 'juhshd dfd dssd', 'Logged in', '192.168.1.240', 'Carlitos-Galaxy-Note9.lan', '2021-10-12 05:10:54', 'ACCOUNTING', 0),
(560, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:16:06', 'ACCOUNTING', 0),
(561, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:20:53', 'ACCOUNTING', 0),
(562, 'juhshd dfd dssd', 'Edit account id 13', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:49:54', 'ACCOUNTING', 0),
(563, 'juhshd dfd dssd', 'Edit account id 12', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:49:57', 'ACCOUNTING', 0),
(564, 'juhshd dfd dssd', 'Edit account id 10', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:01', 'ACCOUNTING', 0),
(565, 'juhshd dfd dssd', 'Edit account id 11', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:05', 'ACCOUNTING', 0),
(566, 'juhshd dfd dssd', 'Edit account id 8', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:10', 'ACCOUNTING', 0),
(567, 'juhshd dfd dssd', 'Edit account id 9', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:14', 'ACCOUNTING', 0),
(568, 'juhshd dfd dssd', 'Edit account id 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:18', 'ACCOUNTING', 0),
(569, 'juhshd dfd dssd', 'Edit account id 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:22', 'ACCOUNTING', 0),
(570, 'juhshd dfd dssd', 'Edit account id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:26', 'ACCOUNTING', 0),
(571, 'juhshd dfd dssd', 'Edit account id 4', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:29', 'ACCOUNTING', 0),
(572, 'juhshd dfd dssd', 'Edit account id 1', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 05:50:34', 'ACCOUNTING', 0),
(573, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 07:37:50', 'ACCOUNTING', 0),
(574, 'juhshd dfd dssd', 'Add account: darrel brock', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 07:38:05', 'ACCOUNTING', 0),
(575, 'juhshd dfd dssd', 'Add account: elvis clay', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 07:38:12', 'ACCOUNTING', 0),
(576, 'juhshd dfd dssd', 'Add account: maggy bernard', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 07:38:26', 'ACCOUNTING', 0),
(577, 'juhshd dfd dssd', 'Add account: anthony farmer', '::1', 'DESKTOP-FO8SD5D', '2021-10-12 07:43:27', 'ACCOUNTING', 0),
(578, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 01:44:42', 'ACCOUNTING', 0),
(579, 'juhshd dfd dssd', 'Add account: alika holt', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 01:45:18', 'ACCOUNTING', 0),
(580, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 01:57:21', 'ACCOUNTING', 0),
(581, '1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 05:32:17', '', 0),
(582, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 05:32:56', 'ACCOUNTING', 0),
(583, 'juhshd dfd dssd', 'Add billing payment: pending', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 06:53:57', 'ACCOUNTING', 0),
(584, 'juhshd dfd dssd', 'Add billing payment: pending', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 06:54:23', 'ACCOUNTING', 0),
(585, 'juhshd dfd dssd', 'Add billing payment: pending', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 06:54:38', 'ACCOUNTING', 0),
(586, 'juhshd dfd dssd', 'Add billing payment: approved', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 06:54:51', 'ACCOUNTING', 0),
(587, 'juhshd dfd dssd', 'Add billing payment: approved', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 06:55:11', 'ACCOUNTING', 0),
(588, 'juhshd dfd dssd', 'Add billing payment: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 06:59:46', 'ACCOUNTING', 0),
(589, 'juhshd dfd dssd', 'Add billing payment: security deposits', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:05:56', 'ACCOUNTING', 0),
(590, 'juhshd dfd dssd', 'Add billing payment: security deposits', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:06:21', 'ACCOUNTING', 0),
(591, 'juhshd dfd dssd', 'Add billing payment: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:08:19', 'ACCOUNTING', 0),
(592, 'juhshd dfd dssd', 'Add billing payment: ', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:08:20', 'ACCOUNTING', 0),
(593, 'juhshd dfd dssd', 'Add billing payment: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:08:44', 'ACCOUNTING', 0),
(594, 'juhshd dfd dssd', 'Add billing payment: other franchising inclusion', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:09:38', 'ACCOUNTING', 0),
(595, 'juhshd dfd dssd', 'Add billing payment: security deposits', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:10:16', 'ACCOUNTING', 0),
(596, 'juhshd dfd dssd', 'Add billing payment: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:11:17', 'ACCOUNTING', 0),
(597, 'juhshd dfd dssd', 'Add billing payment: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:13:10', 'ACCOUNTING', 0),
(598, 'juhshd dfd dssd', 'Add billing payment: security deposits', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:13:53', 'ACCOUNTING', 0),
(599, 'juhshd dfd dssd', 'Edit billing payment id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:26:25', 'ACCOUNTING', 0),
(600, 'juhshd dfd dssd', 'Edit billing payment id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:26:45', 'ACCOUNTING', 0),
(601, 'juhshd dfd dssd', 'Edit billing payment id 8', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:47:05', 'ACCOUNTING', 0),
(602, 'juhshd dfd dssd', 'Edit billing payment id 7', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:47:42', 'ACCOUNTING', 0),
(603, 'juhshd dfd dssd', 'Edit billing payment id 6', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:48:00', 'ACCOUNTING', 0),
(604, 'juhshd dfd dssd', 'Edit billing payment id 5', '::1', 'DESKTOP-FO8SD5D', '2021-10-13 07:48:12', 'ACCOUNTING', 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `image_file` text NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `cost` decimal(10,0) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `category` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `image_file`, `product_code`, `description`, `cost`, `price`, `category`, `is_deleted`, `date_time`) VALUES
(1, 'uploads/wp1808930.jpg', '023423', 'sample itemlllll', '0', '0', '', 0, '2021-10-09 03:47:28'),
(2, 'uploads/wp1808930.jpg', 'rtyrtkjkj', '50% Sugar Levelfdgdfg', '0', '0', '', 0, '2021-10-09 03:47:38'),
(3, 'uploads/wp1808930.jpg', 'sdfsdf', 'sdfsdfffff', '0', '0', '', 0, '2021-10-09 03:46:12'),
(4, 'uploads/532d0341186be12412c6a4362cc08710.jpg', 'dfgdfg', 'sdffsdf', '0', '0', 'NON FOOD', 0, '2021-10-11 08:13:52'),
(5, 'uploads/532d0341186be12412c6a4362cc08710.jpg', 'Ea ut sed in corrupt', 'Nemo voluptas volupt', '0', '0', 'NON FOOD', 0, '2021-10-13 07:10:16'),
(6, 'uploads/532d0341186be12412c6a4362cc08710.jpg', 'Vitae fuga Est adip', 'Labore beatae ducimu', '0', '0', 'OTHERS', 0, '2021-10-13 07:11:17'),
(7, 'uploads/532d0341186be12412c6a4362cc08710.jpg', 'Do sit ullam sit opt', 'Veniam irure aliqua', '60', '100', 'OTHERS', 0, '2021-10-13 07:13:10'),
(8, '', 'Magna ut exercitatio', 'Officia ab voluptate', '0', '0', 'NON FOOD', 0, '2021-10-11 08:05:39'),
(9, '', 'Aliqua Sapiente min', 'At nihil qui sint po', '0', '0', '', 1, '2021-10-09 05:30:33'),
(10, '', 'Eligendi sit ration', 'Fugit et et non et ', '0', '0', '', 1, '2021-10-09 05:22:49'),
(11, '', 'Ad natus nostrum ad ', 'Fugiat eiusmod conseggggggggg', '0', '0', '', 1, '2021-10-09 05:25:07'),
(12, 'uploads/532d0341186be12412c6a4362cc08710.jpg', 'Voluptas consequuntu', 'Culpa neque dolores', '500', '1000', 'FOOD', 0, '2021-10-11 08:04:43'),
(13, '', 'u67867', 'hgjghj', '0', '0', '', 1, '2021-10-09 05:30:28'),
(14, '', 'Autem aperiam porro ', 'Qui voluptas ad modi', '32', '210', 'FOOD', 0, '2021-10-11 08:06:09'),
(15, '', 'Corporis laborum Qu', 'Accusantium omnis ev', '17', '494', 'NON FOOD', 0, '2021-10-11 08:06:15'),
(16, '', 'Molestias eaque a do', 'Autem sit laudantiu', '25', '610', 'FOOD', 0, '2021-10-11 08:06:24');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_address` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `supplier_name`, `supplier_address`, `is_deleted`, `date_time`) VALUES
(1, 'Kane Garrison', 'Et laudantium volup', 0, '2021-10-10 07:07:33'),
(2, 'Quintessa Coffey', 'Voluptas similique e', 0, '2021-10-10 07:07:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_levels`
--
ALTER TABLE `access_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_payments`
--
ALTER TABLE `billing_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commissary_accounts`
--
ALTER TABLE `commissary_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchisee`
--
ALTER TABLE `franchisee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `franchisee_ibfk_1` (`branch_id`);

--
-- Indexes for table `franchise_accounts`
--
ALTER TABLE `franchise_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchise_branch`
--
ALTER TABLE `franchise_branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_code` (`product_code`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_levels`
--
ALTER TABLE `access_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `billing_payments`
--
ALTER TABLE `billing_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `commissary_accounts`
--
ALTER TABLE `commissary_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `franchisee`
--
ALTER TABLE `franchisee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `franchise_accounts`
--
ALTER TABLE `franchise_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `franchise_branch`
--
ALTER TABLE `franchise_branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=605;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `franchisee`
--
ALTER TABLE `franchisee`
  ADD CONSTRAINT `franchisee_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `franchise_branch` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
