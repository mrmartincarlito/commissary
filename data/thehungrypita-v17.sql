-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2021 at 02:13 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thehungrypita`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_levels`
--

CREATE TABLE `access_levels` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `access_dashboard` int(11) NOT NULL DEFAULT 0,
  `access_access_levels` int(11) NOT NULL DEFAULT 0,
  `access_user_accounts` int(11) NOT NULL DEFAULT 0,
  `access_franchise_accounts` int(11) NOT NULL DEFAULT 0,
  `access_commissary_accounts` int(11) NOT NULL DEFAULT 0,
  `access_employee_entries` int(11) NOT NULL DEFAULT 0,
  `access_encode_attendance` int(11) NOT NULL DEFAULT 0,
  `access_payroll` int(11) NOT NULL DEFAULT 0,
  `access_product_information` int(11) NOT NULL DEFAULT 0,
  `access_purchase_order` int(11) NOT NULL DEFAULT 0,
  `access_franchise_branch` int(11) NOT NULL DEFAULT 0,
  `access_franchise_entries` int(11) NOT NULL DEFAULT 0,
  `access_billing_franchising` int(11) NOT NULL DEFAULT 0,
  `access_billing_payments` int(11) NOT NULL DEFAULT 0,
  `access_order_payments` int(11) NOT NULL DEFAULT 0,
  `access_expenses` int(11) NOT NULL DEFAULT 0,
  `access_reports` int(11) NOT NULL DEFAULT 0,
  `access_logs` int(11) NOT NULL DEFAULT 0,
  `access_approval_power` int(11) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_levels`
--

INSERT INTO `access_levels` (`id`, `description`, `access_dashboard`, `access_access_levels`, `access_user_accounts`, `access_franchise_accounts`, `access_commissary_accounts`, `access_employee_entries`, `access_encode_attendance`, `access_payroll`, `access_product_information`, `access_purchase_order`, `access_franchise_branch`, `access_franchise_entries`, `access_billing_franchising`, `access_billing_payments`, `access_order_payments`, `access_expenses`, `access_reports`, `access_logs`, `access_approval_power`, `is_deleted`) VALUES
(3, 'Administrator', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0),
(4, 'User', 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(45, 'Customer', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `is_deleted` int(1) DEFAULT 0,
  `date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `role`, `image_file`, `is_deleted`, `date`) VALUES
(1, 'juhshd', 'dfd', 'dssd', 'utet@gmail.com', 'admin', '$2y$10$7e9rSp7gC.GslurbeSlIL.6NnSdN6/ejq0u/ueIn.00Kc7bMnxWBa', '3', 'uploads/532d0341186be12412c6a4362cc08710.jpg', 0, '2021-10-20 23:35:00'),
(5, 'Nashh', 'Risa David', 'Dominguez', 'licyvanox@mailinator.com', 'xicof', '$2y$10$.Nx2JvDvduut6Zk0fl/EOefUhm9K.CBxQ7eLNqYleOQ0O0LndbSVa', '3', '', 1, '2021-10-09 07:52:25'),
(7, 'Hayden', 'Leah York', 'Neal', 'hydoqu@mailinator.com', 'tutigely', '$2y$10$pGvAKzUbcHJE6vDUap.4Fe5V0PIre/thkcFvPu0xRQm5t4Hov3uzK', '3', '', 0, '2021-10-10 23:39:16'),
(8, 'Victoria', 'Stone Hunt', 'Moon', 'pehahemir@mailinator.com', 'niqezex', '$2y$10$r5xuDpP89LTsGV3yaf17Ie7mYQuaRun7mFYKreFsTaGC58xzveKXe', '3', '', 0, '2021-10-10 23:39:20'),
(9, 'Rosalyn', 'Fredericka Mathews', 'Fulton', 'hitetas@mailinator.com', 'kijog', '$2y$10$GcLRA3RdOrGNjx6.I5CycOvD9/YojXLDVbNUkX5n7lDTBQdsGKCGC', '3', '', 0, '2021-10-11 01:36:48'),
(10, 'Cherokee', 'Trevor Maxwell', 'Black', 'goxylyn@mailinator.com', 'cytesohere', '$2y$10$NhO5UnQRSVmWFrTHRlkVT.tZj7GoQjYt772QFZ1UGfY4ApHMcS52C', '3', '', 0, '2021-10-11 01:37:37'),
(11, 'Sade', 'Timon Barlow', 'Workman', 'nywylogy@mailinator.com', 'user', '$2y$10$W6vlgjt/EZIfMUbmEIyiOusvCj6bTXuO.bypMt6KoUlOo2q.8vvmO', '4', '', 0, '2021-10-19 08:38:00');

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `total_minutes` decimal(10,2) NOT NULL,
  `regular_legal_ot` decimal(10,2) NOT NULL,
  `special_ot` decimal(10,2) NOT NULL,
  `special_holiday` decimal(10,2) NOT NULL,
  `legal_holiday` decimal(10,2) NOT NULL,
  `total_hours` decimal(11,2) NOT NULL,
  `legend` varchar(50) NOT NULL,
  `work_date` date NOT NULL,
  `added_by` text NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `emp_id`, `total_minutes`, `regular_legal_ot`, `special_ot`, `special_holiday`, `legal_holiday`, `total_hours`, `legend`, `work_date`, `added_by`, `is_deleted`, `date_time`) VALUES
(1, 5, '24.00', '32.00', '2.00', '0.00', '0.00', '1.00', 'PRESENT', '2021-08-07', 'admin', 0, '0000-00-00 00:00:00'),
(2, 1, '86.00', '82.00', '56.00', '0.00', '0.00', '3.00', 'PRESENT', '1972-01-06', 'admin', 0, '0000-00-00 00:00:00'),
(3, 1, '480.00', '227.00', '0.00', '0.00', '0.00', '11.00', 'PRESENT', '2021-09-16', 'admin', 1, '0000-00-00 00:00:00'),
(4, 1, '480.00', '227.00', '0.00', '0.00', '0.00', '11.00', 'PRESENT', '2021-10-13', 'admin', 0, '0000-00-00 00:00:00'),
(5, 1, '480.00', '63.00', '0.00', '0.00', '0.00', '9.30', 'PRESENT', '2021-10-12', 'admin', 0, '0000-00-00 00:00:00'),
(6, 5, '480.00', '227.00', '0.00', '0.00', '0.00', '11.47', 'PRESENT', '2021-10-14', 'admin', 0, '0000-00-00 00:00:00'),
(7, 1, '480.00', '0.00', '0.00', '0.00', '0.00', '8.00', 'PRESENT', '2021-10-12', 'admin', 0, '0000-00-00 00:00:00'),
(8, 4, '62.00', '24.00', '79.00', '0.00', '0.00', '2.55', 'DAY-OFF', '1986-07-31', 'admin', 0, '0000-00-00 00:00:00'),
(9, 1, '26.00', '92.00', '86.00', '0.00', '0.00', '3.42', 'DAY-OFF', '1983-08-24', 'admin', 0, '0000-00-00 00:00:00'),
(10, 5, '98.00', '13.00', '12.00', '0.00', '0.00', '2.27', 'DAY-OFF', '2013-05-01', 'admin', 0, '0000-00-00 00:00:00'),
(11, 5, '54.00', '4.00', '20.00', '0.00', '0.00', '1.37', 'DAY-OFF', '2003-07-21', 'admin', 0, '0000-00-00 00:00:00'),
(12, 4, '11.00', '87.00', '72.00', '0.00', '0.00', '3.90', 'DAY-OFF', '1993-04-29', 'admin', 0, '0000-00-00 00:00:00'),
(13, 1, '24.00', '68.00', '31.00', '0.00', '0.00', '2.19', 'PRESENT', '1994-08-02', 'admin', 0, '0000-00-00 00:00:00'),
(14, 1, '73.00', '99.00', '62.00', '0.00', '0.00', '4.14', 'PRESENT', '1996-04-17', 'admin', 0, '0000-00-00 00:00:00'),
(15, 6, '31.00', '35.00', '74.00', '0.00', '0.00', '2.23', 'DAY-OFF', '1972-10-20', 'admin', 0, '2021-10-12 01:12:33'),
(16, 4, '480.00', '60.00', '0.00', '0.00', '0.00', '9.00', 'DAY-OFF', '1980-09-21', 'admin', 0, '2021-10-12 01:13:34'),
(17, 5, '480.00', '133.00', '0.00', '0.00', '0.00', '10.13', 'PRESENT', '2012-11-01', 'admin', 0, '2021-10-12 01:13:55'),
(18, 6, '69.00', '39.00', '67.00', '0.00', '0.00', '3.10', 'DAY-OFF', '1978-12-21', 'admin', 0, '2021-10-12 03:36:20'),
(19, 4, '9.00', '55.00', '100.00', '0.00', '0.00', '2.48', 'PRESENT', '2020-09-14', 'admin', 0, '2021-10-12 03:37:01'),
(20, 4, '9.00', '55.00', '100.00', '0.00', '0.00', '2.48', 'PRESENT', '2020-09-15', 'admin', 0, '2021-10-12 03:37:15'),
(21, 4, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'PRESENT', '2021-10-14', 'admin', 0, '2021-10-12 03:39:21'),
(22, 4, '480.00', '0.00', '0.00', '0.00', '0.00', '8.00', 'PRESENT', '2021-10-15', 'admin', 0, '2021-10-15 01:04:18'),
(23, 4, '480.00', '230.00', '0.00', '0.00', '0.00', '11.50', 'PRESENT', '2021-10-16', 'admin', 0, '2021-10-12 03:39:50'),
(24, 4, '0.00', '0.00', '100.00', '0.00', '0.00', '1.40', 'PRESENT', '2021-10-17', 'admin', 0, '2021-10-12 03:40:02'),
(25, 7, '30.00', '34.00', '11.00', '0.00', '0.00', '1.16', 'DAY-OFF', '1972-09-21', 'admin', 0, '2021-10-12 04:18:23'),
(26, 7, '9.00', '38.00', '24.00', '0.00', '0.00', '1.28', 'PRESENT', '2012-08-26', 'admin', 0, '2021-10-12 04:26:26'),
(27, 7, '480.00', '0.00', '0.00', '0.00', '0.00', '8.00', 'PRESENT', '2012-08-27', 'admin', 0, '2021-10-13 23:47:01'),
(28, 10, '480.00', '0.00', '0.00', '0.00', '0.00', '8.00', 'PRESENT', '2021-10-14', 'admin', 0, '2021-10-13 23:47:36'),
(29, 5, '92.00', '98.00', '62.00', '0.00', '0.00', '4.35', 'PRESENT', '2014-04-07', 'admin', 0, '2021-10-13 23:53:15'),
(30, 7, '51.00', '100.00', '56.00', '0.00', '0.00', '3.55', 'DAY-OFF', '1993-07-06', 'admin', 0, '2021-10-13 23:53:19'),
(31, 5, '48.00', '42.00', '38.00', '0.00', '0.00', '2.31', 'DAY-OFF', '2018-08-25', 'admin', 0, '2021-10-13 23:53:22'),
(32, 12, '92.00', '81.00', '7.00', '0.00', '0.00', '3.10', 'PRESENT', '1973-07-25', 'admin', 0, '2021-10-13 23:53:25'),
(33, 10, '86.00', '77.00', '46.00', '0.00', '0.00', '3.31', 'DAY-OFF', '1980-02-11', 'admin', 0, '2021-10-13 23:53:30'),
(34, 9, '100.00', '100.00', '62.00', '0.00', '0.00', '4.45', 'DAY-OFF', '2007-01-28', 'admin', 0, '2021-10-13 23:53:33'),
(35, 7, '39.00', '6.00', '21.00', '0.00', '0.00', '1.27', 'DAY-OFF', '1985-10-27', 'admin', 0, '2021-10-13 23:53:36'),
(36, 13, '3.00', '66.00', '51.00', '480.00', '480.00', '10.00', 'DAY-OFF', '1994-02-25', 'admin', 0, '2021-10-15 07:06:26'),
(37, 1, '6240.00', '0.00', '0.00', '0.00', '0.00', '104.00', 'PRESENT', '2021-10-22', 'admin', 0, '2021-10-15 07:22:43');

-- --------------------------------------------------------

--
-- Table structure for table `billing_franchise`
--

CREATE TABLE `billing_franchise` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `total_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `or_number` text NOT NULL,
  `invoice_number` text DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'UNPAID',
  `remarks` text NOT NULL,
  `added_by` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `billing_franchise`
--

INSERT INTO `billing_franchise` (`id`, `franchise_id`, `description`, `total_amount`, `or_number`, `invoice_number`, `status`, `remarks`, `added_by`, `date_time`, `is_deleted`) VALUES
(1, 7, 'FRANCHISING FEE', '2720.00', '', '0', 'UNPAID', '', 'admin', '2021-11-07 00:52:08', 1),
(2, 11, 'SECURITY DEPOSITS', '81.00', '', '', 'UNPAID', 'Veritatis officia om', 'admin', '2021-11-07 00:52:04', 1),
(3, 7, 'FRANCHISING FEE', '500.00', '', '', 'UNPAID', '', 'admin', '2021-11-07 01:09:31', 1),
(4, 7, 'FRANCHISING FEE', '1500.00', '', '', 'UNPAID', '', 'admin', '2021-11-07 01:11:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `billing_payments`
--

CREATE TABLE `billing_payments` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) DEFAULT NULL,
  `bill_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `check_no` varchar(50) DEFAULT NULL,
  `check_details` varchar(50) DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `date_paid` date DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `status` varchar(50) DEFAULT 'PENDING',
  `or_number` text DEFAULT NULL,
  `invoice_number` text DEFAULT NULL,
  `approved_by` varchar(100) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `billing_payments`
--

INSERT INTO `billing_payments` (`id`, `franchise_id`, `description`, `total_amount`, `image_file`, `check_no`, `check_details`, `bank`, `date_paid`, `remarks`, `status`, `or_number`, `invoice_number`, `approved_by`, `approved_date`, `is_deleted`, `date_time`) VALUES
(1, 7, 'FRANCHISING FEE', '2720.00', '', 0, '', '', '2021-10-13', '', 'APPROVED', NULL, NULL, 'admin', '2021-10-30 16:33:55', 0, ''),
(2, 7, 'SECURITY DEPOSITS', '37.00', '', 0, '', '', '2020-07-19', 'Fugiat quisquam expe', 'APPROVED', NULL, NULL, 'admin', '2021-10-24 11:53:45', 0, ''),
(3, 7, 'FRANCHISING FEE', '2720.00', '', 0, '', '', '2021-10-14', '', 'APPROVED', NULL, NULL, 'admin', '2021-10-21 16:25:04', 0, ''),
(4, 7, 'OTHER FRANCHISING INCLUSION', '2720.00', '', 0, '', '', '2021-10-14', '', 'APPROVED', NULL, NULL, 'admin', '2021-10-14 15:58:41', 0, ''),
(5, 7, 'SECURITY DEPOSITS', '41.00', '', 0, '', '', '1974-02-28', 'Est velit occaecat ', 'APPROVED', NULL, NULL, 'admin', '2021-10-13 15:48:12', 0, ''),
(6, 7, 'FRANCHISING FEE', '2720.00', 'uploads/wp1808930.jpg', 0, '', '', '2021-10-13', 'inconsistent', 'REJECTED', NULL, NULL, '', NULL, 0, ''),
(7, 7, 'FRANCHISING FEE', '2720.00', '', 0, '', '', '2021-10-13', '', 'APPROVED', NULL, NULL, 'admin', '2021-10-13 15:47:42', 0, ''),
(8, 7, 'SECURITY DEPOSITS', '2720.00', 'uploads/532d0341186be12412c6a4362cc08710.jpg', 0, '', '', '2021-10-14', '', 'APPROVED', NULL, NULL, 'admin', '2021-10-13 00:00:00', 0, ''),
(9, 7, 'OTHER FRANCHISING INCLUSION', '7.00', '', 0, '', '', '2018-04-25', 'Nulla nihil mollitia', 'APPROVED', NULL, NULL, 'admin', '2021-10-14 07:51:51', 0, ''),
(10, 8, 'SYSTEM WIDE AD FUND', '1960.00', '', 0, '', '', '2021-10-15', '', 'APPROVED', NULL, NULL, 'admin', '2021-10-14 15:15:59', 0, ''),
(11, 7, 'SECURITY DEPOSITS', '5000.00', '', 11, 'Est exercitation eiu', 'Ex perspiciatis con', '2021-04-08', 'Labore architecto ve', 'APPROVED', NULL, NULL, 'admin', '2021-10-30 16:33:31', 0, ''),
(12, 7, 'SYSTEM WIDE AD FUND', '5000.00', '', 26, 'Rerum anim earum aut', 'Vel suscipit minim e', '1976-05-25', 'Vel recusandae Dolo', 'APPROVED', NULL, NULL, 'admin', '2021-10-30 16:32:31', 0, ''),
(13, 11, 'SECURITY DEPOSITS', '900.00', '', 33, 'Voluptatibus sunt op', 'Unde vitae quo recus', '1996-04-06', 'Duis et fugiat ut u', 'APPROVED', NULL, NULL, 'admin', '2021-10-30 16:35:42', 0, ''),
(14, 7, 'SECURITY DEPOSITS', '55000.00', '', 21, 'Atque quis commodi f', 'Ex dolore ratione fu', '2004-02-17', 'Sample ito\r\n', 'APPROVED', NULL, NULL, 'admin', '2021-11-05 13:01:54', 0, ''),
(15, 7, 'FRANCHISING FEE', '500.00', 'uploads/wp1808930.jpg', 0, '', '', '2021-11-19', '', 'APPROVED', NULL, NULL, 'admin', '2021-11-05 12:36:18', 0, NULL),
(16, 14, 'FRANCHISING FEE', '89.00', NULL, 98, 'Et placeat necessit', 'Aperiam molestias qu', '1979-02-04', '', 'APPROVED', NULL, NULL, 'admin', '2021-11-07 08:05:09', 0, NULL),
(17, 11, 'ROYALTY FEE', '10.00', NULL, 43, 'Debitis velit dolori', 'Molestiae ut id ad ', '1993-10-10', 'Maiores tempor rerum', 'APPROVED', NULL, NULL, 'admin', '2021-11-07 07:50:25', 0, NULL),
(18, 13, 'FRANCHISING FEE', '77.00', NULL, 69, 'Voluptate velit nisi', 'Eu laboriosam qui e', '1980-01-13', 'Ullamco sed Nam moll', 'APPROVED', '12121', '', 'admin', '2021-11-07 08:08:20', 0, NULL),
(19, 13, 'FRANCHISING FEE', '4.00', NULL, 32, 'Labore voluptatum et', 'Tempore eos sit es', '1994-09-30', 'Sunt elit iure qui ', 'APPROVED', '212121', '121212', 'admin', '2021-11-07 08:09:30', 0, NULL),
(20, 7, 'FRANCHISING FEE', '2333.00', NULL, 0, '', '', NULL, '', NULL, '', '', '', NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `description`, `is_deleted`, `date_time`) VALUES
(1, 'Cart 20', 0, '2021-10-23 21:31:07'),
(2, 'Cart 1', 0, '2021-10-23 21:53:48'),
(3, 'Cart 11', 0, '2021-10-23 22:22:17');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `type`, `category_name`, `is_deleted`, `date_time`) VALUES
(4, 'Non-Operational', 'Taxes', 0, '2021-10-24 00:02:08'),
(5, 'Non-Operational', 'Interest', 0, '2021-10-24 00:02:16'),
(6, 'Operational', 'Cost of Sales', 0, '2021-10-24 00:02:43'),
(7, 'Operational', 'Marketing, Advertising and Promotion', 0, '2021-10-24 00:03:14'),
(8, 'Operational', 'Travel Expenses', 0, '2021-10-24 00:03:25'),
(9, 'Operational', 'Administrative Expenses', 0, '2021-10-24 00:03:40'),
(10, 'Operational', 'Rent and Insurance', 0, '2021-10-24 00:04:00'),
(11, 'Operational', 'Depreciation and Amortization', 0, '2021-10-24 00:04:18'),
(12, 'Operational', 'Commission', 0, '2021-10-24 00:04:33'),
(13, 'Operational', 'In house expenses', 0, '2021-10-24 00:04:48');

-- --------------------------------------------------------

--
-- Table structure for table `commissary_accounts`
--

CREATE TABLE `commissary_accounts` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `is_deleted` int(1) DEFAULT 0,
  `date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `commissary_accounts`
--

INSERT INTO `commissary_accounts` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `is_deleted`, `date`) VALUES
(1, 'DSFDF', 'Thaddeus Oneal', 'Vasquez', 'admin@commissary.com', 'admin', '$2y$10$bOcGC6r9kK6ZNBKjgBhv7uI7k2W6TUfaBBBFIsG8xRrv7qVkmFoKW', 1, '2021-10-10 08:18:48'),
(2, 'Margaret', 'Sonia Chavez', 'Allison', 'kynyro@mailinator.com', 'bikidom', '$2y$10$pSG4dOBfJmkaOpu0DvdeGeH/ba4LvoQaqPU856MnqiCMEJOfCpoxS', 0, '2021-10-10 06:26:39'),
(3, 'Eric', 'Eve Cortez', 'Lara', 'gywewiti@mailinator.com', 'gogyqyrul', '$2y$10$49Le1R2WBJ5QWKVeED1HlumtZJKoQqLjR1AR1Fv0g8I60XAgO/jGy', 0, '2021-10-10 06:26:52');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `employment_date` date DEFAULT NULL,
  `regularization_date` date DEFAULT NULL,
  `employment_type` varchar(100) DEFAULT NULL,
  `rate` decimal(11,2) DEFAULT NULL,
  `date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `middle_name`, `last_name`, `city`, `province`, `employment_date`, `regularization_date`, `employment_type`, `rate`, `date`, `is_deleted`) VALUES
(1, 'Julcess', 'P', 'Mercado', 'Calumpitt', 'Bulacan', '2021-10-01', '2021-10-09', 'ADMIN - ACCOUNTING', '9000.00', '2021-10-12 05:50:34', 0),
(2, 'Jack', 'Oleg Gordon', 'Burgess', 'Eligendi dolores sun', 'Eu incididunt sequi ', '2020-06-22', '2011-05-02', '', '3.00', '2021-10-09 06:15:50', 1),
(3, 'Signe', 'Flynn Hendricks', 'Hardy', 'Officiis soluta duci', 'Earum aut vel delect', '2008-07-29', '2002-06-19', '', '18.00', '2021-10-09 07:57:39', 1),
(4, 'Madeson', 'Brett Rodriquez', 'Mccarty', 'Aut distinctio Quae', 'Officia esse sapient', '1971-11-27', '1984-10-29', 'SERVICE CREW', '57.00', '2021-10-12 05:50:29', 0),
(5, 'Lani', 'Lani Morrison', 'Cruz', 'Ut dolores ex delect', 'Voluptatem ipsam et', '1985-02-12', '1993-06-28', 'COMMISSARY OFFICER', '46.00', '2021-10-12 05:50:18', 0),
(6, 'Cooper', 'Genevieve Ruiz', 'Nguyen', 'Tempor repudiandae q', 'Sunt quisquam Nam su', '2013-04-06', '1971-02-14', 'SERVICE CREW', '48.00', '2021-10-12 05:50:26', 0),
(7, 'Caleb', 'Tatyana Haley', 'Harris', 'Eaque ad quo in mini', 'Cum expedita neque i', '1986-10-20', '1993-05-24', 'OFFICE STAFF', '100.00', '2021-10-12 05:50:22', 0),
(8, 'Kirestin', 'Stewart Charles', 'Flores', 'Sunt esse commodi al', 'Laboris est labore c', '1996-03-09', '1985-03-14', 'DIGITAL MARKETING', '23.00', '2021-10-12 05:50:10', 0),
(9, 'Madonna', 'Abra Mcclure', 'Silva', 'Nesciunt excepteur ', 'Veritatis ullam labo', '2003-12-17', '2007-10-12', 'ADMIN STAFF / TRAINOR', '39.00', '2021-10-12 05:50:14', 0),
(10, 'Yuri', 'Benedict Young', 'Buck', 'Autem alias consequa', 'Blanditiis qui sint ', '1975-12-12', '2006-11-14', 'ADMIN STAFF / TRAINOR', '70.00', '2021-10-12 05:50:01', 0),
(11, 'Lester', 'Nelle Reese', 'Montoya', 'Sit mollit ab in vol', 'Modi aut aut amet i', '1979-01-23', '2020-12-10', 'DIGITAL MARKETING', '79.00', '2021-10-12 05:50:05', 0),
(12, 'Rosalyn', 'Boris Sweeney', 'Green', 'Dolorem assumenda mo', 'Neque laborum elit ', '1979-05-08', '1996-01-14', 'COMMISSARY OFFICER', '2.00', '2021-10-12 05:49:57', 0),
(13, 'Anastasia', 'Delilah Booker', 'Valencia', 'Quia similique labor', 'Ut perferendis repre', '2006-01-24', '1971-08-16', 'OFFICE STAFF', '3.00', '2021-10-12 05:49:54', 0),
(14, 'Len', 'Hayes Odom', 'Rodriguez', 'Porro aut voluptas i', 'Mollit iusto quis ex', '2015-11-19', '2003-04-08', 'DIGITAL MARKETING', '88.00', '2021-10-31 02:12:46', 0);

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(11) NOT NULL,
  `date_paid` date DEFAULT NULL,
  `expense_type` varchar(50) DEFAULT NULL,
  `voucher_number` int(11) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `sub_category` varchar(50) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `added_by` varchar(50) DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `date_paid`, `expense_type`, `voucher_number`, `category`, `sub_category`, `total_amount`, `added_by`, `remarks`, `is_deleted`, `date_time`) VALUES
(1, '2021-12-01', 'Operational', 385, 'Commission', '', '20.00', 'admin', 'Omnis veniam quos f', 0, '2021-11-03 04:37:36'),
(2, '2021-08-18', 'Operational', 60, 'Administrative Expenses', 'Repair and Maintenance Works', '95.00', 'admin', 'Dolorem ullamco nihi', 0, '2021-11-03 04:37:45'),
(3, '2021-02-27', 'Non-Operational', 396, 'Interest', '', '89.00', 'admin', 'Autem et rerum et as', 0, '2021-11-03 04:37:49'),
(4, '2021-11-01', 'Non-Operational', 887799, 'Taxes', '', '5500.00', 'admin', '', 0, '2021-11-03 04:39:35');

-- --------------------------------------------------------

--
-- Table structure for table `franchisee`
--

CREATE TABLE `franchisee` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `mode_of_transaction` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `contactno` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `tin_number` text DEFAULT NULL,
  `package_type` varchar(50) DEFAULT NULL,
  `date_contract_signing` date DEFAULT NULL,
  `date_branch_opening` date DEFAULT NULL,
  `date_contract_expiry` date DEFAULT NULL,
  `billing_balance` decimal(10,2) DEFAULT 0.00,
  `order_balance` decimal(10,2) DEFAULT 0.00,
  `total_amount` decimal(10,2) DEFAULT 0.00,
  `last_payment_amount` decimal(10,2) DEFAULT NULL,
  `last_payment_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchisee`
--

INSERT INTO `franchisee` (`id`, `branch_id`, `mode_of_transaction`, `name`, `address`, `contactno`, `birthday`, `tin_number`, `package_type`, `date_contract_signing`, `date_branch_opening`, `date_contract_expiry`, `billing_balance`, `order_balance`, `total_amount`, `last_payment_amount`, `last_payment_date`, `is_deleted`, `date_time`) VALUES
(7, 1, 'Pick-up', 'Anthony Farmer', 'Quo dolorem sunt dol', '79', '1993-10-10', '', '3', '1974-08-04', '2020-03-08', '1975-10-22', '0.00', '0.00', '0.00', '2720.00', '2021-11-19 00:00:00', 0, '2021-11-07 01:11:46'),
(8, 2, 'Delivery', 'Rigel Arnold', 'Adipisicing aut veni', '62', '2005-10-03', '', '1', '2003-08-27', '1983-01-14', '1977-12-04', '0.00', '0.00', '0.00', '0.00', NULL, 0, '2021-11-07 00:56:37'),
(9, 3, 'Pick-up', 'Gemma Dean', 'Harum porro quod lab', '68', '1996-06-07', '', '2', '2000-07-23', '2017-02-18', '1980-06-02', '0.00', '0.00', '0.00', '49.00', '1987-01-11 00:00:00', 0, '2021-11-07 00:56:46'),
(10, 2, 'Pick-up', 'Eric Pena', 'Omnis laudantium re', '15', '1988-11-23', '', '1', '1989-03-21', '2013-10-23', '2021-12-31', '0.00', '0.00', '0.00', '0.00', NULL, 0, '2021-11-07 00:56:48'),
(11, 2, 'Delivery', 'Christine Zimmerman', 'Officia labore nobis', '66', '1978-05-02', '5345345', '1', '2015-02-23', '1980-08-23', '2022-01-21', '0.00', '0.00', '0.00', '10.00', '1993-10-10 00:00:00', 0, '2021-11-07 00:56:53'),
(12, 6, 'Pick-up', 'Serena Haynes', 'Dignissimos laboris ', '100', '2015-08-26', '686', '1', '2008-09-22', '2001-06-05', '1997-11-27', '0.00', '0.00', '0.00', '4000.00', '1982-02-23 00:00:00', 0, '2021-11-07 00:57:01');

-- --------------------------------------------------------

--
-- Table structure for table `franchise_accounts`
--

CREATE TABLE `franchise_accounts` (
  `id` int(11) NOT NULL,
  `franchisee_id` int(11) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `is_deleted` int(1) DEFAULT 0,
  `image_file` text DEFAULT '',
  `date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchise_accounts`
--

INSERT INTO `franchise_accounts` (`id`, `franchisee_id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `is_deleted`, `image_file`, `date`) VALUES
(1, 10, 'Christian', 'Deborah Vaughn', 'Reynolds', 'xytylida@mailinator.com', 'qugebudega', '$2y$10$MxYwqZU6Liowbw4eimq6aumosRbzZ024rFZrZlFCx2Mp9ojM3opDy', 0, '', '2021-11-04 23:56:02'),
(2, 11, 'Bruce', 'Rae Vincent', 'Hardin', 'nepahipas@mailinator.com', 'jozosemote', '$2y$10$FjCKQwaU0knnezzkHe6Dl.2QS18i7U3MRSBraQ/2LT3GcOh9q9umW', 0, NULL, '2021-10-18 03:34:13'),
(3, 7, 'Anthony', ' ', 'Farmer', 'anthonyfarmer@gmail.com', 'anthony', '$2y$10$jZ.EjBleQysFIXIrVzSSJ.d94hi0a8mAR4gMNsCjbw/jbydtjC4Xq', 0, 'uploads/eternals-k7-1920x1080.jpg', '2021-11-05 00:01:56');

-- --------------------------------------------------------

--
-- Table structure for table `franchise_branch`
--

CREATE TABLE `franchise_branch` (
  `id` int(11) NOT NULL,
  `branch_code` varchar(100) NOT NULL,
  `branch_name` varchar(100) NOT NULL,
  `branch_location` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchise_branch`
--

INSERT INTO `franchise_branch` (`id`, `branch_code`, `branch_name`, `branch_location`, `is_deleted`, `date_time`) VALUES
(1, 'B-000', 'Darrel Brock', 'Blanditiis qui magna', 0, '2021-10-12 07:38:05'),
(2, 'B-001', 'Elvis Clay', 'Quis vitae est est a', 0, '2021-10-12 07:38:12'),
(3, 'B-002', 'Alika Holt', 'Architecto pariatur', 0, '2021-10-13 01:45:18'),
(4, 'B-0003', 'test', 'test', 0, '2021-10-20 06:54:29'),
(5, 'B-0004', 'Jasmine Doyle', 'Nulla molestiae enim', 0, '2021-10-20 06:54:37'),
(6, 'B-0005', 'Molly Cook', 'Eligendi qui dolor m', 0, '2021-10-20 06:54:43'),
(7, 'B-0006', 'Magee Estes', 'Velit dolore ipsa e', 0, '2021-10-20 06:54:47'),
(8, 'B-0007', 'Judith Hart', 'Aliqua In nulla sin', 0, '2021-10-20 06:54:51'),
(9, 'B-0008', 'Candice Patel', 'Atque in maiores pra', 0, '2021-10-20 06:55:03'),
(10, 'B-0009', 'Zena Castillo', 'Qui in quod dolorum ', 0, '2021-10-20 06:55:08'),
(11, 'B-0010', 'Tate Cardenas', 'Esse ut dolor aut s', 0, '2021-10-20 06:55:17');

-- --------------------------------------------------------

--
-- Table structure for table `franchise_renewal_log`
--

CREATE TABLE `franchise_renewal_log` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `renewed_date` date DEFAULT NULL,
  `new_expiry_date` date DEFAULT NULL,
  `approved_by` text DEFAULT NULL,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `franchise_renewal_log`
--

INSERT INTO `franchise_renewal_log` (`id`, `franchise_id`, `expiry_date`, `renewed_date`, `new_expiry_date`, `approved_by`, `date_time`) VALUES
(8, 10, '2021-08-04', '2021-10-25', '2021-12-31', 'admin', '2021-10-24 06:00:36'),
(9, 11, '2021-11-06', '2021-11-10', '2022-01-21', 'admin', '2021-11-07 00:32:49');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `account_id` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `module` text NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `account_id`, `description`, `ip`, `hostname`, `timestamp`, `module`, `is_deleted`) VALUES
(831, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-02 00:21:02', 'ACCOUNTING', 0),
(832, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-02 02:57:45', 'ACCOUNTING', 0),
(833, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-03 04:24:08', 'ACCOUNTING', 0),
(834, 'juhshd dfd dssd', 'Add expense: non-operational', '::1', 'DESKTOP-FO8SD5D', '2021-11-03 04:39:36', 'ACCOUNTING', 0),
(835, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-03 05:24:02', 'ACCOUNTING', 0),
(836, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-03 23:22:11', 'ACCOUNTING', 0),
(837, 'juhshd dfd dssd', 'Add account: anthony', '::1', 'DESKTOP-FO8SD5D', '2021-11-04 23:35:45', 'ACCOUNTING', 0),
(838, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-04 23:35:59', '', 0),
(839, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-04 23:41:28', '', 0),
(840, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-04 23:44:54', '', 0),
(841, 'Anthony   Farmer', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 00:03:57', 'FRANCHISEE', 0),
(842, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 00:16:06', 'ACCOUNTING', 0),
(843, 'Anthony   Farmer', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 00:16:27', 'FRANCHISEE', 0),
(844, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 00:18:03', 'ACCOUNTING', 0),
(845, 'Anthony   Farmer', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 00:26:11', 'FRANCHISEE', 0),
(846, 'Anthony   Farmer', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 00:50:53', 'FRANCHISEE', 0),
(847, 'Anthony   Farmer', 'Edit billing payment id 14', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 00:59:34', 'FRANCHISEE', 0),
(848, 'Anthony   Farmer', 'Add billing payment: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 01:00:28', 'FRANCHISEE', 0),
(849, 'Anthony   Farmer', 'Add order payment: ', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 01:48:39', 'FRANCHISEE', 0),
(850, 'Anthony   Farmer', 'Add order payment: ', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 01:53:27', 'FRANCHISEE', 0),
(851, 'Anthony   Farmer', 'Add order payment: ', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 01:54:09', 'FRANCHISEE', 0),
(852, 'Anthony   Farmer', 'Add order payment: 7', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 01:54:23', 'FRANCHISEE', 0),
(853, 'Anthony   Farmer', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 02:56:56', 'FRANCHISEE', 0),
(854, 'Anthony   Farmer', 'Edit billing payment id 15', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 04:35:51', 'FRANCHISEE', 0),
(855, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 04:36:04', 'ACCOUNTING', 0),
(856, 'juhshd dfd dssd', 'Edit billing payment id 15', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 04:36:18', 'ACCOUNTING', 0),
(857, 'Anthony   Farmer', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 04:36:30', 'FRANCHISEE', 0),
(858, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 04:51:42', 'ACCOUNTING', 0),
(859, 'Anthony   Farmer', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 04:52:42', 'FRANCHISEE', 0),
(860, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 04:58:50', 'ACCOUNTING', 0),
(861, 'juhshd dfd dssd', 'Edit franchisee id 7', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 04:59:16', 'ACCOUNTING', 0),
(862, 'Anthony   Farmer', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 04:59:30', 'FRANCHISEE', 0),
(863, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 05:01:05', 'ACCOUNTING', 0),
(864, 'juhshd dfd dssd', 'Edit billing payment id 14', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 05:01:21', 'ACCOUNTING', 0),
(865, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 05:01:37', 'ACCOUNTING', 0),
(866, 'juhshd dfd dssd', 'Edit billing payment id 14', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 05:01:54', 'ACCOUNTING', 0),
(867, 'Anthony   Farmer', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-05 05:02:06', 'FRANCHISEE', 0),
(868, 'juhshd dfd dssd', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-06 23:39:11', 'ACCOUNTING', 0),
(869, 'juhshd dfd dssd', 'Add franchisee: lillian potter', '::1', 'DESKTOP-FO8SD5D', '2021-11-06 23:40:26', 'ACCOUNTING', 0),
(870, 'juhshd dfd dssd', 'Add franchisee: hedda mejia', '::1', 'DESKTOP-FO8SD5D', '2021-11-06 23:42:09', 'ACCOUNTING', 0),
(871, 'juhshd dfd dssd', 'Edit franchisee id 13', '::1', 'DESKTOP-FO8SD5D', '2021-11-06 23:42:16', 'ACCOUNTING', 0),
(872, 'juhshd dfd dssd', 'Add billing payment: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-11-06 23:45:04', 'ACCOUNTING', 0),
(873, 'juhshd dfd dssd', 'Add billing payment: royalty fee', '::1', 'DESKTOP-FO8SD5D', '2021-11-06 23:50:25', 'ACCOUNTING', 0),
(874, 'juhshd dfd dssd', 'Edit billing payment id 16', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 00:05:09', 'ACCOUNTING', 0),
(875, 'juhshd dfd dssd', 'Add billing payment: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 00:08:07', 'ACCOUNTING', 0),
(876, 'juhshd dfd dssd', 'Edit billing payment id 18', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 00:08:20', 'ACCOUNTING', 0),
(877, 'juhshd dfd dssd', 'Add billing payment: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 00:09:18', 'ACCOUNTING', 0),
(878, 'juhshd dfd dssd', 'Edit billing payment id 19', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 00:09:31', 'ACCOUNTING', 0),
(879, 'juhshd dfd dssd', 'Edit order payment id 24', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 00:24:06', 'ACCOUNTING', 0),
(880, 'juhshd dfd dssd', 'Delete order payment id 21', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 00:24:31', 'ACCOUNTING', 0),
(881, 'juhshd dfd dssd', 'Add billing payment: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 00:46:30', 'ACCOUNTING', 0),
(882, 'juhshd dfd dssd', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 00:47:44', 'ACCOUNTING', 0),
(883, 'juhshd dfd dssd', 'Add bill to franchise: security deposits', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 00:48:21', 'ACCOUNTING', 0),
(884, 'juhshd dfd dssd', 'Delete bill to franchise 2', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 00:52:04', 'ACCOUNTING', 0),
(885, 'juhshd dfd dssd', 'Delete bill to franchise 1', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 00:52:08', 'ACCOUNTING', 0),
(886, 'juhshd dfd dssd', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 01:07:20', 'ACCOUNTING', 0),
(887, 'juhshd dfd dssd', 'Edit bill to franchise 3', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 01:07:37', 'ACCOUNTING', 0),
(888, 'juhshd dfd dssd', 'Edit bill to franchise 3', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 01:08:16', 'ACCOUNTING', 0),
(889, 'juhshd dfd dssd', 'Delete bill to franchise 3', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 01:09:31', 'ACCOUNTING', 0),
(890, 'juhshd dfd dssd', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 01:11:05', 'ACCOUNTING', 0),
(891, 'juhshd dfd dssd', 'Edit bill to franchise 4', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 01:11:19', 'ACCOUNTING', 0),
(892, 'juhshd dfd dssd', 'Edit bill to franchise 4', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 01:11:33', 'ACCOUNTING', 0),
(893, 'juhshd dfd dssd', 'Delete bill to franchise 4', '::1', 'DESKTOP-FO8SD5D', '2021-11-07 01:11:46', 'ACCOUNTING', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_payments`
--

CREATE TABLE `order_payments` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `date_paid` date DEFAULT NULL,
  `check_no` varchar(50) DEFAULT NULL,
  `check_details` varchar(50) DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `status` varchar(50) DEFAULT 'PENDING',
  `or_number` text DEFAULT NULL,
  `invoice_number` text DEFAULT NULL,
  `approved_by` varchar(100) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_payments`
--

INSERT INTO `order_payments` (`id`, `franchise_id`, `order_id`, `total_amount`, `image_file`, `date_paid`, `check_no`, `check_details`, `bank`, `remarks`, `status`, `or_number`, `invoice_number`, `approved_by`, `approved_date`, `is_deleted`, `date_time`) VALUES
(1, 7, 0, '2720.00', '', '2021-10-14', 0, '', '', '', 'APPROVED', NULL, NULL, 'admin', '2021-10-14 13:59:47', 0, ''),
(2, 7, 0, '2720.00', '', '2021-10-14', 0, '', '', '', 'APPROVED', NULL, NULL, 'admin', '2021-10-14 13:59:47', 0, ''),
(3, 7, 0, '2720.00', '', '2021-10-14', 0, '', '', '', 'APPROVED', NULL, NULL, 'admin', '2021-10-14 13:59:47', 0, ''),
(4, 11, 0, '79.00', '', '1980-02-05', 0, '', '', 'Voluptatum nulla cum', 'APPROVED', NULL, NULL, 'admin', '2021-10-14 14:00:10', 0, ''),
(5, 7, 0, '10000.00', '', '1972-12-14', 0, '', '', 'Et architecto except', 'APPROVED', NULL, NULL, 'admin', '2021-10-30 16:28:47', 0, ''),
(6, 8, 0, '2.00', '', '2001-08-03', 0, '', '', 'Eius odio aut fugit', 'REJECTED', NULL, NULL, '', NULL, 0, ''),
(7, 10, 0, '41.00', '', '2019-09-08', 0, '', '', 'Velit voluptatem Vo', 'PENDING', NULL, NULL, '', NULL, 1, ''),
(8, 3, 0, '95.00', '', '1997-07-25', 85, 'Sint commodi et ill', 'Consequatur Incidun', 'Pariatur Itaque ill', 'PENDING', NULL, NULL, '', NULL, 1, ''),
(9, 12, 0, '4000.00', '', '1982-02-23', 71, 'Sunt laboris culpa', 'Voluptatem Rerum de', 'Sunt fugiat ipsam e', 'APPROVED', NULL, NULL, 'admin', '2021-10-30 16:31:01', 0, ''),
(10, 7, 0, '100.00', '', '1978-01-06', 11, 'Dolor ab rerum id q', 'Doloremque sequi quo', 'Ipsum quod sed dicta', 'APPROVED', NULL, NULL, 'admin', '2021-10-30 16:27:28', 0, ''),
(19, 7, 0, '5000.00', '', '2021-10-30', 0, '', '', '', 'APPROVED', NULL, NULL, 'admin', '2021-10-30 16:29:56', 0, ''),
(20, 9, 0, '49.00', '', '1987-01-11', 62, 'Consequatur Aliquip', 'Dolorum labore dolor', 'Voluptas fugiat dol', 'APPROVED', NULL, NULL, 'admin', '2021-10-31 09:51:14', 0, ''),
(21, NULL, NULL, '2720.00', NULL, '2021-11-06', 0, '', '', '', 'PENDING', NULL, NULL, NULL, NULL, 1, NULL),
(22, 7, NULL, '2720.00', NULL, '2021-11-19', 0, '', '', '', 'PENDING', NULL, NULL, NULL, NULL, 0, NULL),
(23, 7, NULL, '2720.00', NULL, '2021-11-19', 0, '', '', '', 'PENDING', NULL, NULL, NULL, NULL, 0, NULL),
(24, 7, NULL, '2720.00', NULL, '2021-11-19', 0, '', '', '', 'APPROVED', '3423423', '234234234', 'admin', '2021-11-07 08:24:06', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

CREATE TABLE `payroll` (
  `id` int(11) NOT NULL,
  `payroll_no` varchar(100) DEFAULT NULL,
  `cut_off_date` date DEFAULT NULL,
  `release_date` date DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `added_by` varchar(100) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payroll`
--

INSERT INTO `payroll` (`id`, `payroll_no`, `cut_off_date`, `release_date`, `total_amount`, `added_by`, `status`, `image_file`, `remarks`, `is_deleted`, `date_time`) VALUES
(8, 'P-0001', '2021-10-20', '2021-10-31', '500.00', 'admin', 'APPROVED', 'payroll/Book1.xlsx', '', 0, '2021-10-31 06:17:37'),
(9, 'P-00002', '2021-10-25', '2021-10-29', '46000.00', 'admin', 'REJECTED', 'payroll/PROJECT KICKOFF.docx', 'sample', 0, '2021-10-31 06:29:57'),
(10, 'P-0003', '2021-10-19', '2021-10-30', '54000.00', 'admin', 'APPROVED', 'payroll/Book1.xlsx', 'ready for release', 0, '2021-10-31 06:29:45'),
(11, 'P-0004', '1989-07-21', '1998-05-01', '88.00', 'admin', 'REJECTED', '', 'Culpa doloribus alia', 0, '2021-10-31 06:31:03'),
(12, 'P-0005', '2017-12-01', '2008-12-15', '57.00', 'admin', 'PENDING', 'payroll/purchaseorder-template.xlsx', 'Accusantium perspici', 0, '2021-10-31 06:32:21');

-- --------------------------------------------------------

--
-- Table structure for table `payroll_employee`
--

CREATE TABLE `payroll_employee` (
  `id` int(11) NOT NULL,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `base_rate` decimal(10,2) NOT NULL,
  `allowance` decimal(10,2) NOT NULL,
  `comm_allowance` decimal(10,2) NOT NULL,
  `ot_reg_day` decimal(10,2) NOT NULL,
  `ot_spcl_day` decimal(10,2) NOT NULL,
  `ot_leg_day` decimal(10,2) NOT NULL,
  `special_holiday` decimal(10,2) NOT NULL,
  `regular_holiday` decimal(10,2) NOT NULL,
  `sss` decimal(10,2) NOT NULL,
  `pag_ibig` decimal(10,2) NOT NULL,
  `philhealth` decimal(10,2) NOT NULL,
  `lates` decimal(10,2) NOT NULL,
  `absent` decimal(10,2) NOT NULL,
  `tax_deduct` decimal(10,2) NOT NULL,
  `sss_loan` decimal(10,2) NOT NULL,
  `pag_ibig_loan` decimal(10,2) NOT NULL,
  `cash_advance` decimal(10,2) NOT NULL,
  `short_remittance` decimal(10,2) NOT NULL,
  `gross_pay` decimal(10,2) NOT NULL,
  `total_deductions` decimal(10,2) NOT NULL,
  `net_pay` decimal(10,2) NOT NULL,
  `total_min_work` decimal(10,2) NOT NULL,
  `reg_legal_ot_work` decimal(10,2) NOT NULL,
  `special_ot_work` decimal(10,2) NOT NULL,
  `special_holiday_work` decimal(10,2) NOT NULL,
  `legal_holiday_work` decimal(10,2) NOT NULL,
  `added_by` varchar(100) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payroll_employee`
--

INSERT INTO `payroll_employee` (`id`, `payroll_id`, `emp_id`, `base_rate`, `allowance`, `comm_allowance`, `ot_reg_day`, `ot_spcl_day`, `ot_leg_day`, `special_holiday`, `regular_holiday`, `sss`, `pag_ibig`, `philhealth`, `lates`, `absent`, `tax_deduct`, `sss_loan`, `pag_ibig_loan`, `cash_advance`, `short_remittance`, `gross_pay`, `total_deductions`, `net_pay`, `total_min_work`, `reg_legal_ot_work`, `special_ot_work`, `special_holiday_work`, `legal_holiday_work`, `added_by`, `date_time`) VALUES
(4, 3, 4, '57.00', '3.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '60.00', '0.00', '60.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '2021-10-17 03:20:34'),
(5, 3, 5, '46.00', '4.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '10.00', '50.00', '10.00', '40.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '2021-10-17 03:20:59'),
(6, 3, 13, '1000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '1000.00', '0.00', '1000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '2021-10-17 07:19:55'),
(7, 3, 12, '5000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '200.00', '200.00', '0.00', '0.00', '5000.00', '400.00', '4600.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '2021-10-17 06:39:55'),
(8, 3, 10, '5000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '200.00', '200.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '5000.00', '400.00', '4600.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '2021-10-17 07:19:35'),
(11, 6, 5, '500.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '500.00', '0.00', '500.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '2021-10-17 06:47:45'),
(13, 6, 6, '500.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '100.00', '0.00', '0.00', '0.00', '0.00', '500.00', '100.00', '400.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'admin', '2021-10-17 07:29:12'),
(14, 6, 12, '5000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '500.00', '0.00', '5000.00', '500.00', '4500.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'admin', '2021-10-17 07:29:33'),
(16, 8, 1, '500.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '500.00', '0.00', '500.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'admin', '2021-10-20 06:59:06'),
(17, 9, 1, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'admin', '2021-10-31 05:49:00');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `cost` decimal(10,2) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `stocks` int(11) DEFAULT 0,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `supplier_id`, `image_file`, `product_code`, `description`, `cost`, `price`, `category`, `stocks`, `is_deleted`, `date_time`) VALUES
(1, 1, 'uploads/wp1808930.jpg', '023423', 'sample itemlllll', '0.00', '0.00', 'FOOD', 20, 0, '2021-10-31 02:04:42'),
(2, 2, 'uploads/wp1808930.jpg', 'rtyrtkjkj', '50% Sugar Levelfdgdfg', '0.00', '0.00', 'NON FOOD', 23, 0, '2021-10-31 02:07:20'),
(3, 1, 'uploads/wp1808930.jpg', 'sdfsdf', 'sdfsdfffff', '0.00', '0.00', 'NON FOOD', 20, 0, '2021-10-31 02:04:43'),
(4, 3, 'uploads/532d0341186be12412c6a4362cc08710.jpg', 'dfgdfg', 'sdffsdf', '0.00', '0.00', 'NON FOOD', 0, 0, '2021-10-18 07:11:57'),
(5, 2, 'uploads/532d0341186be12412c6a4362cc08710.jpg', 'Ea ut sed in corrupt', 'Nemo voluptas volupt', '0.00', '0.00', 'NON FOOD', 0, 0, '2021-10-18 07:12:09'),
(6, 2, 'uploads/532d0341186be12412c6a4362cc08710.jpg', 'Vitae fuga Est adip', 'Labore beatae ducimu', '0.00', '0.00', 'OTHERS', 0, 0, '2021-10-18 07:11:52'),
(7, 1, 'uploads/532d0341186be12412c6a4362cc08710.jpg', '1634541088', 'Veniam irure aliqua', '60.00', '100.00', 'OTHERS', 0, 0, '2021-10-18 07:11:28'),
(8, 3, '', '1634541083', 'Officia ab voluptate', '0.00', '0.00', 'NON FOOD', 0, 0, '2021-10-18 07:11:23'),
(9, 0, '', 'Aliqua Sapiente min', 'At nihil qui sint po', '0.00', '0.00', '', 0, 1, '2021-10-09 05:30:33'),
(10, 0, '', 'Eligendi sit ration', 'Fugit et et non et ', '0.00', '0.00', '', 0, 1, '2021-10-09 05:22:49'),
(11, 0, '', 'Ad natus nostrum ad ', 'Fugiat eiusmod conseggggggggg', '0.00', '0.00', '', 0, 1, '2021-10-09 05:25:07'),
(12, 2, 'uploads/532d0341186be12412c6a4362cc08710.jpg', '1634541095', 'Culpa neque dolores', '500.00', '1000.00', 'FOOD', 0, 0, '2021-10-18 07:11:35'),
(13, 0, '', 'u67867', 'hgjghj', '0.00', '0.00', '', 0, 1, '2021-10-09 05:30:28'),
(14, 2, '', '1634541078', 'Qui voluptas ad modi', '32.00', '210.00', 'FOOD', 0, 0, '2021-10-18 07:11:18'),
(15, 2, '', '1634541073', 'Accusantium omnis ev', '17.00', '494.00', 'NON FOOD', 0, 0, '2021-10-18 07:11:13'),
(16, 2, '', '1634541069', 'Autem sit laudantiu', '25.00', '610.00', 'FOOD', 0, 0, '2021-10-18 07:11:09'),
(17, 1, '', '1634541061', 'Inventore veritatis ', '82.00', '368.00', 'NON FOOD', 0, 0, '2021-10-18 07:11:01'),
(18, 3, '', 'I-0001', 'Veritatis proident ', '77.00', '634.00', 'NON FOOD', 0, 0, '2021-10-20 06:56:24'),
(19, 3, '', 'I-0002', 'Et rerum debitis fug', '46.00', '858.00', 'FOOD', 0, 0, '2021-10-20 06:56:55'),
(20, 1, '', 'I-0003', 'Possimus et exercit', '39.00', '330.00', 'FOOD', 0, 0, '2021-10-25 08:36:01');

-- --------------------------------------------------------

--
-- Table structure for table `product_journal`
--

CREATE TABLE `product_journal` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `transaction` text DEFAULT NULL,
  `ref_no` varchar(50) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_journal`
--

INSERT INTO `product_journal` (`id`, `product_id`, `transaction`, `ref_no`, `qty`, `date_time`) VALUES
(1, 1, 'PURCHASE ORDER', '', 20, '2021-10-31 02:04:43'),
(2, 3, 'PURCHASE ORDER', '', 20, '2021-10-31 02:04:43'),
(3, 2, 'D-0002', 'PURCHASE ORDER', 23, '2021-10-31 02:07:20');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` int(11) NOT NULL,
  `po_number` varchar(100) NOT NULL,
  `date_po` date DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `mode_of_payment` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `added_by` varchar(100) DEFAULT NULL,
  `approved_by` varchar(50) DEFAULT NULL,
  `received_by` varchar(50) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`id`, `po_number`, `date_po`, `supplier_id`, `total_amount`, `mode_of_payment`, `status`, `added_by`, `approved_by`, `received_by`, `is_deleted`, `date_time`) VALUES
(7, 'D-0001', '2021-10-31', 1, '12000.00', 'PICKUP', 'RECEIVED', 'admin', NULL, NULL, 0, '2021-10-31 02:04:43'),
(8, 'D-0002', '2021-10-26', 2, '2300.00', 'PICKUP', 'RECEIVED', 'admin', NULL, NULL, 0, '2021-10-31 02:07:20'),
(9, 'D-0003', '2021-10-26', 2, '14000.00', 'PICKUP', 'APPROVED', 'admin', 'admin', NULL, 0, '2021-10-31 05:36:10'),
(10, 'D-0004', '2021-10-26', 1, '168800.00', 'PICKUP', 'PENDING', 'admin', '', NULL, 0, '2021-11-03 23:22:35');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_items`
--

CREATE TABLE `purchase_order_items` (
  `id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_order_items`
--

INSERT INTO `purchase_order_items` (`id`, `purchase_order_id`, `product_id`, `qty`, `cost`, `total_amount`) VALUES
(22, 7, 1, 20, '500.00', '10000.00'),
(23, 7, 3, 20, '100.00', '2000.00'),
(24, 8, 2, 23, '100.00', '2300.00'),
(25, 9, 2, 200, '20.00', '4000.00'),
(26, 9, 5, 50, '200.00', '10000.00'),
(27, 10, 1, 500, '200.00', '100000.00'),
(28, 10, 3, 100, '550.00', '55000.00'),
(29, 10, 7, 100, '60.00', '6000.00'),
(32, 10, 20, 200, '39.00', '7800.00');

-- --------------------------------------------------------

--
-- Table structure for table `received_po`
--

CREATE TABLE `received_po` (
  `id` int(11) NOT NULL,
  `purchase_order_number` varchar(50) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `received_by` varchar(50) DEFAULT NULL,
  `date_received` date DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `added_by` varchar(50) DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `received_po`
--

INSERT INTO `received_po` (`id`, `purchase_order_number`, `total_amount`, `received_by`, `date_received`, `remarks`, `added_by`, `date_time`) VALUES
(16, 'D-0001', '12.00', '', '2021-10-31', '', 'admin', '2021-10-31 02:04:42'),
(17, 'D-0002', '2.00', '', '2021-11-01', '', 'admin', '2021-10-31 02:07:20');

-- --------------------------------------------------------

--
-- Table structure for table `received_po_items`
--

CREATE TABLE `received_po_items` (
  `id` int(11) NOT NULL,
  `purchase_order_number` varchar(50) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `rec_qty` int(11) DEFAULT NULL,
  `cost` decimal(10,2) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `received_po_items`
--

INSERT INTO `received_po_items` (`id`, `purchase_order_number`, `product_id`, `rec_qty`, `cost`, `total_amount`) VALUES
(13, 'D-0001', 1, 20, '500.00', '10.00'),
(14, 'D-0001', 3, 20, '100.00', '2.00'),
(15, 'D-0002', 2, 23, '100.00', '2.00');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_name` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `category_id`, `sub_category_name`, `is_deleted`, `date_time`) VALUES
(1, 6, 'Cost of Goods', 0, '2021-10-24 00:05:25'),
(2, 8, 'Gas Allowance', 0, '2021-10-24 00:05:43'),
(3, 8, 'Food Allowance', 0, '2021-10-24 00:05:58'),
(4, 8, 'Toll Fee', 0, '2021-10-24 00:06:07'),
(5, 8, 'Board and Lodging', 0, '2021-10-24 00:06:25'),
(6, 9, 'Repair and Maintenance Works', 0, '2021-10-24 00:06:50'),
(7, 9, 'Telephone and Internet Expenses', 0, '2021-10-24 00:07:05'),
(8, 9, 'Electricity and Water Bill', 0, '2021-10-24 00:07:22'),
(9, 9, 'Office Supplies', 0, '2021-10-24 00:07:34'),
(10, 9, 'Professional Fee', 0, '2021-10-24 00:07:46'),
(11, 9, 'Permit and Licenses', 0, '2021-10-24 00:07:56');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_address` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `supplier_name`, `supplier_address`, `is_deleted`, `date_time`) VALUES
(1, 'Kane Garrison', 'Et laudantium volup', 0, '2021-10-10 07:07:33'),
(2, 'Quintessa Coffey', 'Voluptas similique e', 0, '2021-10-10 07:07:45'),
(3, 'Raphael Goodwin', 'Et iusto consectetur', 0, '2021-10-14 06:51:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_levels`
--
ALTER TABLE `access_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_franchise`
--
ALTER TABLE `billing_franchise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_payments`
--
ALTER TABLE `billing_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commissary_accounts`
--
ALTER TABLE `commissary_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchisee`
--
ALTER TABLE `franchisee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `franchisee_ibfk_1` (`branch_id`);

--
-- Indexes for table `franchise_accounts`
--
ALTER TABLE `franchise_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchise_branch`
--
ALTER TABLE `franchise_branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchise_renewal_log`
--
ALTER TABLE `franchise_renewal_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_payments`
--
ALTER TABLE `order_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll`
--
ALTER TABLE `payroll`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll_employee`
--
ALTER TABLE `payroll_employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_code` (`product_code`);

--
-- Indexes for table `product_journal`
--
ALTER TABLE `product_journal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `po_number` (`po_number`);

--
-- Indexes for table `purchase_order_items`
--
ALTER TABLE `purchase_order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `received_po`
--
ALTER TABLE `received_po`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `received_po_items`
--
ALTER TABLE `received_po_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_category` (`category_id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_levels`
--
ALTER TABLE `access_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `billing_franchise`
--
ALTER TABLE `billing_franchise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `billing_payments`
--
ALTER TABLE `billing_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `commissary_accounts`
--
ALTER TABLE `commissary_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `franchisee`
--
ALTER TABLE `franchisee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `franchise_accounts`
--
ALTER TABLE `franchise_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `franchise_branch`
--
ALTER TABLE `franchise_branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `franchise_renewal_log`
--
ALTER TABLE `franchise_renewal_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=894;

--
-- AUTO_INCREMENT for table `order_payments`
--
ALTER TABLE `order_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `payroll`
--
ALTER TABLE `payroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `payroll_employee`
--
ALTER TABLE `payroll_employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `product_journal`
--
ALTER TABLE `product_journal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `purchase_order_items`
--
ALTER TABLE `purchase_order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `received_po`
--
ALTER TABLE `received_po`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `received_po_items`
--
ALTER TABLE `received_po_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `franchisee`
--
ALTER TABLE `franchisee`
  ADD CONSTRAINT `franchisee_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `franchise_branch` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
