-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 03, 2021 at 02:31 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thehungrypita`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_levels`
--

CREATE TABLE `access_levels` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `access_dashboard` int(11) NOT NULL DEFAULT 0,
  `access_access_levels` int(11) NOT NULL DEFAULT 0,
  `access_user_accounts` int(11) NOT NULL DEFAULT 0,
  `access_franchise_accounts` int(11) NOT NULL DEFAULT 0,
  `access_commissary_accounts` int(11) NOT NULL DEFAULT 0,
  `access_employee_entries` int(11) NOT NULL DEFAULT 0,
  `access_encode_attendance` int(11) NOT NULL DEFAULT 0,
  `access_payroll` int(11) NOT NULL DEFAULT 0,
  `access_product_information` int(11) NOT NULL DEFAULT 0,
  `access_purchase_order` int(11) NOT NULL DEFAULT 0,
  `access_franchise_branch` int(11) NOT NULL DEFAULT 0,
  `access_franchise_entries` int(11) NOT NULL DEFAULT 0,
  `access_billing_franchising` int(11) NOT NULL DEFAULT 0,
  `access_billing_payments` int(11) NOT NULL DEFAULT 0,
  `access_order_payments` int(11) NOT NULL DEFAULT 0,
  `access_expenses` int(11) NOT NULL DEFAULT 0,
  `access_reports` int(11) NOT NULL DEFAULT 0,
  `access_logs` int(11) NOT NULL DEFAULT 0,
  `access_approval_power` int(11) NOT NULL DEFAULT 0,
  `access_order` int(11) NOT NULL DEFAULT 0,
  `access_order_tracker` int(11) NOT NULL DEFAULT 0,
  `access_return_goods` int(11) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_levels`
--

INSERT INTO `access_levels` (`id`, `description`, `access_dashboard`, `access_access_levels`, `access_user_accounts`, `access_franchise_accounts`, `access_commissary_accounts`, `access_employee_entries`, `access_encode_attendance`, `access_payroll`, `access_product_information`, `access_purchase_order`, `access_franchise_branch`, `access_franchise_entries`, `access_billing_franchising`, `access_billing_payments`, `access_order_payments`, `access_expenses`, `access_reports`, `access_logs`, `access_approval_power`, `access_order`, `access_order_tracker`, `access_return_goods`, `is_deleted`) VALUES
(3, 'Administrator', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0),
(4, 'User', 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(45, 'Customer', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `is_deleted` int(1) DEFAULT 0,
  `date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `role`, `image_file`, `is_deleted`, `date`) VALUES
(1, 'Administrator', 'A', 'Admin', 'admin@gmail.com', 'admin', '$2y$10$7e9rSp7gC.GslurbeSlIL.6NnSdN6/ejq0u/ueIn.00Kc7bMnxWBa', '3', 'uploads/532d0341186be12412c6a4362cc08710.jpg', 0, '2021-11-11 07:55:12');

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `total_minutes` decimal(10,2) NOT NULL,
  `regular_legal_ot` decimal(10,2) NOT NULL,
  `special_ot` decimal(10,2) NOT NULL,
  `special_holiday` decimal(10,2) NOT NULL,
  `legal_holiday` decimal(10,2) NOT NULL,
  `total_hours` decimal(11,2) NOT NULL,
  `legend` varchar(50) NOT NULL,
  `work_date` date NOT NULL,
  `added_by` text NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `emp_id`, `total_minutes`, `regular_legal_ot`, `special_ot`, `special_holiday`, `legal_holiday`, `total_hours`, `legend`, `work_date`, `added_by`, `is_deleted`, `date_time`) VALUES
(26, 7, '9.00', '38.00', '24.00', '0.00', '0.00', '1.28', 'PRESENT', '2012-08-26', 'admin', 0, '2021-10-12 04:26:26'),
(27, 7, '480.00', '0.00', '0.00', '0.00', '0.00', '8.00', 'PRESENT', '2012-08-27', 'admin', 0, '2021-10-13 23:47:01'),
(28, 10, '480.00', '0.00', '0.00', '0.00', '0.00', '8.00', 'PRESENT', '2021-10-14', 'admin', 0, '2021-10-13 23:47:36'),
(29, 5, '92.00', '98.00', '62.00', '0.00', '0.00', '4.35', 'PRESENT', '2014-04-07', 'admin', 0, '2021-10-13 23:53:15'),
(30, 7, '51.00', '100.00', '56.00', '0.00', '0.00', '3.55', 'DAY-OFF', '1993-07-06', 'admin', 0, '2021-10-13 23:53:19'),
(31, 5, '48.00', '42.00', '38.00', '0.00', '0.00', '2.31', 'DAY-OFF', '2018-08-25', 'admin', 0, '2021-10-13 23:53:22'),
(32, 12, '92.00', '81.00', '7.00', '0.00', '0.00', '3.10', 'PRESENT', '1973-07-25', 'admin', 0, '2021-10-13 23:53:25'),
(33, 10, '86.00', '77.00', '46.00', '0.00', '0.00', '3.31', 'DAY-OFF', '1980-02-11', 'admin', 0, '2021-10-13 23:53:30'),
(34, 9, '100.00', '100.00', '62.00', '0.00', '0.00', '4.45', 'DAY-OFF', '2007-01-28', 'admin', 0, '2021-10-13 23:53:33'),
(35, 7, '39.00', '6.00', '21.00', '0.00', '0.00', '1.27', 'DAY-OFF', '1985-10-27', 'admin', 0, '2021-10-13 23:53:36'),
(36, 13, '3.00', '66.00', '51.00', '480.00', '480.00', '10.00', 'DAY-OFF', '1994-02-25', 'admin', 0, '2021-10-15 07:06:26'),
(37, 1, '6240.00', '0.00', '0.00', '0.00', '0.00', '104.00', 'PRESENT', '2021-10-22', 'admin', 0, '2021-10-15 07:22:43');

-- --------------------------------------------------------

--
-- Table structure for table `billing_franchise`
--

CREATE TABLE `billing_franchise` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `total_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `or_number` text NOT NULL,
  `invoice_number` text DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'UNPAID',
  `remarks` text NOT NULL,
  `added_by` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `billing_franchise`
--

INSERT INTO `billing_franchise` (`id`, `franchise_id`, `description`, `total_amount`, `or_number`, `invoice_number`, `status`, `remarks`, `added_by`, `date_time`, `is_deleted`) VALUES
(11, 15, 'FRANCHISING FEE', '5000.00', '', '', 'UNPAID', '', 'admin', '2021-11-14 00:31:54', 0),
(12, 15, 'OTHER FRANCHISING INCLUSION', '8500.00', '1111', '1111', 'PAID', '', 'admin', '2021-11-14 00:43:30', 0),
(13, 16, 'FRANCHISING FEE', '2720.00', '111122', '11122', 'PAID', '', 'admin', '2021-11-14 00:57:42', 0);

-- --------------------------------------------------------

--
-- Table structure for table `billing_payments`
--

CREATE TABLE `billing_payments` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) DEFAULT NULL,
  `bill_id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `check_no` text DEFAULT NULL,
  `check_details` varchar(50) DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `date_paid` date DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `status` varchar(50) DEFAULT 'PENDING',
  `or_number` text DEFAULT NULL,
  `invoice_number` text DEFAULT NULL,
  `approved_by` varchar(100) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `billing_payments`
--

INSERT INTO `billing_payments` (`id`, `franchise_id`, `bill_id`, `description`, `total_amount`, `image_file`, `check_no`, `check_details`, `bank`, `date_paid`, `remarks`, `status`, `or_number`, `invoice_number`, `approved_by`, `approved_date`, `is_deleted`, `date_time`) VALUES
(31, 15, 11, ' FRANCHISING FEE ', '5000.00', NULL, '', '', '', '2021-11-14', '', 'REJECTED', '', '', '', NULL, 0, NULL),
(32, 15, 11, ' FRANCHISING FEE ', '5000.00', 'uploads/alb7.jpg', '', '', '', '2021-11-14', '', 'REJECTED', '', '', '', NULL, 0, NULL),
(33, 15, 12, ' OTHER FRANCHISING INCLUSION ', '8500.00', NULL, '', '', '', '2021-11-14', '', 'APPROVED', '1111', '1111', 'admin', '2021-11-14 08:43:30', 0, NULL),
(34, 16, 13, ' FRANCHISING FEE ', '2720.00', NULL, '', '', '', '2021-11-14', '', 'APPROVED', '111122', '11122', 'admin', '2021-11-14 08:57:42', 0, NULL),
(35, 15, 11, ' FRANCHISING FEE ', '5000.00', 'uploads/eternals-k7-1920x1080.jpg', '', '', '', '2021-12-03', '', 'PENDING', NULL, NULL, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `description`, `is_deleted`, `date_time`) VALUES
(1, 'Cart 20', 0, '2021-10-23 21:31:07'),
(2, 'Cart 1', 0, '2021-10-23 21:53:48'),
(3, 'Cart 11', 0, '2021-10-23 22:22:17');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `type`, `category_name`, `is_deleted`, `date_time`) VALUES
(4, 'Non-Operational', 'Taxes', 0, '2021-10-24 00:02:08'),
(5, 'Non-Operational', 'Interest', 0, '2021-10-24 00:02:16'),
(6, 'Operational', 'Cost of Sales', 0, '2021-10-24 00:02:43'),
(7, 'Operational', 'Marketing, Advertising and Promotion', 0, '2021-10-24 00:03:14'),
(8, 'Operational', 'Travel Expenses', 0, '2021-10-24 00:03:25'),
(9, 'Operational', 'Administrative Expenses', 0, '2021-10-24 00:03:40'),
(10, 'Operational', 'Rent and Insurance', 0, '2021-10-24 00:04:00'),
(11, 'Operational', 'Depreciation and Amortization', 0, '2021-10-24 00:04:18'),
(12, 'Operational', 'Commission', 0, '2021-10-24 00:04:33'),
(13, 'Operational', 'In house expenses', 0, '2021-10-24 00:04:48');

-- --------------------------------------------------------

--
-- Table structure for table `commissary_accounts`
--

CREATE TABLE `commissary_accounts` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `image_file` text NOT NULL,
  `is_deleted` int(1) DEFAULT 0,
  `date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `commissary_accounts`
--

INSERT INTO `commissary_accounts` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `image_file`, `is_deleted`, `date`) VALUES
(4, 'Commissary', 'C', 'Commissary', 'commissary@gmail.com', 'commissary', '$2y$10$LYhi9uQUgs5pOSI9sqgF8.jG7Yab94vXpe5qDQQsL/YYAN30V9coC', 'uploads/The Hungry Pita_Accounting_BG.png', 0, '2021-11-30 23:10:50');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `employment_date` date DEFAULT NULL,
  `regularization_date` date DEFAULT NULL,
  `employment_type` varchar(100) DEFAULT NULL,
  `rate` decimal(11,2) DEFAULT NULL,
  `date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(11) NOT NULL,
  `date_paid` date DEFAULT NULL,
  `expense_type` varchar(50) DEFAULT NULL,
  `voucher_number` int(11) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `sub_category` varchar(50) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `added_by` varchar(50) DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `formulation`
--

CREATE TABLE `formulation` (
  `form_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `date_process` date NOT NULL,
  `remarks` text NOT NULL,
  `added_by` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `formulation`
--

INSERT INTO `formulation` (`form_id`, `item_id`, `description`, `date_process`, `remarks`, `added_by`, `date_time`, `is_deleted`) VALUES
(1, 21, 'Formulation for ITEM 1', '2021-12-03', 'Sample Formulation', 'commissary', '2021-12-03 00:10:05', 1),
(2, 21, 'Formulation for ITEM 1', '2021-12-03', 'N/A', 'commissary', '2021-12-03 00:10:17', 0),
(3, 22, 'Formulation for ITEM 2', '2021-12-03', 'N/A', 'commissary', '2021-12-03 00:34:28', 1),
(4, 22, 'Formulation for ITEM 2', '2021-12-03', 'Sample', 'commissary', '2021-12-03 00:38:46', 0);

-- --------------------------------------------------------

--
-- Table structure for table `formulation_details`
--

CREATE TABLE `formulation_details` (
  `id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `raw_item_id` int(11) NOT NULL,
  `qty` float NOT NULL,
  `uom` varchar(50) NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `formulation_details`
--

INSERT INTO `formulation_details` (`id`, `form_id`, `raw_item_id`, `qty`, `uom`, `remarks`) VALUES
(8, 4, 25, 25, 'pax', 'N/A'),
(9, 4, 27, 6, 'pax', 'N/A'),
(10, 4, 29, 10, 'pax', 'N/A'),
(11, 2, 25, 25, 'pax', 'N/A'),
(12, 2, 28, 25, 'pax', 'N/A');

-- --------------------------------------------------------

--
-- Table structure for table `franchisee`
--

CREATE TABLE `franchisee` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `mode_of_transaction` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `contactno` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `tin_number` text DEFAULT NULL,
  `package_type` varchar(50) DEFAULT NULL,
  `date_contract_signing` date DEFAULT NULL,
  `date_branch_opening` date DEFAULT NULL,
  `date_contract_expiry` date DEFAULT NULL,
  `billing_balance` decimal(10,2) DEFAULT 0.00,
  `order_balance` decimal(10,2) DEFAULT 0.00,
  `total_amount` decimal(10,2) DEFAULT 0.00,
  `last_payment_amount` decimal(10,2) DEFAULT NULL,
  `last_payment_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchisee`
--

INSERT INTO `franchisee` (`id`, `branch_id`, `mode_of_transaction`, `name`, `address`, `contactno`, `birthday`, `tin_number`, `package_type`, `date_contract_signing`, `date_branch_opening`, `date_contract_expiry`, `billing_balance`, `order_balance`, `total_amount`, `last_payment_amount`, `last_payment_date`, `is_deleted`, `date_time`) VALUES
(15, 12, 'Delivery', 'Franchise 1', 'Address', '09107102498', '2021-11-17', '33444434', '1', '2021-11-19', '2021-12-01', '2022-01-28', '0.00', '4000.00', '4000.00', '8500.00', '2021-11-14 00:00:00', 0, '2021-12-02 23:23:56'),
(16, 12, 'Pick-up', 'Blaine Gibbs', 'Suscipit exercitatio', '92', '2004-10-06', '269', '2', '1974-01-04', '1993-11-22', '1970-09-27', '0.00', '0.00', '0.00', '2720.00', '2021-11-14 00:00:00', 0, '2021-12-01 00:12:46');

-- --------------------------------------------------------

--
-- Table structure for table `franchise_accounts`
--

CREATE TABLE `franchise_accounts` (
  `id` int(11) NOT NULL,
  `franchisee_id` int(11) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `is_deleted` int(1) DEFAULT 0,
  `image_file` text DEFAULT '',
  `date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchise_accounts`
--

INSERT INTO `franchise_accounts` (`id`, `franchisee_id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `is_deleted`, `image_file`, `date`) VALUES
(4, 15, 'Franchise 1', 'F', 'Franchise 1', 'franchise@gmail.com', 'test', '$2y$10$3K0lir.wBKzkoHlY/8LBv.s4ne5O0S.jkxPsq4NrszMUxMrWzropW', 0, '', '2021-11-11 23:21:24'),
(5, 16, 'Harper', 'Jemima Stokes', 'Vazquez', 'cezun@mailinator.com', 'test2', '$2y$10$L47n04is8vXthPHVqX7/NeGD7uCxZKjkY8WLeMlik/WyILo.UBNzK', 0, '', '2021-11-14 00:54:07');

-- --------------------------------------------------------

--
-- Table structure for table `franchise_branch`
--

CREATE TABLE `franchise_branch` (
  `id` int(11) NOT NULL,
  `branch_code` varchar(100) NOT NULL,
  `branch_name` varchar(100) NOT NULL,
  `branch_location` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchise_branch`
--

INSERT INTO `franchise_branch` (`id`, `branch_code`, `branch_name`, `branch_location`, `is_deleted`, `date_time`) VALUES
(12, 'B-0001', 'Branch 1', 'Branch 1', 0, '2021-11-11 23:19:22');

-- --------------------------------------------------------

--
-- Table structure for table `franchise_renewal_log`
--

CREATE TABLE `franchise_renewal_log` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `renewed_date` date DEFAULT NULL,
  `new_expiry_date` date DEFAULT NULL,
  `approved_by` text DEFAULT NULL,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `account_id` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `module` text NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `account_id`, `description`, `ip`, `hostname`, `timestamp`, `module`, `is_deleted`) VALUES
(959, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:18:44', 'ACCOUNTING', 0),
(960, 'Administrator A Admin', 'Add account: branch 1', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:19:22', 'ACCOUNTING', 0),
(961, 'Administrator A Admin', 'Add franchisee: franchise 1', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:20:00', 'ACCOUNTING', 0),
(962, 'Administrator A Admin', 'Add supplier name: supplier 1', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:20:20', 'ACCOUNTING', 0),
(963, 'Administrator A Admin', 'Add product: item 1', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:20:38', 'ACCOUNTING', 0),
(964, 'Administrator A Admin', 'Add product: item 2', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:20:51', 'ACCOUNTING', 0),
(965, 'Administrator A Admin', 'Add account: franchise 1', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:21:24', 'ACCOUNTING', 0),
(966, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:21:36', 'FRANCHISEE', 0),
(967, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-12 00:04:38', 'FRANCHISEE', 0),
(968, 'Franchise 1 F Franchise 1', 'Add order payment: 15', '::1', 'DESKTOP-FO8SD5D', '2021-11-12 00:04:51', 'FRANCHISEE', 0),
(969, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-12 08:27:16', 'FRANCHISEE', 0),
(970, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:06:36', 'ACCOUNTING', 0),
(971, 'Administrator A Admin', 'Edit product id 22', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:13:30', 'ACCOUNTING', 0),
(972, 'Administrator A Admin', 'Edit product id 22', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:14:09', 'ACCOUNTING', 0),
(973, 'Administrator A Admin', 'Edit product id 21', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:14:39', 'ACCOUNTING', 0),
(974, 'Administrator A Admin', 'Edit product id 22', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:14:48', 'ACCOUNTING', 0),
(975, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:16:35', 'ACCOUNTING', 0),
(976, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:16:52', 'FRANCHISEE', 0),
(977, 'Franchise 1 F Franchise 1', 'Edit order payment id 26', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:30:38', 'FRANCHISEE', 0),
(978, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:31:39', 'ACCOUNTING', 0),
(979, 'Administrator A Admin', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:31:54', 'ACCOUNTING', 0),
(980, 'Franchise 1 F Franchise 1', 'Add billing payment: 11 | franchising fee | 5000.00', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:36:31', 'FRANCHISEE', 0),
(981, 'Administrator A Admin', 'Edit billing payment id 31', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:40:04', 'ACCOUNTING', 0),
(982, 'Franchise 1 F Franchise 1', 'Add billing payment: 11 | franchising fee | 5000.00', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:40:12', 'FRANCHISEE', 0),
(983, 'Administrator A Admin', 'Add bill to franchise: other franchising inclusion', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:40:34', 'ACCOUNTING', 0),
(984, 'Franchise 1 F Franchise 1', 'Add billing payment: 12 | other franchising inclusion | 8500.00', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:40:44', 'FRANCHISEE', 0),
(985, 'Administrator A Admin', 'Edit billing payment id 33', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:43:30', 'ACCOUNTING', 0),
(986, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:50:17', 'FRANCHISEE', 0),
(987, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:50:31', 'FRANCHISEE', 0),
(988, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:51:00', 'FRANCHISEE', 0),
(989, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:51:15', 'FRANCHISEE', 0),
(990, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:51:38', 'FRANCHISEE', 0),
(991, '  ', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:52:46', 'FRANCHISEE', 0),
(992, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:53:18', 'FRANCHISEE', 0),
(993, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:53:37', 'ACCOUNTING', 0),
(994, 'Administrator A Admin', 'Add franchisee: blaine gibbs', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:53:51', 'ACCOUNTING', 0),
(995, 'Administrator A Admin', 'Add account: harper', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:54:07', 'ACCOUNTING', 0),
(996, '  ', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:54:13', 'FRANCHISEE', 0),
(997, 'Harper Jemima Stokes Vazquez', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:54:20', 'FRANCHISEE', 0),
(998, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:56:33', 'FRANCHISEE', 0),
(999, 'Harper Jemima Stokes Vazquez', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:56:51', 'FRANCHISEE', 0),
(1000, 'Administrator A Admin', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:57:07', 'ACCOUNTING', 0),
(1001, 'Harper Jemima Stokes Vazquez', 'Add billing payment: 13 | franchising fee | 2720.00', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:57:27', 'FRANCHISEE', 0),
(1002, 'Administrator A Admin', 'Edit billing payment id 34', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:57:42', 'ACCOUNTING', 0),
(1003, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:58:19', 'FRANCHISEE', 0),
(1004, 'Administrator A Admin', 'Add product: item 3', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:12:18', 'ACCOUNTING', 0),
(1005, 'Administrator A Admin', 'Add product: item 4', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:12:34', 'ACCOUNTING', 0),
(1006, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:13:31', 'FRANCHISEE', 0),
(1007, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:13:49', 'FRANCHISEE', 0),
(1008, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:14:08', 'FRANCHISEE', 0),
(1009, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:14:23', 'FRANCHISEE', 0),
(1010, 'Administrator A Admin', 'Edit product id 24', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:24:40', 'ACCOUNTING', 0),
(1011, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 04:57:52', 'FRANCHISEE', 0),
(1012, 'Harper Jemima Stokes Vazquez', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 05:04:10', 'FRANCHISEE', 0),
(1013, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 05:33:57', 'FRANCHISEE', 0),
(1014, 'Harper Jemima Stokes Vazquez', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 05:47:12', 'FRANCHISEE', 0),
(1015, 'Administrator A Admin', 'Edit billing payment id 32', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 05:47:47', 'ACCOUNTING', 0),
(1016, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 22:58:00', 'ACCOUNTING', 0),
(1017, 'Administrator A Admin', 'Add account: commissary', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 22:58:44', 'ACCOUNTING', 0),
(1018, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:03:22', 'ACCOUNTING', 0),
(1019, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:03:29', 'ACCOUNTING', 0),
(1020, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:12:18', 'ACCOUNTING', 0),
(1021, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:14:23', 'ACCOUNTING', 0),
(1022, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:41:11', 'ACCOUNTING', 0),
(1023, 'Administrator A Admin', 'Add purchase order: d-0001', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:41:41', 'ACCOUNTING', 0),
(1024, 'Administrator A Admin', 'Edit purcahse order id 12', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:43:03', 'ACCOUNTING', 0),
(1025, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:47:21', 'ACCOUNTING', 0),
(1026, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:51:12', 'ACCOUNTING', 0),
(1027, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:53:01', 'ACCOUNTING', 0),
(1028, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-01 00:13:27', 'ACCOUNTING', 0),
(1029, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-01 00:14:03', 'ACCOUNTING', 0),
(1030, 'Commissary C Commissary', 'Add purchase order: d-0002', '::1', 'DESKTOP-FO8SD5D', '2021-12-01 00:50:33', 'ACCOUNTING', 0),
(1031, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-01 06:13:57', 'ACCOUNTING', 0),
(1032, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-01 23:06:42', 'ACCOUNTING', 0),
(1033, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 04:02:22', 'ACCOUNTING', 0),
(1034, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 07:35:48', 'ACCOUNTING', 0),
(1035, 'Administrator A Admin', 'Add purchase order: d-0001', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 07:36:03', 'ACCOUNTING', 0),
(1036, 'Administrator A Admin', 'Add purchase order: d-0002', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 07:36:30', 'ACCOUNTING', 0),
(1037, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 07:36:49', 'ACCOUNTING', 0),
(1038, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 08:26:35', 'FRANCHISEE', 0),
(1039, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:06:23', 'ACCOUNTING', 0),
(1040, 'Franchise 1 F Franchise 1', 'Add billing payment: 11 | franchising fee | 5000.00', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:21:27', 'FRANCHISEE', 0),
(1041, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:22:13', 'ACCOUNTING', 0),
(1042, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:23:41', 'ACCOUNTING', 0),
(1043, 'Commissary C Commissary', 'Add product: raw mat 1', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:33:24', 'ACCOUNTING', 0),
(1044, 'Commissary C Commissary', 'Add product: raw mat 2', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:33:46', 'ACCOUNTING', 0),
(1045, 'Commissary C Commissary', 'Add product: raw mat 3', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:34:12', 'ACCOUNTING', 0),
(1046, 'Commissary C Commissary', 'Add product: raw mat 4', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:34:33', 'ACCOUNTING', 0),
(1047, 'Commissary C Commissary', 'Add product: raw mat 5', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:34:58', 'ACCOUNTING', 0),
(1048, 'Commissary C Commissary', 'Add formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:53:07', 'ACCOUNTING', 0),
(1049, 'Commissary C Commissary', 'Add formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:53:13', 'ACCOUNTING', 0),
(1050, 'Commissary C Commissary', 'Add formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:54:33', 'ACCOUNTING', 0),
(1051, 'Commissary C Commissary', 'Edit formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:08:57', 'COMMISSARY', 0),
(1052, 'Commissary C Commissary', 'Delete formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:10:05', 'COMMISSARY', 0),
(1053, 'Commissary C Commissary', 'Add formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:10:17', 'COMMISSARY', 0),
(1054, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:28:14', 'COMMISSARY', 0),
(1055, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:32:19', 'COMMISSARY', 0),
(1056, 'Commissary C Commissary', 'Add formulation: 22', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:33:03', 'COMMISSARY', 0),
(1057, 'Commissary C Commissary', 'Addraw formulation: 27', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:33:17', 'COMMISSARY', 0),
(1058, 'Commissary C Commissary', 'Delete formulation: 22', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:34:28', 'COMMISSARY', 0),
(1059, 'Commissary C Commissary', 'Add formulation: 22', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:37:30', 'COMMISSARY', 0),
(1060, 'Commissary C Commissary', 'Addraw formulation: 26', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:37:37', 'COMMISSARY', 0),
(1061, 'Commissary C Commissary', 'Addraw formulation: 26', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:38:32', 'COMMISSARY', 0),
(1062, 'Commissary C Commissary', 'Edit formulation: 22', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:38:46', 'COMMISSARY', 0),
(1063, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:38:58', 'COMMISSARY', 0),
(1064, 'Commissary C Commissary', 'Addraw formulation: 29', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:39:02', 'COMMISSARY', 0),
(1065, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:40:23', 'COMMISSARY', 0),
(1066, 'Commissary C Commissary', 'Addraw formulation: 27', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:40:28', 'COMMISSARY', 0),
(1067, 'Commissary C Commissary', 'Addraw formulation: 29', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:40:32', 'COMMISSARY', 0),
(1068, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:40:57', 'COMMISSARY', 0),
(1069, 'Commissary C Commissary', 'Addraw formulation: 28', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:44:18', 'COMMISSARY', 0),
(1070, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-03 12:16:54', 'COMMISSARY', 0),
(1071, 'Commissary C Commissary', 'New production plan :jo 1', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:11:09', 'COMMISSARY', 0),
(1072, 'Commissary C Commissary', 'New production plan :sample jo', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:24:00', 'COMMISSARY', 0),
(1073, 'Commissary C Commissary', 'New production plan :sample jo2', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:24:21', 'COMMISSARY', 0),
(1074, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:30:23', 'COMMISSARY', 0),
(1075, 'Commissary C Commissary', 'New production plan :sample 1', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:30:57', 'COMMISSARY', 0),
(1076, 'Commissary C Commissary', 'New production plan :sample 2', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:31:18', 'COMMISSARY', 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_ref` text NOT NULL,
  `franchisee_id` int(11) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `rec_total_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `added_by` text NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'PENDING',
  `date_ordered` date DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `approved_by` varchar(50) DEFAULT NULL,
  `approved_date_time` timestamp NULL DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `date_delivered` date DEFAULT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_ref`, `franchisee_id`, `total_amount`, `rec_total_amount`, `added_by`, `status`, `date_ordered`, `date_time`, `approved_by`, `approved_date_time`, `remarks`, `date_delivered`, `is_deleted`) VALUES
(53, 'O-0001', 15, '2750.00', '0.00', 'test', 'RECEIVED', '2021-12-02', '2021-12-02 08:28:49', 'commissary', '2021-12-02 08:28:10', NULL, NULL, 0),
(54, 'O-0002', 15, '1250.00', '0.00', 'admin', 'APPROVED', '2021-12-03', '2021-12-02 23:23:56', 'commissary', '2021-12-02 23:23:56', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_ref` text NOT NULL,
  `item_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` decimal(10,2) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `rec_qty` decimal(10,2) NOT NULL DEFAULT 0.00,
  `rec_total_amount` decimal(10,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `order_ref`, `item_id`, `price`, `qty`, `total_amount`, `rec_qty`, `rec_total_amount`) VALUES
(167, 53, 'O-0001', 24, '250.00', '5.00', '1250.00', '0.00', '0.00'),
(168, 53, 'O-0001', 23, '300.00', '5.00', '1500.00', '0.00', '0.00'),
(169, 54, 'O-0002', 24, '250.00', '1.00', '250.00', '0.00', '0.00'),
(170, 54, 'O-0002', 23, '300.00', '1.00', '300.00', '0.00', '0.00'),
(171, 54, 'O-0002', 22, '700.00', '1.00', '700.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `order_payments`
--

CREATE TABLE `order_payments` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `date_paid` date DEFAULT NULL,
  `check_no` int(11) DEFAULT NULL,
  `check_details` varchar(50) DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `status` varchar(50) DEFAULT 'PENDING',
  `or_number` text DEFAULT NULL,
  `invoice_number` text DEFAULT NULL,
  `approved_by` varchar(100) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_payments`
--

INSERT INTO `order_payments` (`id`, `franchise_id`, `order_id`, `total_amount`, `image_file`, `date_paid`, `check_no`, `check_details`, `bank`, `remarks`, `status`, `or_number`, `invoice_number`, `approved_by`, `approved_date`, `is_deleted`, `date_time`) VALUES
(26, 15, NULL, '2720.00', 'uploads/vd5.jpg', '2021-11-14', 0, '', '', '', 'PENDING', NULL, NULL, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_returns`
--

CREATE TABLE `order_returns` (
  `id` int(11) NOT NULL,
  `order_ref_no` varchar(50) NOT NULL,
  `return_ref_no` varchar(50) NOT NULL,
  `franchisee_id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'PENDING',
  `added_by` varchar(50) NOT NULL,
  `remarks` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_returns`
--

INSERT INTO `order_returns` (`id`, `order_ref_no`, `return_ref_no`, `franchisee_id`, `status`, `added_by`, `remarks`, `date_time`, `is_deleted`) VALUES
(14, 'O-0001', 'R-0001', 15, 'REJECTED', 'test', 'sira', '2021-12-02 23:09:42', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_return_items`
--

CREATE TABLE `order_return_items` (
  `id` int(11) NOT NULL,
  `return_ref` varchar(50) NOT NULL,
  `item_id` int(11) NOT NULL,
  `order_qty` int(11) NOT NULL,
  `return_qty` int(11) NOT NULL,
  `cost` decimal(10,0) NOT NULL,
  `total_amount` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_return_items`
--

INSERT INTO `order_return_items` (`id`, `return_ref`, `item_id`, `order_qty`, `return_qty`, `cost`, `total_amount`) VALUES
(17, 'R-0001', 23, 0, 3, '300', '900');

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

CREATE TABLE `payroll` (
  `id` int(11) NOT NULL,
  `payroll_no` varchar(100) DEFAULT NULL,
  `cut_off_date` date DEFAULT NULL,
  `release_date` date DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `added_by` varchar(100) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_employee`
--

CREATE TABLE `payroll_employee` (
  `id` int(11) NOT NULL,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `base_rate` decimal(10,2) NOT NULL,
  `allowance` decimal(10,2) NOT NULL,
  `comm_allowance` decimal(10,2) NOT NULL,
  `ot_reg_day` decimal(10,2) NOT NULL,
  `ot_spcl_day` decimal(10,2) NOT NULL,
  `ot_leg_day` decimal(10,2) NOT NULL,
  `special_holiday` decimal(10,2) NOT NULL,
  `regular_holiday` decimal(10,2) NOT NULL,
  `sss` decimal(10,2) NOT NULL,
  `pag_ibig` decimal(10,2) NOT NULL,
  `philhealth` decimal(10,2) NOT NULL,
  `lates` decimal(10,2) NOT NULL,
  `absent` decimal(10,2) NOT NULL,
  `tax_deduct` decimal(10,2) NOT NULL,
  `sss_loan` decimal(10,2) NOT NULL,
  `pag_ibig_loan` decimal(10,2) NOT NULL,
  `cash_advance` decimal(10,2) NOT NULL,
  `short_remittance` decimal(10,2) NOT NULL,
  `gross_pay` decimal(10,2) NOT NULL,
  `total_deductions` decimal(10,2) NOT NULL,
  `net_pay` decimal(10,2) NOT NULL,
  `total_min_work` decimal(10,2) NOT NULL,
  `reg_legal_ot_work` decimal(10,2) NOT NULL,
  `special_ot_work` decimal(10,2) NOT NULL,
  `special_holiday_work` decimal(10,2) NOT NULL,
  `legal_holiday_work` decimal(10,2) NOT NULL,
  `added_by` varchar(100) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `production`
--

CREATE TABLE `production` (
  `id` int(11) NOT NULL,
  `production_id` int(11) NOT NULL,
  `job_order` varchar(50) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `is_started` int(11) NOT NULL DEFAULT 0,
  `added_by` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `production`
--

INSERT INTO `production` (`id`, `production_id`, `job_order`, `batch_no`, `description`, `start_date`, `end_date`, `is_started`, `added_by`, `date_time`, `is_deleted`) VALUES
(5, 1, 'J-0001', 'B-0001', 'Sample 1', '2021-12-01', '2021-12-02', 0, 'commissary', '2021-12-03 13:30:57', 0),
(6, 2, 'J-0002', 'B-0002', 'Sample 2', '2021-12-01', '2021-12-03', 0, 'commissary', '2021-12-03 13:31:18', 0);

-- --------------------------------------------------------

--
-- Table structure for table `production_items`
--

CREATE TABLE `production_items` (
  `id` int(11) NOT NULL,
  `production_id` int(11) NOT NULL,
  `formulation_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `production_qty` float NOT NULL,
  `final_qty` float NOT NULL,
  `credit` float NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `production_items`
--

INSERT INTO `production_items` (`id`, `production_id`, `formulation_id`, `item_id`, `production_qty`, `final_qty`, `credit`, `remarks`) VALUES
(3, 1, 2, 2, 1, 0, 0, ''),
(4, 1, 4, 4, 1, 0, 0, ''),
(5, 2, 2, 2, 1, 0, 0, ''),
(6, 1, 2, 2, 1, 0, 0, ''),
(7, 1, 4, 4, 1, 0, 0, ''),
(8, 2, 2, 2, 1, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `cost` decimal(10,2) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `stocks` int(11) DEFAULT 0,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `supplier_id`, `image_file`, `product_code`, `description`, `cost`, `price`, `category`, `stocks`, `is_deleted`, `date_time`) VALUES
(21, 4, 'uploads/alb1.jpg', 'I-0001', 'ITEM 1', '1000.00', '1500.00', 'FOOD', 130, 0, '2021-12-02 07:49:40'),
(22, 4, 'uploads/vd4.jpg', 'I-0002', 'ITEM 2', '500.00', '700.00', 'FOOD', 100, 0, '2021-12-02 07:49:40'),
(23, 4, 'uploads/vd9.jpg', 'I-0003', 'ITEM 3', '200.00', '300.00', 'FOOD', 10, 0, '2021-12-02 07:49:40'),
(24, 4, 'uploads/vd11.jpg', 'I-0004', 'ITEM 4', '200.00', '250.00', 'FOOD', 20, 0, '2021-12-02 07:51:23'),
(25, 4, NULL, 'I-0005', 'RAW MAT 1', '20.00', '35.00', 'NON FOOD', 0, 0, '2021-12-02 23:33:24'),
(26, 4, NULL, 'I-0006', 'RAW MAT 2', '20.00', '20.00', 'NON FOOD', 0, 0, '2021-12-02 23:33:46'),
(27, 4, NULL, 'I-0007', 'RAW MAT 3', '20.00', '20.00', 'NON FOOD', 0, 0, '2021-12-02 23:34:12'),
(28, 4, NULL, 'I-0008', 'RAW MAT 4', '20.00', '20.00', 'NON FOOD', 0, 0, '2021-12-02 23:34:33'),
(29, 4, NULL, 'I-0009', 'RAW MAT 5', '20.00', '20.00', 'NON FOOD', 0, 0, '2021-12-02 23:34:58');

-- --------------------------------------------------------

--
-- Table structure for table `products_itemize`
--

CREATE TABLE `products_itemize` (
  `id` int(11) NOT NULL,
  `item_count` int(11) NOT NULL COMMENT 'Quantity counter ',
  `item_id` int(11) NOT NULL,
  `process` varchar(50) NOT NULL,
  `ref_no` varchar(50) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `is_available` int(11) NOT NULL DEFAULT 0,
  `manufacturing` date NOT NULL,
  `expiry` date NOT NULL,
  `order_ref` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products_itemize`
--

INSERT INTO `products_itemize` (`id`, `item_count`, `item_id`, `process`, `ref_no`, `batch_no`, `is_available`, `manufacturing`, `expiry`, `order_ref`) VALUES
(1501, 1, 23, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-01', NULL),
(1502, 2, 23, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-01', NULL),
(1503, 3, 23, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-01', NULL),
(1504, 4, 23, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-01', NULL),
(1505, 5, 23, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-01', NULL),
(1506, 6, 23, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-01', NULL),
(1507, 7, 23, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-01', NULL),
(1508, 8, 23, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-01', NULL),
(1509, 9, 23, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-01', NULL),
(1510, 10, 23, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-01', NULL),
(1511, 1, 22, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-03', NULL),
(1512, 2, 22, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-03', NULL),
(1513, 3, 22, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-03', NULL),
(1514, 4, 22, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-03', NULL),
(1515, 5, 22, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-03', NULL),
(1516, 6, 22, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-03', NULL),
(1517, 7, 22, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-03', NULL),
(1518, 8, 22, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-03', NULL),
(1519, 9, 22, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-03', NULL),
(1520, 10, 22, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '2021-12-03', NULL),
(1521, 1, 21, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-01', '0000-00-00', NULL),
(1522, 2, 21, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-02', '0000-00-00', NULL),
(1523, 3, 21, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-03', '0000-00-00', NULL),
(1524, 4, 21, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-04', '0000-00-00', NULL),
(1525, 5, 21, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-05', '0000-00-00', NULL),
(1526, 6, 21, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-06', '0000-00-00', NULL),
(1527, 7, 21, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-07', '0000-00-00', NULL),
(1528, 8, 21, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-08', '0000-00-00', NULL),
(1529, 9, 21, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-09', '0000-00-00', NULL),
(1530, 10, 21, 'PURCHASE_ORDER', 'D-0001', '324234234', 1, '2021-12-11', '0000-00-00', NULL),
(1531, 1, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1532, 2, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1533, 3, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1534, 4, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1535, 5, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1536, 6, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1537, 7, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1538, 8, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1539, 9, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1540, 10, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1541, 11, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1542, 12, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1543, 13, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1544, 14, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1545, 15, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1546, 16, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1547, 17, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1548, 18, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1549, 19, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL),
(1550, 20, 24, 'PURCHASE_ORDER', 'D-0002', '1111111', 1, '2021-12-01', '2021-12-03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_journal`
--

CREATE TABLE `product_journal` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `transaction` text DEFAULT NULL,
  `ref_no` varchar(50) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_journal`
--

INSERT INTO `product_journal` (`id`, `product_id`, `transaction`, `ref_no`, `qty`, `date_time`) VALUES
(6, 21, 'PURCHASE ORDER', 'D-0001', 30, '2021-11-30 23:43:26'),
(7, 22, 'PURCHASE ORDER', 'D-0001', 30, '2021-11-30 23:43:26'),
(8, 21, 'PURCHASE ORDER', 'D-0002', 10, '2021-12-02 03:00:09'),
(9, 21, 'PURCHASE ORDER', 'D-0002', 10, '2021-12-02 04:06:27'),
(10, 21, 'PURCHASE ORDER', 'D-0001', 30, '2021-12-02 04:08:22'),
(11, 22, 'PURCHASE ORDER', 'D-0001', 30, '2021-12-02 04:08:22'),
(12, 21, 'PURCHASE ORDER', 'D-0001', 30, '2021-12-02 04:24:48'),
(13, 22, 'PURCHASE ORDER', 'D-0001', 30, '2021-12-02 04:24:48'),
(14, 21, 'PURCHASE ORDER', 'D-0002', 5, '2021-12-02 07:31:22'),
(15, 21, 'PURCHASE ORDER', 'D-0002', 5, '2021-12-02 07:33:54'),
(16, 21, 'PURCHASE ORDER', 'D-0001', 10, '2021-12-02 07:49:40'),
(17, 22, 'PURCHASE ORDER', 'D-0001', 10, '2021-12-02 07:49:40'),
(18, 23, 'PURCHASE ORDER', 'D-0001', 10, '2021-12-02 07:49:40'),
(19, 24, 'PURCHASE ORDER', 'D-0002', 20, '2021-12-02 07:51:23');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` int(11) NOT NULL,
  `po_number` varchar(100) NOT NULL,
  `date_po` date DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `mode_of_payment` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `added_by` varchar(100) DEFAULT NULL,
  `approved_by` varchar(50) DEFAULT NULL,
  `received_by` varchar(50) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`id`, `po_number`, `date_po`, `supplier_id`, `total_amount`, `mode_of_payment`, `status`, `added_by`, `approved_by`, `received_by`, `is_deleted`, `date_time`) VALUES
(14, 'D-0001', '2021-12-02', 4, '17000.00', 'PICKUP', 'RECEIVED', 'admin', 'admin', 'commissary', 0, '2021-12-02 07:49:40'),
(15, 'D-0002', '2021-12-02', 4, '4000.00', 'PICKUP', 'RECEIVED', 'admin', 'admin', 'commissary', 0, '2021-12-02 07:51:23');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_items`
--

CREATE TABLE `purchase_order_items` (
  `id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_order_items`
--

INSERT INTO `purchase_order_items` (`id`, `purchase_order_id`, `product_id`, `qty`, `cost`, `total_amount`) VALUES
(38, 14, 21, 10, '1000.00', '10000.00'),
(39, 14, 22, 10, '500.00', '5000.00'),
(40, 14, 23, 10, '200.00', '2000.00'),
(41, 15, 24, 20, '200.00', '4000.00');

-- --------------------------------------------------------

--
-- Table structure for table `received_po`
--

CREATE TABLE `received_po` (
  `id` int(11) NOT NULL,
  `purchase_order_number` varchar(50) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `received_by` varchar(50) DEFAULT NULL,
  `date_received` date DEFAULT NULL,
  `supplier_dr` varchar(100) DEFAULT NULL,
  `batch_no` varchar(100) NOT NULL,
  `remarks` text DEFAULT NULL,
  `added_by` varchar(50) DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `received_po`
--

INSERT INTO `received_po` (`id`, `purchase_order_number`, `total_amount`, `received_by`, `date_received`, `supplier_dr`, `batch_no`, `remarks`, `added_by`, `date_time`) VALUES
(26, 'D-0001', '2.00', 'sdfsdf', '2021-12-02', '234234234', '324234234', '', 'commissary', '2021-12-02 07:49:40'),
(27, 'D-0002', '4.00', 'Sample receiver', '2021-12-02', 'asdasdas', '1111111', '', 'commissary', '2021-12-02 07:51:23');

-- --------------------------------------------------------

--
-- Table structure for table `received_po_items`
--

CREATE TABLE `received_po_items` (
  `id` int(11) NOT NULL,
  `purchase_order_number` varchar(50) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `rec_qty` int(11) DEFAULT NULL,
  `cost` decimal(10,2) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `received_po_items`
--

INSERT INTO `received_po_items` (`id`, `purchase_order_number`, `product_id`, `rec_qty`, `cost`, `total_amount`) VALUES
(28, 'D-0001', 21, 10, '1000.00', '10.00'),
(29, 'D-0001', 22, 10, '500.00', '5.00'),
(30, 'D-0001', 23, 10, '200.00', '2000.00'),
(31, 'D-0002', 24, 20, '200.00', '4.00');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_name` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `category_id`, `sub_category_name`, `is_deleted`, `date_time`) VALUES
(1, 6, 'Cost of Goods', 0, '2021-10-24 00:05:25'),
(2, 8, 'Gas Allowance', 0, '2021-10-24 00:05:43'),
(3, 8, 'Food Allowance', 0, '2021-10-24 00:05:58'),
(4, 8, 'Toll Fee', 0, '2021-10-24 00:06:07'),
(5, 8, 'Board and Lodging', 0, '2021-10-24 00:06:25'),
(6, 9, 'Repair and Maintenance Works', 0, '2021-10-24 00:06:50'),
(7, 9, 'Telephone and Internet Expenses', 0, '2021-10-24 00:07:05'),
(8, 9, 'Electricity and Water Bill', 0, '2021-10-24 00:07:22'),
(9, 9, 'Office Supplies', 0, '2021-10-24 00:07:34'),
(10, 9, 'Professional Fee', 0, '2021-10-24 00:07:46'),
(11, 9, 'Permit and Licenses', 0, '2021-10-24 00:07:56');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_address` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `supplier_name`, `supplier_address`, `is_deleted`, `date_time`) VALUES
(4, 'Supplier 1', 'Supplier 1', 0, '2021-11-11 23:20:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_levels`
--
ALTER TABLE `access_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_franchise`
--
ALTER TABLE `billing_franchise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_payments`
--
ALTER TABLE `billing_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commissary_accounts`
--
ALTER TABLE `commissary_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `formulation`
--
ALTER TABLE `formulation`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `formulation_details`
--
ALTER TABLE `formulation_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchisee`
--
ALTER TABLE `franchisee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `franchisee_ibfk_1` (`branch_id`);

--
-- Indexes for table `franchise_accounts`
--
ALTER TABLE `franchise_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchise_branch`
--
ALTER TABLE `franchise_branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchise_renewal_log`
--
ALTER TABLE `franchise_renewal_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_payments`
--
ALTER TABLE `order_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_returns`
--
ALTER TABLE `order_returns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_return_items`
--
ALTER TABLE `order_return_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll`
--
ALTER TABLE `payroll`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll_employee`
--
ALTER TABLE `payroll_employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `production`
--
ALTER TABLE `production`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `production_items`
--
ALTER TABLE `production_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_code` (`product_code`);

--
-- Indexes for table `products_itemize`
--
ALTER TABLE `products_itemize`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_journal`
--
ALTER TABLE `product_journal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `po_number` (`po_number`);

--
-- Indexes for table `purchase_order_items`
--
ALTER TABLE `purchase_order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `received_po`
--
ALTER TABLE `received_po`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `received_po_items`
--
ALTER TABLE `received_po_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_category` (`category_id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_levels`
--
ALTER TABLE `access_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `billing_franchise`
--
ALTER TABLE `billing_franchise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `billing_payments`
--
ALTER TABLE `billing_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `commissary_accounts`
--
ALTER TABLE `commissary_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `formulation`
--
ALTER TABLE `formulation`
  MODIFY `form_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `formulation_details`
--
ALTER TABLE `formulation_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `franchisee`
--
ALTER TABLE `franchisee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `franchise_accounts`
--
ALTER TABLE `franchise_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `franchise_branch`
--
ALTER TABLE `franchise_branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `franchise_renewal_log`
--
ALTER TABLE `franchise_renewal_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1077;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;

--
-- AUTO_INCREMENT for table `order_payments`
--
ALTER TABLE `order_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `order_returns`
--
ALTER TABLE `order_returns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `order_return_items`
--
ALTER TABLE `order_return_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `payroll`
--
ALTER TABLE `payroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `payroll_employee`
--
ALTER TABLE `payroll_employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `production`
--
ALTER TABLE `production`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `production_items`
--
ALTER TABLE `production_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `products_itemize`
--
ALTER TABLE `products_itemize`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1551;

--
-- AUTO_INCREMENT for table `product_journal`
--
ALTER TABLE `product_journal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `purchase_order_items`
--
ALTER TABLE `purchase_order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `received_po`
--
ALTER TABLE `received_po`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `received_po_items`
--
ALTER TABLE `received_po_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `franchisee`
--
ALTER TABLE `franchisee`
  ADD CONSTRAINT `franchisee_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `franchise_branch` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
