-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 05, 2021 at 07:42 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thehungrypita`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_levels`
--

CREATE TABLE `access_levels` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `access_dashboard` int(11) NOT NULL DEFAULT 0,
  `access_access_levels` int(11) NOT NULL DEFAULT 0,
  `access_user_accounts` int(11) NOT NULL DEFAULT 0,
  `access_franchise_accounts` int(11) NOT NULL DEFAULT 0,
  `access_commissary_accounts` int(11) NOT NULL DEFAULT 0,
  `access_employee_entries` int(11) NOT NULL DEFAULT 0,
  `access_encode_attendance` int(11) NOT NULL DEFAULT 0,
  `access_payroll` int(11) NOT NULL DEFAULT 0,
  `access_product_information` int(11) NOT NULL DEFAULT 0,
  `access_purchase_order` int(11) NOT NULL DEFAULT 0,
  `access_franchise_branch` int(11) NOT NULL DEFAULT 0,
  `access_franchise_entries` int(11) NOT NULL DEFAULT 0,
  `access_billing_franchising` int(11) NOT NULL DEFAULT 0,
  `access_billing_payments` int(11) NOT NULL DEFAULT 0,
  `access_order_payments` int(11) NOT NULL DEFAULT 0,
  `access_expenses` int(11) NOT NULL DEFAULT 0,
  `access_reports` int(11) NOT NULL DEFAULT 0,
  `access_logs` int(11) NOT NULL DEFAULT 0,
  `access_approval_power` int(11) NOT NULL DEFAULT 0,
  `access_order` int(11) NOT NULL DEFAULT 0,
  `access_order_tracker` int(11) NOT NULL DEFAULT 0,
  `access_return_goods` int(11) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_levels`
--

INSERT INTO `access_levels` (`id`, `description`, `access_dashboard`, `access_access_levels`, `access_user_accounts`, `access_franchise_accounts`, `access_commissary_accounts`, `access_employee_entries`, `access_encode_attendance`, `access_payroll`, `access_product_information`, `access_purchase_order`, `access_franchise_branch`, `access_franchise_entries`, `access_billing_franchising`, `access_billing_payments`, `access_order_payments`, `access_expenses`, `access_reports`, `access_logs`, `access_approval_power`, `access_order`, `access_order_tracker`, `access_return_goods`, `is_deleted`) VALUES
(3, 'Administrator', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0),
(4, 'User', 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(45, 'Customer', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `is_deleted` int(1) DEFAULT 0,
  `date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `role`, `image_file`, `is_deleted`, `date`) VALUES
(1, 'Administrator', 'A', 'Admin', 'admin@gmail.com', 'admin', '$2y$10$7e9rSp7gC.GslurbeSlIL.6NnSdN6/ejq0u/ueIn.00Kc7bMnxWBa', '3', 'uploads/532d0341186be12412c6a4362cc08710.jpg', 0, '2021-11-11 07:55:12');

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `total_minutes` decimal(10,2) NOT NULL,
  `regular_legal_ot` decimal(10,2) NOT NULL,
  `special_ot` decimal(10,2) NOT NULL,
  `special_holiday` decimal(10,2) NOT NULL,
  `legal_holiday` decimal(10,2) NOT NULL,
  `total_hours` decimal(11,2) NOT NULL,
  `legend` varchar(50) NOT NULL,
  `work_date` date NOT NULL,
  `added_by` text NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `emp_id`, `total_minutes`, `regular_legal_ot`, `special_ot`, `special_holiday`, `legal_holiday`, `total_hours`, `legend`, `work_date`, `added_by`, `is_deleted`, `date_time`) VALUES
(26, 7, '9.00', '38.00', '24.00', '0.00', '0.00', '1.28', 'PRESENT', '2012-08-26', 'admin', 0, '2021-10-12 04:26:26'),
(27, 7, '480.00', '0.00', '0.00', '0.00', '0.00', '8.00', 'PRESENT', '2012-08-27', 'admin', 0, '2021-10-13 23:47:01'),
(28, 10, '480.00', '0.00', '0.00', '0.00', '0.00', '8.00', 'PRESENT', '2021-10-14', 'admin', 0, '2021-10-13 23:47:36'),
(29, 5, '92.00', '98.00', '62.00', '0.00', '0.00', '4.35', 'PRESENT', '2014-04-07', 'admin', 0, '2021-10-13 23:53:15'),
(30, 7, '51.00', '100.00', '56.00', '0.00', '0.00', '3.55', 'DAY-OFF', '1993-07-06', 'admin', 0, '2021-10-13 23:53:19'),
(31, 5, '48.00', '42.00', '38.00', '0.00', '0.00', '2.31', 'DAY-OFF', '2018-08-25', 'admin', 0, '2021-10-13 23:53:22'),
(32, 12, '92.00', '81.00', '7.00', '0.00', '0.00', '3.10', 'PRESENT', '1973-07-25', 'admin', 0, '2021-10-13 23:53:25'),
(33, 10, '86.00', '77.00', '46.00', '0.00', '0.00', '3.31', 'DAY-OFF', '1980-02-11', 'admin', 0, '2021-10-13 23:53:30'),
(34, 9, '100.00', '100.00', '62.00', '0.00', '0.00', '4.45', 'DAY-OFF', '2007-01-28', 'admin', 0, '2021-10-13 23:53:33'),
(35, 7, '39.00', '6.00', '21.00', '0.00', '0.00', '1.27', 'DAY-OFF', '1985-10-27', 'admin', 0, '2021-10-13 23:53:36'),
(36, 13, '3.00', '66.00', '51.00', '480.00', '480.00', '10.00', 'DAY-OFF', '1994-02-25', 'admin', 0, '2021-10-15 07:06:26'),
(37, 1, '6240.00', '0.00', '0.00', '0.00', '0.00', '104.00', 'PRESENT', '2021-10-22', 'admin', 0, '2021-10-15 07:22:43');

-- --------------------------------------------------------

--
-- Table structure for table `billing_franchise`
--

CREATE TABLE `billing_franchise` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `total_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `or_number` text NOT NULL,
  `invoice_number` text DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'UNPAID',
  `remarks` text NOT NULL,
  `added_by` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `billing_franchise`
--

INSERT INTO `billing_franchise` (`id`, `franchise_id`, `description`, `total_amount`, `or_number`, `invoice_number`, `status`, `remarks`, `added_by`, `date_time`, `is_deleted`) VALUES
(11, 15, 'FRANCHISING FEE', '5000.00', '', '', 'UNPAID', '', 'admin', '2021-11-14 00:31:54', 0),
(12, 15, 'OTHER FRANCHISING INCLUSION', '8500.00', '1111', '1111', 'PAID', '', 'admin', '2021-11-14 00:43:30', 0),
(13, 16, 'FRANCHISING FEE', '2720.00', '111122', '11122', 'PAID', '', 'admin', '2021-11-14 00:57:42', 0);

-- --------------------------------------------------------

--
-- Table structure for table `billing_payments`
--

CREATE TABLE `billing_payments` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) DEFAULT NULL,
  `bill_id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `check_no` text DEFAULT NULL,
  `check_details` varchar(50) DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `date_paid` date DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `status` varchar(50) DEFAULT 'PENDING',
  `or_number` text DEFAULT NULL,
  `invoice_number` text DEFAULT NULL,
  `approved_by` varchar(100) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `billing_payments`
--

INSERT INTO `billing_payments` (`id`, `franchise_id`, `bill_id`, `description`, `total_amount`, `image_file`, `check_no`, `check_details`, `bank`, `date_paid`, `remarks`, `status`, `or_number`, `invoice_number`, `approved_by`, `approved_date`, `is_deleted`, `date_time`) VALUES
(31, 15, 11, ' FRANCHISING FEE ', '5000.00', NULL, '', '', '', '2021-11-14', '', 'REJECTED', '', '', '', NULL, 0, NULL),
(32, 15, 11, ' FRANCHISING FEE ', '5000.00', 'uploads/alb7.jpg', '', '', '', '2021-11-14', '', 'REJECTED', '', '', '', NULL, 0, NULL),
(33, 15, 12, ' OTHER FRANCHISING INCLUSION ', '8500.00', NULL, '', '', '', '2021-11-14', '', 'APPROVED', '1111', '1111', 'admin', '2021-11-14 08:43:30', 0, NULL),
(34, 16, 13, ' FRANCHISING FEE ', '2720.00', NULL, '', '', '', '2021-11-14', '', 'APPROVED', '111122', '11122', 'admin', '2021-11-14 08:57:42', 0, NULL),
(35, 15, 11, ' FRANCHISING FEE ', '5000.00', 'uploads/eternals-k7-1920x1080.jpg', '', '', '', '2021-12-03', '', 'PENDING', NULL, NULL, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `description`, `is_deleted`, `date_time`) VALUES
(1, 'Cart 20', 0, '2021-10-23 21:31:07'),
(2, 'Cart 1', 0, '2021-10-23 21:53:48'),
(3, 'Cart 11', 0, '2021-10-23 22:22:17');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `type`, `category_name`, `is_deleted`, `date_time`) VALUES
(4, 'Non-Operational', 'Taxes', 0, '2021-10-24 00:02:08'),
(5, 'Non-Operational', 'Interest', 0, '2021-10-24 00:02:16'),
(6, 'Operational', 'Cost of Sales', 0, '2021-10-24 00:02:43'),
(7, 'Operational', 'Marketing, Advertising and Promotion', 0, '2021-10-24 00:03:14'),
(8, 'Operational', 'Travel Expenses', 0, '2021-10-24 00:03:25'),
(9, 'Operational', 'Administrative Expenses', 0, '2021-10-24 00:03:40'),
(10, 'Operational', 'Rent and Insurance', 0, '2021-10-24 00:04:00'),
(11, 'Operational', 'Depreciation and Amortization', 0, '2021-10-24 00:04:18'),
(12, 'Operational', 'Commission', 0, '2021-10-24 00:04:33'),
(13, 'Operational', 'In house expenses', 0, '2021-10-24 00:04:48');

-- --------------------------------------------------------

--
-- Table structure for table `commissary_accounts`
--

CREATE TABLE `commissary_accounts` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `image_file` text NOT NULL,
  `is_deleted` int(1) DEFAULT 0,
  `date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `commissary_accounts`
--

INSERT INTO `commissary_accounts` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `image_file`, `is_deleted`, `date`) VALUES
(4, 'Commissary', 'C', 'Commissary', 'commissary@gmail.com', 'commissary', '$2y$10$LYhi9uQUgs5pOSI9sqgF8.jG7Yab94vXpe5qDQQsL/YYAN30V9coC', 'uploads/The Hungry Pita_Accounting_BG.png', 0, '2021-11-30 23:10:50');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `employment_date` date DEFAULT NULL,
  `regularization_date` date DEFAULT NULL,
  `employment_type` varchar(100) DEFAULT NULL,
  `rate` decimal(11,2) DEFAULT NULL,
  `date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(11) NOT NULL,
  `date_paid` date DEFAULT NULL,
  `expense_type` varchar(50) DEFAULT NULL,
  `voucher_number` int(11) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `sub_category` varchar(50) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `added_by` varchar(50) DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `formulation`
--

CREATE TABLE `formulation` (
  `form_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `date_process` date NOT NULL,
  `remarks` text NOT NULL,
  `added_by` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `formulation`
--

INSERT INTO `formulation` (`form_id`, `item_id`, `description`, `date_process`, `remarks`, `added_by`, `date_time`, `is_deleted`) VALUES
(5, 21, 'Sample ITEM 1', '2021-12-04', 'N/A', 'commissary', '2021-12-03 22:45:24', 0),
(6, 24, 'SAmple for Item 4', '2021-12-04', 'N/A', 'commissary', '2021-12-03 22:51:55', 0),
(7, 22, 'Sample Formulation for Sample Item 2', '2021-12-04', 'N/A', 'commissary', '2021-12-04 06:54:38', 0),
(8, 23, 'For Item 3', '2021-12-05', 'N/A', 'commissary', '2021-12-05 02:10:39', 0);

-- --------------------------------------------------------

--
-- Table structure for table `formulation_details`
--

CREATE TABLE `formulation_details` (
  `id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `raw_item_id` int(11) NOT NULL,
  `qty` float NOT NULL,
  `uom` varchar(50) NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `formulation_details`
--

INSERT INTO `formulation_details` (`id`, `form_id`, `raw_item_id`, `qty`, `uom`, `remarks`) VALUES
(8, 4, 25, 25, 'pax', 'N/A'),
(9, 4, 27, 6, 'pax', 'N/A'),
(10, 4, 29, 10, 'pax', 'N/A'),
(11, 2, 25, 25, 'pax', 'N/A'),
(12, 2, 28, 25, 'pax', 'N/A'),
(13, 5, 25, 2, 'pc', 'N/A'),
(14, 5, 26, 2, 'pc', 'N/A'),
(15, 5, 28, 5, 'pc', 'N/A'),
(16, 6, 27, 10, 'pc', 'N/A'),
(17, 7, 25, 5, 'pc', 'N/A'),
(18, 7, 26, 5, 'pc', 'N/A'),
(19, 7, 28, 5, 'pc', 'N/A'),
(20, 8, 25, 5, 'pc', 'N/A'),
(21, 8, 26, 5, 'pc', 'N/A'),
(22, 8, 27, 2, 'pc', 'N/A'),
(23, 8, 28, 8, 'pc', 'N/A'),
(24, 8, 29, 5, 'pc', 'N/A');

-- --------------------------------------------------------

--
-- Table structure for table `franchisee`
--

CREATE TABLE `franchisee` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `mode_of_transaction` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `contactno` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `tin_number` text DEFAULT NULL,
  `package_type` varchar(50) DEFAULT NULL,
  `date_contract_signing` date DEFAULT NULL,
  `date_branch_opening` date DEFAULT NULL,
  `date_contract_expiry` date DEFAULT NULL,
  `billing_balance` decimal(10,2) DEFAULT 0.00,
  `order_balance` decimal(10,2) DEFAULT 0.00,
  `total_amount` decimal(10,2) DEFAULT 0.00,
  `last_payment_amount` decimal(10,2) DEFAULT NULL,
  `last_payment_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchisee`
--

INSERT INTO `franchisee` (`id`, `branch_id`, `mode_of_transaction`, `name`, `address`, `contactno`, `birthday`, `tin_number`, `package_type`, `date_contract_signing`, `date_branch_opening`, `date_contract_expiry`, `billing_balance`, `order_balance`, `total_amount`, `last_payment_amount`, `last_payment_date`, `is_deleted`, `date_time`) VALUES
(15, 12, 'Delivery', 'Franchise 1', 'Address', '09107102498', '2021-11-17', '33444434', '1', '2021-11-19', '2021-12-01', '2022-01-28', '0.00', '12500.00', '12500.00', '5500.00', '2021-12-05 00:00:00', 0, '2021-12-05 02:30:25'),
(16, 12, 'Pick-up', 'Blaine Gibbs', 'Suscipit exercitatio', '92', '2004-10-06', '269', '2', '1974-01-04', '1993-11-22', '1970-09-27', '0.00', '7500.00', '7500.00', '2720.00', '2021-11-14 00:00:00', 0, '2021-12-05 02:34:10');

-- --------------------------------------------------------

--
-- Table structure for table `franchise_accounts`
--

CREATE TABLE `franchise_accounts` (
  `id` int(11) NOT NULL,
  `franchisee_id` int(11) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `is_deleted` int(1) DEFAULT 0,
  `image_file` text DEFAULT '',
  `date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchise_accounts`
--

INSERT INTO `franchise_accounts` (`id`, `franchisee_id`, `first_name`, `middle_name`, `last_name`, `email`, `username`, `password`, `is_deleted`, `image_file`, `date`) VALUES
(4, 15, 'Franchise 1', 'F', 'Franchise 1', 'franchise@gmail.com', 'test', '$2y$10$3K0lir.wBKzkoHlY/8LBv.s4ne5O0S.jkxPsq4NrszMUxMrWzropW', 0, '', '2021-11-11 23:21:24'),
(5, 16, 'Harper', 'Jemima Stokes', 'Vazquez', 'cezun@mailinator.com', 'test2', '$2y$10$L47n04is8vXthPHVqX7/NeGD7uCxZKjkY8WLeMlik/WyILo.UBNzK', 0, '', '2021-11-14 00:54:07');

-- --------------------------------------------------------

--
-- Table structure for table `franchise_branch`
--

CREATE TABLE `franchise_branch` (
  `id` int(11) NOT NULL,
  `branch_code` varchar(100) NOT NULL,
  `branch_name` varchar(100) NOT NULL,
  `branch_location` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `franchise_branch`
--

INSERT INTO `franchise_branch` (`id`, `branch_code`, `branch_name`, `branch_location`, `is_deleted`, `date_time`) VALUES
(12, 'B-0001', 'Branch 1', 'Branch 1', 0, '2021-11-11 23:19:22');

-- --------------------------------------------------------

--
-- Table structure for table `franchise_renewal_log`
--

CREATE TABLE `franchise_renewal_log` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `renewed_date` date DEFAULT NULL,
  `new_expiry_date` date DEFAULT NULL,
  `approved_by` text DEFAULT NULL,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `account_id` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `module` text NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `account_id`, `description`, `ip`, `hostname`, `timestamp`, `module`, `is_deleted`) VALUES
(959, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:18:44', 'ACCOUNTING', 0),
(960, 'Administrator A Admin', 'Add account: branch 1', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:19:22', 'ACCOUNTING', 0),
(961, 'Administrator A Admin', 'Add franchisee: franchise 1', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:20:00', 'ACCOUNTING', 0),
(962, 'Administrator A Admin', 'Add supplier name: supplier 1', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:20:20', 'ACCOUNTING', 0),
(963, 'Administrator A Admin', 'Add product: item 1', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:20:38', 'ACCOUNTING', 0),
(964, 'Administrator A Admin', 'Add product: item 2', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:20:51', 'ACCOUNTING', 0),
(965, 'Administrator A Admin', 'Add account: franchise 1', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:21:24', 'ACCOUNTING', 0),
(966, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-11 23:21:36', 'FRANCHISEE', 0),
(967, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-12 00:04:38', 'FRANCHISEE', 0),
(968, 'Franchise 1 F Franchise 1', 'Add order payment: 15', '::1', 'DESKTOP-FO8SD5D', '2021-11-12 00:04:51', 'FRANCHISEE', 0),
(969, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-12 08:27:16', 'FRANCHISEE', 0),
(970, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:06:36', 'ACCOUNTING', 0),
(971, 'Administrator A Admin', 'Edit product id 22', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:13:30', 'ACCOUNTING', 0),
(972, 'Administrator A Admin', 'Edit product id 22', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:14:09', 'ACCOUNTING', 0),
(973, 'Administrator A Admin', 'Edit product id 21', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:14:39', 'ACCOUNTING', 0),
(974, 'Administrator A Admin', 'Edit product id 22', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:14:48', 'ACCOUNTING', 0),
(975, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:16:35', 'ACCOUNTING', 0),
(976, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:16:52', 'FRANCHISEE', 0),
(977, 'Franchise 1 F Franchise 1', 'Edit order payment id 26', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:30:38', 'FRANCHISEE', 0),
(978, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:31:39', 'ACCOUNTING', 0),
(979, 'Administrator A Admin', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:31:54', 'ACCOUNTING', 0),
(980, 'Franchise 1 F Franchise 1', 'Add billing payment: 11 | franchising fee | 5000.00', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:36:31', 'FRANCHISEE', 0),
(981, 'Administrator A Admin', 'Edit billing payment id 31', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:40:04', 'ACCOUNTING', 0),
(982, 'Franchise 1 F Franchise 1', 'Add billing payment: 11 | franchising fee | 5000.00', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:40:12', 'FRANCHISEE', 0),
(983, 'Administrator A Admin', 'Add bill to franchise: other franchising inclusion', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:40:34', 'ACCOUNTING', 0),
(984, 'Franchise 1 F Franchise 1', 'Add billing payment: 12 | other franchising inclusion | 8500.00', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:40:44', 'FRANCHISEE', 0),
(985, 'Administrator A Admin', 'Edit billing payment id 33', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:43:30', 'ACCOUNTING', 0),
(986, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:50:17', 'FRANCHISEE', 0),
(987, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:50:31', 'FRANCHISEE', 0),
(988, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:51:00', 'FRANCHISEE', 0),
(989, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:51:15', 'FRANCHISEE', 0),
(990, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:51:38', 'FRANCHISEE', 0),
(991, '  ', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:52:46', 'FRANCHISEE', 0),
(992, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:53:18', 'FRANCHISEE', 0),
(993, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:53:37', 'ACCOUNTING', 0),
(994, 'Administrator A Admin', 'Add franchisee: blaine gibbs', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:53:51', 'ACCOUNTING', 0),
(995, 'Administrator A Admin', 'Add account: harper', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:54:07', 'ACCOUNTING', 0),
(996, '  ', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:54:13', 'FRANCHISEE', 0),
(997, 'Harper Jemima Stokes Vazquez', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:54:20', 'FRANCHISEE', 0),
(998, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:56:33', 'FRANCHISEE', 0),
(999, 'Harper Jemima Stokes Vazquez', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:56:51', 'FRANCHISEE', 0),
(1000, 'Administrator A Admin', 'Add bill to franchise: franchising fee', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:57:07', 'ACCOUNTING', 0),
(1001, 'Harper Jemima Stokes Vazquez', 'Add billing payment: 13 | franchising fee | 2720.00', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:57:27', 'FRANCHISEE', 0),
(1002, 'Administrator A Admin', 'Edit billing payment id 34', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:57:42', 'ACCOUNTING', 0),
(1003, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 00:58:19', 'FRANCHISEE', 0),
(1004, 'Administrator A Admin', 'Add product: item 3', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:12:18', 'ACCOUNTING', 0),
(1005, 'Administrator A Admin', 'Add product: item 4', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:12:34', 'ACCOUNTING', 0),
(1006, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:13:31', 'FRANCHISEE', 0),
(1007, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:13:49', 'FRANCHISEE', 0),
(1008, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:14:08', 'FRANCHISEE', 0),
(1009, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:14:23', 'FRANCHISEE', 0),
(1010, 'Administrator A Admin', 'Edit product id 24', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 02:24:40', 'ACCOUNTING', 0),
(1011, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 04:57:52', 'FRANCHISEE', 0),
(1012, 'Harper Jemima Stokes Vazquez', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 05:04:10', 'FRANCHISEE', 0),
(1013, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 05:33:57', 'FRANCHISEE', 0),
(1014, 'Harper Jemima Stokes Vazquez', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 05:47:12', 'FRANCHISEE', 0),
(1015, 'Administrator A Admin', 'Edit billing payment id 32', '::1', 'DESKTOP-FO8SD5D', '2021-11-14 05:47:47', 'ACCOUNTING', 0),
(1016, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 22:58:00', 'ACCOUNTING', 0),
(1017, 'Administrator A Admin', 'Add account: commissary', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 22:58:44', 'ACCOUNTING', 0),
(1018, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:03:22', 'ACCOUNTING', 0),
(1019, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:03:29', 'ACCOUNTING', 0),
(1020, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:12:18', 'ACCOUNTING', 0),
(1021, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:14:23', 'ACCOUNTING', 0),
(1022, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:41:11', 'ACCOUNTING', 0),
(1023, 'Administrator A Admin', 'Add purchase order: d-0001', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:41:41', 'ACCOUNTING', 0),
(1024, 'Administrator A Admin', 'Edit purcahse order id 12', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:43:03', 'ACCOUNTING', 0),
(1025, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:47:21', 'ACCOUNTING', 0),
(1026, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:51:12', 'ACCOUNTING', 0),
(1027, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-11-30 23:53:01', 'ACCOUNTING', 0),
(1028, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-01 00:13:27', 'ACCOUNTING', 0),
(1029, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-01 00:14:03', 'ACCOUNTING', 0),
(1030, 'Commissary C Commissary', 'Add purchase order: d-0002', '::1', 'DESKTOP-FO8SD5D', '2021-12-01 00:50:33', 'ACCOUNTING', 0),
(1031, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-01 06:13:57', 'ACCOUNTING', 0),
(1032, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-01 23:06:42', 'ACCOUNTING', 0),
(1033, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 04:02:22', 'ACCOUNTING', 0),
(1034, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 07:35:48', 'ACCOUNTING', 0),
(1035, 'Administrator A Admin', 'Add purchase order: d-0001', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 07:36:03', 'ACCOUNTING', 0),
(1036, 'Administrator A Admin', 'Add purchase order: d-0002', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 07:36:30', 'ACCOUNTING', 0),
(1037, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 07:36:49', 'ACCOUNTING', 0),
(1038, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 08:26:35', 'FRANCHISEE', 0),
(1039, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:06:23', 'ACCOUNTING', 0),
(1040, 'Franchise 1 F Franchise 1', 'Add billing payment: 11 | franchising fee | 5000.00', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:21:27', 'FRANCHISEE', 0),
(1041, 'Administrator A Admin', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:22:13', 'ACCOUNTING', 0),
(1042, 'Commissary C Commissary', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:23:41', 'ACCOUNTING', 0),
(1043, 'Commissary C Commissary', 'Add product: raw mat 1', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:33:24', 'ACCOUNTING', 0),
(1044, 'Commissary C Commissary', 'Add product: raw mat 2', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:33:46', 'ACCOUNTING', 0),
(1045, 'Commissary C Commissary', 'Add product: raw mat 3', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:34:12', 'ACCOUNTING', 0),
(1046, 'Commissary C Commissary', 'Add product: raw mat 4', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:34:33', 'ACCOUNTING', 0),
(1047, 'Commissary C Commissary', 'Add product: raw mat 5', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:34:58', 'ACCOUNTING', 0),
(1048, 'Commissary C Commissary', 'Add formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:53:07', 'ACCOUNTING', 0),
(1049, 'Commissary C Commissary', 'Add formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:53:13', 'ACCOUNTING', 0),
(1050, 'Commissary C Commissary', 'Add formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-02 23:54:33', 'ACCOUNTING', 0),
(1051, 'Commissary C Commissary', 'Edit formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:08:57', 'COMMISSARY', 0),
(1052, 'Commissary C Commissary', 'Delete formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:10:05', 'COMMISSARY', 0),
(1053, 'Commissary C Commissary', 'Add formulation: 21', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:10:17', 'COMMISSARY', 0),
(1054, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:28:14', 'COMMISSARY', 0),
(1055, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:32:19', 'COMMISSARY', 0),
(1056, 'Commissary C Commissary', 'Add formulation: 22', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:33:03', 'COMMISSARY', 0),
(1057, 'Commissary C Commissary', 'Addraw formulation: 27', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:33:17', 'COMMISSARY', 0),
(1058, 'Commissary C Commissary', 'Delete formulation: 22', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:34:28', 'COMMISSARY', 0),
(1059, 'Commissary C Commissary', 'Add formulation: 22', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:37:30', 'COMMISSARY', 0),
(1060, 'Commissary C Commissary', 'Addraw formulation: 26', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:37:37', 'COMMISSARY', 0),
(1061, 'Commissary C Commissary', 'Addraw formulation: 26', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:38:32', 'COMMISSARY', 0),
(1062, 'Commissary C Commissary', 'Edit formulation: 22', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:38:46', 'COMMISSARY', 0),
(1063, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:38:58', 'COMMISSARY', 0),
(1064, 'Commissary C Commissary', 'Addraw formulation: 29', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:39:02', 'COMMISSARY', 0),
(1065, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:40:23', 'COMMISSARY', 0),
(1066, 'Commissary C Commissary', 'Addraw formulation: 27', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:40:28', 'COMMISSARY', 0),
(1067, 'Commissary C Commissary', 'Addraw formulation: 29', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:40:32', 'COMMISSARY', 0),
(1068, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:40:57', 'COMMISSARY', 0),
(1069, 'Commissary C Commissary', 'Addraw formulation: 28', '::1', 'DESKTOP-FO8SD5D', '2021-12-03 00:44:18', 'COMMISSARY', 0),
(1070, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-03 12:16:54', 'COMMISSARY', 0),
(1071, 'Commissary C Commissary', 'New production plan :jo 1', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:11:09', 'COMMISSARY', 0),
(1072, 'Commissary C Commissary', 'New production plan :sample jo', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:24:00', 'COMMISSARY', 0),
(1073, 'Commissary C Commissary', 'New production plan :sample jo2', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:24:21', 'COMMISSARY', 0),
(1074, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:30:23', 'COMMISSARY', 0),
(1075, 'Commissary C Commissary', 'New production plan :sample 1', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:30:57', 'COMMISSARY', 0),
(1076, 'Commissary C Commissary', 'New production plan :sample 2', '::1', 'LAPTOP-UI866FP5', '2021-12-03 13:31:18', 'COMMISSARY', 0),
(1077, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:08:24', 'COMMISSARY', 0),
(1078, 'Commissary C Commissary', 'New production plan :sample', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:42:15', 'COMMISSARY', 0),
(1079, 'Commissary C Commissary', 'Add formulation: 21', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:45:24', 'COMMISSARY', 0),
(1080, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:45:32', 'COMMISSARY', 0),
(1081, 'Commissary C Commissary', 'Addraw formulation: 26', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:45:38', 'COMMISSARY', 0),
(1082, 'Commissary C Commissary', 'Addraw formulation: 28', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:45:44', 'COMMISSARY', 0),
(1083, 'Commissary C Commissary', 'Add formulation: 24', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:45:58', 'COMMISSARY', 0),
(1084, 'Commissary C Commissary', 'Edit formulation: 21', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:46:16', 'COMMISSARY', 0),
(1085, 'Commissary C Commissary', 'Addraw formulation: 27', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:46:27', 'COMMISSARY', 0),
(1086, 'Commissary C Commissary', 'New production plan :job order1', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:46:57', 'COMMISSARY', 0),
(1087, 'Commissary C Commissary', 'New production plan :job order1', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:47:50', 'COMMISSARY', 0),
(1088, 'Commissary C Commissary', 'New production plan :test2', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:48:44', 'COMMISSARY', 0),
(1089, 'Commissary C Commissary', 'New production plan :test1', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:50:25', 'COMMISSARY', 0),
(1090, 'Commissary C Commissary', 'Edit formulation: 24', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:51:55', 'COMMISSARY', 0),
(1091, 'Commissary C Commissary', 'New production plan :test2', '::1', 'LAPTOP-UI866FP5', '2021-12-03 22:52:24', 'COMMISSARY', 0),
(1092, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:13:09', 'COMMISSARY', 0),
(1093, 'Commissary C Commissary', 'New production plan :test again', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:17:16', 'COMMISSARY', 0),
(1094, 'Administrator A Admin', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:37:42', 'ACCOUNTING', 0),
(1095, 'Administrator A Admin', 'Add purchase order: d-0003', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:38:00', 'ACCOUNTING', 0),
(1096, 'Administrator A Admin', 'Edit purcahse order id 16', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:38:36', 'ACCOUNTING', 0),
(1097, 'Commissary C Commissary', 'Marked production plan id as finished:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:42:29', 'COMMISSARY', 0),
(1098, 'Commissary C Commissary', 'Marked production plan id as finished:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:48:21', 'COMMISSARY', 0),
(1099, 'Commissary C Commissary', 'Marked production plan id as finished:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:48:21', 'COMMISSARY', 0),
(1100, 'Commissary C Commissary', 'Marked production plan id as finished:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:49:28', 'COMMISSARY', 0),
(1101, 'Commissary C Commissary', 'Marked production plan id as finished:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:49:28', 'COMMISSARY', 0),
(1102, 'Commissary C Commissary', 'Marked production plan id as finished:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:51:00', 'COMMISSARY', 0),
(1103, 'Commissary C Commissary', 'Marked production plan id as finished:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:51:00', 'COMMISSARY', 0),
(1104, 'Commissary C Commissary', 'Updated production plan id:3', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:57:21', 'COMMISSARY', 0),
(1105, 'Commissary C Commissary', 'New production plan :test3', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:58:47', 'COMMISSARY', 0),
(1106, 'Commissary C Commissary', 'Updated production plan id:4', '::1', 'LAPTOP-UI866FP5', '2021-12-04 04:59:01', 'COMMISSARY', 0),
(1107, 'Commissary C Commissary', 'New production plan :test1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 05:01:59', 'COMMISSARY', 0),
(1108, 'Commissary C Commissary', 'New production plan :test1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 05:03:41', 'COMMISSARY', 0),
(1109, 'Commissary C Commissary', 'Updated production plan id:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 05:03:55', 'COMMISSARY', 0),
(1110, 'Commissary C Commissary', 'Marked production plan id as finished:1', '::1', 'LAPTOP-UI866FP5', '2021-12-04 05:06:03', 'COMMISSARY', 0),
(1111, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:05:42', 'COMMISSARY', 0),
(1112, '  ', 'Logged in', '192.168.137.103', 'HUAWEI_Mate_30_Pro-f24a83.mshome.net', '2021-12-04 06:09:10', 'FRANCHISEE', 0),
(1113, 'Franchise 1 F Franchise 1', 'Logged in', '192.168.137.103', 'HUAWEI_Mate_30_Pro-f24a83.mshome.net', '2021-12-04 06:09:17', 'FRANCHISEE', 0),
(1114, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:14:24', 'COMMISSARY', 0),
(1115, '  ', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:14:56', 'FRANCHISEE', 0),
(1116, 'Franchise 1 F Franchise 1', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:14:59', 'FRANCHISEE', 0),
(1117, 'Administrator A Admin', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:16:04', 'ACCOUNTING', 0),
(1118, 'Commissary C Commissary', 'Add formulation: 22', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:54:38', 'COMMISSARY', 0),
(1119, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:54:57', 'COMMISSARY', 0),
(1120, 'Commissary C Commissary', 'Addraw formulation: 26', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:55:26', 'COMMISSARY', 0),
(1121, 'Commissary C Commissary', 'Addraw formulation: 28', '::1', 'LAPTOP-UI866FP5', '2021-12-04 06:55:31', 'COMMISSARY', 0),
(1122, 'Commissary C Commissary', 'New production plan :test jo', '::1', 'LAPTOP-UI866FP5', '2021-12-04 07:02:57', 'COMMISSARY', 0),
(1123, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-04 23:35:42', 'COMMISSARY', 0),
(1124, 'Commissary C Commissary', 'Marked production plan id as finished:2', '::1', 'LAPTOP-UI866FP5', '2021-12-04 23:44:21', 'COMMISSARY', 0),
(1125, 'Commissary C Commissary', 'New production plan :test3', '::1', 'LAPTOP-UI866FP5', '2021-12-04 23:49:30', 'COMMISSARY', 0),
(1126, 'Commissary C Commissary', 'New production plan :test', '::1', 'LAPTOP-UI866FP5', '2021-12-05 00:11:47', 'COMMISSARY', 0),
(1127, 'Commissary C Commissary', 'Updated production plan id:4', '::1', 'LAPTOP-UI866FP5', '2021-12-05 00:12:01', 'COMMISSARY', 0),
(1128, 'Commissary C Commissary', 'New production plan :', '::1', 'LAPTOP-UI866FP5', '2021-12-05 00:17:01', 'COMMISSARY', 0),
(1129, 'Commissary C Commissary', 'Updated production plan id:5', '::1', 'LAPTOP-UI866FP5', '2021-12-05 00:17:37', 'COMMISSARY', 0),
(1130, 'Commissary C Commissary', 'Updated production plan id:5', '::1', 'LAPTOP-UI866FP5', '2021-12-05 00:17:54', 'COMMISSARY', 0),
(1131, 'Commissary C Commissary', 'Marked production plan id as finished:4', '::1', 'LAPTOP-UI866FP5', '2021-12-05 00:30:34', 'COMMISSARY', 0),
(1132, 'Administrator A Admin', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-05 00:53:02', 'ACCOUNTING', 0),
(1133, 'Administrator A Admin', 'Add purchase order: d-0004', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:38:30', 'ACCOUNTING', 0),
(1134, 'Commissary C Commissary', 'New production plan :sample test again', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:45:22', 'COMMISSARY', 0),
(1135, 'Commissary C Commissary', 'Updated production plan id:6', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:45:51', 'COMMISSARY', 0),
(1136, 'Commissary C Commissary', 'Marked production plan id as finished:6', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:46:11', 'COMMISSARY', 0),
(1137, 'Commissary C Commissary', 'New production plan :test', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:53:45', 'COMMISSARY', 0),
(1138, 'Commissary C Commissary', 'New production plan :test again', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:55:40', 'COMMISSARY', 0),
(1139, 'Commissary C Commissary', 'Updated production plan id:8', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:56:03', 'COMMISSARY', 0),
(1140, 'Commissary C Commissary', 'Marked production plan id as finished:8', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:56:23', 'COMMISSARY', 0),
(1141, 'Administrator A Admin', 'Add purchase order: d-0005', '::1', 'LAPTOP-UI866FP5', '2021-12-05 01:57:49', 'ACCOUNTING', 0),
(1142, 'Commissary C Commissary', 'New production plan :for item 4', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:00:40', 'COMMISSARY', 0),
(1143, 'Commissary C Commissary', 'Marked production plan id as finished:9', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:00:57', 'COMMISSARY', 0),
(1144, 'Administrator A Admin', 'Add purchase order: d-0006', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:05:55', 'ACCOUNTING', 0),
(1145, 'Commissary C Commissary', 'New production plan :test again', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:09:05', 'COMMISSARY', 0),
(1146, 'Commissary C Commissary', 'Marked production plan id as finished:10', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:09:29', 'COMMISSARY', 0),
(1147, 'Commissary C Commissary', 'Add formulation: 23', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:10:39', 'COMMISSARY', 0),
(1148, 'Commissary C Commissary', 'Addraw formulation: 25', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:10:53', 'COMMISSARY', 0),
(1149, 'Commissary C Commissary', 'Addraw formulation: 26', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:10:57', 'COMMISSARY', 0),
(1150, 'Commissary C Commissary', 'Addraw formulation: 27', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:11:05', 'COMMISSARY', 0),
(1151, 'Commissary C Commissary', 'Addraw formulation: 28', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:11:11', 'COMMISSARY', 0),
(1152, 'Commissary C Commissary', 'Addraw formulation: 29', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:11:21', 'COMMISSARY', 0),
(1153, 'Commissary C Commissary', 'New production plan :test', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:11:44', 'COMMISSARY', 0),
(1154, 'Commissary C Commissary', 'Marked production plan id as finished:11', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:12:07', 'COMMISSARY', 0),
(1155, 'Franchise 1 F Franchise 1', 'Delete order payment id 26', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:19:37', 'FRANCHISEE', 0),
(1156, 'Franchise 1 F Franchise 1', 'Add order payment: 15', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:20:21', 'FRANCHISEE', 0),
(1157, 'Administrator A Admin', 'Edit order payment id 27', '::1', 'LAPTOP-UI866FP5', '2021-12-05 02:20:48', 'ACCOUNTING', 0),
(1158, 'Commissary C Commissary', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2021-12-05 06:20:29', 'COMMISSARY', 0),
(1159, 'Commissary C Commissary', 'Edit product id 29', '::1', 'LAPTOP-UI866FP5', '2021-12-05 06:41:09', 'COMMISSARY', 0),
(1160, 'Commissary C Commissary', 'Edit product id 24', '::1', 'LAPTOP-UI866FP5', '2021-12-05 06:42:11', 'COMMISSARY', 0),
(1161, 'Commissary C Commissary', 'Edit product id 23', '::1', 'LAPTOP-UI866FP5', '2021-12-05 06:42:17', 'COMMISSARY', 0),
(1162, 'Commissary C Commissary', 'Edit product id 23', '::1', 'LAPTOP-UI866FP5', '2021-12-05 06:42:28', 'COMMISSARY', 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_ref` text NOT NULL,
  `franchisee_id` int(11) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `rec_total_amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `added_by` text NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'PENDING',
  `date_ordered` date DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `approved_by` varchar(50) DEFAULT NULL,
  `approved_date_time` timestamp NULL DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `date_delivered` date DEFAULT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_ref`, `franchisee_id`, `total_amount`, `rec_total_amount`, `added_by`, `status`, `date_ordered`, `date_time`, `approved_by`, `approved_date_time`, `remarks`, `date_delivered`, `is_deleted`) VALUES
(56, 'O-0001', 15, '5500.00', '0.00', 'test', 'RECEIVED', '2021-12-05', '2021-12-05 02:29:16', 'commissary', '2021-12-05 00:45:12', NULL, NULL, 0),
(57, 'O-0002', 15, '7500.00', '0.00', 'test', 'RECEIVED', '2021-12-05', '2021-12-05 02:29:19', 'commissary', '2021-12-05 02:22:15', NULL, NULL, 0),
(58, 'O-0003', 15, '5000.00', '0.00', 'admin', 'RECEIVED', '2021-12-05', '2021-12-05 02:33:13', 'commissary', '2021-12-05 02:30:25', NULL, NULL, 0),
(59, 'O-0004', 16, '7500.00', '0.00', 'admin', 'RECEIVED', '2021-12-05', '2021-12-05 02:49:04', 'commissary', '2021-12-05 02:34:10', NULL, '2021-12-05', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_ref` text NOT NULL,
  `item_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` decimal(10,2) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `rec_qty` decimal(10,2) NOT NULL DEFAULT 0.00,
  `rec_total_amount` decimal(10,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `order_ref`, `item_id`, `price`, `qty`, `total_amount`, `rec_qty`, `rec_total_amount`) VALUES
(174, 56, 'O-0001', 24, '250.00', '10.00', '2500.00', '0.00', '0.00'),
(175, 56, 'O-0001', 23, '300.00', '10.00', '3000.00', '0.00', '0.00'),
(176, 57, 'O-0002', 21, '1500.00', '5.00', '7500.00', '0.00', '0.00'),
(177, 58, 'O-0003', 23, '300.00', '5.00', '1500.00', '0.00', '0.00'),
(178, 58, 'O-0003', 22, '700.00', '5.00', '3500.00', '0.00', '0.00'),
(179, 59, 'O-0004', 21, '1500.00', '5.00', '7500.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `order_payments`
--

CREATE TABLE `order_payments` (
  `id` int(11) NOT NULL,
  `franchise_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `date_paid` date DEFAULT NULL,
  `check_no` int(11) DEFAULT NULL,
  `check_details` varchar(50) DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `status` varchar(50) DEFAULT 'PENDING',
  `or_number` text DEFAULT NULL,
  `invoice_number` text DEFAULT NULL,
  `approved_by` varchar(100) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_payments`
--

INSERT INTO `order_payments` (`id`, `franchise_id`, `order_id`, `total_amount`, `image_file`, `date_paid`, `check_no`, `check_details`, `bank`, `remarks`, `status`, `or_number`, `invoice_number`, `approved_by`, `approved_date`, `is_deleted`, `date_time`) VALUES
(26, 15, NULL, '2720.00', 'uploads/vd5.jpg', '2021-11-14', 0, '', '', '', 'PENDING', NULL, NULL, NULL, NULL, 1, NULL),
(27, 15, NULL, '5500.00', 'uploads/The Hungry Pita_Accounting_BG.png', '2021-12-05', 0, '', '', '', 'APPROVED', '', '', 'admin', '2021-12-05 10:20:48', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_returns`
--

CREATE TABLE `order_returns` (
  `id` int(11) NOT NULL,
  `order_ref_no` varchar(50) NOT NULL,
  `return_ref_no` varchar(50) NOT NULL,
  `franchisee_id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'PENDING',
  `added_by` varchar(50) NOT NULL,
  `remarks` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_returns`
--

INSERT INTO `order_returns` (`id`, `order_ref_no`, `return_ref_no`, `franchisee_id`, `status`, `added_by`, `remarks`, `date_time`, `is_deleted`) VALUES
(14, 'O-0001', 'R-0001', 15, 'PENDING', 'test', 'sira', '2021-12-04 06:45:29', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_return_items`
--

CREATE TABLE `order_return_items` (
  `id` int(11) NOT NULL,
  `return_ref` varchar(50) NOT NULL,
  `item_id` int(11) NOT NULL,
  `order_qty` int(11) NOT NULL,
  `return_qty` int(11) NOT NULL,
  `cost` decimal(10,0) NOT NULL,
  `total_amount` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_return_items`
--

INSERT INTO `order_return_items` (`id`, `return_ref`, `item_id`, `order_qty`, `return_qty`, `cost`, `total_amount`) VALUES
(17, 'R-0001', 23, 0, 3, '300', '900');

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

CREATE TABLE `payroll` (
  `id` int(11) NOT NULL,
  `payroll_no` varchar(100) DEFAULT NULL,
  `cut_off_date` date DEFAULT NULL,
  `release_date` date DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `added_by` varchar(100) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_employee`
--

CREATE TABLE `payroll_employee` (
  `id` int(11) NOT NULL,
  `payroll_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `base_rate` decimal(10,2) NOT NULL,
  `allowance` decimal(10,2) NOT NULL,
  `comm_allowance` decimal(10,2) NOT NULL,
  `ot_reg_day` decimal(10,2) NOT NULL,
  `ot_spcl_day` decimal(10,2) NOT NULL,
  `ot_leg_day` decimal(10,2) NOT NULL,
  `special_holiday` decimal(10,2) NOT NULL,
  `regular_holiday` decimal(10,2) NOT NULL,
  `sss` decimal(10,2) NOT NULL,
  `pag_ibig` decimal(10,2) NOT NULL,
  `philhealth` decimal(10,2) NOT NULL,
  `lates` decimal(10,2) NOT NULL,
  `absent` decimal(10,2) NOT NULL,
  `tax_deduct` decimal(10,2) NOT NULL,
  `sss_loan` decimal(10,2) NOT NULL,
  `pag_ibig_loan` decimal(10,2) NOT NULL,
  `cash_advance` decimal(10,2) NOT NULL,
  `short_remittance` decimal(10,2) NOT NULL,
  `gross_pay` decimal(10,2) NOT NULL,
  `total_deductions` decimal(10,2) NOT NULL,
  `net_pay` decimal(10,2) NOT NULL,
  `total_min_work` decimal(10,2) NOT NULL,
  `reg_legal_ot_work` decimal(10,2) NOT NULL,
  `special_ot_work` decimal(10,2) NOT NULL,
  `special_holiday_work` decimal(10,2) NOT NULL,
  `legal_holiday_work` decimal(10,2) NOT NULL,
  `added_by` varchar(100) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `production`
--

CREATE TABLE `production` (
  `id` int(11) NOT NULL,
  `production_id` int(11) NOT NULL,
  `job_order` varchar(50) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `is_started` int(11) NOT NULL DEFAULT 0,
  `added_by` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `production`
--

INSERT INTO `production` (`id`, `production_id`, `job_order`, `batch_no`, `description`, `start_date`, `end_date`, `is_started`, `added_by`, `date_time`, `is_deleted`) VALUES
(16, 1, 'J-0001', 'B-0001', 'test1', '2021-12-01', '2021-12-03', 2, 'commissary', '2021-12-04 05:06:03', 0),
(17, 2, 'J-0002', 'B-0002', 'test JO', '2021-12-04', '2021-12-04', 2, 'commissary', '2021-12-04 23:44:21', 0),
(18, 3, 'J-0003', 'B-0003', 'test3', '2021-12-06', '2021-12-07', 0, 'commissary', '2021-12-05 00:09:46', 1),
(19, 4, 'J-0004', 'B-0004', 'test', '2021-12-05', '2021-12-05', 2, 'commissary', '2021-12-05 00:30:34', 0),
(20, 5, 'J-0005', 'B-0005', 'test4', '2021-12-05', '2021-12-07', 0, 'commissary', '2021-12-05 00:21:11', 1),
(21, 6, 'J-0006', 'B-0006', 'sample test again', '2021-12-05', '2021-12-05', 2, 'commissary', '2021-12-05 01:46:11', 0),
(22, 7, 'J-0007', 'B-0007', 'test', '2021-12-05', '2021-12-05', 0, 'commissary', '2021-12-05 01:53:45', 0),
(23, 8, 'J-0008', 'B-0008', 'test again', '2021-12-05', '2021-12-05', 2, 'commissary', '2021-12-05 01:56:23', 0),
(24, 9, 'J-0009', 'B-0009', 'for item 4', '2021-12-05', '2021-12-05', 2, 'commissary', '2021-12-05 02:00:57', 0),
(25, 10, 'J-0010', 'B-0010', 'test again', '2021-12-05', '2021-12-05', 2, 'commissary', '2021-12-05 02:09:29', 0),
(26, 11, 'J-0011', 'B-0011', 'test', '2021-12-05', '2021-12-05', 2, 'commissary', '2021-12-05 02:12:07', 0);

-- --------------------------------------------------------

--
-- Table structure for table `production_items`
--

CREATE TABLE `production_items` (
  `id` int(11) NOT NULL,
  `production_id` int(11) NOT NULL,
  `formulation_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `production_qty` float NOT NULL,
  `final_qty` float NOT NULL,
  `credit` float NOT NULL,
  `remarks` text NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `production_items`
--

INSERT INTO `production_items` (`id`, `production_id`, `formulation_id`, `item_id`, `production_qty`, `final_qty`, `credit`, `remarks`, `is_deleted`) VALUES
(28, 1, 5, 21, 10, 0, 0, '', 0),
(29, 1, 6, 24, 10, 0, 0, '', 0),
(30, 2, 7, 22, 10, 0, 0, '', 0),
(31, 2, 6, 24, 10, 0, 0, '', 0),
(32, 3, 5, 21, 10, 0, 0, '', 1),
(35, 4, 6, 24, 5, 0, 0, '', 0),
(36, 4, 7, 22, 15, 0, 0, '', 0),
(41, 6, 7, 22, 5, 0, 0, '', 0),
(42, 6, 6, 24, 5, 0, 0, '', 0),
(43, 7, 6, 24, 1, 0, 0, '', 0),
(47, 8, 5, 21, 10, 0, 0, '', 0),
(48, 8, 6, 24, 10, 0, 0, '', 0),
(49, 8, 7, 22, 10, 0, 0, '', 0),
(50, 9, 6, 24, 20, 0, 0, '', 0),
(51, 10, 5, 21, 10, 0, 0, '', 0),
(52, 10, 6, 24, 10, 0, 0, '', 0),
(53, 10, 7, 22, 5, 0, 0, '', 0),
(54, 11, 8, 23, 15, 0, 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `image_file` text DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `cost` decimal(10,2) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `stocks` int(11) DEFAULT 0,
  `order_count` int(11) NOT NULL DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `supplier_id`, `image_file`, `product_code`, `description`, `cost`, `price`, `category`, `stocks`, `order_count`, `is_deleted`, `date_time`) VALUES
(21, 4, 'uploads/alb1.jpg', 'I-0001', 'ITEM 1', '1000.00', '1500.00', 'FOOD', 0, 1, 0, '2021-12-05 06:41:46'),
(22, 4, 'uploads/vd4.jpg', 'I-0002', 'ITEM 2', '500.00', '700.00', 'FOOD', 0, 1, 0, '2021-12-05 06:41:44'),
(23, 4, 'uploads/vd9.jpg', 'I-0003', 'ITEM 3', '200.00', '300.00', 'FOOD', 0, 5, 0, '2021-12-05 06:42:28'),
(24, 4, 'uploads/vd11.jpg', 'I-0004', 'ITEM 4', '200.00', '250.00', 'FOOD', 0, 20, 0, '2021-12-05 06:42:11'),
(25, 4, NULL, 'I-0005', 'RAW MAT 1', '20.00', '35.00', 'NON FOOD', 95, 1, 0, '2021-12-05 06:41:37'),
(26, 4, NULL, 'I-0006', 'RAW MAT 2', '20.00', '20.00', 'NON FOOD', 145, 1, 0, '2021-12-05 06:41:34'),
(27, 4, NULL, 'I-0007', 'RAW MAT 3', '20.00', '20.00', 'NON FOOD', 170, 1, 0, '2021-12-05 06:41:29'),
(28, 4, NULL, 'I-0008', 'RAW MAT 4', '20.00', '20.00', 'NON FOOD', 155, 1, 0, '2021-12-05 06:41:26'),
(29, 4, NULL, 'I-0009', 'RAW MAT 5', '20.00', '20.00', 'NON FOOD', 275, 10, 0, '2021-12-05 06:41:09');

-- --------------------------------------------------------

--
-- Table structure for table `products_itemize`
--

CREATE TABLE `products_itemize` (
  `id` int(11) NOT NULL,
  `item_count` int(11) NOT NULL COMMENT 'Quantity counter ',
  `item_id` int(11) NOT NULL,
  `process` varchar(50) NOT NULL,
  `ref_no` varchar(50) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `is_available` int(11) NOT NULL DEFAULT 0,
  `manufacturing` date NOT NULL,
  `expiry` date NOT NULL,
  `order_ref` varchar(50) DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products_itemize`
--

INSERT INTO `products_itemize` (`id`, `item_count`, `item_id`, `process`, `ref_no`, `batch_no`, `is_available`, `manufacturing`, `expiry`, `order_ref`, `date_time`) VALUES
(2801, 1, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2802, 2, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2803, 3, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2804, 4, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2805, 5, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2806, 6, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2807, 7, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2808, 8, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2809, 9, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2810, 10, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2811, 11, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2812, 12, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2813, 13, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2814, 14, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2815, 15, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2816, 16, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2817, 17, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2818, 18, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2819, 19, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2820, 20, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2821, 21, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2822, 22, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2823, 23, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2824, 24, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2825, 25, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2826, 26, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2827, 27, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2828, 28, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2829, 29, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2830, 30, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2831, 31, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2832, 32, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2833, 33, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2834, 34, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2835, 35, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2836, 36, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2837, 37, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2838, 38, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2839, 39, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2840, 40, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2841, 41, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2842, 42, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2843, 43, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2844, 44, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2845, 45, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2846, 46, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2847, 47, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2848, 48, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2849, 49, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2850, 50, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2851, 51, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2852, 52, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2853, 53, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2854, 54, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2855, 55, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2856, 56, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2857, 57, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2858, 58, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2859, 59, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2860, 60, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2861, 61, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2862, 62, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2863, 63, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2864, 64, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2865, 65, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2866, 66, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2867, 67, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2868, 68, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2869, 69, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2870, 70, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2871, 71, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2872, 72, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2873, 73, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2874, 74, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2875, 75, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2876, 76, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2877, 77, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2878, 78, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2879, 79, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2880, 80, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2881, 81, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2882, 82, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2883, 83, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2884, 84, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2885, 85, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2886, 86, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2887, 87, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2888, 88, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2889, 89, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2890, 90, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2891, 91, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2892, 92, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2893, 93, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2894, 94, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2895, 95, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2896, 96, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2897, 97, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2898, 98, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2899, 99, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2900, 100, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2901, 101, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2902, 102, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2903, 103, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2904, 104, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2905, 105, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2906, 106, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2907, 107, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2908, 108, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2909, 109, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2910, 110, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2911, 111, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2912, 112, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2913, 113, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2914, 114, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2915, 115, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2916, 116, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2917, 117, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2918, 118, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2919, 119, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2920, 120, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2921, 121, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2922, 122, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2923, 123, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2924, 124, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2925, 125, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2926, 126, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2927, 127, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2928, 128, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2929, 129, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2930, 130, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2931, 131, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2932, 132, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2933, 133, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2934, 134, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2935, 135, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2936, 136, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2937, 137, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2938, 138, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2939, 139, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2940, 140, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2941, 141, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2942, 142, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2943, 143, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2944, 144, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2945, 145, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2946, 146, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2947, 147, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2948, 148, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2949, 149, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2950, 150, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2951, 151, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2952, 152, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2953, 153, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2954, 154, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2955, 155, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2956, 156, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2957, 157, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2958, 158, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2959, 159, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2960, 160, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2961, 161, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2962, 162, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2963, 163, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2964, 164, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2965, 165, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2966, 166, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2967, 167, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2968, 168, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2969, 169, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2970, 170, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2971, 171, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2972, 172, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2973, 173, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2974, 174, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2975, 175, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2976, 176, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2977, 177, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2978, 178, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2979, 179, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2980, 180, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2981, 181, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2982, 182, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2983, 183, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2984, 184, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2985, 185, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2986, 186, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2987, 187, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2988, 188, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2989, 189, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2990, 190, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2991, 191, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2992, 192, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2993, 193, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2994, 194, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2995, 195, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2996, 196, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2997, 197, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2998, 198, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(2999, 199, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3000, 200, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3001, 201, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3002, 202, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3003, 203, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3004, 204, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3005, 205, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3006, 206, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3007, 207, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3008, 208, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3009, 209, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3010, 210, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3011, 211, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3012, 212, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3013, 213, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3014, 214, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3015, 215, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3016, 216, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3017, 217, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3018, 218, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3019, 219, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3020, 220, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3021, 221, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3022, 222, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3023, 223, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3024, 224, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3025, 225, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3026, 226, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3027, 227, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3028, 228, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3029, 229, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3030, 230, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3031, 231, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3032, 232, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3033, 233, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3034, 234, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3035, 235, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3036, 236, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3037, 237, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3038, 238, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3039, 239, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3040, 240, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3041, 241, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3042, 242, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3043, 243, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3044, 244, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3045, 245, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3046, 246, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3047, 247, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3048, 248, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3049, 249, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3050, 250, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3051, 251, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3052, 252, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3053, 253, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3054, 254, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3055, 255, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3056, 256, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3057, 257, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3058, 258, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3059, 259, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3060, 260, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3061, 261, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3062, 262, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3063, 263, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3064, 264, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3065, 265, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3066, 266, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3067, 267, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3068, 268, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3069, 269, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3070, 270, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3071, 271, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3072, 272, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3073, 273, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3074, 274, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3075, 275, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3076, 276, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3077, 277, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3078, 278, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3079, 279, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3080, 280, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3081, 281, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3082, 282, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3083, 283, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3084, 284, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3085, 285, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3086, 286, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3087, 287, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3088, 288, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3089, 289, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3090, 290, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3091, 291, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3092, 292, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3093, 293, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3094, 294, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3095, 295, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3096, 296, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3097, 297, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3098, 298, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3099, 299, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3100, 300, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3101, 301, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3102, 302, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3103, 303, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3104, 304, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3105, 305, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3106, 306, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3107, 307, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3108, 308, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3109, 309, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3110, 310, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3111, 311, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3112, 312, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3113, 313, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3114, 314, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3115, 315, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3116, 316, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3117, 317, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3118, 318, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3119, 319, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3120, 320, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3121, 321, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3122, 322, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3123, 323, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3124, 324, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3125, 325, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3126, 326, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3127, 327, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3128, 328, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3129, 329, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3130, 330, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3131, 331, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3132, 332, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3133, 333, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3134, 334, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3135, 335, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3136, 336, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3137, 337, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3138, 338, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3139, 339, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3140, 340, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3141, 341, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3142, 342, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3143, 343, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3144, 344, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3145, 345, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3146, 346, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3147, 347, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3148, 348, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3149, 349, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3150, 350, 29, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3151, 1, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3152, 2, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3153, 3, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3154, 4, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3155, 5, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3156, 6, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3157, 7, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3158, 8, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3159, 9, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3160, 10, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3161, 11, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3162, 12, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3163, 13, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3164, 14, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3165, 15, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3166, 16, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3167, 17, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3168, 18, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3169, 19, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3170, 20, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3171, 21, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3172, 22, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3173, 23, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3174, 24, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3175, 25, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3176, 26, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3177, 27, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3178, 28, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3179, 29, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3180, 30, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3181, 31, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3182, 32, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3183, 33, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3184, 34, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3185, 35, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3186, 36, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3187, 37, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3188, 38, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3189, 39, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3190, 40, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3191, 41, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3192, 42, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3193, 43, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3194, 44, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3195, 45, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3196, 46, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3197, 47, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3198, 48, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3199, 49, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3200, 50, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3201, 51, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3202, 52, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3203, 53, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3204, 54, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3205, 55, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3206, 56, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3207, 57, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3208, 58, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3209, 59, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3210, 60, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3211, 61, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3212, 62, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3213, 63, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3214, 64, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3215, 65, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3216, 66, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3217, 67, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3218, 68, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3219, 69, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3220, 70, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3221, 71, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3222, 72, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3223, 73, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3224, 74, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3225, 75, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3226, 76, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3227, 77, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3228, 78, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3229, 79, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3230, 80, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3231, 81, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3232, 82, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3233, 83, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3234, 84, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3235, 85, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11');
INSERT INTO `products_itemize` (`id`, `item_count`, `item_id`, `process`, `ref_no`, `batch_no`, `is_available`, `manufacturing`, `expiry`, `order_ref`, `date_time`) VALUES
(3236, 86, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3237, 87, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3238, 88, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3239, 89, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3240, 90, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3241, 91, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3242, 92, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3243, 93, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3244, 94, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3245, 95, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3246, 96, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3247, 97, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3248, 98, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3249, 99, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3250, 100, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3251, 101, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3252, 102, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3253, 103, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3254, 104, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3255, 105, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3256, 106, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3257, 107, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3258, 108, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3259, 109, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3260, 110, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3261, 111, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3262, 112, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3263, 113, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3264, 114, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3265, 115, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3266, 116, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3267, 117, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3268, 118, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3269, 119, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3270, 120, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3271, 121, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3272, 122, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3273, 123, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3274, 124, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3275, 125, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3276, 126, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3277, 127, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3278, 128, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3279, 129, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3280, 130, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3281, 131, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3282, 132, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3283, 133, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3284, 134, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3285, 135, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3286, 136, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3287, 137, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3288, 138, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3289, 139, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3290, 140, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3291, 141, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3292, 142, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3293, 143, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3294, 144, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3295, 145, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3296, 146, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3297, 147, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3298, 148, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3299, 149, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3300, 150, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3301, 151, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3302, 152, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3303, 153, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3304, 154, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3305, 155, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3306, 156, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3307, 157, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3308, 158, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3309, 159, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3310, 160, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3311, 161, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3312, 162, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3313, 163, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3314, 164, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3315, 165, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3316, 166, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3317, 167, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3318, 168, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3319, 169, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3320, 170, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3321, 171, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3322, 172, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3323, 173, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3324, 174, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3325, 175, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3326, 176, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3327, 177, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3328, 178, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3329, 179, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3330, 180, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3331, 181, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3332, 182, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3333, 183, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3334, 184, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3335, 185, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3336, 186, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3337, 187, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3338, 188, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3339, 189, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3340, 190, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3341, 191, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3342, 192, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3343, 193, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3344, 194, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3345, 195, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3346, 196, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3347, 197, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3348, 198, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3349, 199, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3350, 200, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3351, 201, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3352, 202, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3353, 203, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3354, 204, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3355, 205, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3356, 206, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3357, 207, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3358, 208, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3359, 209, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3360, 210, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3361, 211, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3362, 212, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3363, 213, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3364, 214, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3365, 215, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3366, 216, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3367, 217, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3368, 218, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3369, 219, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3370, 220, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3371, 221, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3372, 222, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3373, 223, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3374, 224, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3375, 225, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3376, 226, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3377, 227, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3378, 228, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3379, 229, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3380, 230, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3381, 231, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3382, 232, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3383, 233, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3384, 234, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3385, 235, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3386, 236, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3387, 237, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3388, 238, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3389, 239, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3390, 240, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3391, 241, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3392, 242, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3393, 243, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3394, 244, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3395, 245, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3396, 246, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3397, 247, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3398, 248, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3399, 249, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3400, 250, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3401, 251, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3402, 252, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3403, 253, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3404, 254, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3405, 255, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3406, 256, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3407, 257, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3408, 258, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3409, 259, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3410, 260, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3411, 261, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3412, 262, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3413, 263, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3414, 264, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3415, 265, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3416, 266, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3417, 267, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3418, 268, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3419, 269, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3420, 270, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3421, 271, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3422, 272, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3423, 273, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3424, 274, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3425, 275, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3426, 276, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3427, 277, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3428, 278, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3429, 279, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3430, 280, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3431, 281, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3432, 282, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3433, 283, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3434, 284, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3435, 285, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3436, 286, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3437, 287, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3438, 288, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3439, 289, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3440, 290, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3441, 291, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3442, 292, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3443, 293, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3444, 294, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3445, 295, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3446, 296, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3447, 297, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3448, 298, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3449, 299, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3450, 300, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3451, 301, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3452, 302, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3453, 303, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3454, 304, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3455, 305, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3456, 306, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3457, 307, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3458, 308, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3459, 309, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3460, 310, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3461, 311, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3462, 312, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3463, 313, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3464, 314, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3465, 315, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3466, 316, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3467, 317, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3468, 318, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3469, 319, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3470, 320, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3471, 321, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3472, 322, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3473, 323, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3474, 324, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3475, 325, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3476, 326, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3477, 327, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3478, 328, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3479, 329, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3480, 330, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3481, 331, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3482, 332, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3483, 333, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3484, 334, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3485, 335, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3486, 336, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3487, 337, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3488, 338, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3489, 339, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3490, 340, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3491, 341, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3492, 342, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3493, 343, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3494, 344, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3495, 345, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3496, 346, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3497, 347, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3498, 348, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3499, 349, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3500, 350, 28, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3501, 1, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3502, 2, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3503, 3, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3504, 4, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3505, 5, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3506, 6, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3507, 7, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3508, 8, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3509, 9, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3510, 10, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3511, 11, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3512, 12, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3513, 13, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3514, 14, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3515, 15, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3516, 16, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3517, 17, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3518, 18, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3519, 19, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3520, 20, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3521, 21, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3522, 22, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3523, 23, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3524, 24, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3525, 25, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3526, 26, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3527, 27, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3528, 28, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3529, 29, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3530, 30, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3531, 31, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3532, 32, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3533, 33, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3534, 34, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3535, 35, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3536, 36, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3537, 37, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3538, 38, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3539, 39, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3540, 40, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3541, 41, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3542, 42, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3543, 43, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3544, 44, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3545, 45, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3546, 46, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3547, 47, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3548, 48, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3549, 49, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3550, 50, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3551, 51, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3552, 52, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3553, 53, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3554, 54, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3555, 55, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3556, 56, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3557, 57, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3558, 58, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3559, 59, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3560, 60, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3561, 61, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3562, 62, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3563, 63, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3564, 64, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3565, 65, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3566, 66, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3567, 67, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3568, 68, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3569, 69, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3570, 70, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3571, 71, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3572, 72, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3573, 73, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3574, 74, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3575, 75, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3576, 76, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3577, 77, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3578, 78, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3579, 79, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3580, 80, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3581, 81, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3582, 82, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3583, 83, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3584, 84, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3585, 85, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3586, 86, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3587, 87, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3588, 88, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3589, 89, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3590, 90, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3591, 91, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3592, 92, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3593, 93, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3594, 94, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3595, 95, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3596, 96, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3597, 97, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3598, 98, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3599, 99, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3600, 100, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3601, 101, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3602, 102, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3603, 103, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3604, 104, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3605, 105, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3606, 106, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3607, 107, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3608, 108, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3609, 109, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3610, 110, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3611, 111, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3612, 112, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3613, 113, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3614, 114, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3615, 115, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3616, 116, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3617, 117, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3618, 118, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3619, 119, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3620, 120, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3621, 121, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3622, 122, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3623, 123, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3624, 124, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3625, 125, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3626, 126, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3627, 127, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3628, 128, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3629, 129, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3630, 130, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3631, 131, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3632, 132, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3633, 133, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3634, 134, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3635, 135, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3636, 136, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3637, 137, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3638, 138, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3639, 139, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3640, 140, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3641, 141, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3642, 142, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3643, 143, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3644, 144, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3645, 145, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3646, 146, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3647, 147, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3648, 148, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3649, 149, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3650, 150, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3651, 151, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3652, 152, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3653, 153, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3654, 154, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3655, 155, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3656, 156, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3657, 157, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3658, 158, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3659, 159, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3660, 160, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3661, 161, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3662, 162, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3663, 163, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3664, 164, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3665, 165, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3666, 166, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3667, 167, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3668, 168, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3669, 169, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11');
INSERT INTO `products_itemize` (`id`, `item_count`, `item_id`, `process`, `ref_no`, `batch_no`, `is_available`, `manufacturing`, `expiry`, `order_ref`, `date_time`) VALUES
(3670, 170, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3671, 171, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3672, 172, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3673, 173, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3674, 174, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3675, 175, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3676, 176, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3677, 177, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3678, 178, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3679, 179, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3680, 180, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3681, 181, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3682, 182, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3683, 183, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3684, 184, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3685, 185, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3686, 186, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3687, 187, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3688, 188, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3689, 189, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3690, 190, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3691, 191, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3692, 192, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3693, 193, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3694, 194, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3695, 195, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3696, 196, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3697, 197, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3698, 198, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3699, 199, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3700, 200, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3701, 201, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3702, 202, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3703, 203, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3704, 204, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3705, 205, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3706, 206, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3707, 207, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3708, 208, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3709, 209, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3710, 210, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3711, 211, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3712, 212, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3713, 213, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3714, 214, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3715, 215, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3716, 216, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3717, 217, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3718, 218, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3719, 219, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3720, 220, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3721, 221, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3722, 222, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3723, 223, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3724, 224, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3725, 225, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3726, 226, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3727, 227, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3728, 228, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3729, 229, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3730, 230, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3731, 231, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3732, 232, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3733, 233, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3734, 234, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3735, 235, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3736, 236, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3737, 237, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3738, 238, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3739, 239, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3740, 240, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3741, 241, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3742, 242, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3743, 243, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3744, 244, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3745, 245, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3746, 246, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3747, 247, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3748, 248, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3749, 249, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3750, 250, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3751, 251, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3752, 252, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3753, 253, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3754, 254, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3755, 255, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3756, 256, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3757, 257, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3758, 258, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3759, 259, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3760, 260, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3761, 261, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3762, 262, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3763, 263, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3764, 264, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3765, 265, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3766, 266, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3767, 267, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3768, 268, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3769, 269, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3770, 270, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3771, 271, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3772, 272, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3773, 273, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3774, 274, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3775, 275, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3776, 276, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3777, 277, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3778, 278, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3779, 279, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3780, 280, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3781, 281, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3782, 282, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3783, 283, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3784, 284, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3785, 285, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3786, 286, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3787, 287, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3788, 288, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3789, 289, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3790, 290, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3791, 291, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3792, 292, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3793, 293, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3794, 294, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3795, 295, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3796, 296, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3797, 297, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3798, 298, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3799, 299, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3800, 300, 27, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '0000-00-00', NULL, '2021-12-05 02:08:11'),
(3801, 1, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3802, 2, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3803, 3, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3804, 4, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3805, 5, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3806, 6, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3807, 7, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3808, 8, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3809, 9, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3810, 10, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3811, 11, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3812, 12, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3813, 13, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3814, 14, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3815, 15, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3816, 16, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3817, 17, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3818, 18, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3819, 19, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3820, 20, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3821, 21, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3822, 22, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3823, 23, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3824, 24, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3825, 25, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3826, 26, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3827, 27, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3828, 28, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3829, 29, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3830, 30, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3831, 31, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3832, 32, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3833, 33, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3834, 34, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3835, 35, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3836, 36, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3837, 37, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3838, 38, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3839, 39, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3840, 40, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3841, 41, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3842, 42, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3843, 43, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3844, 44, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3845, 45, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3846, 46, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3847, 47, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3848, 48, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3849, 49, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3850, 50, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3851, 51, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3852, 52, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3853, 53, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3854, 54, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3855, 55, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3856, 56, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3857, 57, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3858, 58, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3859, 59, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3860, 60, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3861, 61, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3862, 62, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3863, 63, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3864, 64, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3865, 65, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3866, 66, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3867, 67, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3868, 68, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3869, 69, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3870, 70, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3871, 71, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3872, 72, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3873, 73, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3874, 74, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3875, 75, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3876, 76, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3877, 77, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3878, 78, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3879, 79, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3880, 80, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3881, 81, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3882, 82, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3883, 83, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3884, 84, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3885, 85, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3886, 86, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3887, 87, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3888, 88, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3889, 89, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3890, 90, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3891, 91, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3892, 92, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3893, 93, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3894, 94, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3895, 95, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3896, 96, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3897, 97, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3898, 98, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3899, 99, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3900, 100, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3901, 101, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3902, 102, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3903, 103, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3904, 104, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3905, 105, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3906, 106, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3907, 107, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3908, 108, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3909, 109, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3910, 110, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3911, 111, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3912, 112, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3913, 113, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3914, 114, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3915, 115, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3916, 116, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3917, 117, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3918, 118, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3919, 119, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3920, 120, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3921, 121, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3922, 122, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3923, 123, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3924, 124, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3925, 125, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3926, 126, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3927, 127, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3928, 128, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3929, 129, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3930, 130, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3931, 131, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3932, 132, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3933, 133, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3934, 134, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3935, 135, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3936, 136, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3937, 137, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3938, 138, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3939, 139, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3940, 140, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3941, 141, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3942, 142, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3943, 143, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3944, 144, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3945, 145, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3946, 146, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3947, 147, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3948, 148, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3949, 149, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3950, 150, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3951, 151, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3952, 152, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3953, 153, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3954, 154, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3955, 155, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3956, 156, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3957, 157, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3958, 158, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3959, 159, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3960, 160, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3961, 161, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3962, 162, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3963, 163, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3964, 164, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3965, 165, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3966, 166, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3967, 167, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3968, 168, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3969, 169, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3970, 170, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3971, 171, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3972, 172, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3973, 173, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3974, 174, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3975, 175, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3976, 176, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3977, 177, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3978, 178, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3979, 179, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3980, 180, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3981, 181, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3982, 182, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3983, 183, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3984, 184, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3985, 185, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3986, 186, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3987, 187, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3988, 188, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3989, 189, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3990, 190, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3991, 191, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3992, 192, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3993, 193, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3994, 194, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3995, 195, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3996, 196, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3997, 197, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3998, 198, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(3999, 199, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4000, 200, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4001, 201, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4002, 202, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4003, 203, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4004, 204, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4005, 205, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4006, 206, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4007, 207, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4008, 208, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4009, 209, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4010, 210, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4011, 211, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4012, 212, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4013, 213, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4014, 214, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4015, 215, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4016, 216, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4017, 217, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4018, 218, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4019, 219, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4020, 220, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4021, 221, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4022, 222, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4023, 223, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4024, 224, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4025, 225, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4026, 226, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4027, 227, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4028, 228, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4029, 229, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4030, 230, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4031, 231, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4032, 232, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4033, 233, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4034, 234, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4035, 235, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4036, 236, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4037, 237, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4038, 238, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4039, 239, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4040, 240, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4041, 241, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4042, 242, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4043, 243, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4044, 244, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4045, 245, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4046, 246, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4047, 247, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4048, 248, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4049, 249, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4050, 250, 26, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4051, 1, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4052, 2, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4053, 3, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4054, 4, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4055, 5, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4056, 6, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4057, 7, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4058, 8, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4059, 9, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4060, 10, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4061, 11, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4062, 12, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4063, 13, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4064, 14, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4065, 15, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4066, 16, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4067, 17, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4068, 18, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4069, 19, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4070, 20, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4071, 21, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4072, 22, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4073, 23, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4074, 24, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4075, 25, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4076, 26, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4077, 27, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4078, 28, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4079, 29, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4080, 30, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4081, 31, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4082, 32, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4083, 33, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4084, 34, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4085, 35, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4086, 36, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4087, 37, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4088, 38, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4089, 39, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4090, 40, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4091, 41, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4092, 42, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4093, 43, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4094, 44, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4095, 45, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4096, 46, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4097, 47, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4098, 48, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4099, 49, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4100, 50, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4101, 51, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4102, 52, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4103, 53, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11');
INSERT INTO `products_itemize` (`id`, `item_count`, `item_id`, `process`, `ref_no`, `batch_no`, `is_available`, `manufacturing`, `expiry`, `order_ref`, `date_time`) VALUES
(4104, 54, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4105, 55, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4106, 56, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4107, 57, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4108, 58, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4109, 59, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4110, 60, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4111, 61, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4112, 62, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4113, 63, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4114, 64, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4115, 65, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4116, 66, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4117, 67, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4118, 68, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4119, 69, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4120, 70, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4121, 71, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4122, 72, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4123, 73, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4124, 74, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4125, 75, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4126, 76, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4127, 77, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4128, 78, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4129, 79, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4130, 80, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4131, 81, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4132, 82, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4133, 83, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4134, 84, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4135, 85, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4136, 86, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4137, 87, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4138, 88, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4139, 89, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4140, 90, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4141, 91, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4142, 92, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4143, 93, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4144, 94, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4145, 95, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4146, 96, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4147, 97, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4148, 98, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4149, 99, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4150, 100, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4151, 101, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4152, 102, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4153, 103, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4154, 104, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4155, 105, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4156, 106, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4157, 107, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4158, 108, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4159, 109, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4160, 110, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4161, 111, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4162, 112, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4163, 113, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4164, 114, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4165, 115, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4166, 116, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4167, 117, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4168, 118, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4169, 119, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4170, 120, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4171, 121, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4172, 122, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4173, 123, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4174, 124, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4175, 125, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4176, 126, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4177, 127, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4178, 128, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4179, 129, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4180, 130, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4181, 131, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4182, 132, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4183, 133, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4184, 134, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4185, 135, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4186, 136, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4187, 137, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4188, 138, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4189, 139, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4190, 140, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4191, 141, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4192, 142, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4193, 143, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4194, 144, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4195, 145, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4196, 146, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4197, 147, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4198, 148, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4199, 149, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4200, 150, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4201, 151, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4202, 152, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4203, 153, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4204, 154, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4205, 155, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4206, 156, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4207, 157, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4208, 158, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4209, 159, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4210, 160, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4211, 161, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4212, 162, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4213, 163, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4214, 164, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4215, 165, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4216, 166, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4217, 167, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4218, 168, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4219, 169, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4220, 170, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4221, 171, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4222, 172, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4223, 173, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4224, 174, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4225, 175, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4226, 176, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4227, 177, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4228, 178, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4229, 179, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4230, 180, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4231, 181, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4232, 182, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4233, 183, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4234, 184, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4235, 185, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4236, 186, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4237, 187, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4238, 188, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4239, 189, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4240, 190, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4241, 191, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4242, 192, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4243, 193, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4244, 194, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4245, 195, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4246, 196, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4247, 197, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4248, 198, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4249, 199, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4250, 200, 25, 'PURCHASE_ORDER', 'D-0006', '22323231', 1, '2021-12-01', '2021-12-01', NULL, '2021-12-05 02:08:11'),
(4251, 1, 21, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0002', '2021-12-05 02:22:29'),
(4252, 2, 21, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0002', '2021-12-05 02:22:29'),
(4253, 3, 21, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0002', '2021-12-05 02:22:29'),
(4254, 4, 21, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0002', '2021-12-05 02:22:29'),
(4255, 5, 21, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0002', '2021-12-05 02:22:29'),
(4256, 6, 21, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0004', '2021-12-05 02:41:17'),
(4257, 7, 21, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0004', '2021-12-05 02:41:17'),
(4258, 8, 21, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0004', '2021-12-05 02:41:17'),
(4259, 9, 21, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0004', '2021-12-05 02:41:17'),
(4260, 10, 21, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0004', '2021-12-05 02:41:17'),
(4261, 1, 24, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4262, 2, 24, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4263, 3, 24, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4264, 4, 24, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4265, 5, 24, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4266, 6, 24, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4267, 7, 24, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4268, 8, 24, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4269, 9, 24, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4270, 10, 24, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4271, 1, 22, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0003', '2021-12-05 02:30:35'),
(4272, 2, 22, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0003', '2021-12-05 02:30:35'),
(4273, 3, 22, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0003', '2021-12-05 02:30:35'),
(4274, 4, 22, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0003', '2021-12-05 02:30:35'),
(4275, 5, 22, 'PRODUCTION (+)', 'J-0010', 'B-0010', 0, '0000-00-00', '0000-00-00', 'O-0003', '2021-12-05 02:30:35'),
(4276, 1, 23, 'PRODUCTION (+)', 'J-0011', 'B-0011', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4277, 2, 23, 'PRODUCTION (+)', 'J-0011', 'B-0011', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4278, 3, 23, 'PRODUCTION (+)', 'J-0011', 'B-0011', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4279, 4, 23, 'PRODUCTION (+)', 'J-0011', 'B-0011', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4280, 5, 23, 'PRODUCTION (+)', 'J-0011', 'B-0011', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4281, 6, 23, 'PRODUCTION (+)', 'J-0011', 'B-0011', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4282, 7, 23, 'PRODUCTION (+)', 'J-0011', 'B-0011', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4283, 8, 23, 'PRODUCTION (+)', 'J-0011', 'B-0011', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4284, 9, 23, 'PRODUCTION (+)', 'J-0011', 'B-0011', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4285, 10, 23, 'PRODUCTION (+)', 'J-0011', 'B-0011', 0, '0000-00-00', '0000-00-00', 'O-0001', '2021-12-05 02:12:48'),
(4286, 11, 23, 'PRODUCTION (+)', 'J-0011', 'B-0011', 0, '0000-00-00', '0000-00-00', 'O-0003', '2021-12-05 02:30:35'),
(4287, 12, 23, 'PRODUCTION (+)', 'J-0011', 'B-0011', 0, '0000-00-00', '0000-00-00', 'O-0003', '2021-12-05 02:30:35'),
(4288, 13, 23, 'PRODUCTION (+)', 'J-0011', 'B-0011', 0, '0000-00-00', '0000-00-00', 'O-0003', '2021-12-05 02:30:35'),
(4289, 14, 23, 'PRODUCTION (+)', 'J-0011', 'B-0011', 0, '0000-00-00', '0000-00-00', 'O-0003', '2021-12-05 02:30:35'),
(4290, 15, 23, 'PRODUCTION (+)', 'J-0011', 'B-0011', 0, '0000-00-00', '0000-00-00', 'O-0003', '2021-12-05 02:30:35');

-- --------------------------------------------------------

--
-- Table structure for table `product_journal`
--

CREATE TABLE `product_journal` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `transaction` text DEFAULT NULL,
  `ref_no` varchar(50) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_journal`
--

INSERT INTO `product_journal` (`id`, `product_id`, `transaction`, `ref_no`, `qty`, `date_time`) VALUES
(99, 25, 'PURCHASE ORDER', 'D-0006', 200, '2021-12-05 02:08:11'),
(100, 26, 'PURCHASE ORDER', 'D-0006', 250, '2021-12-05 02:08:11'),
(101, 27, 'PURCHASE ORDER', 'D-0006', 300, '2021-12-05 02:08:11'),
(102, 28, 'PURCHASE ORDER', 'D-0006', 350, '2021-12-05 02:08:11'),
(103, 29, 'PURCHASE ORDER', 'D-0006', 350, '2021-12-05 02:08:11'),
(104, 21, 'PRODUCTION (+)', 'J-0010', 10, '2021-12-05 02:09:29'),
(105, 24, 'PRODUCTION (+)', 'J-0010', 10, '2021-12-05 02:09:29'),
(106, 22, 'PRODUCTION (+)', 'J-0010', 5, '2021-12-05 02:09:29'),
(107, 25, 'PRODUCTION (-)', 'J-0010', 30, '2021-12-05 02:09:29'),
(108, 26, 'PRODUCTION (-)', 'J-0010', 30, '2021-12-05 02:09:29'),
(109, 28, 'PRODUCTION (-)', 'J-0010', 75, '2021-12-05 02:09:29'),
(110, 27, 'PRODUCTION (-)', 'J-0010', 100, '2021-12-05 02:09:29'),
(111, 23, 'PRODUCTION (+)', 'J-0011', 15, '2021-12-05 02:12:07'),
(112, 25, 'PRODUCTION (-)', 'J-0011', 75, '2021-12-05 02:12:07'),
(113, 26, 'PRODUCTION (-)', 'J-0011', 75, '2021-12-05 02:12:07'),
(114, 27, 'PRODUCTION (-)', 'J-0011', 30, '2021-12-05 02:12:07'),
(115, 28, 'PRODUCTION (-)', 'J-0011', 120, '2021-12-05 02:12:07'),
(116, 29, 'PRODUCTION (-)', 'J-0011', 75, '2021-12-05 02:12:07'),
(117, 24, 'ORDER', 'O-0001', 10, '2021-12-05 02:12:48'),
(118, 23, 'ORDER', 'O-0001', 10, '2021-12-05 02:12:48'),
(119, 21, 'ORDER', 'O-0002', 5, '2021-12-05 02:22:29'),
(120, 23, 'ORDER', 'O-0003', 5, '2021-12-05 02:30:35'),
(121, 22, 'ORDER', 'O-0003', 5, '2021-12-05 02:30:35'),
(124, 21, 'ORDER', 'O-0004', 5, '2021-12-05 02:41:17');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` int(11) NOT NULL,
  `po_number` varchar(100) NOT NULL,
  `date_po` date DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `mode_of_payment` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `added_by` varchar(100) DEFAULT NULL,
  `approved_by` varchar(50) DEFAULT NULL,
  `received_by` varchar(50) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `date_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`id`, `po_number`, `date_po`, `supplier_id`, `total_amount`, `mode_of_payment`, `status`, `added_by`, `approved_by`, `received_by`, `is_deleted`, `date_time`) VALUES
(14, 'D-0001', '2021-12-02', 4, '17000.00', 'PICKUP', 'RECEIVED', 'admin', 'admin', 'commissary', 0, '2021-12-02 07:49:40'),
(15, 'D-0002', '2021-12-02', 4, '4000.00', 'PICKUP', 'RECEIVED', 'admin', 'admin', 'commissary', 0, '2021-12-02 07:51:23'),
(16, 'D-0003', '2021-12-01', 4, '10000.00', 'PICKUP', 'RECEIVED', 'admin', 'admin', 'commissary', 0, '2021-12-04 04:40:02'),
(17, 'D-0004', '2021-12-01', 4, '125000.00', 'PICKUP', 'RECEIVED', 'admin', 'admin', 'commissary', 0, '2021-12-05 01:43:55'),
(18, 'D-0005', '2021-12-05', 4, '4000.00', 'PICKUP', 'RECEIVED', 'admin', 'admin', 'commissary', 0, '2021-12-05 01:59:01'),
(19, 'D-0006', '2021-12-01', 4, '29000.00', 'PICKUP', 'RECEIVED', 'admin', 'admin', 'commissary', 0, '2021-12-05 02:08:11');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_items`
--

CREATE TABLE `purchase_order_items` (
  `id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_order_items`
--

INSERT INTO `purchase_order_items` (`id`, `purchase_order_id`, `product_id`, `qty`, `cost`, `total_amount`) VALUES
(38, 14, 21, 10, '1000.00', '10000.00'),
(39, 14, 22, 10, '500.00', '5000.00'),
(40, 14, 23, 10, '200.00', '2000.00'),
(41, 15, 24, 20, '200.00', '4000.00'),
(42, 16, 25, 100, '20.00', '2000.00'),
(43, 16, 26, 100, '20.00', '2000.00'),
(44, 16, 27, 100, '20.00', '2000.00'),
(45, 16, 28, 100, '20.00', '2000.00'),
(46, 16, 29, 100, '20.00', '2000.00'),
(47, 17, 21, 50, '1000.00', '50000.00'),
(48, 17, 22, 50, '500.00', '50000.00'),
(49, 17, 23, 50, '200.00', '10000.00'),
(50, 17, 24, 50, '200.00', '10000.00'),
(51, 17, 25, 50, '20.00', '1000.00'),
(52, 17, 26, 50, '20.00', '1000.00'),
(53, 17, 27, 50, '20.00', '1000.00'),
(54, 17, 28, 50, '20.00', '1000.00'),
(55, 17, 29, 50, '20.00', '1000.00'),
(56, 18, 27, 200, '20.00', '4000.00'),
(57, 19, 25, 200, '20.00', '4000.00'),
(58, 19, 26, 250, '20.00', '5000.00'),
(59, 19, 27, 300, '20.00', '6000.00'),
(60, 19, 28, 350, '20.00', '7000.00'),
(61, 19, 29, 350, '20.00', '7000.00');

-- --------------------------------------------------------

--
-- Table structure for table `received_po`
--

CREATE TABLE `received_po` (
  `id` int(11) NOT NULL,
  `purchase_order_number` varchar(50) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `received_by` varchar(50) DEFAULT NULL,
  `date_received` date DEFAULT NULL,
  `supplier_dr` varchar(100) DEFAULT NULL,
  `batch_no` varchar(100) NOT NULL,
  `remarks` text DEFAULT NULL,
  `added_by` varchar(50) DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `received_po`
--

INSERT INTO `received_po` (`id`, `purchase_order_number`, `total_amount`, `received_by`, `date_received`, `supplier_dr`, `batch_no`, `remarks`, `added_by`, `date_time`) VALUES
(26, 'D-0001', '2.00', 'sdfsdf', '2021-12-02', '234234234', '324234234', '', 'commissary', '2021-12-02 07:49:40'),
(27, 'D-0002', '4.00', 'Sample receiver', '2021-12-02', 'asdasdas', '1111111', '', 'commissary', '2021-12-02 07:51:23'),
(28, 'D-0003', '10.00', 'Sample receiver', '2021-12-16', '2021000001', 'hhbbbasas', 'test', 'commissary', '2021-12-04 04:40:01'),
(29, 'D-0004', '125.00', 'Sample receiver', '2021-12-05', '222333', '889988', '', 'commissary', '2021-12-05 01:43:55'),
(30, 'D-0005', '4.00', 'Sample receiver', '2021-12-06', '7776868', '8787778', '', 'commissary', '2021-12-05 01:59:00'),
(31, 'D-0006', '29.00', 'Sample receiver', '2021-12-06', '2021000001', '22323231', '', 'commissary', '2021-12-05 02:08:11');

-- --------------------------------------------------------

--
-- Table structure for table `received_po_items`
--

CREATE TABLE `received_po_items` (
  `id` int(11) NOT NULL,
  `purchase_order_number` varchar(50) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `rec_qty` int(11) DEFAULT NULL,
  `cost` decimal(10,2) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `received_po_items`
--

INSERT INTO `received_po_items` (`id`, `purchase_order_number`, `product_id`, `rec_qty`, `cost`, `total_amount`) VALUES
(28, 'D-0001', 21, 10, '1000.00', '10.00'),
(29, 'D-0001', 22, 10, '500.00', '5.00'),
(30, 'D-0001', 23, 10, '200.00', '2000.00'),
(31, 'D-0002', 24, 20, '200.00', '4.00'),
(32, 'D-0003', 25, 100, '20.00', '2.00'),
(33, 'D-0003', 26, 100, '20.00', '2.00'),
(34, 'D-0003', 27, 100, '20.00', '2.00'),
(35, 'D-0003', 28, 100, '20.00', '2.00'),
(36, 'D-0003', 29, 100, '20.00', '2.00'),
(37, 'D-0004', 21, 50, '1000.00', '50.00'),
(38, 'D-0004', 22, 50, '500.00', '50.00'),
(39, 'D-0004', 23, 50, '200.00', '10.00'),
(40, 'D-0004', 24, 50, '200.00', '10.00'),
(41, 'D-0004', 25, 50, '20.00', '1.00'),
(42, 'D-0004', 26, 50, '20.00', '1.00'),
(43, 'D-0004', 27, 50, '20.00', '1.00'),
(44, 'D-0004', 28, 50, '20.00', '1.00'),
(45, 'D-0004', 29, 50, '20.00', '1.00'),
(46, 'D-0005', 27, 200, '20.00', '4.00'),
(47, 'D-0006', 25, 200, '20.00', '4.00'),
(48, 'D-0006', 26, 250, '20.00', '5.00'),
(49, 'D-0006', 27, 300, '20.00', '6.00'),
(50, 'D-0006', 28, 350, '20.00', '7.00'),
(51, 'D-0006', 29, 350, '20.00', '7.00');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_name` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `category_id`, `sub_category_name`, `is_deleted`, `date_time`) VALUES
(1, 6, 'Cost of Goods', 0, '2021-10-24 00:05:25'),
(2, 8, 'Gas Allowance', 0, '2021-10-24 00:05:43'),
(3, 8, 'Food Allowance', 0, '2021-10-24 00:05:58'),
(4, 8, 'Toll Fee', 0, '2021-10-24 00:06:07'),
(5, 8, 'Board and Lodging', 0, '2021-10-24 00:06:25'),
(6, 9, 'Repair and Maintenance Works', 0, '2021-10-24 00:06:50'),
(7, 9, 'Telephone and Internet Expenses', 0, '2021-10-24 00:07:05'),
(8, 9, 'Electricity and Water Bill', 0, '2021-10-24 00:07:22'),
(9, 9, 'Office Supplies', 0, '2021-10-24 00:07:34'),
(10, 9, 'Professional Fee', 0, '2021-10-24 00:07:46'),
(11, 9, 'Permit and Licenses', 0, '2021-10-24 00:07:56');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_address` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `supplier_name`, `supplier_address`, `is_deleted`, `date_time`) VALUES
(4, 'Supplier 1', 'Supplier 1', 0, '2021-11-11 23:20:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_levels`
--
ALTER TABLE `access_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_franchise`
--
ALTER TABLE `billing_franchise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_payments`
--
ALTER TABLE `billing_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commissary_accounts`
--
ALTER TABLE `commissary_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `formulation`
--
ALTER TABLE `formulation`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `formulation_details`
--
ALTER TABLE `formulation_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchisee`
--
ALTER TABLE `franchisee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `franchisee_ibfk_1` (`branch_id`);

--
-- Indexes for table `franchise_accounts`
--
ALTER TABLE `franchise_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchise_branch`
--
ALTER TABLE `franchise_branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `franchise_renewal_log`
--
ALTER TABLE `franchise_renewal_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_payments`
--
ALTER TABLE `order_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_returns`
--
ALTER TABLE `order_returns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_return_items`
--
ALTER TABLE `order_return_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll`
--
ALTER TABLE `payroll`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll_employee`
--
ALTER TABLE `payroll_employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `production`
--
ALTER TABLE `production`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `production_items`
--
ALTER TABLE `production_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_code` (`product_code`);

--
-- Indexes for table `products_itemize`
--
ALTER TABLE `products_itemize`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_journal`
--
ALTER TABLE `product_journal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `po_number` (`po_number`);

--
-- Indexes for table `purchase_order_items`
--
ALTER TABLE `purchase_order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `received_po`
--
ALTER TABLE `received_po`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `received_po_items`
--
ALTER TABLE `received_po_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_category` (`category_id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_levels`
--
ALTER TABLE `access_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `billing_franchise`
--
ALTER TABLE `billing_franchise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `billing_payments`
--
ALTER TABLE `billing_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `commissary_accounts`
--
ALTER TABLE `commissary_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `formulation`
--
ALTER TABLE `formulation`
  MODIFY `form_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `formulation_details`
--
ALTER TABLE `formulation_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `franchisee`
--
ALTER TABLE `franchisee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `franchise_accounts`
--
ALTER TABLE `franchise_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `franchise_branch`
--
ALTER TABLE `franchise_branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `franchise_renewal_log`
--
ALTER TABLE `franchise_renewal_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1163;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=180;

--
-- AUTO_INCREMENT for table `order_payments`
--
ALTER TABLE `order_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `order_returns`
--
ALTER TABLE `order_returns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `order_return_items`
--
ALTER TABLE `order_return_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `payroll`
--
ALTER TABLE `payroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `payroll_employee`
--
ALTER TABLE `payroll_employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `production`
--
ALTER TABLE `production`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `production_items`
--
ALTER TABLE `production_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `products_itemize`
--
ALTER TABLE `products_itemize`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4291;

--
-- AUTO_INCREMENT for table `product_journal`
--
ALTER TABLE `product_journal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `purchase_order_items`
--
ALTER TABLE `purchase_order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `received_po`
--
ALTER TABLE `received_po`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `received_po_items`
--
ALTER TABLE `received_po_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `franchisee`
--
ALTER TABLE `franchisee`
  ADD CONSTRAINT `franchisee_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `franchise_branch` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
