<?php
	require_once("../api/config.php");
?>
<title>SI <?php echo $_GET['or']?></title>
<style>
body {
  background-image: url('receipts/si.jpg');
  background-repeat: no-repeat;
  background-size: 100% 100%;
  color: blue;
}
</style>
<body>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

<?php
    if(isset($_GET['or'])){

        $values = array();

        $franchisee = "";
        $branch = "";
        $items = "";

        if (isset($_GET['x'])) {

            $database->where("code", $_GET['or']);
            $disposal = $database->getOne(PRODUCT_DISPOSAL);

            $database->where("disposal_id", $disposal["id"]);
            $disposalItems = $database->get(PRODUCT_DISPOSAL_ITEMS);

            $values["or"] = $disposal;
            $values["items"] = $disposalItems;

            $franchiseeDetails = "";
            $name = "";
            $tin_number = "";
            $date = date("Y-m-d", strtotime($values["or"]["date_time"]));
            $branch = "";
            
            $total = 0;
            foreach($values["items"] as $item){

                $total = $total + $item['total_amount'];
                $product = getProduct($item["item_id"]);
                $items = $items . '<tr>
                            <td style="width:120px;"><center>'.abs($item["qty"]).'</center></td>
                            <td style="width:100px;">'.$product["uom"].'</td>
                            <td style="width:200px;">'.$product["description"].'</td>
                            <td style="width:200px;text-align:right">'.number_format($item['cost']).'&emsp;</td>
                            <td style="text-align:right">&emsp;&emsp;&emsp;'.number_format($item['total_amount']).'</td>
                        </tr>';
            }

            $totalAmount = number_format($total);
            $netVat = number_format($total / 1.12);
            $lessVat = number_format(($total / 1.12) * 0.12);

        } else {
            $database->where("order_ref", $_GET['or']);
            $poDetails = $database->getOne(ORDERS);
    
            $database->where("order_id", $poDetails["id"]);
            $itemsDisposals = $database->get(ORDER_ITEMS);

            $values["or"] = $poDetails;
            $values["items"] = $itemsDisposals;

            $franchiseeDetails = getFranchise($values['or']["franchisee_id"]);
            $name = $franchiseeDetails["name"];
            $tin_number = $franchiseeDetails["tin_number"];
            $date = $values["or"]["date_ordered"];

            $branch = getBranch($franchiseeDetails['branch_id'])["branch_location"];

            foreach($values["items"] as $item){
                $product = getProduct($item["item_id"]);
                $items = $items . '<tr>
                            <td style="width:120px;"><center>'.abs($item["qty"] + $item["rec_qty"]).'</center></td>
                            <td style="width:100px;">'.$product["uom"].'</td>
                            <td style="width:200px;">'.$product["description"].'</td>
                            <td style="width:200px;text-align:right">'.number_format($item['price']).'&emsp;</td>
                            <td style="text-align:right">&emsp;&emsp;&emsp;'.number_format($item['total_amount']).'</td>
                        </tr>';
            }

            $totalAmount = number_format($values['or']['total_amount']);
            $netVat = number_format($values['or']['total_amount'] / 1.12);
            $lessVat = number_format(($values['or']['total_amount'] / 1.12) * 0.12);
        }

        $amountTable = "<table style='float:right'>
            <tr>
                <td style='text-align:right'>$totalAmount</td>
            </tr>
            <tr>
                <td style='text-align:right'>$lessVat</td>
            </tr>
            <tr>
                <td style='text-align:right'>$netVat</td>
            </tr>
            <tr>
                <td><br/></td>
            </tr>
            <tr>
                <td style='text-align:right'>$netVat</td>
            </tr>
            <tr>
                <td style='text-align:right'>$lessVat</td>
            </tr>
            <tr>
                <td style='text-align:right;font-size:20px'><b>$totalAmount</b></td>
            </tr>
        </table>";

        echo "

<pre>



                $name                                                           $date

                $tin_number 

                $branch





                <table>
                $items
                </table>
        
















                                                                                        $amountTable
</pre>
    
    ";
    } else {
        echo "File Not Found";
    }
?>
</body>
<script>
	window.print()
</script>