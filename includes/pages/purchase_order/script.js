
  const API = './api/purchase_order.php'
  const SUPPLIER_API = './api/supplier.php'
  const PRODUCT_API = './api/product_information.php'

  // get supplier list
	$.ajax({
		url: SUPPLIER_API + '?get',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			var newVal = '<option selected disabled>Select Supplier</option>'
			
			newVal += json.map(value => {
				return `<option value="${value.id}">${value.supplier_name}</option>`
			})
			$(`#supplier_id`).html(newVal)
		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
	})

  var myTable = null;
	$.fn.dataTable.ext.search.push(
		function (settings, data, dataIndex) {
			var startDate = Date.parse($('#start-date').val(), 10);
			var endDate = Date.parse($('#end-date').val(), 10);
			var columnDate = Date.parse(data[4]) || 0; // use data for the age column
			if ((isNaN(startDate) && isNaN(endDate)) ||
				(isNaN(startDate) && columnDate <= endDate) ||
				(startDate <= columnDate && isNaN(endDate)) ||
				(startDate <= columnDate && columnDate <= endDate)) {
				return true;
			}
			return false;
		}
	);

	$('.date-range-filter').change(function () {
		myTable.draw();
	});

	myTable = $('#datatable').DataTable({
		processing: true,
		serverSide: true,
		rowReorder: {
			selector: 'td:nth-child(3)'
		},
		responsive: true,
		order: [
			[0, 'desc']
		],
		buttons: [
			{
				extend: 'excel',
				text: 'Export to Excel'
			},
			{

				extend: 'pdf',
				text: 'Export to Pdf',
				orientation: 'portrait',
				filename: 'Purchase Order Information',
				paging: true,
				customize: function (doc) {
					doc.content.splice(0, 1);
					var now = new Date();
					var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
					doc.pageMargins = [20, 60, 20, 30];
					doc.defaultStyle.fontSize = 8;
					doc.styles.tableHeader.fontSize = 8;
					doc['header'] = (function () {
						return {
							columns: [{
								alignment: 'left',
								text: 'Purchase Order Information',
								fontSize: 20,
								margin: [20, 20]
							}]
						}
					});
					doc['footer'] = (function () {
						return {
							columns: [{
								alignment: 'left',
								text: 'Created on: ' + jsDate.toString(),
								margin: [10, 10]
							}]
						}
					})
				}
			}
		],
		dom: 'lBfrtip',
		"language": {
			"lengthMenu": 'Display <select>' +
				'<option value="10">10</option>' +
				'<option value="50">50</option>' +
				'<option value="70">70</option>' +
				'<option value="80">80</option>' +
				'<option value="100">100</option>' +
				'<option value="-1">All</option>' +
				'</select> records'
		},
		ajax: {
			url: API + '?get&status=PENDING',
			complete: function () {

				$('#addModal').on('click', function () {
					$('form').trigger('reset')
					$('input[type=checkbox]').prop('checked', false)
					$(`input[name*="modifyId"]`).val('')
					$(`input[name*="formAction"]`).val('add')
					$('#formModal').modal('show')
				})
			}
		}
	})

  $('form').on('submit', function(e) {
	  e.preventDefault()

	  $.blockUI({
		  baseZ: 2000
	  })

	  var data = $('form').serializeArray()
	  var params = postParams('', data)

	  $.ajax({
		  url: API,
		  type: 'post',
		  data: 'data=' + params,
		  processData: false
	  })
		  .done(data => {
			  $.unblockUI()
			  console.log('Save Changes Response: ', data)

			  responseJSON = $.parseJSON(data)

			  new PNotify(responseJSON)

			  if (responseJSON.type == 'success') {
				  myTable.ajax.reload(null, false)
				  $('#formModal').modal('hide')
			  }
		  })
		  .fail(errorThrown => {
			  $.unblockUI()
			  console.log('Save Changes Post Error: ', errorThrown)
			  return false
		  })
  })

  //delete button
  $('#deleteButton').on('click', function() {
	  if (!confirm('Are you sure you want to remove?')) {
		  return false
	  }

	  $.blockUI({
		  baseZ: 2000
	  })

	  $(`input[name*="formAction"]`).val('delete')

	  var data = $('form').serializeArray()
	  var params = postParams('', data)

	  $.ajax({
		  url: API,
		  type: 'post',
		  data: 'data=' + params,
		  processData: false
	  })
		  .done(data => {
			  $.unblockUI()
			  console.log('Delete Response: ', data)

			  responseJSON = $.parseJSON(data)
			  new PNotify(responseJSON)
			  if (responseJSON.type == 'success') {
				  myTable.ajax.reload(null, false)
				  $('#formModal').modal('hide')
			  }
		  })
		  .fail(errorThrown => {
			  $.unblockUI()
			  console.log('Delete Post Error: ', errorThrown)
			  return false
		  })

	  return false
  })

function modify(data){
	$(`input[name*="modifyId"]`).val(data)
	$(`input[name*="formAction"]`).val('edit')

	$.ajax({
		url: API + '?getDetails=' + data,
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)

			$.unblockUI()
			populateForm($('form'), json)
			$('#formModal').modal('show')
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Account Details Get Error', errorThrown)
			return false
		})
}

function viewPO(po_id){
	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: API + '?viewPO=' + po_id,
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			
			$("#po_number").html(json.po_information.po_number)
			$("#supplier_id_items").val(json.supplier_name)
			$(`input[name*="purchase_order_id"]`).val(json.po_information.id)
			loadPurchaseItemTable(json.po_information.id)
			productLookUpBySupplier(json.po_information.supplier_id)

			$.unblockUI()
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Get Error: ', errorThrown)
			return false
	})

	$('#poModal').modal('show')
}

var poItemsDataTable = null

function loadPurchaseItemTable(id){
	$('#poItemsTable').DataTable().destroy()
	poItemsDataTable = $('#poItemsTable').DataTable({
		processing: true,
		ajax: {
			url: API + '?viewPOItems=' + id,
			dataSrc: function(json) {
				var return_data = new Array()
				for (var i = 0; i < json.length; i++) {
					
					return_data.push({
						id: json[i].id,
						product_id: json[i].description ,
						qty: json[i].qty ,
						cost: json[i].cost ,
						total_amount: json[i].total_amount ,
						action: `
								<button title="DELETE" type="button" class="btn btn-danger btn-circle" onclick="deleteItem(`+json[i].id+`)"><i class="fa fa-times"></i> </button>`,
					})
				}
				return return_data
			},

			complete: function() {
				//do something soon
			}
		},
		columns: [
			{ data: 'id' },
			{ data: 'product_id' },
			{ data: 'qty' },
			{ data: 'cost' },
			{ data: 'total_amount' },
			{ data: 'action' },
		],
		order: [[0, 'desc']],
		rowReorder: {
			selector: 'td:nth-child(4)'
		},
		responsive: true,
	})
}

function loadPODetails(){
	var po_number = $("#po_number_receive").val()
	$("#supplier_name").val("")
	$("#total_po_amount").val("")

	$('#poItemsTableReceiving').DataTable().destroy()
	poItemsDataTable = $('#poItemsTableReceiving').DataTable({
		processing: true,
		ajax: {
			url: API + '?viewReceivedPO=' + po_number ,
			dataSrc: function(json) {
				var return_data = new Array()
				var total_po_amount = 0;

				for (var i = 0; i < json.length; i++) {
					$("#supplier_name").val(json[i].supplier)
					$("#po_table_id").val(json[i].po_id)
					$("#received_by").val(json[i].received_by)
					$("#date_received").val(json[i].date_received)
					$("#remarks").val(json[i].remarks)

					total_po_amount = total_po_amount + json[i].total_amount;

					return_data.push({
						id: json[i].id,
						product_id: `<select class="form-control" name="po_product_`+json[i].id+`"><option value="`+json[i].product_id+`">`+json[i].description+`</option></select>` ,
						cost : `<input type="text" style="text-align:right" id="po_cost_`+json[i].id+`" readonly name="po_cost_`+json[i].id+`"  value="`+formatNumber(json[i].cost)+`">`,
						qty: json[i].qty ,
						receive_qty: `<input type="number" style="text-align:right" id="po_qty_`+json[i].id+`" name="po_qty_`+json[i].id+`" value="`+json[i].rec_qty+`">`,
						total_amount: `<input type="text" style="text-align:right" readonly id="po_item_total_`+json[i].id+`" name="po_item_total_`+json[i].id+`" value="`+formatNumber(json[i].total_amount)+`">`
					})
				}

				$("#total_po_amount").val(formatNumber(total_po_amount))

				return return_data
			},

			complete: function() {
				//do something soon
			}
		},
		columns: [
			{ data: 'id' },
			{ data: 'product_id' },
			{ data: 'cost'} ,
			{ data: 'qty' },
			{ data: 'receive_qty' },
			{ data: 'total_amount' },
		],
		order: [[0, 'desc']],
		rowReorder: {
			selector: 'td:nth-child(4)'
		},
		responsive: true,
	})
}

function saveReceivingPO(){
	if(!confirm("Please check the details before receiving, You want to proceed? ")){
		return
	}

	$.blockUI({
		baseZ: 2000
	})

	var form = $("#receivePOForm").serializeArray()
	var params = postParams('receiving', form)

	$.ajax({
		url: API,
		type: 'post',
		data: 'saveReceivingPO=' + params,
		processData: false
	})
		.done(data => {
			$.unblockUI()
			
			responseJSON = $.parseJSON(data)
			new PNotify(responseJSON)
			myTable.ajax.reload(null, false)

			$("#receiveModal").modal('hide')
			$("#receivePOForm").trigger('reset')
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Post Error: ', errorThrown)
			return false
		})
}


function productLookUpBySupplier(supplier_id){

	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: PRODUCT_API + '?getProductBySupplierId=' + supplier_id,
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			var newVal = '<option selected disabled>Select Product</option>'
			
			newVal += json.map(value => {
				return `<option value="${value.id}">${value.description}</option>`
			})
			$(`#product_id`).html(newVal)
			$.unblockUI()
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Get Error: ', errorThrown)
			return false
	})
}

function productCostLookUp(productId){
	$.ajax({
		url: PRODUCT_API + '?getDetails=' + productId,
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			
			$("#cost").val(json.cost)
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Account Details Get Error', errorThrown)
			return false
		})
}

function addItem(){
	$.blockUI({
		baseZ: 2000
	})

	var form = $("#poForm").serializeArray()
	var params = postParams('add', form)

	$.ajax({
		url: API,
		type: 'post',
		data: 'poItems=' + params,
		processData: false
	})
		.done(data => {
			$.unblockUI()
			
			responseJSON = $.parseJSON(data)
			new PNotify(responseJSON)
			loadPurchaseItemTable($(`input[name*="purchase_order_id"]`).val())
			myTable.ajax.reload(null, false)
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Post Error: ', errorThrown)
			return false
		})
}

function deleteItem(po_item_id){

	if(!confirm("Are you sure you want to delete this item on the po ?")){
		return
	}

	$.blockUI({
		baseZ: 2000
	})
	
	$.ajax({
		url: API,
		type: 'post',
		data: 'deletePoItem=' + po_item_id,
		processData: false
	})
		.done(data => {
			$.unblockUI()
			
			responseJSON = $.parseJSON(data)
			new PNotify(responseJSON)
			loadPurchaseItemTable($(`input[name*="purchase_order_id"]`).val())
			myTable.ajax.reload(null, false)
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Post Error: ', errorThrown)
			return false
		})
}

function calculate(){
	var cost = $("#cost").val() == "" ? 0 : $("#cost").val()
	var qty = $("#qty").val() == "" ? 0 : $("#qty").val()

	$("#total_amount").val(cost * qty)
}

function clear(){
	$("#cost").val("")
	$("#qty").val("")
	$("#total_amount").val("")
}

function receivePO(id){
	$("#po_number_receive").val(id)
	$("#receiveModal").modal('show')
	loadPODetails()
}

function viewAll(){
	myTable.ajax.url(API + '?get').load()
}

function viewApproved(){
	myTable.ajax.url(API + '?get&status=APPROVED').load()
}

function viewPending(){
	myTable.ajax.url(API + '?get&status=PENDING').load()
}

function viewReceived(){
	myTable.ajax.url(API + '?get&status=RECEIVED').load()
}

function viewRejected(){
	myTable.ajax.url(API + '?get&status=REJECTED').load()
}

function printPO(){
	window.open('reports/purchase_order.php?po='+ $("#po_number").html(), '_blank')
}