
const API = './api/product_information.php'
const IMAGE = './api/upload_image.product.php'
const SUPPLIER_API = './api/supplier.php'

// get supplier list
$.ajax({
	url: SUPPLIER_API + '?get',
	processData: false
})
	.done(data => {
		var json = $.parseJSON(data)
		var newVal = '<option selected disabled>Select Supplier</option>'

		newVal += json.map(value => {
			return `<option value="${value.id}">${value.supplier_name}</option>`
		})
		$(`#supplier_id`).html(newVal)
	})
	.fail(errorThrown => {
		console.log('Get Error: ', errorThrown)
		return false
	})

var myTable = null;
$.fn.dataTable.ext.search.push(
	function (settings, data, dataIndex) {
		var startDate = Date.parse($('#start-date').val(), 10);
		var endDate = Date.parse($('#end-date').val(), 10);
		var columnDate = Date.parse(data[4]) || 0; // use data for the age column
		if ((isNaN(startDate) && isNaN(endDate)) ||
			(isNaN(startDate) && columnDate <= endDate) ||
			(startDate <= columnDate && isNaN(endDate)) ||
			(startDate <= columnDate && columnDate <= endDate)) {
			return true;
		}
		return false;
	}
);

$('.date-range-filter').change(function () {
	myTable.draw();
});

myTable = $('#datatable').DataTable({
	processing: true,
	serverSide: true,
	rowReorder: {
		selector: 'td:nth-child(3)'
	},
	responsive: true,
	order: [
		[0, 'desc']
	],
	buttons: [
		{
			extend: 'excel',
			text: 'Export to Excel'
		},
		{

			extend: 'pdf',
			text: 'Export to Pdf',
			orientation: 'portrait',
			filename: 'Product Information',
			paging: true,
			customize: function (doc) {
				doc.content.splice(0, 1);
				var now = new Date();
				var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
				doc.pageMargins = [20, 60, 20, 30];
				doc.defaultStyle.fontSize = 8;
				doc.styles.tableHeader.fontSize = 8;
				doc['header'] = (function () {
					return {
						columns: [{
							alignment: 'left',
							text: 'Product Information',
							fontSize: 20,
							margin: [20, 20]
						}]
					}
				});
				doc['footer'] = (function () {
					return {
						columns: [{
							alignment: 'left',
							text: 'Created on: ' + jsDate.toString(),
							margin: [10, 10]
						}]
					}
				})
			}
		}
	],
	dom: 'lBfrtip',
	"language": {
		"lengthMenu": 'Display <select>' +
			'<option value="10">10</option>' +
			'<option value="50">50</option>' +
			'<option value="70">70</option>' +
			'<option value="80">80</option>' +
			'<option value="100">100</option>' +
			'<option value="-1">All</option>' +
			'</select> records'
	},
	ajax: {
		url: API + '?get',
		complete: function () {

			$('#addModal').on('click', function () {
				$('form').trigger('reset')
				$('input[type=checkbox]').prop('checked', false)
				$(`input[name*="modifyId"]`).val('')
				$(`input[name*="formAction"]`).val('add')
				$('#formModal').modal('show')
			})
		}
	}
})

$('form').on('submit', function (e) {
	e.preventDefault()

	$.blockUI({
		baseZ: 2000
	})

	var data = $('form').serializeArray()
	var params = postParams('', data)

	$.ajax({
		url: API,
		type: 'post',
		data: 'data=' + params,
		processData: false
	})
		.done(data => {
			$.unblockUI()
			console.log('Save Changes Response: ', data)

			responseJSON = $.parseJSON(data)

			new PNotify(responseJSON)

			if (responseJSON.type == 'success') {
				saveImage()
				myTable.ajax.reload(null, false)
				$('#formModal').modal('hide')
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
})

//delete button
$('#deleteButton').on('click', function () {
	if (!confirm('Are you sure you want to remove?')) {
		return false
	}

	$.blockUI({
		baseZ: 2000
	})

	$(`input[name*="formAction"]`).val('delete')

	var data = $('form').serializeArray()
	var params = postParams('', data)

	$.ajax({
		url: API,
		type: 'post',
		data: 'data=' + params,
		processData: false
	})
		.done(data => {
			$.unblockUI()
			console.log('Delete Response: ', data)

			responseJSON = $.parseJSON(data)
			new PNotify(responseJSON)
			if (responseJSON.type == 'success') {
				myTable.ajax.reload(null, false)
				$('#formModal').modal('hide')
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Delete Post Error: ', errorThrown)
			return false
		})

	return false
})

function saveImage() {
	var formData = new FormData();
	formData.append('image_file', $('#image_file')[0].files[0]);

	$.ajax({
		url: IMAGE,
		type: 'post',
		data: formData,
		cache: false,
		contentType: false,
		processData: false
	})
		.done(data => {

			response = JSON.parse(data)
			myTable.ajax.reload(null, false)
		})
		.fail(errorThrown => {
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})

}

function modify(data) {
	$(`input[name*="modifyId"]`).val(data)
	$(`input[name*="formAction"]`).val('edit')

	$.ajax({
		url: API + '?getDetails=' + data,
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)

			$.unblockUI()
			populateForm($('form'), json)
			$('#formModal').modal('show')
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Account Details Get Error', errorThrown)
			return false
		})
}

function viewProductJournal(id) {

	$.blockUI({
		baseZ: 2000
	})

	$('#inventoryTable').DataTable().destroy()
	var poItemsDataTable = $('#inventoryTable').DataTable({
		processing: true,
		ajax: {
			url: API + '?viewProductJournal=' + id,
			dataSrc: function (json) {
				var return_data = new Array()
				for (var i = 0; i < json.length; i++) {

					return_data.push({
						item: json[i].item,
						transaction: json[i].transaction,
						ref_no: json[i].ref_no,
						qty: json[i].qty,
						updated_by : json[i].updated_by,
						date_time: moment(json[i].date_time).format('LLL'),
					})
				}
				return return_data
			},

			complete: function () {
				$("#inventoryModal").modal("show")
				$.unblockUI()
			}
		},
		columns: [
			{ data: 'item' },
			{ data: 'transaction' },
			{ data: 'ref_no' },
			{ data: 'qty' },
			{ data: 'updated_by' },
			{ data: 'date_time' },
		],
		rowReorder: {
			selector: 'td:nth-child(4)'
		},
		responsive: true,
	})
}


function viewFood() {
	myTable.ajax.url(API + '?get&food').load()
}

function viewNonFood() {
	myTable.ajax.url(API + '?get&nonfood').load()
}

function viewOthers() {
	myTable.ajax.url(API + '?get&others').load()
}

function viewAll() {
	myTable.ajax.url(API + '?get').load()
}

function actualCount(id) {
	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: API + '?getDetails=' + id,
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)

			$.unblockUI()
			$("#item_id").html(id)
			$("#item_code").val(json.product_code)
			$("#stocks").val(json.stocks)
			$("#actualCountModal").modal("show")
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Account Details Get Error', errorThrown)
			return false
		})
}

function saveActualCount(){
	if (!confirm('Are you sure you want to save the actual count?')) {
		return false
	}

	var params = {
		item_id : $("#item_id").html(),
		actual_stocks : $("#actual_stocks").val(),
		reason : $("#reason").val(),
		expiration : $("#expiration").val(),
		manufacturing : $("#manufacturing").val(),
		batch_no : $("#batch_no").val()
	}

	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: API + '?saveActual=' + JSON.stringify(params),
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)

			$.unblockUI()
			new PNotify(json)
			$("#actualCountModal").modal("hide")
			myTable.ajax.reload(null, false)
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Account Details Get Error', errorThrown)
			return false
		})
}

