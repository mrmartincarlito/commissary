const FRANCHISEE_ENTRIES_API = './api/franchise_entries.php';
const SUPPLIER_API = './api/supplier.php'
const PRODUCT_API = './api/product_information.php'
const REPORTS_API = './api/reports.php'
const BRANCH_API = './api/franchise_branch.php'

var selectedReport = ""

function joborder(){
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "")
	$("#joborder").css("display", "")
	selectedReport = "jobOrder"
}

function finishedGoods() {
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "")
	$("#joborder").css("display", "")
	selectedReport = "finishedGoods"
}

function usedGoods() {
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "")
	$("#joborder").css("display", "")
	selectedReport = "usedGoods"
}


function preview(){
	var data = {
		"report" : selectedReport,
		"data" : {
			"dateFrom" : $("#date_from").val(),
			"dateTo" : $("#date_to").val(),
			"job_order" : $("#job_order_number").val(),
			
		}
	}

	$.ajax({
		url: REPORTS_API + '?' + selectedReport + '=' + JSON.stringify(data),
		processData: false
	})
		.done(data => {
			var json = JSON.parse(data)

			$("#caption").html(json.caption)

			//get thead
			var th = "<tr>"
			$.each(json.columns, function (i, item) {
				th = th + "<th style='text-align:center'>"+item+"</th>"
			});
			th = th + "</tr>"
			$("#thead").html(th)

			var tr = ""

			if (json.rows.length == 0) {
				tr = tr + "<tr><td style='text-align:center' colspan='"+json.columns.length+"'>No Record Found</td></tr>"
			}

			//get tbody for joborder
			if(selectedReport == "jobOrder"){
				$.each(json.rows, function (i, item) {
					var is_started = "NO"
					
					if (item.is_started == "1") {
						is_started = "YES"
					}

					if (item.is_started == "2") {
						is_started = "FINISHED"
					}


					tr = tr + "<tr>"+
						"<td>"+item.job_order+"</td>"+
						"<td>"+item.start_date+"</td>"+
						"<td>"+item.end_date+"</td>"+
						"<td>"+is_started+"</td>"+
						"<td>"+item.added_by+"</td>"+
					"</tr>"
				});
			}

			//get tbody for finished goods
			if(selectedReport == "finishedGoods"){
				$.each(json.rows, function (i, item) {
					
					tr = tr + "<tr>"+
						"<td>"+item.job_order+"</td>"+
						"<td>"+item.batch_no+"</td>"+
						"<td>"+item.product+"</td>"+
						"<td>"+item.produced_qty+"</td>"+
						"<td>"+item.expiration+"</td>"+
						"<td>"+item.manufacturing+"</td>"+
					"</tr>"
				});
			}

			//get tbody for used goods
			if(selectedReport == "usedGoods"){
				$.each(json.rows, function (i, item) {
					
					tr = tr + "<tr>"+
						"<td>"+item.job_order+"</td>"+
						"<td>"+item.product+"</td>"+
						"<td>"+item.used_qty+"</td>"
					"</tr>"
				});
			}

			$("#tbody").html(tr)

			var others = "<table class='table table-bordered'>"
			//other data on report
			$.each(json.others, function (i, item) {
				var value = item.value
				
				if(item.is_numeric == "true"){
					value = formatNumber(item.value)
				}
				others = others + "<tr><td style='text-align:center'>"+item.name+"</td><td style='text-align:right'><b>"+  value +"</b></td></tr>"
			})

			others = others + "</table>"

			$("#others").html(others)

		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
		})
}

function printReport(printpage){
	var headstr = "<html><head><title></title></head><body>";
	var footstr = "</body>";
	var newstr = document.all.item(printpage).innerHTML;
	var oldstr = document.body.innerHTML;
	document.body.innerHTML = headstr + newstr + footstr;
	window.print();
	document.body.innerHTML = oldstr;
	return false;
}


getSupplier()
function getSupplier(){
	  // get supplier list
	  $.ajax({
		url: SUPPLIER_API + '?get',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			var newVal = '<option selected  value="">Select Supplier</option>'
			
			newVal += json.map(value => {
				return `<option value="${value.id}">${value.supplier_name}</option>`
			})
			$(`#supplier_id`).html(newVal)
		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
	})
}

getProducts()
function getProducts(){
	$.ajax({
		url: PRODUCT_API + '?getAll',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			var newVal = '<option selected value="">All Products</option>'
			
			newVal += json.map(value => {
				return `<option value="${value.id}">${value.description}</option>`
			})
			$(`#product_id`).html(newVal)
			$.unblockUI()
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Get Error: ', errorThrown)
			return false
	})
}
