$(document).ready(() => {
	$('form').submit(function(e) {
		var data = $(this).serializeArray()
		var params = postParams('login', data)

		$.blockUI({
			baseZ: 2000
		})

		$.ajax({
			url: './api/auth.php',
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				
				$.unblockUI()

				responseJSON = $.parseJSON(data)
				new PNotify(responseJSON)
				if (responseJSON.type == 'success') {
					window.location.href = './?'
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				alert("Something was wrong, Please contact your adminstrator")
				console.log('Login POST Response: ', errorThrown)
				console.log(errorThrown)
				return false
			})

		e.preventDefault()
	})
})

function forgot(){
	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: './api/forgot_password.php',
		type: 'post',
		data: 'reset=' + $("#email").val(),
		processData: false
	})
		.done(data => {
			
			$.unblockUI()
			
			responseJSON = $.parseJSON(data)
			
			new PNotify(responseJSON)
			
		})
		.fail(errorThrown => {
			$.unblockUI()
			alert("Something was wrong, Please contact your adminstrator")
			console.log('Login POST Response: ', errorThrown)
			console.log(errorThrown)
			return false
		})
}
