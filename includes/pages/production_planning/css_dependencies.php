<?php
  //in order
  $csses = [
    "plugins/bower_components/datatables/jquery.dataTables.min.css", //jquery datatable css
    "plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css", //bootstrap datepicker css
    "plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css", //bootstrap spinner css
    "plugins/bower_components/sweetalert/sweetalert.css", //swal js
    "plugins/bower_components/bootstrap-select/bootstrap-select.min.css",
    "plugins/bower_components/custom-select/custom-select.css",
  ];

  foreach($csses as $css){
    echo '<link href="'.$css.'" rel="stylesheet">
    ';
  }
?>