const FORMULATION_API = './api/formulation.php'
const PRODUCTION_PLANNING_API = './api/production_planning.php'
const EMPLOYEE_API = './api/employee.php'

$(document).ready(function () {
	var myTable = $('#datatable').DataTable({
		processing: true,
		ajax: {
			url: PRODUCTION_PLANNING_API + '?get',
			dataSrc: function (json) {
				var return_data = new Array()
				for (var i = 0; i < json.length; i++) {
					let status = ''
					let buttonAction = ''
					if (json[i].is_started == 2) {
						status = '<b style="color:green">Finished</b>'
						buttonAction = '<button type="button" class="btn btn-primary" onclick="empProduction(' + json[i].production_id + ')">EMPLOYEE PRODUCTION</button>'
					} else if (json[i].is_started == 1) {
						status = '<b style="color:red">Yes</b>'
					} else {
						status = '<b style="color:blue">No</b>'
					}

					return_data.push({
						id: json[i].production_id,
						job_order: json[i].job_order,
						batch_no: json[i].batch_no,
						description: json[i].description,
						start_date: moment(json[i].start_date).format('LL'),
						end_date: moment(json[i].end_date).format('LL'),
						added_by: json[i].added_by,
						date_time: moment(json[i].date_time).format('LLL'),
						is_started: status,
						action : buttonAction
					})
				}
				return return_data
			},

			complete: function () {
				$('#datatable tbody').on('dblclick', 'tr', function () {
					$('form').trigger('reset')

					var data = $('#datatable')
						.DataTable()
						.row(this)
						.data().id
					$(`input[name*="modifyId"]`).val(data)
					$(`input[name*="mode"]`).val('edit')
					$(`#deleteButton`).show()

					$.blockUI()

					$.ajax({
						url: PRODUCTION_PLANNING_API + '?getDetails=' + data,
						processData: false
					})
						.done(data => {
							var json = $.parseJSON(data)

							$.ajax({
								url:
									PRODUCTION_PLANNING_API + '?getItemsToProduce=' +
									json.production_id,
								processData: false
							}).done(data => {
								var itemsJson = $.parseJSON(data)

								items_to_produce.clear().draw()

								let mode = $(`input[name*="mode"]`).val()
								itemsJson.forEach(item => {
									addItemToItemsTable(mode, json.is_started, item)
								})

								$('#formModal :input').prop('disabled', false)
								$('[type="submit"]').show()
								$('#finish_production').hide()
								$('#start_production').show()
								if (json.is_started == 1) {
									$('#formModal :input').prop('disabled', true)
									$('[type="hidden"]').prop('disabled', false)
									$('[data-dismiss="modal"]').prop('disabled', false)
									$('#datatable_items_to_produce_wrapper  :input').prop(
										'disabled',
										false
									)
									$('[type="submit"]').hide()
									$('#finish_production').show()
									$('#start_production').hide()
									$('#finish_production').prop('disabled', false)
									$('[class*="bootstrap-touchspin"]').prop('disabled', false)
								} else if (json.is_started == 2) {
									$('#finish_production').hide()
									$('#start_production').hide()
									$('#formModal :input').prop('disabled', true)
									$('[data-dismiss="modal"]').prop('disabled', false)
								}

								$.unblockUI()
								populateForm($('form'), json)
								$('#formModal').modal('show')
							})
						})
						.fail(errorThrown => {
							$.unblockUI()
							console.log('supplier Details Get Error', errorThrown)
							return false
						})
				})
			}
		},
		columns: [
			{ data: 'id' },
			{ data: 'job_order' },
			{ data: 'batch_no' },
			{ data: 'start_date' },
			{ data: 'end_date' },
			{ data: 'added_by' },
			{ data: 'date_time' },
			{ data: 'is_started' },
			{ data: 'action' }
		],
		order: [[0, 'desc']]
	})

	var items_to_produce = $('#datatable_items_to_produce').DataTable({})

	$('#addModal').on('click', function () {
		$('form').trigger('reset')
		$(`input[name*="modifyId"]`).val('')
		$(`input[name*="mode"]`).val('add')
		$(`#deleteButton`).hide()
		items_to_produce.clear().draw()
		$('#formModal').modal('show')
		$('#formModal :input').prop('disabled', false)
		$('[type="submit"]').show()
		$('#finish_production').hide()
		$('#start_production').hide()
		$("#addItem").show()
	})

	$('#addItem').on('click', function () {
		var data = $('#item_id').val()

		var exists = false
		items_to_produce.rows().every(function (rowIdx, tableLoop, rowLoop) {
			var orderID = $(this.data()[0]).html()

			if (data == orderID) {
				exists = true
				return
			}
		})

		if (exists) {
			new PNotify({ "type": "error", "title": "Error", "text": "Item Already Exists" })
		} else {
			$.ajax({
				url: FORMULATION_API + '?getDetails=' + data,
				processData: false
			})
				.done(data => {
					var json = $.parseJSON(data)

					addItemToItemsTable($('#mode').val(), 0, json)
				})
				.fail(errorThrown => {
					$.unblockUI()
					console.log('Items Details Get Error', errorThrown)
					return false
				})
		}
	})

	function addItemToItemsTable(mode, is_started, json) {
		let currRowCount = items_to_produce.rows().count()
		console.log(mode, is_started, json)
		if (mode == 'add') {
			var exists = false
			items_to_produce.rows().every(function (rowIdx, tableLoop, rowLoop) {
				var orderID = $(this.data()[0]).html()

				if (json[0].id == orderID) {
					exists = true
					return
				}
			})
			if (!exists) {
				items_to_produce.row
					.add([
						`<p id="item_id_${currRowCount}" 
					data-index="${currRowCount}">${json[0].id}</p>`,
						`<p id="prodname_${currRowCount}"
					data-index="${currRowCount}">${json[0].name}</p>`,
						`<p
					data-index="${currRowCount}"><select id="formulation_id_${currRowCount}" class="form-control">${json.map(
							value => {
								return `<option value="${value.form_id}">${value.description
									}</option>`
							}
						)}</select></p>`,
						`<input class="vertical-spin" 
					type="text" 
					data-bts-button-down-class="btn btn-default btn-outline" 
					data-bts-button-up-class="btn btn-default btn-outline" 
					value="1" 
					name="production_qty" 
					id="production_qty_${currRowCount}"
					data-index="${currRowCount}"
					> <label>by <b id="kilo_${currRowCount}">${json[0].kilo}</b> kilo(s)</label>
					<label> = <b id="pcs_${currRowCount}">${json[0].produce}</b> pc(s)</label>
					`,
						`<input class="form-control" 
					type="text" 
					data-bts-button-down-class="btn btn-default btn-outline" 
					data-bts-button-up-class="btn btn-default btn-outline" 
					value="0" 
					readonly
					name="final_qty" 
					id="final_qty_${currRowCount}"
					data-index="${currRowCount}">`,
						`<input class="form-control" 
					type="text" 
					data-bts-button-down-class="btn btn-default btn-outline" 
					data-bts-button-up-class="btn btn-default btn-outline" 
					value="0" 
					readonly
					name="credit" 
					readonly
					id="credit_${currRowCount}"
					data-index="${currRowCount}">`,
					//produced in pcs
					`<input class="form-control" 
					type="text" 
					data-bts-button-down-class="btn btn-default btn-outline" 
					data-bts-button-up-class="btn btn-default btn-outline" 
					name="produced_pcs" 
					id="produced_pcs_${currRowCount}"
					data-index="${currRowCount}"
					value="${json.credit ? json.credit : 0}">`,
					`<input class="form-control" 
					type="text" 
					data-bts-button-down-class="btn btn-default btn-outline" 
					data-bts-button-up-class="btn btn-default btn-outline" 
					name="unproduced_pcs" 
					readonly
					id="unproduced_pcs_${currRowCount}"
					data-index="${currRowCount}"
					value="${json.credit ? json.credit : 0}">`,
					//end produced in pcs
						`<input class="form-control" 
					type="text" 
					name="remarks"
					id="remarks_${currRowCount}"
					data-index="${currRowCount}">`,
						`<button class="btn-danger">X</button>`
					])
					.draw(true)
					.node().id = json[0].id
			}
		}

		if (mode == 'edit') {
			$("#addItem").hide()
			items_to_produce.row
				.add([
					`<p id="item_id_${currRowCount}" 
				data-index="${currRowCount}">${json.id}</p>`,
					`<p id="prodname_${currRowCount}"
				data-index="${currRowCount}">${json.name}</p>`,
					`<p
					data-index="${currRowCount}"><select id="formulation_id_${currRowCount}" class="form-control">${json.formulations.map(
						value => {
							return `<option value="${value.form_id}">${value.description
								}</option>`
						}
					)}</select></p>`,
					`<input class="${is_started == 1 ? 'form-control' : 'vertical-spin'}" 
				type="text" 
				data-bts-button-down-class="btn btn-default btn-outline" 
				data-bts-button-up-class="btn btn-default btn-outline" 
				name="production_qty" 
				${is_started == 1 ? 'readonly' : ''}
				id="production_qty_${currRowCount}"
				data-index="${currRowCount}"
				value="${json.production_qty ? json.production_qty : 0}"> 
				<label>by <b id="kilo_${currRowCount}">${json.formulations[0].kilo}</b> kilo(s)</label>
				<label> = <b id="pcs_${currRowCount}">${json.formulations[0].produce}</b> pc(s)</label>`,
					`<input class="
						${is_started == 1 ? 'vertical-spin' : 'form-control'}" 
				type="text" 
				data-bts-button-down-class="btn btn-default btn-outline" 
				data-bts-button-up-class="btn btn-default btn-outline" 
				name="final_qty" 
				${is_started == 1 ? '' : 'readonly'}
				id="final_qty_${currRowCount}"
				data-index="${currRowCount}"
				value="${json.final_qty ? json.final_qty : 0}">`,
					`<input class="form-control" 
				type="text" 
				data-bts-button-down-class="btn btn-default btn-outline" 
				data-bts-button-up-class="btn btn-default btn-outline" 
				name="credit" 
				readonly
				id="credit_${currRowCount}"
				data-index="${currRowCount}"
				value="${json.credit ? json.credit : 0}">`,
				//produced in pcs
				`<input class="form-control" 
				type="text" 
				data-bts-button-down-class="btn btn-default btn-outline" 
				data-bts-button-up-class="btn btn-default btn-outline" 
				name="produced_pcs" 
				id="produced_pcs_${currRowCount}"
				data-index="${currRowCount}"
				value="${json.credit ? json.credit : 0}">`,
				`<input class="form-control" 
				type="text" 
				data-bts-button-down-class="btn btn-default btn-outline" 
				data-bts-button-up-class="btn btn-default btn-outline" 
				name="unproduced_pcs" 
				id="unproduced_pcs_${currRowCount}"
				data-index="${currRowCount}"
				value="${json.credit ? json.credit : 0}">`,
				//end produced in pcs
					`<input class="form-control" 
				type="text" 
				name="remarks"
				id="remarks_${currRowCount}"
				data-index="${currRowCount}"
				value="${json.remarks}">`,
					`<button class="btn-danger">X</button>`
				])
				.draw(true)
				.node().id = json.id
		}

		$('[id^=final_qty]').on('change focus blur', function () {
			let index = $(this).data('index')

			let production_qty = $('#production_qty_' + index).val()
			let produced_qty = $('#final_qty_' + index).val()

			$('#credit_' + index).val(production_qty - produced_qty)
		})

		$('[id^=production_qty]').on('change focus blur', function () {
			let index = $(this).data('index')

			let production_qty = $('#production_qty_' + index).val()
			let kilo = $('#kilo_' + index).html()
			let pcs = $("#pcs_" + index).html()

			$('#produced_pcs_' + index).val(production_qty * pcs)
		})
		

		$('.vertical-spin').TouchSpin({
			verticalbuttons: true,
			verticalupclass: 'ti-plus',
			verticaldownclass: 'ti-minus',
			max : 1000,
			step : 1
		})
	}

	function getItemsToProduce() {
		itemsArray = []
		items_to_produce.rows().every(function (rowIdx, tableLoop, rowLoop) {
			itemsArray.push({
				item_id: $('#item_id_' + rowIdx).html(),
				formulation_id: $('#formulation_id_' + rowIdx).val(),
				production_qty: $('#production_qty_' + rowIdx).val(),
				final_qty: $('#final_qty_' + rowIdx).val(),
				credit: $('#credit_' + rowIdx).val(),
				remarks: $('#remarks_' + rowIdx).val()
			})
		})

		return itemsArray
	}

	$('#datatable_items_to_produce tbody').on(
		'click',
		'button.btn-danger',
		function () {
			items_to_produce
				.row($(this).parents('tr'))
				.remove()
				.draw()
		}
	)

	//delete button
	$('#deleteButton').on('click', function () {
		if (!confirm('Are you sure you want to remove this plan?')) {
			return false
		}

		$.blockUI({
			baseZ: 2000
		})

		$(`input[name*="mode"]`).val('delete')

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: PRODUCTION_PLANNING_API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Delete Response: ', data)

				responseJSON = $.parseJSON(data)
				new PNotify(responseJSON)
				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Delete Post Error: ', errorThrown)
				return false
			})

		return false
	})

	$('#start_production').on('click', function (e) {
		$.ajax({
			url:
				PRODUCTION_PLANNING_API + '?checkProduction=' +
				$('[name="modifyId"]').val(),
			type: 'get',
			processData: false
		})
			.done(data => {
				var data = $.parseJSON(data)

				if (data.type == 'success') {
					var text = ''
					var hasProblem = false
					data.simulation_log.forEach(element => {
						if (element.includes('Planned')) {
							text +=
								'<p style="color: blue; font-size:15px;">' +
								element +
								'</p><br>'
						} else if (element.includes('Estimated')) {
							text +=
								'<p style="color: green; font-size: 15px;">' +
								element +
								'</p><hr>'
						} else if (element.includes('Not enough')) {
							hasProblem = true
							text +=
								'<p style="color: red; font-size: 15px;">' + element + '</p><br>'
						}
					})
					text +=
						'<p style="color: black; font-size: 15px;">Above is a simulation of the production</p><br>'

					function submitAnyway() {
						if (confirm('Do you want to really want to start production?')) {
							$.ajax({
								url: PRODUCTION_PLANNING_API,
								type: 'post',
								data: 'startProduction=' + $('[name="modifyId"]').val(),
								processData: false
							})
								.done(data => {
									$.unblockUI()
									console.log('Save Changes Response: ', data)

									responseJSON = $.parseJSON(data)

									new PNotify(responseJSON)

									if (responseJSON.type == 'success') {
										myTable.ajax.reload(null, false)
										$('#formModal').modal('hide')
									}
								})
								.fail(errorThrown => {
									$.unblockUI()
									console.log('Save Changes Post Error: ', errorThrown)
									return false
								})
						}
					}

					if (hasProblem) {
						swal(
							{
								title: 'Problem',
								html: true,
								text: '<div style="text-align: left;">' + text + '</div>',
								// showCancelButton: true,
								// confirmButtonText: 'Proceed anyway',
								// cancelButtonText: 'Cancel',
								// closeOnConfirm: true
							},
							// function(isConfirm) {
							// 	if (isConfirm) {
							// 		submitAnyway()
							// 	}
							// }
						)
					} else {
						submitAnyway()
					}
				} else {
					new PNotify(data)
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Check Production Error: ', errorThrown)
				return false
			})
	})

	$('#finish_production').on('click', function (e) {
		swal(
			{
				title: 'Prompt',
				text:
					'Do you really want to mark production as finished? Details wont be editable anymore!',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes',
				cancelButtonText: 'No',
				closeOnConfirm: true
			},
			function () {
				$('#mode').val('finishprod')
				$('form').trigger('submit')
			}
		)
	})

	$('form').on('submit', function (e) {
		e.preventDefault()

		$.blockUI({
			baseZ: 2000
		})

		var data = $('form').serializeArray()

		//get employees
		var selectedEmployees = $("#emp_production option:selected").toArray().map(item => item.value)

		let itemsArray = getItemsToProduce()
		data.push({ name: 'items_to_produce', value: itemsArray })
		data.push({ name: 'emp_production', value: selectedEmployees })

		var params = postParams('', data)


		$.ajax({
			url: PRODUCTION_PLANNING_API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				responseJSON = $.parseJSON(data)

				new PNotify(responseJSON)

				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	})

	//select non raw materials
	$.ajax({
		url: FORMULATION_API + '?get',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)

			var newVal = ``
			newVal += json.map(value => {
				return `<option value="${value.form_id}">${value.description}</option>`
			})

			//console.log(newVal)
			$(`#item_id`).html(newVal)
			$('#item_id').select2()
		})
		.fail(errorThrown => {
			console.log('Produceable Items Get Error: ', errorThrown)
			return false
		})

	//select employees
	$.ajax({
		url: EMPLOYEE_API + '?get',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)

			var newVal = ``
			newVal += json.map(value => {
				return `<option value="${value.id}">${value.first_name} ${value.last_name}</option>`
			})

			//console.log(newVal)
			$(`#emp_production`).html(newVal)
			$("#emp_production").select2({
				maximumSelectionSize: 20
			})
		})
		.fail(errorThrown => {
			console.log('Produceable Items Get Error: ', errorThrown)
			return false
		})


	$('.mydatepicker').datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd',
		todayHighlight: true
	})
})
var empIdsSelected = []
function empProduction(productionId) {
	$("#empProductionModal").modal('show')

	$.ajax({
		url: EMPLOYEE_API + '?getProductionEmployees=' + productionId,
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			
			var tr = ""
			for (var i = 0; i < json.length; i++) {
				$("#jo_number_emp_production").html(json[i].job_order)
				empIdsSelected[i] = json[i].id

				tr = tr + `
				<tr>
					<td>${json[i].id} <input type="hidden" readonly value="${json[i].id}" id="prod_${json[i].id}"></td>
					<td style="text-align:center">${json[i].first_name} ${json[i].last_name}</td>
					<td><input type="text" style="width:100%" value="${json[i].qty_made}" placeholder="Enter Number of work made" id="emp_qty_${json[i].id}"></td>
				</tr>
				`
			}
			
			$("#productionEmployees").html(tr)
		})
		.fail(errorThrown => {
			console.log('Produceable Items Get Error: ', errorThrown)
			return false
		})
}

function saveEmpChanges() {
	if (!confirm("Are you sure you want to save changes ? ")) {
		return;
	}

	var requestParams = []

	for (var i =0; i<empIdsSelected.length; i++) {
		requestParams[i] = {
			'id' : $("#prod_" + empIdsSelected[i]).val(),
			'qty' : $("#emp_qty_" + empIdsSelected[i]).val()
		}
	}

	$.ajax({
		url: EMPLOYEE_API + '?saveEmployeeProduction=' + JSON.stringify(requestParams),
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)

			new PNotify(json)

			if (json.type == 'success') {
				$('#empProductionModal').modal('hide')
			}

		})
		.fail(errorThrown => {
			console.log('Produceable Items Get Error: ', errorThrown)
			return false
		})
}