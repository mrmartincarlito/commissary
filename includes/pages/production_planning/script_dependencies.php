<?php
  //in order
  $scripts = [
    "plugins/bower_components/datatables/jquery.dataTables.min.js", //jquery datatable js
    "plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js", //bootstrap datepicker js
    "plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js", //bootstrap spinner js
    "plugins/bower_components/sweetalert/sweetalert.min.js", //swal js
    "plugins/bower_components/custom-select/custom-select.min.js",
    "plugins/bower_components/styleswitcher/jQuery.style.switcher.js",
    "plugins/bower_components/multiselect/js/jquery.multi-select.js",
    "plugins/bower_components/bootstrap-select/bootstrap-select.min.js",
  ];

  foreach($scripts as $script){
    echo '<script src="'.$script.'"></script>
    ';
  }
?>