const DISPOSAL_API = './api/product_disposal.php'
const PRODUCT_API = './api/product_information.php'

var rawTable
var products

function deleteRaw(id, remainedID) {
	var x = confirm('Are you sure you want to delete ? ')
	if (x) {
		$.ajax({
			url: DISPOSAL_API,
			type: 'post',
			data: 'deleteRawMat=' + id,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				//$('#viewModal').modal('hide')
				rawTable.ajax.reload()
				responseJSON = $.parseJSON(data)

				new PNotify(responseJSON)
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	}
}

function viewModal(form_id){
	$('#viewModal').modal('show')
	viewDataTable(form_id)
}

function viewDataTable(form_id) {
	$(`input[name*="formAction"]`).val('addRaw')
	$(`input[name*="modifyId"]`).val(form_id)
	$("#formulation_item_name").html(form_id)
	$('#datatable_raw')
		.DataTable()
		.destroy()
	rawTable = $('#datatable_raw').DataTable({
		ajax: {
			url: DISPOSAL_API + '?getDisposalTable=' + form_id,
			dataSrc: function(json) {
				var return_data = new Array()
				for (var i = 0; i < json.length; i++) {

					var selectReuseItem = ""

					if (json[i].status == "REUSED") {

						if (json[i].reuse_item_id == 0) {
							selectReuseItem = `<select class="form-control" onchange="reuseItem(`+ form_id +`, this.value, 'reuse')"><option selected disabled>Select Items</option>`
				
							selectReuseItem += products.map(value => {
								var s = ""
								if (value.id == json[i].reuse_item_id){
									s = "selected"
								}
								return `<option ${s} value="${value.id}">${value.description}</option>`
							})
	
							selectReuseItem += '</select>'
						} else {
							selectReuseItem = "Already reused as " + json[i].reuse_item
						}
						
					}	

					if (json[i].status == "DISPOSAL") {
						if (json[i].is_disposed == 0) {
							selectReuseItem = `<button value="` +json[i].id+ `" class="btn btn-danger" onclick="reuseItem(`+ form_id +`, this.value, 'disposed')">DISPOSED</button>`
						} else {
							selectReuseItem = "Already disposed"
						}
					}

					if (json[i].status == "RETURN TO SUPPLIER") {
						if (json[i].is_po_received == 0) {
							selectReuseItem = `<button value="` +json[i].id+ `" class="btn btn-info" onclick="reuseItem(`+ form_id +`, this.value, 'received')">RECEIVED</button>`
						} else {
							selectReuseItem = "Already received"
						}
					}


					return_data.push({
						id: json[i].id,
						raw_item_id: json[i].name,
						qty: json[i].req_qty,
						uom: json[i].req_uom,
						remarks: selectReuseItem,
						action:
							'<a href="#" title="Delete" onclick="deleteRaw(' +
							json[i].id +
							',' +
							json[i].raw_item_id +
							')" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>' +
							'<!--<a href="#" title="Edit" value="' +
							json[i].uom +
							'" onclick="editRaw(' +
							json[i].id +
							',this.value)" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-edit text-danger"></i> </a>-->'
					})
				}
				return return_data
			}
		},
		columns: [
			{
				data: 'id'
			},
			{
				data: 'raw_item_id'
			},
			{
				data: 'qty'
			},
			{
				data: 'uom'
			},
			{
				data: 'remarks'
			},
			{
				data: 'action'
			}
		],
		order: [[0, 'desc']]
	})
}
$(document).ready(function() {
	var myTable = $('#datatable').DataTable({
		processing: true,
		ajax: {
			url: DISPOSAL_API + '?get',
			dataSrc: function(json) {
				var return_data = new Array()
				for (var i = 0; i < json.length; i++) {

					var siButton = "";

					if (json[i].status == "DISPOSAL") {
						siButton = `<button type="button" class="btn btn-warning btn-circle" onclick="printSI('` +json[i].code+ `')" title="Sales Invoice"><i class="fa  fa-pencil-square-o"></i> </button>`;
					}

					return_data.push({
						id: json[i].id,
						code: json[i].code,
						reason: json[i].reason,
						status: json[i].status,
						added_by: json[i].added_by,
						date_time: moment(json[i].date_time).format('LLL'),
						action:
							`<button type="button" class="btn btn-info btn-circle" onclick="viewModal(` +json[i].id+ `)" title="View Disposal"><i class="fa fa-eye"></i> </button>
							<!--<button type="button" class="btn btn-success btn-circle" onclick="printDR('` +json[i].code+ `')" title="Delivery Receipt"><i class="fa  fa-book"></i> </button>-->
							` + siButton
					})
				}
				return return_data
			},

			complete: function() {
				$('#datatable tbody').on('dblclick', 'tr', function() {
					$('form').trigger('reset')

					var data = $('#datatable')
						.DataTable()
						.row(this)
						.data().id
					$(`input[name*="modifyId"]`).val(data)
					$(`input[name*="formAction"]`).val('edit')

					$.blockUI()

					$.ajax({
						url: DISPOSAL_API + '?getDetails=' + data,
						processData: false
					})
						.done(data => {
							var json = $.parseJSON(data)

							$.unblockUI()
							populateForm($('form#content_form'), json)

							$('#formModal').modal('show')
						})
						.fail(errorThrown => {
							$.unblockUI()
							console.log('Bank Details Get Error', errorThrown)
							return false
						})
				})

				$('#addModal').on('click', function() {
					$('form').trigger('reset')
					$('input[type=checkbox]').prop('checked', false)
					$(`input[name*="modifyId"]`).val('')
					$(`input[name*="formAction"]`).val('add')
					$('#formModal').modal('show')
				})
			}
		},
		columns: [
			{
				data: 'id'
			},
			{
				data: 'code'
			},
			{
				data: 'reason'
			},
			{
				data: 'status'
			},
			{
				data: 'added_by'
			},
			{
				data: 'date_time'
			},
			{
				data: 'action'
			}
		],
		order: [[0, 'desc']]
	})

	$('form').on('submit', function(e) {
		e.preventDefault()

		$.blockUI({
			baseZ: 2000
		})

		var data = $(this).serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: DISPOSAL_API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				responseJSON = $.parseJSON(data)

				new PNotify(responseJSON)

				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
					viewDataTable($(`input[name*="modifyId"]`).val())
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	})

	//delete button
	$('#deleteButton').on('click', function() {
		if (!confirm('Are you sure you want to remove this classification?')) {
			return false
		}

		$.blockUI({
			baseZ: 2000
		})

		$(`input[name*="formAction"]`).val('delete')

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: DISPOSAL_API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Delete Response: ', data)

				responseJSON = $.parseJSON(data)
				new PNotify(responseJSON)
				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Delete Post Error: ', errorThrown)
				return false
			})

		return false
	})

	$.ajax({
		url: PRODUCT_API + '?getAll',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			products = json;
			var newVal = ''

			for (var i = 0; i < json.length; i++) {
				newVal +=
					'<option value=' + json[i].id + '>' + json[i].description + '</option>'
			}

			//console.log(newVal)
			$(`#raw_item_id`).html(newVal)
		})
		.fail(errorThrown => {
			console.log('Warehouse Get Error: ', errorThrown)
			return false
		})
})

function reuseItem(id, itemId, process) {
	if (!confirm("Are you sure you want to " +process+ " this items ? This is irreversible.")) {
		return
	}

	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: DISPOSAL_API,
		type: 'post',
		data: 'reuseReturnItemId=' + id + '&itemId=' + itemId + '&process=' + process.toUpperCase(),
		processData: false
	})
		.done(data => {
			$.unblockUI()

			responseJSON = $.parseJSON(data)

			new PNotify(responseJSON)
			rawTable.ajax.reload(null, false)

		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Post Error: ', errorThrown)
			return false
		})
}

function printDR() {
	window.open('reports/dr.php?or=' + $("#order_ref_no").html(), '_blank')
}

function printSI(code) {
	window.open('reports/si.php?x=1&or=' + code, '_blank')
}