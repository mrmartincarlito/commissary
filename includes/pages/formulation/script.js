const PRODUCT_API = './api/product_information.php'
const FORMULATION_API = './api/formulation.php'

var rawTable

function deleteRaw(id, remainedID) {
	var x = confirm('Are you sure you want to delete ? ')
	if (x) {
		$.ajax({
			url: FORMULATION_API,
			type: 'post',
			data: 'deleteRawMat=' + id,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				//$('#viewModal').modal('hide')
				rawTable.ajax.reload()
				responseJSON = $.parseJSON(data)

				new PNotify(responseJSON)
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	}
}

function editRaw(id_raw, uoms) {
	var x = confirm("This will update to your raw item's records")
	var qty = 0
	if (x) {
		while ((y = true)) {
			var reg = new RegExp('^[0-9]+$')
			qty = prompt('Enter Quantity')
			if (qty == '' || !reg.test(qty)) {
				alert('Numbers only!')
				continue
			} else {
				y = false
				break
			}
		}
	}

	var uom = prompt('Unit of Measurement?', uoms)
	var remarks = prompt('Any remarks or comments?')
	var message = {
		id: id_raw,
		qty: qty,
		uom: uom,
		remarks: remarks
	}

	$.ajax({
		url: './api/items.php',
		type: 'post',
		data: 'updateRawItem=' + JSON.stringify(message),
		processData: true
	})
		.done(data => {
			$.unblockUI()
			console.log('Save Changes Response: ', data)

			responseJSON = $.parseJSON(data)

			new PNotify(responseJSON)
			//$('#viewModal').modal('hide')
			rawTable.ajax.reload()
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
}

function viewModal(form_id){
	$('#viewModal').modal('show')
	viewDataTable(form_id)
}

function viewDataTable(form_id) {
	$(`input[name*="formAction"]`).val('addRaw')
	$(`input[name*="modifyId"]`).val(form_id)
	$("#formulation_item_name").html(form_id)
	$('#datatable_raw')
		.DataTable()
		.destroy()
	rawTable = $('#datatable_raw').DataTable({
		ajax: {
			url: FORMULATION_API + '?getNonFoodFormProductionId=' + form_id,
			dataSrc: function(json) {
				var return_data = new Array()
				for (var i = 0; i < json.length; i++) {
					return_data.push({
						id: json[i].id,
						raw_item_id: json[i].name,
						qty: json[i].req_qty,
						uom: json[i].req_uom,
						remarks: json[i].remarks,
						action:
							'<a href="#" title="Delete" onclick="deleteRaw(' +
							json[i].id +
							',' +
							json[i].raw_item_id +
							')" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>' +
							'<!--<a href="#" title="Edit" value="' +
							json[i].uom +
							'" onclick="editRaw(' +
							json[i].id +
							',this.value)" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-edit text-danger"></i> </a>-->'
					})
				}
				return return_data
			}
		},
		columns: [
			{
				data: 'id'
			},
			{
				data: 'raw_item_id'
			},
			{
				data: 'qty'
			},
			{
				data: 'uom'
			},
			{
				data: 'remarks'
			},
			{
				data: 'action'
			}
		],
		order: [[0, 'desc']]
	})
}
$(document).ready(function() {
	var myTable = $('#datatable').DataTable({
		processing: true,
		ajax: {
			url: FORMULATION_API + '?get',
			dataSrc: function(json) {
				var return_data = new Array()
				for (var i = 0; i < json.length; i++) {
					return_data.push({
						form_id: json[i].form_id,
						description: json[i].description,
						item_name: json[i].name,
						kilo : json[i].kilo,
						produce : json[i].produce,
						remarks: json[i].formulation_remarks,
						added_by: json[i].added_by,
						date_time: moment(json[i].date_time).format('LLL'),
						action:
							'<button class="btn btn-primary" onclick="viewModal(this.value)" value="' +
							json[i].form_id +
							'">VIEW INGREDIENTS</button>'
					})
				}
				return return_data
			},

			complete: function() {
				$('#datatable tbody').on('dblclick', 'tr', function() {
					$('form#content_form').trigger('reset')

					var data = $('#datatable')
						.DataTable()
						.row(this)
						.data().form_id
					$(`input[name*="modifyId"]`).val(data)
					$(`input[name*="formAction"]`).val('edit')

					$.blockUI()

					$.ajax({
						url: FORMULATION_API + '?getDetails=' + data,
						processData: false
					})
						.done(data => {
							var json = $.parseJSON(data)

							$.unblockUI()
							populateForm($('form#content_form'), json[0])

							$('#formModal').modal('show')
						})
						.fail(errorThrown => {
							$.unblockUI()
							console.log('Bank Details Get Error', errorThrown)
							return false
						})
				})

				$('#addModal').on('click', function() {
					$('form').trigger('reset')
					$('input[type=checkbox]').prop('checked', false)
					$(`input[name*="modifyId"]`).val('')
					$(`input[name*="formAction"]`).val('add')
					$('#formModal').modal('show')
				})
			}
		},
		columns: [
			{
				data: 'form_id'
			},
			{
				data: 'description'
			},
			{
				data: 'item_name'
			},
			{
				data: 'kilo'
			},
			{
				data: 'produce'
			},
			{
				data: 'remarks'
			},
			{
				data: 'added_by'
			},
			{
				data: 'date_time'
			},
			{
				data: 'action'
			}
		],
		order: [[0, 'desc']]
	})

	$('form').on('submit', function(e) {
		e.preventDefault()

		$.blockUI({
			baseZ: 2000
		})

		var data = $(this).serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: FORMULATION_API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				responseJSON = $.parseJSON(data)

				new PNotify(responseJSON)

				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
					viewDataTable($(`input[name*="modifyId"]`).val())
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	})

	//delete button
	$('#deleteButton').on('click', function() {
		if (!confirm('Are you sure you want to remove this classification?')) {
			return false
		}

		$.blockUI({
			baseZ: 2000
		})

		$(`input[name*="formAction"]`).val('delete')

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: FORMULATION_API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Delete Response: ', data)

				responseJSON = $.parseJSON(data)
				new PNotify(responseJSON)
				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Delete Post Error: ', errorThrown)
				return false
			})

		return false
	})

	$.ajax({
		url: PRODUCT_API + '?getProduct=FOOD',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)

			var newVal = ''

			for (var i = 0; i < json.length; i++) {
				newVal +=
					'<option value=' + json[i].id + '>' + json[i].description + '</option>'
			}

			//console.log(newVal)
			$(`#item_id`).html(newVal)
		})
		.fail(errorThrown => {
			console.log('Warehouse Get Error: ', errorThrown)
			return false
		})

	$.ajax({
		url: PRODUCT_API + '?getAll',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)

			var newVal = ''

			for (var i = 0; i < json.length; i++) {
				newVal +=
					'<option value=' + json[i].id + '>' + json[i].description + '</option>'
			}

			//console.log(newVal)
			$(`#raw_item_id`).html(newVal)
		})
		.fail(errorThrown => {
			console.log('Warehouse Get Error: ', errorThrown)
			return false
		})
	$('.select2').select2()
})
