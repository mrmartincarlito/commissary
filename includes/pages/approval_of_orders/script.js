const ORDER_API = './api/order.php'

var myTable = null;
$.fn.dataTable.ext.search.push(
	function (settings, data, dataIndex) {
		var startDate = Date.parse($('#start-date').val(), 10);
		var endDate = Date.parse($('#end-date').val(), 10);
		var columnDate = Date.parse(data[4]) || 0; // use data for the age column
		if ((isNaN(startDate) && isNaN(endDate)) ||
			(isNaN(startDate) && columnDate <= endDate) ||
			(startDate <= columnDate && isNaN(endDate)) ||
			(startDate <= columnDate && columnDate <= endDate)) {
			return true;
		}
		return false;
	}
);

$('.date-range-filter').change(function () {
	myTable.draw();
});

myTable = $('#datatable').DataTable({
	processing: true,
	serverSide: true,
	rowReorder: {
		selector: 'td:nth-child(3)'
	},
	responsive: true,
	order: [
		[0, 'desc']
	],
	buttons: [
		{
			extend: 'excel',
			text: 'Export to Excel'
		},
		{

			extend: 'pdf',
			text: 'Export to Pdf',
			orientation: 'portrait',
			filename: 'Order Information',
			paging: true,
			customize: function (doc) {
				doc.content.splice(0, 1);
				var now = new Date();
				var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
				doc.pageMargins = [20, 60, 20, 30];
				doc.defaultStyle.fontSize = 8;
				doc.styles.tableHeader.fontSize = 8;
				doc['header'] = (function () {
					return {
						columns: [{
							alignment: 'left',
							text: 'Order Information',
							fontSize: 20,
							margin: [20, 20]
						}]
					}
				});
				doc['footer'] = (function () {
					return {
						columns: [{
							alignment: 'left',
							text: 'Created on: ' + jsDate.toString(),
							margin: [10, 10]
						}]
					}
				})
			}
		}
	],
	"language": {
		"lengthMenu": 'Display <select>' +
			'<option value="10">10</option>' +
			'<option value="50">50</option>' +
			'<option value="70">70</option>' +
			'<option value="80">80</option>' +
			'<option value="100">100</option>' +
			'<option value="-1">All</option>' +
			'</select> records'
	},
	ajax: {
		url: ORDER_API + '?get',
		complete: function () {

		}
	}
})

function cancelOrder() {
	var order_ref = $("#order_ref_no").html()

	if (!confirm('Are you sure you want to cancel the order?')) {
		return false
	}

	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: ORDER_API + '?cancelOrder=' + order_ref,
		processData: false
	})
		.done(data => {

			$.unblockUI()

			responseJSON = $.parseJSON(data)

			new PNotify(responseJSON)
			if (responseJSON.type == 'success') {
				myTable.ajax.reload(null, false)
				$('#poModal').modal('hide')
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Get Error: ', errorThrown)
			return false
		})
}

function approveOrder(){
	var order_ref = $("#order_ref_no").html()

	if (!confirm('Are you sure you want to approve this order?')) {
		return false
	}

	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: ORDER_API + '?approveOrder=' + order_ref,
		processData: false
	})
		.done(data => {

			$.unblockUI()

			responseJSON = $.parseJSON(data)

			new PNotify(responseJSON)
			if (responseJSON.type == 'success') {
				myTable.ajax.reload(null, false)
				$('#poModal').modal('hide')
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Get Error: ', errorThrown)
			return false
		})
}

function receiveItems(id){
	loadOrderItems(id, "receiving")
}

var selectedItemId = []
function loadOrderItems(id, action = "view") {
	$("#poModal").modal("show")
	$('#poItemsTable').DataTable().destroy()
	poItemsDataTable = $('#poItemsTable').DataTable({
		processing: true,
		ajax: {
			url: ORDER_API + '?viewOrderedItems=' + id,
			dataSrc: function (json) {
				var return_data = new Array()
				var total_amount = 0;

				if(action == "view"){
					$("#receiveOrderButton").hide()
					$("#printDrButton").show()
					$("#receivingForm").hide()
					$("#orderAgainButton").show()
					$("#cancelButton").show()
					for (var i = 0; i < json.length; i++) {
						$("#order_ref_no").html(json[i].order_ref_no)
						$("#remarks").val(json[i].remarks)
						total_amount = total_amount + parseFloat(json[i].total_amount)

						var itemStatus = json[i].item_status;
						var buttonAction = '';
						var mot = json[i].mode_of_transaction;

						if ((itemStatus == "" || itemStatus == null) || (itemStatus == "Delivery" || itemStatus == "Pick-up")) {
							itemStatus = "Set as " + mot;
						}

						if (itemStatus == "Received") {
							itemStatus = "Order Received";
							buttonAction = 'disabled="true"'
						} else {
							if (parseInt(json[i].qty) == 0) {
								itemStatus = "Set as Received";
								mot = 'Received';
							}
						}

						
						return_data.push({
							id: json[i].id,
							product_id: json[i].description,
							qty: '<label style="text-align:center">' + json[i].qty + "</label>",
							del_qty: '<input type="text" class="form-control" id="del_qty_'+json[i].id+'" value="'+ json[i].qty +'">',
							rec_qty: '<label style="text-align:center">' + json[i].del_qty + "</label>",
							cost: '<label style="text-align:center">' + json[i].price + "</label>",
							total_amount: '<label style="text-align:center">' + formatNumber(json[i].total_amount) + "</label>",
							action : '<input type="hidden" value="'+mot+'" id="mot_'+json[i].id+'"> <button type="button" onclick="setAsReadyPerItem('+json[i].id+', '+id+')" class="btn btn-info waves-effect" '+buttonAction+'>'+itemStatus+'</button>'
						})
					}
				}

				if(action == "receiving"){
					$("#receiveOrderButton").show()
					$("#printDrButton").hide()
					$("#receivingForm").show()
					$("#orderAgainButton").hide()
					$("#cancelButton").hide()
					for (var i = 0; i < json.length; i++) {
						selectedItemId.push(json[i].id)
						$("#order_ref_no").html(json[i].order_ref_no)
						total_amount = total_amount + parseFloat(json[i].total_amount)
						return_data.push({
							id: json[i].id,
							product_id: json[i].description,
							qty: "<input class='form-control' onkeyup='calculatePrice("+json[i].id+")' id='qty_"+json[i].id+"' type='number' style='text-align:right' value='"+json[i].qty+"'/>",
							cost: "<input class='form-control' id='price_"+json[i].id+"' type='text' style='text-align:right' readonly value='"+json[i].price+"'>",
							total_amount: "<input class='form-control' id='total_amount_"+json[i].id+"' type='text' style='text-align:right' readonly value='"+json[i].total_amount+"'>",
						})
					}
				}

				$("#overall_total_amount").html(formatNumber(total_amount))
				return return_data
			},

			complete: function () {
				//do something soon
			}
		},
		columns: [
			{ data: 'id' },
			{ data: 'product_id' },
			{ data: 'qty' },
			{ data: 'del_qty' },
			{ data: 'rec_qty' },
			{ data: 'cost' },
			{ data: 'total_amount' },
			{ data: 'action' }
		],
		order: [[0, 'desc']],
		rowReorder: {
			selector: 'td:nth-child(4)'
		},
		responsive: true,
	})
}

function calculatePrice(item_id){
	var price = $("#price_"+item_id).val()
	var qty = $("#qty_"+item_id).val()
	
	if(qty == "" || qty == "null" || qty == "undefined" || qty == "0"){
		$("#total_amount_"+item_id).val(1 * price)
	}else{
		$("#total_amount_"+item_id).val(qty * price)
	}

	calculateOverAllTotal()
}

function calculateOverAllTotal(){
	var total_amount = 0;

	for(var i=0; i<selectedItemId.length; i++){
		total_amount = total_amount + parseFloat($("#total_amount_"+selectedItemId[i]).val())
	}

	$("#overall_total_amount").html(formatNumber(total_amount))
}

function receiveOrders(id){

	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: ORDER_API,
		type: 'post',
		data: 'receiveOrder=' + id,
		processData: false
	})
		.done(data => {
			$.unblockUI()
			
			responseJSON = $.parseJSON(data)
			new PNotify(responseJSON)
			if (responseJSON.type == 'success') {
				myTable.ajax.reload(null, false)
				$('#poModal').modal('hide')
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Delete Post Error: ', errorThrown)
			return false
		})
}

function setAsReadyPerItem(item_id, order_id) {
	if (!confirm('Are you sure you want to place this?')) {
		return false
	}

	var mot = $("#mot_"+item_id).val()
	var remarks = $("#remarks").val();

	if (remarks == "") {
		alert("Please input remarks")
		return;
	}

	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: ORDER_API,
		type: 'post',
		data: 'markAsReadyPerItem=' + item_id + '&mot=' + mot + '&qty=' + $("#del_qty_"+ item_id).val() + '&remarks=' + remarks,
		processData: false
	})
		.done(data => {
			$.unblockUI()
			
			responseJSON = $.parseJSON(data)
			
			new PNotify(responseJSON)
			loadOrderItems(order_id)
			myTable.ajax.reload(null, false)
			
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Delete Post Error: ', errorThrown)
			return false
		})
}

function populateOrder(id){
	if (!confirm('Are you sure you want to place this?')) {
		return false
	}

	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: ORDER_API,
		type: 'post',
		data: 'markAsReady=' + id,
		processData: false
	})
		.done(data => {
			$.unblockUI()
			
			responseJSON = $.parseJSON(data)
			
			new PNotify(responseJSON)
			myTable.ajax.reload(null, false)
			
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Delete Post Error: ', errorThrown)
			return false
		})
}

setInterval(viewAll, 10000)
viewAll()

function viewAll() {
	myTable.ajax.url(ORDER_API + '?get').load()
}

function viewApproved() {
	myTable.ajax.url(ORDER_API + '?get&status=APPROVED').load()
}

function viewPending() {
	myTable.ajax.url(ORDER_API + '?get&status=PENDING').load()
}

function viewRejected() {
	myTable.ajax.url(ORDER_API + '?get&status=REJECTED').load()
}

function viewReceived() {
	myTable.ajax.url(ORDER_API + '?get&status=RECEIVED').load()
}

function printDR() {
	window.open('reports/dr.php?or=' + $("#order_ref_no").html(), '_blank')
}

function printSI() {
	window.open('reports/si.php?or=' + $("#order_ref_no").html(), '_blank')
}

function printCR() {
	window.open('reports/cr.php?or=' + $("#order_ref_no").html(), '_blank')
}

function printAR() {
	window.open('reports/ar.php?or=' + $("#order_ref_no").html(), '_blank')
}

function printOR() {
	window.open('reports/or.php?or=' + $("#order_ref_no").html(), '_blank')
}

function printCDR() {
	window.open('reports/cdr.php?or=' + $("#order_ref_no").html(), '_blank')
}