<?php
    $links = array( 
        array( 
            "name" => "Approval of Orders", 
            "data-icon" => "&#xe01b;", 
            "link" => "./?approval_of_orders", 
            "linea-type" => "linea-basic",
            "child_items" => array(
            ) 
        ),

        array( 
            "name" => "Return Requests", 
            "data-icon" => "g", 
            "link" => "./?return_goods_approval", 
            "linea-type" => "linea-ecommerce",
            "child_items" => array(
            ) 
        ),

        array( 
            "name" => "Create PO", 
            "data-icon" => "d", 
            "link" => "./?purchase_order", 
            "linea-type" => "linea-ecommerce",
            "child_items" => array(
            ) 
        ),

        array( 
            "name" => "Incoming of Products", 
            "data-icon" => "d", 
            "link" => "./?incoming_po", 
            "linea-type" => "linea-ecommerce",
            "child_items" => array(
            ) 
        ),

        array( 
            "name" => "Product Assembly", 
            "data-icon" => "n", 
            "link" => "./?formulation", 
            "linea-type" => "linea-ecommerce",
            "child_items" => array(
            ) 
        ),

        array( 
            "name" => "Production Procedure", 
            "data-icon" => "s", 
            "link" => "./?production_planning", 
            "linea-type" => "linea-ecommerce",
            "child_items" => array(
            ) 
        ),

        array( 
            "name" => "Product Disposal", 
            "data-icon" => "i", 
            "link" => "./?product_disposal", 
            "linea-type" => "linea-ecommerce",
            "child_items" => array(
            ) 
        ),

        array( 
            "name" => "Product Information", 
            "data-icon" => "&#xe025;", 
            "link" => "./?product_information", 
            "linea-type" => "linea-basic",
            "child_items" => array(
            ) 
        ),

        array( 
            "name" => "Reports", 
            "data-icon" => "m", 
            "link" => "./?reports", 
            "linea-type" => "linea-basic",
            "child_items" => array(
            ) 
        ),
    ); 

    //do not meddle with the code below

    $links_HTML = "";
    foreach ($links as $link) {
        $isactive = '';
        if($page_name == strtolower($link['name'])){
            $isactive = 'active';
        }

        $child_links = '';
        $arrow_extra = '';
        if(count($link['child_items']) > 0){
            $child_links = '<ul class="nav nav-second-level">';
            $arrow_extra = '<span class="fa arrow"></span>';
            foreach ($link['child_items'] as $child_item) {
                $child_links .= '
                <li>
                    <a href="'.$child_item['link'].'">
                        <i data-icon="'.$child_item['data-icon'].'" class="linea-icon '.$child_item["linea-type"].' fa-fw"></i><span class="hide-menu">'.$child_item['name'].'</span>
                    </a>
                </li>
                ';
            }
            $child_links .= '</ul>';
        }

        $links_HTML .= '
            <li>
                <a href="'.$link['link'].'" class="waves-effect '.$isactive.'">
                    <i data-icon="'.$link['data-icon'].'" class="linea-icon '.$link["linea-type"].' fa-fw"></i>
                    <span class="hide-menu">'.$link['name'].'</span>
                    '.$arrow_extra.'
                </a> 
                    '.$child_links.'
            </li>
        ';
    }
?>
