<?php
require_once "./api/config.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/thp-logo.png">
  <title>Control Panel Login</title>
  <?php include './includes/load_on_page_start.php'?>
</head>

<body>
  <!-- Preloader -->
  <div class="preloader">
    <div class="cssload-speeding-wheel"></div>
  </div>
  <section id="wrapper" class="new-login-register">
      <div class="lg-info-panel">
          <div class="inner-panel">
              <a href="javascript:void(0)" class="p-20 di"><img src="plugins/images/thp-logo-100px.png"></a>
              <div class="lg-content">
                  <h2>THE HUNGRY PITA</h2>
                  <p class="text-muted">The Best Tasting Pinoy-Style Shawarma</p>
              </div>
          </div>
      </div>
      <div class="new-login-box">
        <div class="white-box">
            <h3 class="box-title m-b-0">Sign In to Commissary</h3>
            <small>Enter your details below</small>
          <form class="form-horizontal new-lg-form">

            <div class="form-group  m-t-20">
              <div class="col-xs-12">
                <label>Username</label>
                <input class="form-control" type="text" required="" placeholder="Username" name="username">
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-12">
                <label>Password</label>
                <input class="form-control" type="password" required="" placeholder="Password" name="password">
              </div>
            </div>
            <!-- <div class="form-group">
              <div class="col-md-12">
                <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a> </div>
            </div> -->
            <div class="form-group text-center m-t-20">
              <div class="col-xs-12">
                <button class="btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light" type="submit">Log In</button>
              </div>
            </div>
           
          </form>
          <form class="form-horizontal" id="recoverform" action="index.html">
            <div class="form-group ">
              <div class="col-xs-12">
                <h3>Recover Password</h3>
                <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
              </div>
            </div>
            <div class="form-group ">
              <div class="col-xs-12">
                <input class="form-control" type="text" required="" placeholder="Email">
              </div>
            </div>
            <div class="form-group text-center m-t-20">
              <div class="col-xs-12">
                <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
              </div>
            </div>
            <div class="form-group text-center m-t-20">
              <div class="col-xs-12">
                <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="button" onclick="window.location.href = 'login.php'">Back to Login</button>
              </div>
            </div>
          </form>
        </div>
      </div>


</section>
  <?php include './includes/load_on_page_end.php'?>
  <script src="./includes/pages/login/script.js"></script>
</body>

</html>