<?php

include "env.php";
include "env.".ENV.".php";

// TABLES
define("COMMISSARY", "commissary_accounts");
define("ORDERS", "orders");
define("ORDER_ITEMS", "order_items");
define("FRANCHISEE", "franchisee");
define("ORDER_RETURNS", "order_returns");
define("ORDER_RETURN_ITEMS", "order_return_items");
define("PURCHASE_ORDER", "purchase_order");
define("PURCHASE_ORDER_ITEMS", "purchase_order_items");
define("SUPPLIER_TABLE","supplier");
define("PRODUCTS_TABLE", "products");
define("RECEIVING_PO","received_po");
define("RECEIVING_PO_ITEMS","received_po_items");
define("PRODUCT_TABLE", "products");
define("PRODUCT_ITEMIZE", "products_itemize");
define("FORMULATION", "formulation");
define("FORMULATION_DETAILS","formulation_details");
define("PRODUCTION", "production");
define("PRODUCTION_ITEMS", "production_items");
define("PRODUCT_JOURNAL", "product_journal");
define("EMPLOYEES", "employees");
define("PRODUCTION_EMPLOYEES", "production_employees");
define("BRANCHES", "franchise_branch");
define("PRODUCT_DISPOSAL", "disposal");
define("PRODUCT_DISPOSAL_ITEMS", "disposal_items");

function generateRefno($database, $initialCount, $table, $columnRef, $pattern){
    $data = $database->get($table);

    if(!empty($data)){
        $database->orderBy("id","DESC");
        $fetchedData = $database->getOne($table);

        $columnValue = $fetchedData[$columnRef];
        $toIncrement = substr($columnValue, 2);
        $toIncrement = $toIncrement + 1;

        return $pattern.str_pad($toIncrement, 4, '0', STR_PAD_LEFT);
    }else{
        return $pattern.str_pad($initialCount, 4, '0', STR_PAD_LEFT);
    }
    
}

function convertStatusColor($data){
    $color = "";
    if($data == "APPROVED" || $data == "PAID"){
        $color = '<span class="label label-success">' . $data . '</span>';
    }

    if($data == "REJECTED" || $data == "UNPAID" || $data == "CANCELLED"){
        $color = '<span class="label label-danger">' . $data . '</span>';
    }

    if($data == "PENDING"){
        $color = '<span class="label label-info">' . $data . '</span>';
    }

    if($data == "RECEIVED" || $data == "DISPOSED"){
        $color = '<span class="label label-warning">' . $data . '</span>';
    }

    if($data == "Delivery" || $data == "Pick-up" || strpos($data, "Partial") !== false){
        $color = '<span class="label label-info">' . $data . '</span>';
    }

    return $color;
}

function checkApproval($database, $status){
    $loggedUser = json_decode(getLoggedUserDetails($database));

    if($status != "PENDING"){
        $database->where("id", $loggedUser->role);
        $database->where("access_approval_power", 1);
        $access = $database->getOne("access_levels");

        if(empty($access)){
            return "NO ACCESS";
        }else{
            return "ACCESS";
        }
    }

    return "PROCEED";
}

function getBatch($itemId, $orderRef) {
    global $database;

    return $database->rawQuery("SELECT batch_no FROM ".PRODUCT_ITEMIZE." WHERE order_ref = '".$orderRef."' and item_id = ".$itemId." group by batch_no");
}

function getSupplier($id){
    global $database;

    $database->where ("id", $id);
    $supplier = $database->getOne("supplier");
    return $supplier;
}

function getProduct($id){
    global $database;

    $database->where ("id", $id);
    $products = $database->getOne("products");
    return $products;
}

function getFranchise($id){
    global $database;

    $database->where ("id", $id);
    $products = $database->getOne("franchisee");
    return $products;
}

function getOrderItems($id){
    global $database;

    $database->where ("id", $id);
    $products = $database->getOne("order_items");
    return $products;
}

function getOrderItemsByOrderId($id) {
    global $database;

    $database->where("order_id", $id);
    return $database->get(ORDER_ITEMS);
}

function getBranch($id) {
    global $database;

    $database->where("id", $id);
    return $database->getOne(BRANCHES);
}