<?php
require_once("config.php");
require_once("logs.php");

if (isset($_GET['get'])) {
    $database->where("is_deleted", 0);
    echo json_encode( $database->get(EMPLOYEES) );
}

if (isset($_GET['getProductionEmployees'])) {
    $productionId = $_GET['getProductionEmployees'];

    $database->where("production_id", $productionId);
    $prodDetails = $database->getOne(PRODUCTION);

    $database->where("p.job_order", $prodDetails['job_order']);
    $database->join(EMPLOYEES . " e", "e.id=p.emp_id");
    $details = $database->get(PRODUCTION_EMPLOYEES. " p", null, "p.*, e.first_name, e.last_name");

    echo json_encode($details);
}

if (isset($_GET['saveEmployeeProduction'])) {
    $values = json_decode($_GET['saveEmployeeProduction']);

    foreach ($values as $value) {
        $database->where("id", $value->id);
        $database->update(PRODUCTION_EMPLOYEES, array(
            "qty_made" => $value->qty
        ));
    }

    echo json_encode(array(
        "type" => "success",
        "title" => "Successful!",
        "text" => "Successfully Save Employee Production",
    ));
}