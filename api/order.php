<?php
require_once("config.php");
require_once("logs.php");
require_once("inventory.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_POST['saveOrder'])){
    $items = json_decode($_POST['saveOrder']);

    $total_amount = 0;
    $refNo = generateRefno($database, "1", ORDERS, "order_ref", "O-");

    foreach($items->orders as $item){
        $total_amount = $total_amount + str_replace("," , "" , $item->total_amount); 
    }

    if(empty($items->franchisee_id)){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "Please select franchisee"
        ));
        return;
    }

    $id = $database->insert(ORDERS, array(
        "order_ref" => $refNo,
        "franchisee_id" => $items->franchisee_id,
        "total_amount" => $total_amount,
        "added_by" => $loggedUser->username,
        "date_ordered" => $database->now()
    ));

    if($id){
        foreach($items->orders as $item){
            $database->insert(ORDER_ITEMS, array(
                "order_id" => $id,
                "order_ref" => $refNo,
                "item_id" => $item->item_id,
                "price" => $item->price,
                "qty" => $item->qty,
                "total_amount" => $item->total_amount
            ));
        }

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Successfully Placed your order please track order status!"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }
}

if(isset($_GET["get"])){
    $primaryKey = 'id';
    $columns = array(
        array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'order_ref',  'dt' => 1 ),
        array(  'db' => 'franchisee_id',  
                'dt' => 2,
                'formatter' => function ($data, $row){

                    $franchisee = getFranchise($data);

                    return $franchisee["name"];
                }
        ),
        array( 'db' => 'total_amount',   'dt' => 3 , 'formatter' => function($data ,$row) { return number_format($data);}),
        array(  'db' => 'status',   
                'dt' => 4,
                'formatter' => function ($data, $row){

                    return convertStatusColor($data);
                }
            ),
        array( 'db' => 'added_by',   'dt' => 5 ),
        array( 'db' => 'remarks',   'dt' => 6 ),
        array( 'db' => 'date_ordered',   'dt' => 7 ),
        array(  'db' => 'id',   
                'dt' => 8 ,
                'formatter' => function($data ,$row) {

                    $receiving  = "";

                    $items = getOrderItemsByOrderId($data);

                    $count = 0;
                    foreach($items as $item) {
                        if ($item["status"] == "Received") {
                            $count++;
                        }
                    }

                    if (count($items) == $count) {
                        $receiving = '<li><a href="#" onclick="receiveOrders('.$data.')">Set as Received</a></li>';
                    }

                    return ' <div class="btn-group dropdown">
                                <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                                <ul role="menu" class="dropdown-menu animated">
                                    <li><a href="#" onclick="loadOrderItems('.$data.')">View Order</a></li>
                                    '.$receiving.'
                                </ul>
                            </div>';
                
                }
            ),
    );
    
    $condition = "is_deleted = 0";

    if(isset($_GET['status'])){
        $condition .= " and status = '{$_GET['status']}'";
    }

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, ORDERS , $primaryKey, $columns, $condition )
    );
}

if (isset($_POST['markAsReadyPerItem'])) {
    $itemId = $_POST['markAsReadyPerItem'];
    $del_qty = $_POST['qty'];

    $database->where("id", $itemId);
    $orderItems = $database->get(ORDER_ITEMS);

    $success = true;
    //checker if order is enoungh
    foreach($orderItems as $orderItem){
        //get items on product items
        $database->where("item_id", $orderItem["item_id"]);
        $database->where("is_available" , 1);
        $itemize = $database->get(PRODUCT_ITEMIZE, $del_qty);

        if(count($itemize) != $del_qty){
            $product = getProduct($orderItem["item_id"]);
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error",
                "text" => "Not Enough ".$product["description"]. ", Current Stocks (".count($itemize).") Not Enough for (".$del_qty."). Please do production"
            ));
            $success = false;
            break;
        }
    }

    if($success){
        $update = 0;
        foreach($orderItems as $orderItem){
            //get items on product items
            $database->where("item_id", $orderItem["item_id"]);
            $database->where("is_available" , 1);
            $itemize = $database->get(PRODUCT_ITEMIZE, $del_qty);

            //set product itemize with order ref
            foreach($itemize as $item){
                $database->where("id", $item["id"]);
                $database->update(PRODUCT_ITEMIZE, array(
                    "is_available" => 0,
                    "order_ref" => $orderItem["order_ref"]
                ));
            }

            //insert to product journal
            updateProductStocks($database, $orderItem["item_id"], $del_qty, "ORDER", $orderItem["order_ref"], $movement = "-"); 
            
            $database->where("id", $orderItem["id"]);
            $update = $database->update(ORDER_ITEMS, array(
                "status" => $_POST['mot'],
                "qty" => $orderItem["qty"] - $del_qty,
                "rec_qty" => $orderItem["rec_qty"] + $del_qty
            ));

            $database->where("order_ref", $orderItem["order_ref"]);
            $update = $database->update(ORDERS, array(
                "remarks" => $_POST['remarks'],
                "status" => "Partial " . $_POST['mot']
            ));
        }

        if($update){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Successfully Placed Order"
            ));
        }else {
            echo json_encode(array(
                "type" => "error",
                "title" => "Error!",
                "text" => "Error while approving order " . $database->getLastError(),
            ));
        }

    }
}

// if(isset($_POST['markAsReady'])){
//     $id = $_POST['markAsReady'];

//     $database->where("id", $id);
//     $order = $database->getOne(ORDERS);

//     $database->where("order_id", $id);
//     $orderItems = $database->get(ORDER_ITEMS);

//     $success = true;
//     //checker if order is enoungh
//     foreach($orderItems as $orderItem){
//         //get items on product items
//         $database->where("item_id", $orderItem["item_id"]);
//         $database->where("is_available" , 1);
//         $itemize = $database->get(PRODUCT_ITEMIZE, $orderItem["qty"]);

//         if(count($itemize) != $orderItem["qty"]){
//             $product = getProduct($orderItem["item_id"]);
//             echo json_encode(Array (
//                 "type" => "error",
//                 "title" => "Error",
//                 "text" => "Not Enough ".$product["description"]. ", Current Stocks (".$product["stocks"].") Not Enough for (".$orderItem["qty"]."). Please do production"
//             ));
//             $success = false;
//             break;
//         }
//     }

//     if($success){
//         foreach($orderItems as $orderItem){
//             //get items on product items
//             $database->where("item_id", $orderItem["item_id"]);
//             $database->where("is_available" , 1);
//             $itemize = $database->get(PRODUCT_ITEMIZE, $orderItem["qty"]);

//             //set product itemize with order ref
//             foreach($itemize as $item){
//                 $database->where("id", $item["id"]);
//                 $database->update(PRODUCT_ITEMIZE, array(
//                     "is_available" => 0,
//                     "order_ref" => $order["order_ref"]
//                 ));
//             }

//             //insert to product journal
//             updateProductStocks($database, $orderItem["item_id"], $orderItem["qty"], "ORDER", $order["order_ref"], $movement = "-");          
//         }

//         $mot = getFranchise($order["franchisee_id"])["mode_of_transaction"];

//         $database->where("order_ref", $order["order_ref"]);
//         $update = $database->update(ORDERS, array(
//             "status" => $mot
//         ));

//         if($update){
//             echo json_encode(Array (
//                 "type" => "success",
//                 "title" => "Successful!",
//                 "text" => "Successfully Placed Order"
//             ));
//         }else {
//             echo json_encode(array(
//                 "type" => "error",
//                 "title" => "Error!",
//                 "text" => "Error while approving order " . $database->getLastError(),
//             ));
//         }

//     }
// }

if(isset($_POST['previewOrder'])){
    $items = json_decode($_POST['previewOrder']);

    $response = array();

    foreach($items as $item){
        $product = getProduct($item);

        array_push($response, $product);
    }

    echo json_encode($response);
}

if(isset($_GET['viewOrderedItems'])){
    $order_id = $_GET['viewOrderedItems'];

    if(isset($_GET['order_ref'])){
        $database->where("order_ref", $_GET['order_ref']);
    }else{
        $database->where("id", $order_id);
    }
    $response = array();

    $orderInfo = $database->getOne(ORDERS);

    $franchisee = getFranchise($orderInfo["franchisee_id"]);

    $database->where("order_id", $orderInfo["id"]);
    $orders = $database->get(ORDER_ITEMS);
    
    foreach($orders as $item){
        $values["id"] = $item["id"];
        $values["item_id"] = $item["item_id"];

        $product = getProduct($item["item_id"]);
        $values["description"] = $product["description"] . " ({$product["stocks"]} stck/s)";
        $values["franchisee_owner"] = $franchisee["name"];

        // if($orderInfo["status"] == "RECEIVED"){
        //     $values["total_amount"] = $item["rec_total_amount"];
        //     $values["qty"] = $item["rec_qty"];
        // }else{
        //     $values["total_amount"] = $item["total_amount"];
        //     $values["qty"] = $item["qty"];
        // }

        $values["total_amount"] = $item["total_amount"];
        $values["qty"] = $item["qty"];
        
        $values["price"] = $item["price"];
        $values["order_ref_no"] = $item["order_ref"];
        $values["mode_of_transaction"] = $orderInfo["delivery_method"];
        $values["item_status"] = $item["status"];
        $values["del_qty"] = $item["rec_qty"];
        $values["remarks"] = $orderInfo["remarks"];

        array_push($response, $values);
    }

    echo json_encode($response);
}

if(isset($_GET['cancelOrder'])){
    $order_ref = $_GET['cancelOrder'];

    $database->where("order_ref", $order_ref);
    $order = $database->getOne(ORDERS);

    if($order["status"] == "RECEIVED"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "This order is already RECEIVED"
        ));

        return;
    }

    if($order["status"] == "APPROVED"){
        $database->rawQuery("Update ".FRANCHISEE." set order_balance = order_balance - ".$order["total_amount"]. ", 
        total_amount = order_balance + billing_balance 
        where id = ".$order["franchisee_id"]);

        $database->where("order_ref", $order_ref);
        $id = $database->update(ORDERS, array(
            "status" => "CANCELLED"
        ));

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "You just cancelled an order"
        ));

        return;
    }

    if($order["status"] == "PENDING"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error",
            "text" => "This is still PENDING you can tell the franchise to cancel on their account"
        ));
    }

    if($order["status"] == "CANCELLED"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error",
            "text" => "It's already cancelled"
        ));
    }
}

if(isset($_GET['approveOrder'])){
    $order_ref = $_GET['approveOrder'];

    $database->where("order_ref", $order_ref);
    $order = $database->getOne(ORDERS);

    if($order["status"] == "APPROVED"){
        
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error",
            "text" => "This is already approved"
        ));

        return;
    }

    if($order["status"] == "RECEIVED"){
        
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error",
            "text" => "This is already received"
        ));

        return;
    }

    $database->where("order_ref", $order_ref);
    $update = $database->update(ORDERS, array(
        "status" => "APPROVED",
        "approved_by" => $loggedUser->username,
        "approved_date_time" => $database->now()
    ));

    if ($update) {

        $database->rawQuery("Update ".FRANCHISEE." set order_balance = order_balance + ".$order["total_amount"]. ",
        total_amount = order_balance + billing_balance 
        where id = ".$order["franchisee_id"]);

        $response = json_encode(array(
            "type" => "success",
            "title" => "Successful!",
            "text" => "Successfully Approved Order",
        ));
        echo $response;
    } else {
        $response = json_encode(array(
            "type" => "error",
            "title" => "Error!",
            "text" => "Error while approving order " . $database->getLastError(),
        ));
        echo $response;
    }

}

if(isset($_POST['receiveOrder'])){
    $id = $_POST['receiveOrder'];

    $database->where("id", $id);
    $checkOrder = $database->getOne(ORDERS);

    if($checkOrder["status"] == "RECEIVED"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error",
            "text" => "This order has already been received"
        ));

        return;
    }
   
    $database->where("id", $id);
    $id = $database->update(ORDERS, array(
        "status" => "RECEIVED",
        "date_delivered" => $database->now()
    ));

    if($id){
        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful",
            "text" => "Successfully received order!"
        ));

    }else{
        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Something went while receiving please try again later"
        ));
    }
}