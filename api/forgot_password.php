<?php
require_once("autoload.php");
require_once ('dbhelper/MysqliDb.php');
require_once("db.php");

define("TABLE_NAME", "accounts");

if(isset($_POST['reset'])){
    $email = $_POST['reset'];
    
    $database->where("email", $email);
    $database->where("is_deleted", 0);
    $user = $database->getOne(TABLE_NAME);

    if(empty($user)){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "Email does not exist, contact your administrator"
        ));

        return;
    }

    //sendEmail here

    echo json_encode($user);
}