<?php
require_once("config.php");
require_once("inventory.php");

$userDetails = json_decode(getLoggedUserDetails($database));

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    $stats = $database->getOne ("production", "max(production_id) total");
    $prod_id = 0;
    if($stats["total"] == "") {
        $prod_id = 1;
    }else{
        $prod_id = $stats["total"]+1;
    }

    if ($data->mode == "add") {
        
        if (empty($data->start_date) || empty($data->end_date)) {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => "Please dont forget to input description, start and end date to this Job Order"
            ));
            return;
        }
        $jo = generateRefno($database, "1", PRODUCTION, "job_order", "J-");
        $insertData = Array (
            "job_order" => $jo,
            "batch_no" => generateRefno($database, "1", PRODUCTION, "batch_no", "B-"),
            "production_id" => $prod_id,
            "description" => $jo,
            "start_date" => $data->start_date,
            "end_date" => $data->end_date,
            "manufacturing" => isset($data->manufacturing) ? $data->manufacturing : NULL,
            "expiration" => isset($data->expiration) ? $data->expiration : NULL,
            "added_by" => $userDetails->username
        );
    
        $id = $database->insert (PRODUCTION, $insertData);
        if($id){

            $database->where("job_order", $jo);
            $database->delete(PRODUCTION_EMPLOYEES);
            //insert to emp production
            foreach ($data->emp_production as $empId) {

                $database->insert(PRODUCTION_EMPLOYEES, array(
                    "job_order" => $jo,
                    "emp_id" => $empId
                ));
            }

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Production added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    
        foreach ($data->items_to_produce as $item) {
            $database->where("form_id", $item->formulation_id);
            $getItem = $database->getOne(FORMULATION);

            $insertData = Array (
                "production_id" => $prod_id,
                "formulation_id" => $item->formulation_id,
                "item_id" => $getItem["item_id"],
                "production_qty" => $item->production_qty,
                "final_qty" => $item->final_qty,
                "credit" => $item->credit,
                "remarks" => $item->remarks
            );
    
            $id = $database->insert (PRODUCTION_ITEMS, $insertData);
        }
    
        saveLog($database,"New production plan :" . $jo);
    }

    if($data->mode == "edit"){
        if(empty($data->start_date) || empty($data->end_date)){
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => "Please dont forget to input description, start and end date to this Job Order"
            ));
            return;
        }

        $updateData = Array (
            "start_date" => $data->start_date,
            "end_date" => $data->end_date,
            "manufacturing" => isset($data->manufacturing) ? $data->manufacturing : NULL,
            "expiration" => isset($data->expiration) ? $data->expiration : NULL,
            "added_by" => $userDetails->username
        );
    
        $database->where ('production_id', $data->modifyId);
        $id = $database->update (PRODUCTION, $updateData);
        if($id){

            $database->where("production_id", $data->modifyId);
            $details = $database->getOne(PRODUCTION);

            if (count($data->emp_production) > 0) {
                $database->where("job_order", $details['job_order']);
                $database->delete(PRODUCTION_EMPLOYEES);
    
                //insert to emp production
                foreach ($data->emp_production as $empId) {
    
                    $database->insert(PRODUCTION_EMPLOYEES, array(
                        "job_order" => $details['job_order'],
                        "emp_id" => $empId
                    ));
                }
    
            }

            $database->where('production_id', $data->modifyId);
            if($database->delete(PRODUCTION_ITEMS)){
                foreach ($data->items_to_produce as $item) {
                    $database->where("form_id", $item->formulation_id);
                    $getItem = $database->getOne(FORMULATION);
                    
                    $insertData = Array (
                        "production_id" => $data->modifyId,
                        "formulation_id" => $item->formulation_id,
                        "item_id" => $getItem["item_id"],
                        "production_qty" => $item->production_qty,
                        "final_qty" => $item->final_qty,
                        "credit" => $item->credit,
                        "remarks" => $item->remarks,
                    );
            
                    $id = $database->insert (PRODUCTION_ITEMS, $insertData);
                }
            }
        
            saveLog($database,"Updated production plan id:" . $data->modifyId);

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Production plan updated successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    } 

    if($data->mode == "finishprod"){
        $database->where("production_id",$data->modifyId);
        $checkCols = $database->getOne(PRODUCTION);

        if(count($checkCols) > 0){
            if($checkCols['is_started'] == 2){
                return;
            }
        }

        $productionResult = getProductionResult($data->modifyId);
        $productionItems = $productionResult['items'];
        $productionRawItems = $productionResult['raw_items'];

        foreach ($productionItems as $item) {
            $insertItemJournal = array(
                    "product_id" => $item['item_id'],
                    "transaction" => "PRODUCTION (+)",
                    "ref_no" => $checkCols["job_order"],
                    "qty" => $item['estimated_produced']
                );

            
            $result = $database->insert(PRODUCT_JOURNAL,$insertItemJournal);

            $updateData = Array (
                "stocks" => getProduct($item['item_id'])["stocks"] + $item['estimated_produced']
            );
        
            $database->where ('id', $item['item_id']);
            $id = $database->update (PRODUCTS_TABLE, $updateData);

            //save to product itemize
            for($i=1; $i<=$item['estimated_produced']; $i++){
                $database->insert(PRODUCT_ITEMIZE, array(
                    "item_count" => $i,
                    "item_id" => $item['item_id'],
                    "process" => "PRODUCTION (+)",
                    "ref_no" => $checkCols["job_order"],
                    "batch_no" => $checkCols["batch_no"],
                    "is_available" => 1,
                    "manufacturing" => $checkCols["manufacturing"],
                    "expiry" => $checkCols["expiration"]
                ));
            }
        }

        foreach ($productionRawItems as $item) {
            $consumed_qty = $item['start_qty'] - $item['curr_qty'];
            $insertItemJournal = array(
                    "product_id" => $item['item_id'],
                    "transaction" => "PRODUCTION (-)",
                    "ref_no" => $checkCols["job_order"],
                    "qty" => $consumed_qty
                );

            $result = $database->insert(PRODUCT_JOURNAL,$insertItemJournal);

            $updateData = Array (
                "stocks" => $item['start_qty'] - $consumed_qty
            );
        
            $database->where ('id', $item['id']);
            $id = $database->update (PRODUCTS_TABLE, $updateData);

            //get items on product itemize
            $database->where("item_id", $item['item_id']);
            $database->where("is_available" , 1);
            $itemize = $database->get(PRODUCT_ITEMIZE, $consumed_qty);

            foreach($itemize as $i){
                $database->where("id", $i["id"]);
                $database->update(PRODUCT_ITEMIZE, array(
                    "is_available" => 0,
                    "order_ref" => $checkCols["job_order"]
                ));
            }
           

        }
    
        saveLog($database,"Marked production plan id as finished:" . $data->modifyId);
        
        $updateData = Array (
            "is_started" => 2
        );
    
        $database->where ('production_id', $data->modifyId);
        $id = $database->update (PRODUCTION, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Production plan finished successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    } 

    if($data->mode == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('production_id', $data->modifyId);
        $id = $database->update (PRODUCTION, $updateData);
        if($id){
            $database->where ('production_id', $data->modifyId);
            $id = $database->update (PRODUCTION_ITEMS, $updateData);

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Production deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }
}

if(isset($_POST["startProduction"])){
    $prodId = $_POST["startProduction"];

    $updateData = Array (
        "is_started" => "1"
    );

    $database->where ('production_id', $prodId);
    $id = $database->update (PRODUCTION, $updateData);
    if($id){
        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Production marked as started!"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $database->where("is_deleted",0);
    $production = $database->get(PRODUCTION);
    echo json_encode($production);
}


if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("production_id", $id);
    $userDB = $database->getOne(PRODUCTION);
    echo json_encode($userDB);
}

if(isset($_GET["getItemsToProduce"])){
    echo json_encode(getItemsProduction($_GET["getItemsToProduce"]));
}

if(isset($_GET['checkProduction'])){
    echo json_encode(getProductionResult($_GET['checkProduction']));
}


function getItemsProduction($id){
    global $database;
    $database->where ("production_id", $id);
    $database->where ("is_deleted", 0);
    $production = $database->getOne(PRODUCTION);

    $reformattedArray = array();

    $database->where("production_id", $id);
    $itemFormulations = $database->get(PRODUCTION_ITEMS);

    foreach($itemFormulations as $item){
        array_push($reformattedArray,
            Array (
                "id" => $item['id'],
                "item_id" => $item["item_id"],
                "name" => getProduct($item["item_id"])["description"],
                "production_qty" => $item['production_qty'],
                "final_qty" => $item['final_qty'],
                "credit" => $item['credit'],
                "formulation_id" => $item['formulation_id'],
                "formulations" => getFormulation($item["formulation_id"]),
                "remarks" => $item['remarks']
            ));
    }

    return $reformattedArray;
}

function getFormulation($formId){
    global $database;
    $database->where("form_id",$formId);
    $formulation = $database->get(FORMULATION);
  
      $response = array();
  
      foreach($formulation as $formula){
          array_push($response, array(
              "id" => $formula["form_id"],
              "form_id" => $formula['form_id'],
              "description" => $formula["description"],
              "item_id" => $formula["item_id"],
              "name" => getProduct($formula["item_id"])["description"],
              "date_process" => $formula["date_process"],
              "formulation_remarks" => $formula["remarks"],
              "added_by" => $formula["added_by"],
              "date_time" => $formula["date_time"],
              "produce" => $formula["produce"],
              "kilo" => $formula["kilo"]
          ));
      }
  
      return $response;
}

function getProductionResult($production_id){
    global $database;

    $production_items = getItemsProduction($production_id);
    
    $database->where ("production_id", $production_id);
    $productionDetails = $database->getOne(PRODUCTION);

    $rawItemsArray = array();
    $insertedRawItemNames = array();
    $simulation_log = array();

    foreach ($production_items as $product_idx => $product) {
        $target_product_qty = $product['production_qty'];

        if($productionDetails['is_started'] == 2){
            $target_product_qty = $product['final_qty'];
        }

        $product_name = $product['name'];
        $product_required_rawitems = array();

        $database->where("form_id",$product['formulation_id']);
        $rawItemsTemp = $database->get(FORMULATION_DETAILS);

        foreach ($rawItemsTemp as $rawItem) {
            $needed_qty_to_produce_one = $rawItem['qty'];
            
            $database->where("id",$rawItem['raw_item_id']);
            $rawItemDetails = $database->getOne(PRODUCTS_TABLE);

            $raw_item_name = $rawItemDetails['description'];
            $raw_item_curr_quantity = $rawItemDetails['stocks'];

            $rawItem = array(
                "id" => $rawItem['raw_item_id'],
                "item_id" => $rawItemDetails["id"],
                "name" => $raw_item_name,
                "required_qty_to_one" => $needed_qty_to_produce_one,
                "old_qty" => $raw_item_curr_quantity,
                "curr_qty" => $raw_item_curr_quantity,
                "start_qty" => $raw_item_curr_quantity,
            );

            array_push($product_required_rawitems, $raw_item_name);

            if(!in_array($rawItem['name'],$insertedRawItemNames)){
                array_push($rawItemsArray, $rawItem);
                array_push($insertedRawItemNames, $rawItem['name']);
            }
        }

        

        $current_produced_num = 0;
        $produce = true;
        do {
            foreach ($rawItemsArray as $rawitem_index => $rawItem) {
                if(in_array($rawItem['name'], $product_required_rawitems)){
                    if($target_product_qty == $current_produced_num){
                        array_push($simulation_log,"~Planned qty for item ". $product_name . " :" . $target_product_qty);
                        array_push($simulation_log,"~Estimated successful production: " . $current_produced_num);
                        break 2;
                    }else if($rawItem['curr_qty'] < $rawItem['required_qty_to_one']){
                        array_push($simulation_log,"~Not enough " . $rawItem['name'] ." | Curr Qty = ".$rawItem['curr_qty'] . " | Req Qty 1 = ". $rawItem['required_qty_to_one']);
                        foreach ($rawItemsArray as $key => $value) {
                            if($rawItemsArray[$key]['curr_qty'] != $value['old_qty']){
                                array_push($simulation_log,"~Restoring " . $value['name'] ." original qty");
                            }
                            $rawItemsArray[$key]['curr_qty'] = $value['old_qty'];
                        }
                        array_push($simulation_log,"~Planned qty for item ". $product_name . " : " . $target_product_qty);
                        array_push($simulation_log,"~Estimated successful production: " . $current_produced_num);
                        
                        break 2;
                    }else{
                        $rawItemsArray[$rawitem_index]['old_qty'] = $rawItemsArray[$rawitem_index]['curr_qty'];
                        $rawItemsArray[$rawitem_index]['curr_qty'] = $rawItem['curr_qty'] - $rawItem['required_qty_to_one'];
                        array_push($simulation_log,"-- Get " . $rawItem['required_qty_to_one'] ." of " . $rawItem['name'] . ", current quantity is :" . $rawItem['curr_qty']);
                        $produce = true;  
                    }
                }
            }

            if($produce){
                $current_produced_num++;
                foreach ($rawItemsArray as $key => $value) {
                    $rawItemsArray[$key]['old_qty'] = $value['curr_qty'];
                }
                //array_push($simulation_log,"> Produced 1 of " . $product_name . ", current num produced is :" . $current_produced_num);
            }
        } while ($produce);

        $production_items[$product_idx]['estimated_produced'] = $current_produced_num;
    }

    foreach ($rawItemsArray as $key => $value) {
        array_push($simulation_log,"[ " . $value['name'] . " consumed: " . ($value['start_qty']-$value['curr_qty']) . " out of " . $value['start_qty'] ." ]");
    }

    return array(
        "production_details" => $productionDetails,
        "items" => $production_items,
        "raw_items" => $rawItemsArray,
        "simulation_log" => $simulation_log,
        "type" => "success",
    );
}




?>