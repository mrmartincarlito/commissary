<?php

function updateProductStocks($database, $productId, $qty, $transaction, $refno, $movement = "+"){
    $database->rawQuery("Update products set stocks = stocks $movement $qty where id = $productId");

    addProductJournal($database,
        array(
            "product_id" => $productId,
            "transaction" => $transaction,
            "qty" => $qty,
            "ref_no" => $refno,
            "updated_by" => $_SESSION["username-commissary"]
        )
    );
}   

function addProductJournal($database, $insertData){
    $database->insert("product_journal", $insertData);
}