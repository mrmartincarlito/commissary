<?php
require_once("config.php");
require_once("logs.php");

$userDetails = json_decode(getLoggedUserDetails($database));

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){
        $insertData = Array (
            "item_id" => $data->item_id,
            "description" => $data->description,
            "date_process" => $data->date_process,
            "remarks" => $data->remarks,
            "added_by" => $userDetails->username,
            "produce" => $data->produce,
            "kilo" => $data->kilo
        );

        $id = $database->insert (FORMULATION, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Formulation added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
        saveLog($database,"{$data->formAction} Formulation: {$data->item_id}");
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "item_id" => $data->item_id,
            "description" => $data->description,
            "date_process" => $data->date_process,
            "remarks" => $data->remarks,
            "added_by" => $userDetails->username,
            "produce" => $data->produce,
            "kilo" => $data->kilo
        );

        $database->where ('form_id', $data->modifyId);
        $id = $database->update (FORMULATION, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Formulation details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
        saveLog($database,"{$data->formAction} Formulation: {$data->item_id}");
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('form_id', $data->modifyId);
        $id = $database->update (FORMULATION, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Formulation deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
        saveLog($database,"{$data->formAction} Formulation: {$data->item_id}");
    }

   
    if($data->formAction == "addRaw"){

        $form_id = $data->modifyId;
        $raw_item = $data->raw_item_id;

        $database->where("form_id",$form_id);
        $database->where("raw_item_id",$raw_item);
        $look_up = $database->get(FORMULATION_DETAILS);

        if(count($look_up) > 0){
            $raw = array(
                "form_id" => $data->modifyId,
                "raw_item_id" => $data->raw_item_id,
                //"qty" => $data->qty,
                "orig_qty" => $data->orig_qty,
                "orig_uom" => $data->orig_uom,
                "req_qty" => $data->req_qty,
                "req_uom" => $data->req_uom,
                "remarks" => $data->remarks
            );
            
            $database->where("form_id",$form_id);
            $database->where("raw_item_id",$raw_item);
            $id = $database->update (FORMULATION_DETAILS, $raw);
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Formulation Raw Item updated successfully!"
            ));
            return;
        }

        $raw = array(
            "form_id" => $data->modifyId,
            "raw_item_id" => $data->raw_item_id,
            "qty" => $data->orig_qty / $data->req_qty,
            "orig_qty" => $data->orig_qty,
            "orig_uom" => $data->orig_uom,
            "req_qty" => $data->req_qty,
            "req_uom" => $data->req_uom,
            "remarks" => $data->remarks
        );

        $id = $database->insert (FORMULATION_DETAILS, $raw);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Formulation Raw Item added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
        saveLog($database,"{$data->formAction} Formulation: {$data->raw_item_id}");
    }
   
}

if(isset($_POST['deleteRawMat'])){
    $id = $_POST['deleteRawMat'];

    $database->where("id",$id);
    $get = $database->delete(FORMULATION_DETAILS);
    if($get){
        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Raw Item Deleted!"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }
}

if(isset($_GET['getNonFoodFormProductionId'])){
    $form_id = $_GET['getNonFoodFormProductionId'];

    $database->where("form_id", $form_id);
    $rawMats = $database->get(FORMULATION_DETAILS);

    $response = array();

    foreach($rawMats as $rawMat){
        array_push($response, array(
            "id" => $rawMat["id"],
            "form_id" => $rawMat["form_id"],
            "name" => getProduct($rawMat["raw_item_id"])["description"],
            "req_qty" => $rawMat["req_qty"],
            "req_uom" => $rawMat["req_uom"],
            "remarks" => $rawMat["remarks"]
        ));
    }

    echo json_encode($response);
}


//GET METHODS
if(isset($_GET["get"])){
    $database->where("is_deleted", 0); 
    $formulation = $database->get(FORMULATION);

    $response = array();

    foreach($formulation as $formula){
        array_push($response, array(
            "form_id" => $formula['form_id'],
            "description" => $formula["description"],
            "item_id" => $formula["item_id"],
            "name" => getProduct($formula["item_id"])["description"],
            "date_process" => $formula["date_process"],
            "formulation_remarks" => $formula["remarks"],
            "added_by" => $formula["added_by"],
            "date_time" => $formula["date_time"],
            "produce" => $formula["produce"],
            "kilo" => $formula["kilo"]
        ));
    }

    echo json_encode($response);
}

if(isset($_GET["getDetails"])){
  $id = $_GET["getDetails"];
  $database->where("form_id",$id);
  $formulation = $database->get(FORMULATION);

    $response = array();

    foreach($formulation as $formula){
        array_push($response, array(
            "id" => $formula["form_id"],
            "form_id" => $formula['form_id'],
            "description" => $formula["description"],
            "item_id" => $formula["item_id"],
            "name" => getProduct($formula["item_id"])["description"],
            "date_process" => $formula["date_process"],
            "formulation_remarks" => $formula["remarks"],
            "added_by" => $formula["added_by"],
            "date_time" => $formula["date_time"],
            "produce" => $formula["produce"],
            "kilo" => $formula["kilo"]
        ));
    }

    echo json_encode($response);
}
