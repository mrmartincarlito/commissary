<?php
require_once("config.php");
require_once("logs.php");

if(isset($_POST['saveItemizeProduct'])){
    $items = json_decode($_POST['saveItemizeProduct']);
    
    $database->where("item_id", $_POST['item']);
    $database->where("ref_no", $_POST['refno']);
    $database->delete(PRODUCT_ITEMIZE);

    $all = false;
    $total = 0;
    foreach($items as $item){
        if(!is_numeric($item->item_count)){
            $all = true;
            $total = $item->total_qty;
            break;
        }

        insertProductItems($item->item_count, $item);
    }

    if($all){
        for($i=1; $i<=$total; $i++){
            insertProductItems($i, $item);
        }
    }

    echo json_encode(Array (
        "type" => "success",
        "title" => "Successful!",
        "text" => "Successfully added itemize products"
    ));
}

if(isset($_GET['getItemize'])){
    $database->where("ref_no", $_GET['getItemize']);
    $database->where("item_id", $_GET['item']);
    $items = $database->get(PRODUCT_ITEMIZE);

    echo json_encode($items);
}

function insertProductItems($i, $item){
    global $database;

    $database->insert(PRODUCT_ITEMIZE, array(
        "item_count" => $i,
        "item_id" => $item->item_id,
        "ref_no" => $item->ref_no,
        "process" => $item->process,
        "batch_no" => $item->batch_no,
        "manufacturing" => $item->manufacturing,
        "expiry" => $item->expiry
    ));
    echo $database->getLastError();
}