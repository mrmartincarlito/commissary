<?php
require_once("config.php");
require_once("logs.php");
require_once("inventory.php");

$userDetails = json_decode(getLoggedUserDetails($database));

if (isset($_POST["data"])) {
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if ($data->formAction == "add") {
        $code = generateRefno($database, 1, PRODUCT_DISPOSAL, "code", "X-");
        $insertData = array(
            "code" => $code,
            "reason" => $data->reason,
            "status" => $data->status,
            "added_by" => $userDetails->username,
        );

        $id = $database->insert(PRODUCT_DISPOSAL, $insertData);
        if ($id) {
            echo json_encode(array(
                "type" => "success",
                "title" => "Successful!",
                "text" => "Disposal added successfully add items in this disposal!"
            ));
        } else {
            echo json_encode(array(
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
        saveLog($database, "{$data->formAction} Disposal: {$code}");
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "reason" => $data->reason,
            "status" => $data->status,
            "added_by" => $userDetails->username,
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (PRODUCT_DISPOSAL, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Product Disposal details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
        saveLog($database,"{$data->formAction} Disposal: {$data->modifyId}");
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (PRODUCT_DISPOSAL, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Disposal deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
        saveLog($database,"{$data->formAction} Disposal: {$data->modifyId}");
    }


    if ($data->formAction == "addRaw") {
        
        $form_id = $data->modifyId;
        $raw_item = $data->raw_item_id;

        $database->where("disposal_id",$form_id);
        $database->where("item_id",$raw_item);
        $look_up = $database->get(PRODUCT_DISPOSAL_ITEMS);
        $product = getProduct($data->raw_item_id);

        if(count($look_up) > 0){
            
            $raw = array(
                "disposal_id" => $data->modifyId,
                "item_id" => $data->raw_item_id,
                "qty" => $data->orig_qty,
                "uom" => $data->orig_uom,
                "cost" => $product["cost"],
                "total_amount" => $product["cost"] * $data->orig_qty
            );
            
            $database->where("disposal_id",$form_id);
            $database->where("item_id",$raw_item);
            $id = $database->update (PRODUCT_DISPOSAL_ITEMS, $raw);
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Product Dispodal updated successfully!"
            ));
            return;
        }

        $raw = array(
            "disposal_id" => $data->modifyId,
            "item_id" => $data->raw_item_id,
            "qty" => $data->orig_qty,
            "uom" => $data->orig_uom,
            "cost" => $product["cost"],
            "total_amount" => $product["cost"] * $data->orig_qty
        );

        $id = $database->insert (PRODUCT_DISPOSAL_ITEMS, $raw);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Product Disposal added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
        saveLog($database,"{$data->formAction} Disposal ID: {$data->modifyId}");
    }
}

if(isset($_POST['deleteRawMat'])){
    $id = $_POST['deleteRawMat'];

    $database->where("id",$id);
    $get = $database->delete(PRODUCT_DISPOSAL_ITEMS);
    if($get){
        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Item Deleted!"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }
}

if (isset($_GET['get'])) {
    $database->where("is_deleted", 0);
    echo json_encode($database->get(PRODUCT_DISPOSAL));
}

if (isset($_GET['getDetails'])) {
    $database->where("id", $_GET['getDetails']);
    $database->where("is_deleted", 0);
    echo json_encode($database->getOne(PRODUCT_DISPOSAL));
}

if (isset($_GET['getDisposalTable'])) {
    $form_id = $_GET['getDisposalTable'];

    $database->where("disposal_id", $form_id);
    $rawMats = $database->get(PRODUCT_DISPOSAL_ITEMS);

    $database->where("id", $form_id);
    $database->where("is_deleted", 0);
    $disp = $database->getOne(PRODUCT_DISPOSAL);

    $response = array();

    foreach($rawMats as $rawMat){
        array_push($response, array(
            "id" => $rawMat["id"],
            "form_id" => $rawMat["item_id"],
            "name" => getProduct($rawMat["item_id"])["description"],
            "req_qty" => $rawMat["qty"],
            "req_uom" => $rawMat["uom"],
            "status" => $disp["status"],
            "reuse_item_id" => $rawMat["reuse_item_id"],
            "reuse_item" => !empty(getProduct($rawMat["reuse_item_id"])) ? getProduct($rawMat["reuse_item_id"])["description"] : "",
            "is_disposed" => $rawMat["is_disposed"],
            "is_po_received" => $rawMat["is_po_received"]
        ));
    }

    echo json_encode($response);
}

if (isset($_POST['reuseReturnItemId'])) {
    $id = $_POST['reuseReturnItemId'];
    $itemId = $_POST['itemId'];
    $process = $_POST["process"];

    $database->where("id", $id);
    $returnedItem = $database->getOne(PRODUCT_DISPOSAL);

    $database->where("disposal_id", $id);
    $disposalItems = $database->get(PRODUCT_DISPOSAL_ITEMS);

    foreach ($disposalItems as $disposalItem) {

        if ($process == "REUSE") {
            $database->where("item_id", $disposalItem["item_id"]);
            $database->where("is_available", 1);
            $database->orderBy("item_count","asc");
            $update = $database->update(PRODUCT_ITEMIZE, array(
                "process" => "REUSED (-)",
                "is_available" => 0,
                "order_ref" => $returnedItem["code"]
            ) ,$disposalItem["qty"]);
    
            
            updateProductStocks($database, $disposalItem["item_id"], $disposalItem["qty"], "REUSED (-)", $returnedItem["code"], $movement = "-"); 
    
            for($i = 0; $i<$disposalItem["qty"]; $i++) {
                $database->insert(PRODUCT_ITEMIZE, array(
                    "item_count" => $i + 1,
                    "item_id" => $itemId,
                    "process" => "REUSED (+)",
                    "ref_no" => $returnedItem["code"],
                    "batch_no" => $returnedItem["code"],
                    "is_available" => 1
                ));
            }
    
            updateProductStocks($database, $itemId, $disposalItem["qty"], "REUSED (+)", $returnedItem["code"], $movement = "+"); 
    
            $database->where("item_id", $disposalItem["item_id"]);
            $database->where("disposal_id", $id);
            $database->update(PRODUCT_DISPOSAL_ITEMS, array(
                "reuse_item_id" => $itemId
            ));
        }

        if ($process == "DISPOSED") {
            $database->where("item_id", $disposalItem["item_id"]);
            $database->where("is_available", 1);
            $database->orderBy("item_count","asc");
            $update = $database->update(PRODUCT_ITEMIZE, array(
                "process" => "DISPOSED",
                "is_available" => 0,
                "order_ref" => $returnedItem["code"]
            ) ,$disposalItem["qty"]);
    
            updateProductStocks($database, $disposalItem["item_id"], $disposalItem["qty"], "DISPOSED", $returnedItem["code"], $movement = "-"); 
            
            $database->where("item_id", $disposalItem["item_id"]);
            $database->where("disposal_id", $id);
            $database->update(PRODUCT_DISPOSAL_ITEMS, array(
                "is_disposed" => 1
            ));
        }


        if ($process == "RECEIVED") {
            $database->where("item_id", $disposalItem["item_id"]);
            $database->where("is_available", 1);
            $database->orderBy("item_count","asc");
            $update = $database->update(PRODUCT_ITEMIZE, array(
                "process" => "RETURN TO SUPPLIER (-)",
                "is_available" => 0,
                "order_ref" => $returnedItem["code"]
            ) ,$disposalItem["qty"]);
    
            updateProductStocks($database, $disposalItem["item_id"], $disposalItem["qty"], "RETURN TO SUPPLIER (-)", $returnedItem["code"], $movement = "-"); 
            
            for($i = 0; $i<$disposalItem["qty"]; $i++) {
                $database->insert(PRODUCT_ITEMIZE, array(
                    "item_count" => $i + 1,
                    "item_id" => $disposalItem["item_id"],
                    "process" => "RETURN TO SUPPLIER (+)",
                    "ref_no" => $returnedItem["code"],
                    "batch_no" => $returnedItem["code"],
                    "is_available" => 1
                ));
            }
    
            updateProductStocks($database, $disposalItem["item_id"], $disposalItem["qty"], "RETURN TO SUPPLIER (+)", $returnedItem["code"], $movement = "+"); 

            $database->where("item_id", $disposalItem["item_id"]);
            $database->where("disposal_id", $id);
            $database->update(PRODUCT_DISPOSAL_ITEMS, array(
                "is_po_received" => 1
            ));
        }

    }

    echo json_encode(Array (
        "type" => "success",
        "title" => "Successful!",
        "text" => "You just $process an item"
    ));
}
