<?php
require_once("config.php");
require_once("logs.php");
require_once("inventory.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    $po_number = generateRefno($database, "1", PURCHASE_ORDER, "po_number", "D-");
    $approvedBy = "";
    if($data->status == "APPROVED"){
        $approvedBy = $loggedUser->username;
    }
    
    if($data->formAction == "add"){

        $insertData = Array (
            "po_number" => $po_number, //to be finalized
            "date_po" => $data->date_po,
            "supplier_id" => $data->supplier_id,
            "mode_of_payment" => $data->mode_of_payment,
            "status" => $data->status,
            "added_by" =>$loggedUser->username,
            "approved_by" => $approvedBy
        );

        $id = $database->insert (PURCHASE_ORDER, $insertData);
        if($id){
            
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Purchase Order Added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "date_po" => $data->date_po,
            "supplier_id" => $data->supplier_id,
            "mode_of_payment" => $data->mode_of_payment,
            "status" => $data->status,
            "added_by" =>$loggedUser->username,
            "approved_by" => $approvedBy
        );
        
        $database->where ('id', $data->modifyId);
        $id = $database->update (PURCHASE_ORDER, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Purchase Order Details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (PURCHASE_ORDER, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Purchase Order Deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} Purchase order: {$po_number}");
    }else{
        saveLog($database,"{$data->formAction} Purcahse Order ID {$data->modifyId}");
    }
}

//save purchase order items
if(isset($_POST['poItems'])){
    $postData = json_decode($_POST["poItems"]);
    $data = json_decode($postData->data);

    $values = array(
        "purchase_order_id" => $data->purchase_order_id,
        "product_id" => $data->product_id,
        "qty" => $data->qty,
        "cost" => $data->cost,
        "total_amount" => $data->total_amount
    );

    //check if existing
    $database->where("purchase_order_id", $data->purchase_order_id);
    $database->where("product_id", $data->product_id);
    $items = $database->getOne(PURCHASE_ORDER_ITEMS);

    //empty insert items
    if(empty($items)){
        $id = $database->insert(PURCHASE_ORDER_ITEMS, $values);
    }else{
        //update items
        $database->where("purchase_order_id", $data->purchase_order_id);
        $database->where("product_id", $data->product_id);
        $id = $database->update(PURCHASE_ORDER_ITEMS, $values);
    }

    updatePOTotalAmount($data->purchase_order_id);

    if($id){
        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Item added successfully!"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }
    
}

if(isset($_POST['deletePoItem'])){
    $id = $_POST['deletePoItem'];

    $database->where("id", $id);
    $poInfo = $database->getOne(PURCHASE_ORDER_ITEMS);

    updatePOTotalAmount($poInfo["purchase_order_id"]);

    $database->where("id", $id);
    $id = $database->delete(PURCHASE_ORDER_ITEMS);

    if($id){
        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Removed item successfully!"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }
}

if(isset($_POST['saveReceivingPO'])){
    $postData = json_decode($_POST["saveReceivingPO"]);
    $data = json_decode($postData->data);

    //check if po already received
    $database->where("id", $data->po_number_receive);
    $po = $database->getOne(PURCHASE_ORDER);

    if(empty($po)){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "PO Number is invalid"
        ));

        return;
    }
    

    //insert receiving
    $receivingDetails = array(
        "total_amount" => str_replace("," , "" ,$data->total_po_amount),
        "received_by" => isset($data->received_by) ? $data->received_by : "",
        "date_received" => isset($data->date_received) ? $data->date_received : "",
        "remarks" => isset($data->remarks) ? $data->remarks : "",
        "added_by" => $loggedUser->username
    );

    $database->where("purchase_order_number", $po["po_number"]);
    $id = $database->update(RECEIVING_PO, $receivingDetails);
    if($id){
        //look items inside po items
        $database->where("purchase_order_id", $data->po_table_id);
        $poItems = $database->get(PURCHASE_ORDER_ITEMS);

        foreach($poItems as $poItem){
            $po_qty_id = "po_qty_".$poItem["id"];
            $po_cost_id = "po_cost_".$poItem["id"];
            $po_item_total_id = "po_item_total_".$poItem["id"];
            $po_product_id = "po_product_".$poItem["id"];

            $insertRecevingItems = array(
                "rec_qty" => $data->$po_qty_id,
                "cost" => str_replace("," , "" ,$data->$po_cost_id),
                "total_amount" => str_replace("," , "" ,$data->$po_item_total_id)
            );

            $database->where("purchase_order_number", $po["po_number"]);
            $database->where("product_id", $data->$po_product_id );
            $database->update(RECEIVING_PO_ITEMS, $insertRecevingItems);
        }

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful",
            "text" => "You successfully updated Received PO"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }
}

function updatePOTotalAmount($id){
    global $database;

    $database->rawQuery("
        update ".PURCHASE_ORDER." set total_amount = (select SUM(total_amount) from ".PURCHASE_ORDER_ITEMS." where purchase_order_id = $id)
        where id = $id
    ");
}

//GET METHODS
if(isset($_GET["get"])){
    $primaryKey = 'id';
    $columns = array(
        array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'po_number',  'dt' => 1 ),
        array( 'db' => 'date_po',  'dt' => 2 ),
        array(  'db' => 'supplier_id',  
                'dt' => 3,
                'formatter' => function ($data, $row){
                    global $database;

                    $database->where("id", $data);
                    $supplier = $database->getOne(SUPPLIER_TABLE);
                    return empty($supplier) ? "" : $supplier["supplier_name"];
                }
            ),
        array( 'db' => 'total_amount',   'dt' => 4 ),
        array( 'db' => 'mode_of_payment',   'dt' => 5 ),
        array(  'db' => 'status',   
                'dt' => 6,
                'formatter' => function ($data, $row){

                    return convertStatusColor($data);
                }
            ),
        array( 'db' => 'added_by',   'dt' => 7 ),
        array(  'db' => 'id',   
                'dt' => 8 ,
                'formatter' => function($data ,$row) {

                    if($row["status"] != "RECEIVED"){
                        return ' <div class="btn-group dropdown">
                                <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                                <ul role="menu" class="dropdown-menu animated">
                                    <li><a href="#" onclick="modify('.$data.')">Modify</a></li>
                                    <li><a href="#" onclick="viewPO('.$data.')">Add Items</a></li>
                                </ul>
                            </div>';
                    }else{
                        return ' <button type="button" class="btn btn-primary" onclick="receivePO('.$data.')"><i class="fa fa-eye m-r-5"></i> <span>View Received PO</span></button>';
                    }
                    
                }
            ),
    );
    
    $condition = "is_deleted = 0";

    if(isset($_GET['status'])){
        $condition .= " and status = '{$_GET['status']}'";
    }

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, PURCHASE_ORDER , $primaryKey, $columns, $condition )
    );
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $userDB = $database->getOne(PURCHASE_ORDER);
    echo json_encode($userDB);
}

if(isset($_GET['viewPO'])){
    $poId = $_GET['viewPO'];

    $response = array();

    //Get Purchase Order table
    $database->where("id", $poId);
    $poInformation = $database->getOne(PURCHASE_ORDER);

    $response["po_information"] = $poInformation;

    //get supplier details
    $database->where("id", $poInformation["supplier_id"]);
    $supplier = $database->getOne(SUPPLIER_TABLE);

    $response["supplier_name"] = $supplier["supplier_name"];

    echo json_encode($response);
}

if(isset($_GET['viewPOItems'])){
    $poId = $_GET['viewPOItems'];
    $supplier = "";
    $total_po_amount = 0;

    //get po items

    if(isset($_GET['byPONumber'])){
        $database->where ("po_number", $poId); 
    } else {
        $database->where ("id", $poId); 
    }

    $purchaseOrderDetail = $database->getOne(PURCHASE_ORDER);
    $poId = $purchaseOrderDetail["id"];
    $supplier = getSupplier($purchaseOrderDetail["supplier_id"])["supplier_name"];
    $total_po_amount = $purchaseOrderDetail["total_amount"];
        
    $database->where ("purchase_order_id", $poId);
    $poItems = $database->get(PURCHASE_ORDER_ITEMS);

    $response = array();

    foreach($poItems as $poItem){
        $value["id"] = $poItem["id"];
        $value["purchase_order_id"] = $poItem["purchase_order_id"];
        $value["product_id"] = $poItem["product_id"];
        $value["qty"] = $poItem["qty"];
        $value["cost"] = $poItem["cost"];
        $value["total_amount"] = $poItem["total_amount"];
        $value["supplier"] = $supplier;
        $value["total_po_amount"] = $total_po_amount;
        $value["po_id"] = $poId;

        $database->where("id", $poItem["product_id"]);
        $product = $database->getOne(PRODUCT_TABLE);

        $value["description"] = $product["description"];

        array_push($response, $value);
    }
 
     echo json_encode($response);
}

if (isset($_GET['viewReceivedPO'])) {
    $id = $_GET['viewReceivedPO'];

    $database->where("id", $id);
    $poDetails = $database->getOne(PURCHASE_ORDER);

    $database->where("purchase_order_id", $poDetails["id"]);
    $poItems = $database->get(PURCHASE_ORDER_ITEMS);
    $supplier = getSupplier($poDetails["supplier_id"])["supplier_name"];
    $response = array();

    $receivingDetails = getReceivedPODetails($poDetails["po_number"]);

    foreach($poItems as $poItem) {
        $value["id"] = $poItem["id"];
        $value["purchase_order_id"] = $poItem["purchase_order_id"];
        $value["product_id"] = $poItem["product_id"];
        $value["qty"] = $poItem["qty"];
        $value["rec_qty"] = getReceivedPOByItem($poDetails["po_number"], $poItem["product_id"])["rec_qty"];
        $value["cost"] = $poItem["cost"];
        $value["total_amount"] = $poItem["cost"] * $value["rec_qty"];
        $value["supplier"] = $supplier;
        $value["total_po_amount"] = 0; //to be calculated
        $value["po_id"] = $id;

        $database->where("id", $poItem["product_id"]);
        $product = $database->getOne(PRODUCT_TABLE);

        $value["description"] = $product["description"];

        //receiving 
        $value["received_by"] = $receivingDetails["received_by"];
        $value["date_received"] = $receivingDetails["date_received"];
        $value["remarks"] = $receivingDetails["remarks"];

        array_push($response, $value);
    }

    echo json_encode($response);
}

function getReceivedPODetails ($poNumber) {
    global $database;

    $database->where("purchase_order_number" , $poNumber);
    return $database->getOne(RECEIVING_PO);
}

function getReceivedPOByItem($poNumber, $productId) {
    global $database;

    $database->where("purchase_order_number", $poNumber);
    $database->where("product_id", $productId);

    return $database->getOne(RECEIVING_PO_ITEMS);
}

?>