    <?php
require_once("config.php");
require_once("logs.php");
require_once("inventory.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_GET["get"])){

    $primaryKey = 'id';
    $columns = array(
        array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'order_ref_no',  'dt' => 1 ),
        array( 'db' => 'return_ref_no',  'dt' => 2 ),
        array(  'db' => 'order_ref_no',  
                'dt' => 3,
                'formatter' => function ($data, $row){

                    global $database;

                    $database->where("order_ref", $data);
                    $order = $database->getOne(ORDERS);

                    $franchisee = getFranchise($order["franchisee_id"]);

                    return $franchisee["name"];
                }
        ),
        array(  'db' => 'status',   
                'dt' => 4,
                'formatter' => function ($data, $row){

                    return convertStatusColor($data);
                }
            ),
        array( 'db' => 'date_time',   'dt' => 5 ),
        array( 'db' => 'remarks',   'dt' => 6 ),
        array( 'db' => 'item_condition',   'dt' => 7 ),
        array(  'db' => 'return_ref_no',   
                'dt' => 8 ,
                'formatter' => function($data ,$row) {

                    $poNumber = "'".$data."'";

                    $button = '<button class="btn btn-primary" onclick="viewReturnGoods('.$poNumber.')"><i class="ti-eye"></i> VIEW RETURN GOODS</button>';   
                    
                    return $button;
                }
            ),
    );
    
    $condition = "is_deleted = 0 and NOT(status = 'PENDING')";

    if(isset($_GET['status'])){
        $condition .= " and status = '{$_GET['status']}'";
    }

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, ORDER_RETURNS , $primaryKey, $columns, $condition )
    );
}

if(isset($_GET['getReturnItems'])){

    $returnRef = $_GET['getReturnItems'];

    $database->where("return_ref_no", $returnRef);
    $returns = $database->getOne(ORDER_RETURNS);

    $database->where("return_ref", $returnRef);
    $items = $database->get(ORDER_RETURN_ITEMS);

    $response = array();

    $database->where("is_deleted", 0);
    $reuseItemAs = $database->get(PRODUCT_TABLE);

    foreach($items as $item){

        $product = "";
        if (!empty($item["reuse_item_id"])) {
            $product = getProduct($item["reuse_item_id"])["description"];
        }

        array_push($response, array(
            "id" => $item["id"],
            "return_ref" => $item['return_ref'],
            "item_id" => $item['item_id'],
            "return_qty" => $item['return_qty'],
            "cost" => number_format($item['cost']),
            "total_amount" => number_format($item['total_amount']),
            "item" => getProduct($item['item_id'])["description"],
            "status" => $returns["status"],
            "item_status" => $item["status"],
            "reuse_item" => $product,
            "item_condition" => $returns["item_condition"],
            "products" => $reuseItemAs
        ));
    }

    echo json_encode($response);
}

if (isset($_POST['reuseReturnItemId'])) {
    $id = $_POST['reuseReturnItemId'];
    $itemId = $_POST['itemId'];

    $database->where("id", $id);
    $returnedItem = $database->getOne(ORDER_RETURN_ITEMS);

    $database->where("return_ref_no", $returnedItem["return_ref"]);
    $returnedDetails = $database->getOne(ORDER_RETURNS);

    //get initial details of reused product
    $database->where("order_ref", $returnedDetails["order_ref_no"]);
    $productItemize = $database->getOne(PRODUCT_ITEMIZE);

    for($i = 0; $i<$returnedItem["return_qty"]; $i++) {
        $database->insert(PRODUCT_ITEMIZE, array(
            "item_count" => $i,
            "item_id" => $itemId,
            "process" => "REUSED",
            "ref_no" => $productItemize["ref_no"],
            "batch_no" => $productItemize["batch_no"],
            "is_available" => 1,
            "manufacturing" => $productItemize["manufacturing"],
            "expiry" => $productItemize["expiry"]
        ));
    }

    //insert to product journal
    updateProductStocks($database, $itemId, $returnedItem["return_qty"], "REUSED GOODS", $returnedItem["return_ref"], $movement = "+"); 

    $database->where("id", $id);
    $database->update(ORDER_RETURN_ITEMS, array(
        "status" => "REUSED",
        "reuse_item_id" => $itemId
    ));

    echo json_encode(Array (
        "type" => "success",
        "title" => "Successful!",
        "text" => "You just DISPOSED a return request"
    ));
}

if(isset($_POST['changeItems'])){
    $status = $_POST['changeItems'];
    $ref = $_POST['return_ref'];

    $database->where("return_ref_no", $ref);
    $return = $database->getOne(ORDER_RETURNS);

    if($return["status"] != "APPROVED"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "This return request is already ".$return["status"]
        ));
        return;
    }

    if ( $status == "DISPOSED" ){
        $database->where("return_ref_no", $ref);
        $database->update(ORDER_RETURNS, array(
            "item_condition" => "DISPOSED",
            "status" => "DISPOSED"
        ));

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "You just DISPOSED a return request"
        ));
    } 

    if ($status == "REPLACED"){
        $database->where("return_ref_no", $ref);
        $order = $database->getOne(ORDER_RETURNS);

        $database->where("reuse_item_id", 0);
        $database->where("return_ref", $ref);
        $orderItems = $database->get(ORDER_RETURN_ITEMS);

        $success = true;
        //checker if order is enoungh
        foreach($orderItems as $orderItem){
            //get items on product items
            $database->where("item_id", $orderItem["item_id"]);
            $database->where("is_available" , 1);
            $itemize = $database->get(PRODUCT_ITEMIZE, $orderItem["return_qty"]);

            if(count($itemize) != $orderItem["return_qty"]){
                $product = getProduct($orderItem["item_id"]);
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error",
                    "text" => "Not Enough ".$product["description"]. ", Current Stocks (".count($itemize).") Not Enough for (".$orderItem["return_qty"]."). Please do production"
                ));
                $success = false;
                break;
            }
        }

        if($success){
            foreach($orderItems as $orderItem){
                //get items on product items
                $database->where("item_id", $orderItem["item_id"]);
                $database->where("is_available" , 1);
                $itemize = $database->get(PRODUCT_ITEMIZE, $orderItem["return_qty"]);
    
                //set product itemize with order ref
                foreach($itemize as $item){
                    $database->where("id", $item["id"]);
                    $database->update(PRODUCT_ITEMIZE, array(
                        "is_available" => 0,
                        "order_ref" => $ref
                    ));
                }
    
                //insert to product journal
                updateProductStocks($database, $orderItem["item_id"], $orderItem["return_qty"], "RETURN GOODS", $ref, $movement = "-");          
            }
    
            $mot = getFranchise($order["franchisee_id"])["mode_of_transaction"];
    
            $database->where("return_ref_no", $ref);
            $update = $database->update(ORDER_RETURNS, array(
                "status" => $mot,
                "item_condition" => "REPLACED"
            ));
    
            if($update){
                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "text" => "Successfully Replaced Order"
                ));
            }else {
                echo json_encode(array(
                    "type" => "error",
                    "title" => "Error!",
                    "text" => "Error while approving order " . $database->getLastError(),
                ));
            }
    
        }
    }
}
