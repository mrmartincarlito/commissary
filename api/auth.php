<?php
require_once("config.php");
require_once("logs.php");

function getLoggedUserDetails($database){
    $username = @$_SESSION["username-commissary"];

    $database->where ("username", $username);
    $userDB = $database->getOne(COMMISSARY);
    return json_encode($userDB);
}

function setLoggedUser($username){
    $_SESSION["username-commissary"] = $username;
}

if(isset($_GET["getLoggedUserDetails"])){
    echo getLoggedUserDetails($database);
}

if(isset($_POST["data"]) && preg_match('/auth.php/', $actual_link)) {
    $postData = json_decode($_POST["data"]);
    $action = $postData->action;
    $data = json_decode($postData->data);

    if($action == "login"){
        $database->where("is_deleted", 0);
        $database->where ("username", $data->username);
        $userDB = $database->getOne (COMMISSARY);
        if($userDB['username'] == $data->username && password_verify($data->password,$userDB['password'])){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Logged in succesfully!"
            ));
            setLoggedUser($userDB['username']);
            saveLog($database,"Logged in");
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => "Wrong username or password!"
            ));
        }
    }
}



?>