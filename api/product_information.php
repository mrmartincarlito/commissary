<?php
require_once("config.php");
require_once("logs.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){

        $insertData = Array (
            "product_code" => generateRefno($database, "1", PRODUCT_TABLE, "product_code", "I-"),
            "supplier_id" => $data->supplier_id,
            "description" => $data->description,
            "cost" => $data->cost,
            "price" => $data->price,
            "category" => $data->category,
            "order_count" => $data->order_count,
            "uom" => $data->uom
        );

        $id = $database->insert (PRODUCT_TABLE, $insertData);
        if($id){
            $_SESSION['image-product-id'] = $id;
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Product Added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            //"product_code" => time(), //to be finalized
            "supplier_id" => $data->supplier_id,
            "description" => $data->description,
            "cost" => $data->cost,
            "price" => $data->price,
            "category" => $data->category,
            "order_count" => $data->order_count,
            "uom" => $data->uom
        );
        $_SESSION['image-product-id'] = $data->modifyId;
        $database->where ('id', $data->modifyId);
        $id = $database->update (PRODUCT_TABLE, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Product Details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (PRODUCT_TABLE, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Product Deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} Product: {$data->description}");
    }else{
        saveLog($database,"{$data->formAction} Product ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $primaryKey = 'id';
    $columns = array(
        array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'product_code',  'dt' => 1 ),
        array(  'db' => 
                'image_file',  'dt' => 2 ,
                'formatter' => function ($data, $row){
                    $image = empty($data) ? "plugins/images/no-item.png" : "api/$data";
                    return '<img src="'.$image.'" alt="NO IMAGE" class="img-thumbnail">';

                }
        ),
        array(  'db' => 'supplier_id',  
                'dt' => 3,
                'formatter' => function ($data, $row){
                    global $database;

                    $database->where("id", $data);
                    $supplier = $database->getOne(SUPPLIER_TABLE);
                    return empty($supplier) ? "" : $supplier["supplier_name"];
                }
            ),
        array( 'db' => 'description',   'dt' => 4 ),
        array( 'db' => 'cost',   'dt' => 5 ),
        array( 'db' => 'price',   'dt' => 6 ),
        array( 'db' => 'category',   'dt' => 7 ),
        array( 'db' => 'stocks',   'dt' => 8 ),
        array(  'db' => 'id',   
                'dt' => 9 ,
                'formatter' => function($data ,$row) {
                    return ' <div class="btn-group dropdown">
                                <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                                <ul role="menu" class="dropdown-menu animated">
                                    <li><a href="#" onclick="modify('.$data.')">Modify</a></li>
                                    <li><a href="#" onclick="viewProductJournal('.$data.')">Product Journal</a></li>
                                    <li><a href="#" onclick="actualCount('.$data.')">Edit Actual Count</a></li>
                                </ul>
                            </div>';
                }
            ),
    );
    
    $condition = "is_deleted = 0";

    if(isset($_GET['food'])){
        $condition .= " and category = 'FOOD'";
    }

    if(isset($_GET['nonfood'])){
        $condition .= " and category = 'NON FOOD'";
    }

    if(isset($_GET['others'])){
        $condition .= " and category = 'OTHERS'";
    }

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, PRODUCT_TABLE , $primaryKey, $columns, $condition )
    );
}

if(isset($_GET['viewProductJournal'])){
    $id = $_GET['viewProductJournal'];
    
    $database->orderBy("date_time", "desc");
    $database->where("product_id", $id);
    $journals = $database->get(PRODUCT_JOURNAL);

    $response = array();

    foreach($journals as $journal){
        array_push($response, array(
            "item" => getProduct($journal["product_id"])["description"],
            "transaction" => $journal["transaction"],
            "ref_no" => $journal["ref_no"],
            "qty" => $journal["qty"],
            "updated_by" => $journal["updated_by"],
            "date_time" => $journal["date_time"]
        ));
    }

    echo json_encode($response);
}

if(isset($_GET["getItemsChoices"])){
    $primaryKey = 'id';
    $columns = array(
        array( 'db' => 'product_code', 'dt' => 0 ),
        array(  'db' => 
                'image_file',  'dt' => 1 ,
                'formatter' => function ($data, $row){
                    $image = empty($data) ? "plugins/images/no-item.png" : "api/$data";
                    return '<img src="'.$image.'" alt="NO IMAGE" class="img-thumbnail">';

                }
        ),
        array(  'db' => 'description',   
                'dt' => 2 ,
                'formatter' => function ($data, $row){ 
                    return "<h3>".$data."</h3>"; 
                } 
        ),
        array( 'db' => 'price',   
                'dt' => 3, 
                'formatter' => function ($data, $row){ 
                    return "<h3>Php ".number_format($data)."</h3>"; 
                } 
        ),
        array(  'db' => 'id',   
                'dt' => 4 ,
                'formatter' => function($data ,$row) {
                    return '<button type="button" id="cart_'.$data.'" class="btn btn-danger" onclick="addToCart('.$data.')"><i class="fa fa-cart-plus m-r-5"></i> <span>ADD TO CART</span></button>';
                }
            ),
    );
    
    $condition = "is_deleted = 0 and category = 'FOOD'";

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, PRODUCT_TABLE , $primaryKey, $columns, $condition )
    );
}

if(isset($_GET["getAll"])){
    $database->where("is_deleted" , 0);
    $userDB = $database->get(PRODUCT_TABLE);
    echo json_encode($userDB);
}

if(isset($_GET['getProduct'])){
    $database->where("is_deleted" , 0);
    $database->where("category", $_GET['getProduct']);
    $userDB = $database->get(PRODUCT_TABLE);
    echo json_encode($userDB);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $userDB = $database->getOne(PRODUCT_TABLE);
    echo json_encode($userDB);
}


if(isset($_GET['getProductBySupplierId'])){
    $supplierId = $_GET['getProductBySupplierId'];

    $database->where("supplier_id", $supplierId);
    $database->where("is_deleted", 0);
    $products = $database->get(PRODUCT_TABLE);

    echo json_encode($products);
}

if(isset($_GET['saveActual'])) {
    $data = json_decode($_GET['saveActual']);

    $database->where("id", $data->item_id);
    $id = $database->update(PRODUCT_TABLE, array(
        "stocks" => $data->actual_stocks
    ));

    if($id){
        $ref = uniqid();
        //insert to product journal
        $database->insert(PRODUCT_JOURNAL, array(
            "product_id" => $data->item_id,
            "transaction" => "ACTUAL EDIT",
            "ref_no" => $ref,
            "qty" => $data->actual_stocks,
            "reason" => $data->reason,
            "updated_by" => $loggedUser->username
        ));

        //remove existing product itemize
        $database->where("item_id", $data->item_id);
        $database->where("is_available", 1);
        $database->update(PRODUCT_ITEMIZE, array(
            "is_available" => 0
        ));

        //add new product itemize
        for($i=0; $i<$data->actual_stocks; $i++) {
            $database->insert(PRODUCT_ITEMIZE, array(
                "item_count" => $i + 1,
                "item_id" => $data->item_id,
                "process" => "ACTUAL EDIT",
                "ref_no" => $ref,
                "batch_no" => empty($data->batch_no) ? $ref : $data->batch_no,
                "is_available" => 1,
                "manufacturing" => $data->manufacturing,
                "expiry" => $data->expiration
            ));
        }

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Actual Stocks Edited Successfully"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }
}

?>