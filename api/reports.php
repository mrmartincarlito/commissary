<?php
require_once("config.php");
require_once("logs.php");

if (isset($_GET['jobOrder'])){
    $data = json_decode($_GET['jobOrder']);
    $from = date("F j, Y", strtotime($data->data->dateFrom));
    $to = date("F j, Y", strtotime($data->data->dateTo));
    $response = array();

    //th columns
    $columns = array(
        "JOB ORDER" , "START DATE" , "END DATE" , "STATUS" , "CREATED BY"
    );

    $response["columns"] = $columns;

    if (!empty($data->data->job_order)) {
        $response["caption"] = "<h3>JOB ORDER</h3><h4># {$data->data->job_order}</h4>";
        $database->where("job_order", $data->data->job_order);
    } else {    
        $response["caption"] = "<h3>JOB ORDER</h3><h4>Date From : {$from} - {$to}</h4>";
        $database->where("DATE(start_date)", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
    }
    
    $database->where("is_deleted", 0);
    $production = $database->get(PRODUCTION);

    $response["rows"] = $production;

    $response["others"] = array(
    );

    echo json_encode($response);

}

if (isset($_GET['finishedGoods'])){
    $data = json_decode($_GET['finishedGoods']);
    $from = date("F j, Y", strtotime($data->data->dateFrom));
    $to = date("F j, Y", strtotime($data->data->dateTo));
    $response = array();

    //th columns
    $columns = array(
        "JOB ORDER" , "BATCH NO", "PRODUCT" , "PRODUCED QTY" , "EXPIRATION" , "MANUFACTURING"
    );

    $response["columns"] = $columns;

    if (!empty($data->data->job_order)) {
        $response["caption"] = "<h3>FINISHED GOODS</h3><h4>Job Order # {$data->data->job_order}</h4>";
        $database->where("job_order", $data->data->job_order);
    } else {    
        $response["caption"] = "<h3>Finished Goods</h3><h4>Date From : {$from} - {$to}</h4>";
        $database->where("DATE(start_date)", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
    }
    
    $database->where("is_deleted", 0);
    $production = $database->get(PRODUCTION);

    $rowValues = array();

    foreach($production as $prod) {
        $database->where("production_id", $prod["id"]);
        $prodItems = $database->get(PRODUCTION_ITEMS);

        foreach($prodItems as $prodItem) {
            array_push($rowValues, array(
                    "job_order" => $prod["job_order"],
                    "batch_no" => $prod["batch_no"],
                    "product" => getProduct($prodItem["item_id"])["description"],
                    "produced_qty" => $prodItem["production_qty"],
                    "expiration" => $prod["expiration"],
                    "manufacturing" => $prod["manufacturing"]
                )
            );
        }
    }

    $response["rows"] = $rowValues;

    $response["others"] = array(
    );

    echo json_encode($response);

}

if (isset($_GET['usedGoods'])){
    $data = json_decode($_GET['usedGoods']);
    $from = date("F j, Y", strtotime($data->data->dateFrom));
    $to = date("F j, Y", strtotime($data->data->dateTo));
    $response = array();

    //th columns
    $columns = array(
        "JOB ORDER" , "PRODUCT" , "USED QTY"
    );

    $response["columns"] = $columns;

    if (!empty($data->data->job_order)) {
        $response["caption"] = "<h3>USED GOODS</h3><h4>Job Order # {$data->data->job_order}</h4>";
        $database->where("ref_no", $data->data->job_order);
    } else {    
        $response["caption"] = "<h3>USED GOODS</h3><h4>Date From : {$from} - {$to}</h4>";
        $database->where("DATE(date_time)", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
    }
    
    $database->where("transaction", "PRODUCTION (-)");
    $production = $database->get(PRODUCT_JOURNAL);

    $rowValues = array();

    foreach($production as $prodItem) {
        array_push($rowValues, array(
                "job_order" => $prodItem["ref_no"],
                "product" => getProduct($prodItem["product_id"])["description"],
                "used_qty" => $prodItem["qty"]
            )
        );
    }

    $response["rows"] = $rowValues;

    $response["others"] = array(
    );

    echo json_encode($response);

}

